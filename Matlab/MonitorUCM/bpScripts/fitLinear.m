function [results] = fitLinear(data1, data2, data3)
%
% Calcula el ajuste lineal de 2 vectores o multilineal 3 vectores de datos.
%
% Entrada (2 argumentos)
%   data1           Vector de datos x
%   data2           Vector de datos y
%
% Entrada (3 argumentos)
%   data1           Vector de datos x1
%   data2           Vector de datos x2
%   data3           Vector de datos Y
%
% Salida
%   results         Vector de resultados
%   results(:,1)    Coeficiente de determinación R²
%   results(:,2)    Coeficiente de determinación R² ajustado
%   results(:,3)	Valor-P
%   results(:,4)	Ordenada
%   results(:,5)    Pendiente 1
%   results(:,6)    Pendiente 2 (en ajustes de 3 vectores)
%
% Autor: Jose Manuel Bote
%

results = zeros(1,6);

if nargin == 3
    objectLinearModel = LinearModel.fit([data1, data2], data3);
else
    objectLinearModel = LinearModel.fit(data1, data2);
end

results(1,1) = objectLinearModel.Rsquared.Ordinary;
results(1,2) = objectLinearModel.Rsquared.Adjusted;
results(1,3) = objectLinearModel.coefTest;
results(1,4) = objectLinearModel.Coefficients.Estimate(1);
results(1,5) = objectLinearModel.Coefficients.Estimate(2);
if nargin == 3
    results(1,6) = objectLinearModel.Coefficients.Estimate(3);
end

end
