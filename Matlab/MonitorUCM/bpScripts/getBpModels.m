function [bp] = getBpModels(ecg, pleth, bp)
%
% Calcula el mejor modelo entre los disponibles para estimar la presion a
% partir de la mayor correlación.
%
% Entrada
%   ecg                     Struct de ECG
%   pleth                   Struct de pletismografia
%   bp                      Struct de presiones sanguineas
%
% Salida (xxx: {systolic, diastolic})
%   bp.model.xxx            Modelos para cada presion
%   bp.bestModel.xxx(:,1)   Mejor modelo para cada presion
%
% Autor: Jose Manuel Bote
%

bp.model = getBpModelTimes(ecg, pleth, bp, 10);
bp = getBpModelFit(bp);
logBpModels(bp);

end
