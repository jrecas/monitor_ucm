function [model] = getBpModelTimes(ecg, pleth, bp, numBeats)
%
% Calcula para cada presion sistolica y diastolica una media de N tiempos
% (numbeats) asociado a los latidos siguiented para el ECG y la
% pletismografia.
%
% Entrada
%   ecg                     Struct de ECG
%   pleth                   Struct de pletismografia
%   bp                      Struct de presiones sanguineas
%   numBeats                Numero de latidos promediados
%
% Salida
%   model.times             Tiempo promedio asociado a cada valor de
%                           presion sanguinea
%   model.timeBounds        Intervalo temporal dentro del cual un tiempo es
%                           considerado valido en milisegundos
%   model.indType.PATp      = 1;
%   model.indType.PATf      = 2;
%   model.indType.PATs      = 3;
%   model.indType.WT        = 4;
%   model.indType.t1        = 5;
%   model.indType.t2        = 6;
%   model.indType.PATfWT    = 7;
%   model.indType.invHR     = 8;
%
% Autor: Jose Manuel Bote
%

model = struct;

model.indType.PATp      = 1;
model.indType.PATf      = 2;
model.indType.PATs      = 3;
model.indType.WT        = 4;
model.indType.t1        = 5;
model.indType.t2        = 6;
model.indType.PATfWT    = 7;
model.indType.invHR     = 8;

model.timeBounds(:,1) = [60; 800];
model.timeBounds(:,2) = [40; 500];
model.timeBounds(:,3) = [50; 650];
model.timeBounds(:,4) = [30; 300];
model.timeBounds(:,5) = [30; 300];
model.timeBounds(:,6) = [150; 1600];
model.timeBounds(:,7) = [40; 500];
model.timeBounds(:,8) = [150; 1600];

for i = 1 : length(bp.systolic(:,1))
    index = bp.time(i) < pleth.times.time;
    tmp = pleth.times.value((index),:);
    tmp = tmp(1:numBeats,:);
    for j = 1 : length(tmp(1,:))
        index2 = (tmp(:,j) < model.timeBounds(1,j)) | ...
            (tmp(:,j) > model.timeBounds(2,j));
        tmp(index2) = NaN;
    end
    tmp = nanmean(tmp);
    model.times(i,:) = tmp(1,:);
end

tmpEnd = 1 + length(model.times(1,:));
for i = 1 : length(bp.systolic(:,1))
    index = bp.time(i) < ecg.hr.time;
    tmp = (60 * 1000) ./ ecg.hr.value((index),:);
    tmp = tmp(1:numBeats,:);
    tmp(tmp(:,1) < model.timeBounds(1,tmpEnd) | ...
        tmp(:,1) > model.timeBounds(2,tmpEnd)) = NaN;
    tmp = nanmean(tmp);
    model.times(i,tmpEnd) = tmp(1,:);
end

end
