function [] = logBpModels(bp)
%
% Crea el fichero bpModels.log que contiene la informacion necesaria para
% que la aplicacion de Android pueda estimar las presiones sistolica y
% diastolica.
%
% Entrada
%   bp      Struct de presiones sanguineas
%
% Autor: Jose Manuel Bote
%

fid = fopen('bpModels.log', 'w', 'b');
if fid == -1
    disp('Error al crear bpModels.log');
else
    fwrite(fid, bp.bestModel.systolic.number, 'int32');
    fwrite(fid, bp.bestModel.diastolic.number, 'int32');
    fwrite(fid, bp.bestModel.systolic.coefficients, 'double');
    fwrite(fid, bp.bestModel.diastolic.coefficients, 'double');
end
fclose(fid);

end
