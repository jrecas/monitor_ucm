function [bp] = getBpModelFit(bp)
%
% Calcula el mejor modelo entre los disponibles para estimar la presion a
% partir de la mayor correlación.
%
% Entrada
%   bp                              Struct de presiones sanguineas
%
% Salida (xxx: {systolic, diastolic})
%   bp.model.xxx                    Modelos para cada presion
%   bp.model.xxx(:,1)               Coeficiente R^2
%   bp.model.xxx(:,2)               Coeficiente R^2 ajustado
%   bp.model.xxx(:,3)               Valor p
%   bp.model.xxx(:,4)               Ordenada
%   bp.model.xxx(:,5)               Pendiente 1
%   bp.model.xxx(:,6)               Pendiente 2 (en ajustes multilineales)
%   bp.bestModel.xxx.number         Numero del modelo que mejor se ajusta a
%                                   los datos
%   bp.bestModel.xxx.coefficients   Coeficientes del mejor ajuste
%
% Autor: Jose Manuel Bote
%

% PEP (Pre-Ejection Period): PAT = PEP + PTT
% Es variable, en Foo (2005) estiman 160 ms para un adulto. Se ha visto
% experimentalmente en el hospital La Princesa que se sitúa en torno a 100
% milisegundos
PEP = 100;

% MODELO 1. Variante de Cattivelli, Garudadri (2009)
% Modelo 1
% sbp = a + b * PATp + c * HR
% dbp = a + b * PATp + c * HR
bp.model.systolic(1,:)  = fitLinear(bp.model.times(:,1), 1./bp.model.times(:,8), bp.systolic);
bp.model.diastolic(1,:) = fitLinear(bp.model.times(:,1), 1./bp.model.times(:,8), bp.diastolic);

% MODELOS 2-4. BP vs PAT
% Modelo 2
% sbp = a + b * PATp
% dbp = a + b * PATp
bp.model.systolic(2,:)  = fitLinear(bp.model.times(:,1), bp.systolic);
bp.model.diastolic(2,:) = fitLinear(bp.model.times(:,1), bp.diastolic);

% Modelo 3
% sbp = a + b * PATf
% dbp = a + b * PATf
bp.model.systolic(3,:)  = fitLinear(bp.model.times(:,2), bp.systolic);
bp.model.diastolic(3,:) = fitLinear(bp.model.times(:,2), bp.diastolic);

% Modelo 4
% sbp = a + b * PATs
% dbp = a + b * PATs
bp.model.systolic(4,:)  = fitLinear(bp.model.times(:,3), bp.systolic);
bp.model.diastolic(4,:) = fitLinear(bp.model.times(:,3), bp.diastolic);

% MODELOS 5-7. BP vs 1 / PAT²
% Modelo 5
% sbp = a + b / PATp²
% dbp = a + b / PATp²
bp.model.systolic(5,:)  = fitLinear(1 ./ (bp.model.times(:,1).^2), bp.systolic);
bp.model.diastolic(5,:) = fitLinear(1 ./ (bp.model.times(:,1).^2), bp.diastolic);

% Modelo 6
% sbp = a + b / PATf²
% dbp = a + b / PATf²
bp.model.systolic(6,:)  = fitLinear(1 ./ (bp.model.times(:,2).^2), bp.systolic);
bp.model.diastolic(6,:) = fitLinear(1 ./ (bp.model.times(:,2).^2), bp.diastolic);

% Modelo 7
% sbp = a + b / PATs²
% dbp = a + b / PATs²
bp.model.systolic(7,:)  = fitLinear(1 ./ (bp.model.times(:,3).^2), bp.systolic);
bp.model.diastolic(7,:) = fitLinear(1 ./ (bp.model.times(:,3).^2), bp.diastolic);

% MODELOS 8-10. BP vs 1 / (PAT - PEP)²
% Modelo 8
% sbp = a + b / (PATp - PEP)²
% dbp = a + b / (PATp - PEP)²
bp.model.systolic(8,:)  = fitLinear(1 ./ ((bp.model.times(:,1) - PEP).^2), bp.systolic);
bp.model.diastolic(8,:) = fitLinear(1 ./ ((bp.model.times(:,1) - PEP).^2), bp.diastolic);

% Modelo 9
% sbp = a + b / (PATf - PEP)²
% dbp = a + b / (PATf - PEP)²
bp.model.systolic(9,:)  = fitLinear(1 ./ ((bp.model.times(:,2) - PEP).^2), bp.systolic);
bp.model.diastolic(9,:) = fitLinear(1 ./ ((bp.model.times(:,2) - PEP).^2), bp.diastolic);

% Modelo 10
% sbp = a + b / (PATs - PEP)²
% dbp = a + b / (PATs - PEP)²
bp.model.systolic(10,:)  = fitLinear(1 ./ ((bp.model.times(:,3) - PEP).^2), bp.systolic);
bp.model.diastolic(10,:) = fitLinear(1 ./ ((bp.model.times(:,3) - PEP).^2), bp.diastolic);

% MODELOS 11-13. Awad (2001), Teng (2003) y Yoon (2009)
% Modelo 11
% sbp = a + b * WT
% dbp = a + b * WT
bp.model.systolic(11,:)  = fitLinear(bp.model.times(:,4), bp.systolic);
bp.model.diastolic(11,:) = fitLinear(bp.model.times(:,4), bp.diastolic);

% Modelo 12
% sbp = a + b * t1
% dbp = a + b * t1
bp.model.systolic(12,:)  = fitLinear(bp.model.times(:,5), bp.systolic);
bp.model.diastolic(12,:) = fitLinear(bp.model.times(:,5), bp.diastolic);

% Modelo 13
% sbp = a + b * t2
% dbp = a + b * t2
bp.model.systolic(13,:)  = fitLinear(bp.model.times(:,6), bp.systolic);
bp.model.diastolic(13,:) = fitLinear(bp.model.times(:,6), bp.diastolic);

% MODELOS 14. Propuesta TFM (Jose Manuel Bote).
% Modelo 14
% sbp = a + b * PATfWT
% dbp = a + b * PATfWT
bp.model.systolic(14,:)  = fitLinear(bp.model.times(:,7), bp.systolic);
bp.model.diastolic(14,:) = fitLinear(bp.model.times(:,7), bp.diastolic);

% Mejor modelo: mejor coeficiente R² ajustado
[~, bp.bestModel.systolic.number] = max(abs(bp.model.systolic(:,2)));
if bp.bestModel.systolic.number == 1
    bp.bestModel.systolic.coefficients = [...
        bp.model.systolic(bp.bestModel.systolic.number, 4);
        bp.model.systolic(bp.bestModel.systolic.number, 5);
        bp.model.systolic(bp.bestModel.systolic.number, 6)];
else
    bp.bestModel.systolic.coefficients = [...
        bp.model.systolic(bp.bestModel.systolic.number, 4);
        bp.model.systolic(bp.bestModel.systolic.number, 5);
        0];
end

[~, bp.bestModel.diastolic.number] = max(abs(bp.model.diastolic(:,2)));
if bp.bestModel.diastolic.number == 1
    bp.bestModel.diastolic.coefficients = [...
        bp.model.diastolic(bp.bestModel.diastolic.number, 4);
        bp.model.diastolic(bp.bestModel.diastolic.number, 5);
        bp.model.diastolic(bp.bestModel.diastolic.number, 6)];
else
    bp.bestModel.diastolic.coefficients = [...
        bp.model.diastolic(bp.bestModel.diastolic.number, 4);
        bp.model.diastolic(bp.bestModel.diastolic.number, 5);
        0];
end

end
