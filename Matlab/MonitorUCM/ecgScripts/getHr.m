function [hr] = getHr(timeEcgBumps)
%
% Obtiene el pulso en bpm latido a latido de un experimento a partir de la
% delineacion del ECG.
%
% Entrada
%   timeEcgBumps    Tiempos de los bumps de ECG
%
% Salida
%   hr.value        Valor del pulso latido a latido en bpm
%   hr.time         Tiempo de cada valor del pulso latido a latido en
%                   segundos
%
% Autor: Jose Manuel Bote
%

hr = struct;

hr.value = 60 ./ (timeEcgBumps(2:end,1) - timeEcgBumps(1:end-1,1));
hr.time = timeEcgBumps(1:end-1,1);

end
