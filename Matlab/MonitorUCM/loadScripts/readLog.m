function [data] = readLog(dataDir, fileName, format, endian)
%
% Lee fichero log con formato: "fileName.log" y los devuelve en forma de
% vector.
%
% Entrada
%   dataDir     Directorio de los ficheros
%   fileName    Nombre de los ficheros
%   format      Formato de los datos
%
% Salida
%   data        Vector de bytes con los datos
%
% Autor: Jose Manuel Bote
%

fid = fopen([dataDir, fileName, '.log']);
if (fid == -1)
    disp(['No existe el fichero ', fileName, '.log en el experimento: ', dataDir]);
    data = -1;
else
    data = fread(fid, format, endian);
    fclose(fid);
end

end
