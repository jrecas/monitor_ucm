function [pleth] = getPleth(dataDir)
%
% Extrae los valores de pleth del experimento.
%
% Entrada
%   dataDir         Directorio del experimento
%
% Salida
%   pleth.raw       Valores de pleth en crudo
%   pleth.spo2      Valores de SpO2
%   pleth.time      Vector de tiempos en segundos
%   pleth.frequency Frecuencia de muestreo en Hz
%   pleth.period    Periodo de muestreo en segundos
%
% Autor: Jose Manuel Bote
%

data = readLog(dataDir, 'PLETH', 'uint8');

pleth = struct;

if data == -1
    disp('No hay datos de pletismografía');
    return;
else
    pleth.period = 0.012;
    pleth.frequency = 1 / pleth.period;

    data = reshape(data, 2, length(data) / 2)';

    pleth.raw = data(:,1);
    pleth.spo2 = data(:,2);

    pleth.time = pleth.period * (0 : length(pleth.raw)-1)'; 
end

end
