function [experimentInfo, ecg, pleth, bp] = getExpData(dataDir)
%
% Extrae todos los datos del experimento.
%
% Entrada
%   dataDir                 Directorio del experimento
%
% Salida
%
% Autor: Jose Manuel Bote
%

experimentInfo = getExpInfo(dataDir);

fileName = {'raw', 'ecg', 'delEcg', 'pleth', 'delPleth', 'bp'};
for i = 1 : length(fileName)
    catLog(dataDir, cell2mat(fileName(i)));
end

ecg = getEcg(dataDir);
ecg = getDelEcg(dataDir, ecg);
pleth = getPleth(dataDir);
pleth = getDelPleth(dataDir, pleth);
bp = getBp(dataDir);

if (~isempty(fieldnames(experimentInfo)) && ~isempty(fieldnames(ecg)) &&...
        ~isempty(fieldnames(pleth)) && ~isempty(fieldnames(bp)))
    disp('Datos obtenidos con éxito!!');
end

end
