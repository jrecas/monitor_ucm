function [pleth] = getDelPleth(dataDir, pleth)
%
% Extrae los valores de la delineacion de la pletismografia del
% experimento.
%
% Entrada
%   pleth                       Struct con la pletismografia
%
% Salida
%   pleth.points.position       Posicion de los puntos de referencia
%   pleth.points.raw            Valor de los puntos de referencia
%   pleth.points.time           Tiempo de los puntos de referencia
%   pleth.points.indType.max    Indice del maximo
%   pleth.points.indType.foot   Indice del pie
%   pleth.points.indType.slope  Indice del punto de maxima pendiente
%   pleth.points.indType.wt0    Indice del inicio de 2/3 de la anchura a
%                               media altura
%   pleth.points.indType.wt1    Indice del final de 2/3 de la anchura a
%                               media altura
%
% Autor: Jose Manuel Bote
%

data = readLog(dataDir, 'DELPLETH', 'int32');

if data == -1
    disp('No hay datos de la delineación de la pletismografía');
    return;
else
    pleth.points.position       = struct;
    pleth.points.raw            = struct;
    pleth.points.time           = struct;
    pleth.points.indType.max    = 1;
    pleth.points.indType.foot   = 2;
    pleth.points.indType.slope  = 3;
    pleth.points.indType.wt0    = 4;
    pleth.points.indType.wt1    = 5;
    
    data = reshape(data, 5, length(data) / 5)';
    index = data <= 0;
    data(index) = 1;
    
    pleth.points.position = data;
    
    pleth.points.raw = pleth.raw(data);
    pleth.points.raw(index) = NaN;
    
    pleth.points.time = pleth.period * (data - 1);
    
    % Al guardar la delineacion de la pletismografia, la primera
    % delineacion no es valida
    pleth.points.raw = pleth.points.raw(2:end,:);
    pleth.points.position = pleth.points.position(2:end,:);
    pleth.points.time = pleth.points.time(2:end,:);
end

end
