function [experiment] = getExpInfo(dataDir)
%
% Extrae los datos importantes del experimento.
%
% Entrada
%   dataDir                 Directorio del experimento
%
% Salida
%   experiment.timeStart    Tiempo inicial (absoluto) del experimento
%   experiment.timeEnd      Tiempo final (absoluto) del experimento
%   experiment.length      	Duracion del experimento en segundos
%
% Autor: Jose Manuel Bote
%

fid1 = fopen([dataDir, 'logStart.txt']);
fid2 = fopen([dataDir, 'logEnd.txt']);

experiment = struct;

if fid1 == -1
    disp('No puede abrirse logStart.txt');
    return;
else
    tStart = fgetl(fid1);
    fclose(fid1);
    if length(tStart) < 27
        disp('logStart.txt tiene un formato erróneo');
        return;
    end
end

if fid1 == -2
    disp('No puede abrirse logEnd.txt');
    return;
else
    tEnd = fgetl(fid2);
    fclose(fid2);
    if length(tEnd) < 25
        disp('logEnd.txt tiene un formato erróneo');
        return;
    end
end

if (length(tStart) == 30 && length(tEnd) == 28)
    experiment = struct;
    experiment.timeStart = datenum(tStart(12:end), 'yyyy.mm.dd-HH.MM');
    experiment.timeEnd = datenum(tEnd(12:end), 'yyyy.mm.dd-HH.MM');
    experiment.length = 24 * 60 * 60 * (experiment.timeEnd - experiment.timeStart);
else
    disp(['No hay información sobre el experimento ', dataDir]);
    return
end

end
