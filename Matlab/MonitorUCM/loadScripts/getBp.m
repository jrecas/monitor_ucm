function [bp] = getBp(dataDir)
%
% Extrae los valores de presion sanguinea del experimento.
%
% Entrada
%   dataDir         Directorio del experimento
%
% Salida
%   bp.time         Tiempo en el que se midio las presiones sanguineas
%   bp.systolic     Presion sistolica
%   bp.diastolic    Presion diastolica
%
% Autor: Jose Manuel Bote
%

data = readLog(dataDir, 'BP', 'int32');

bp = struct;

if data == -1
    disp('No hay datos de presiones sanguíneas');
    return;
else
    data = reshape(data, 3, length(data) / 3)';
    
    bp.time = (4 / 1000) * data(:,1);
    bp.systolic = data(:,2);
    bp.diastolic = data(:,3);
end

end
