function [ecg] = getEcg(dataDir)
%
% Extrae los valores de ECG del experimento.
%
% Entrada
%   dataDir         Directorio del experimento
%
% Salida
%   ecg.raw         Valores de ECG en crudo
%   ecg.time        Vector de tiempos en segundos
%   ecg.frequency   Frecuencia de muestreo en Hz
%   ecg.period      Periodo de muestreo en segundos
%
% Autor: Jose Manuel Bote
%

data = readLog(dataDir, 'ECG', 'bit24');

ecg = struct;

if data == -1
    disp('No hay datos de ECG');
    return;
else
    ecg.period = 0.004;
    ecg.frequency = 1 / ecg.period;
    ecg.raw = data;
    ecg.time = ecg.period * (0 : length(ecg.raw)-1)';
end

end
