function [ecg] = getDelEcg(dataDir, ecg)
%
% Extrae los valores de la delineacion del ECG del experimento.
%
% Entrada
%   ecg                     Struct con el ECG
%
% Salida
%   ecg.bumps.raw           Bumps
%   ecg.bumps.position      Posicion de los bumps
%   ecg.bumps.time          Tiempo de los bumps
%   ecg.bumps.indType.R     Indice de la onda R
%   ecg.bumps.indType.Ron   Indice del inicio de la onda R
%   ecg.bumps.indType.Roff  Indice del final de la onda R
%   ecg.bumps.indType.P     Indice de la onda P
%   ecg.bumps.indType.Pon   Indice del inicio de la onda P
%   ecg.bumps.indType.Poff  Indice del final de la onda P
%   ecg.bumps.indType.T     Indice de la onda T
%   ecg.bumps.indType.Ton   Indice del inicio de la onda T
%   ecg.bumps.indType.Toff  Indice del final de la onda T
%
% Autor: Jose Manuel Bote
%

data = readLog(dataDir, 'DELECG', 'int32');

if data == -1
    disp('No hay datos de la delineación del ECG');
    return;
else
    ecg.bumps.raw           = struct;
    ecg.bumps.position      = struct;
    ecg.bumps.time          = struct;
    ecg.bumps.indType.R     = 1;
    ecg.bumps.indType.Ron   = 2;
    ecg.bumps.indType.Roff  = 3;
    ecg.bumps.indType.P     = 4;
    ecg.bumps.indType.Pon   = 5;
    ecg.bumps.indType.Poff  = 6;
    ecg.bumps.indType.T     = 7;
    ecg.bumps.indType.Ton   = 8;
    ecg.bumps.indType.Toff  = 9;
    
    data = reshape(data, 9, length(data) / 9)';
    index = data <= 0;
    data(index) = 1;
    
    ecg.bumps.position = data;
    
    ecg.bumps.raw = ecg.raw(data);
    ecg.bumps.raw(index) = NaN;
    
    ecg.bumps.time = ecg.period * (data - 1);
    

end

end

