function [] = catLog(dataDir, fileName)
%
% Concatena ficheros log con formato: "fileNameXXX.log".
%
% Entrada
%   dataDir     Directorio de los ficheros
%   fileName    Nombre de los ficheros
%
% Autor: Jose Manuel Bote
%

fid = fopen([dataDir, upper(fileName), '.log']);

if (fid == -1)
    if (system(['ls ', dataDir, fileName, '???.log']) ~= 0 || ...
            system(['cat ', dataDir, fileName, '???.log > ', dataDir, upper(fileName), '.log']) ~= 0 )
        disp(['No existen ficheros de ', fileName, ' en el experimento: ', dataDir]);
        return
    else
        disp(['Fichero ', upper(fileName), '.log generado con éxito para el experimento: ', dataDir]);
    end
end
