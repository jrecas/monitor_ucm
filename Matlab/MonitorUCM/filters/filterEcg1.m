function [filtEcg] = filterEcg1(ecg, fSample, plotEnable)
%
% Filtrar el ECG con el filtro de Fran.
%
% Entrada
%   ecg         Struct de ECG
%   plotEnable  Habilitar grafica
%
% Salida
%   filtEcg     ECG filtrado
%
% Autor: Jose Manuel Bote
%

javaPath = './util/ECGDelineatorOf.jar';
javaaddpath(javaPath, '-end');
filterObj = javaObjectEDT('ECGDelineator.ECGFilter');

filtEcg = ecg;

for i = 1 : length(ecg)
    filtEcg(i) = filterObj.filterSample(ecg(i));
end

filtEcg = [filtEcg(1+filterObj.filterDelay : end); zeros(filterObj.filterDelay, 1)];

% Plot
if plotEnable == true
    t = (0:length(ecg)-1) / fSample;
    figure;
    hold on;
    plot(t, ecg, 'b.-')
    plot(t, filtEcg, 'r.-')
    title('Filtering ECG')
    legend('Raw ECG', 'Filtered ECG')
    grid();
    hold off;
end

end
