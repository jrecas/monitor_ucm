function [filtData] = notchFilter(data, fCut, fSample, plotEnable)
%
% Implementacion de un filtro Notch.
%
% Entrada
%   data        Señal de entrada
%   fCut        Frecuencia de corte del filtro
%   fSample     Frecuencia de muestreo de la señal
%   plotEnable  Habilitar grafica
%
% Salida
%   filtData    Señal filtrada
%
% Autor: Jose Manuel Bote
%

Q = 1;
[b, a] = iirnotch(fCut / (fSample / 2), fCut / (Q * (fSample / 2)));

% Bode
if plotEnable == true
    fRange = logspace(-1, 2, 1e4);
    figure;    
    freqz(b, a, fRange, fSample);
    title('Bode - Notch Filter');
    grid();
end

% Filtrado
filtData = filter(b, a, data);

end
