function [filtData] = closing(data, windowDilation, windowErosion)
%
% Implementa el cierre de un filtro baseline.
%
% Entrada
%   data            Señal de entrada
%   windowDilation  Ventana de dilatacion
%   windowErosion   Ventana de erosion
%
% Salida
%   filtData        Señal filtrada
%
% Autor: Jose Manuel Bote
%

temp = dilation(data, windowDilation);
filtData = erosion(temp, windowErosion);

end
