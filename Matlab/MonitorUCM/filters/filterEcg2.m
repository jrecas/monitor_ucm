function [filtEcg] = filterEcg2(ecg, fSample, plotEnable)
%
% Filtrar el ECG.
%
% Entrada
%   ecg         Struct de ECG
%   plotEnable  Habilitar grafica
%
% Salida
%   filtEcg     ECG filtrado
%
% Autor: Jose Manuel Bote
%

filtEcg = ecg;

% Filtro Notch
%fCut = 50;
%filterDelay = 3;       % Retardo variable
%filtEcg = notchFilter(filtEcg, fCut, fSample, false);
%filtEcg = [filtEcg(1 + filterDelay : end); zeros(filterDelay, 1)];

% Filtro FIR pasa baja
order = 40;
filterDelay = order / 2;
if fSample == 1000
    fCut = 40;
else
    fCut = 38.885;
end
filtEcg = FIRFilter(filtEcg, order, fCut, fSample, false);
filtEcg = [filtEcg(1+filterDelay : end); zeros(filterDelay, 1)];

% Filtro baseline
filtEcg = baselineFilter(filtEcg, fSample, false);

% Plot
if plotEnable == true
    t = (0:length(ecg)-1) / fSample;
    figure;
    hold on;    
    plot(t, ecg, 'b.-');
    plot(t, filtEcg, 'r.-');
    title('Filtering ECG');
    legend('Raw ECG', 'Filtered ECG');
    grid();
    hold off;
end
