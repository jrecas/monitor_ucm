function [filtPleth] = filterPleth(pleth, fSample, plotEnable)
%
% Filtrar la pletismografia.
%
% Entrada
%   pleth       Struct de pletismografia
%   plotEnable  Habilitar grafica
%
% Salida
%   filtPleth   Pletismografia filtrada
%
% Autor: Jose Manuel Bote
%

filtPleth = pleth;

order = 16;
fCut = 10;
filterDelay = order / 2;

filtPleth = FIRFilter(filtPleth, order, fCut, fSample, false);
filtPleth = [filtPleth(1+filterDelay : end); zeros(filterDelay, 1)];

% Plot
if plotEnable == true
    t = (0:length(pleth)-1) / fSample;
    figure;
    hold on;    
    plot(t, pleth, 'b.-');
    plot(t, filtPleth, 'r.-');
    title('Filtering Pleth');
    legend('Raw Pleth', 'Filtered Pleth');
    grid();
    hold off;
end

end
