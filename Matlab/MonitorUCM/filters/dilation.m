function [filtData] = dilation(data, window)
%
% Implementa la dilatacion de un filtro baseline.
%
% Entrada
%   data        Señal de entrada
%   window      Ventana
%
% Salida
%   filtData    Señal filtrada
%
% Autor: Jose Manuel Bote
%

if mod(length(window), 2) ~= 1
    window = window(1:end-1);
end

winSize = length(window);

N = length(data);
M = winSize;
halfM = (M - 1) / 2;

filtData = nan(N,1);

if abs(sum(window)) == 0
    for i = halfM+1 : N-halfM;
        filtData(i) = max(data(i-halfM : i+halfM));
    end
else
    for i = halfM+1 : N-halfM;
        filtData(i) = max(data(i-halfM : i+halfM) + window);
    end
end

if length(filtData(:,1)) ~= length(data(:,1))
    filtData = filtData';

end
