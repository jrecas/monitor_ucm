function [ecg, pleth] = filterExperiment(ecg, pleth)
%
% Filtrar los datos de un experimento.
%
% Entrada
%   ecg         Struct de ECG
%   pleth       Struct de pletismografia
%
% Autor: Jose Manuel Bote
%

% Filtrar el ECG con el filtro de Fran
ecg.filtered = filterEcg1(ecg.raw, ecg.frequency, false);

% Filtrar el ECG con el filtro de Jose Manuel
%ecg.filtered = filterEcg2(ecg.raw, ecg.frequency, false);

% Filtrar la pletismopgrafia
pleth.filtered = filterPleth(pleth.raw, pleth.frequency, false);

% Actualizar bumps para el ECG filtrado
ecg.bumps.filtered = ecg.filtered(ecg.bumps.position);

% Actualizar puntos de referencia para la pletismografía filtrada
pleth.points.filtered = pleth.filtered(pleth.points.position);

end
