function [times] = getPlethTimes(timeEcgBumps, timePlethPoints)
%
% Obtiene la diferencia de tiempos entre el inicio de un latido y los
% puntos de referencia localizados en la pletismografia, para cada latido.
%
% Entrada
%   timeEcgBumps            Tiempos de los bumps de ECG
%   timePlethPoints         Tiempos de los puntos de pletismografia
%
% Salida
%   times.value             Valores de los tiempos relacionando ECG y
%                           pletismografia en milisegundos
%   times.time              Tiempo de los valores relacionando ECG y
%                           pletismografia en segundos
%   times.indType.PATp      = 1;
%   times.indType.PATf      = 2;
%   times.indType.PATs      = 3;
%   times.indType.WT        = 4;
%   times.indType.t1        = 5;
%   times.indType.t2        = 6;
%   times.indType.PATfWT    = 7;
%
% Autor: Jose Manuel Bote
%

times = struct;

times.indType.PATp      = 1;
times.indType.PATf      = 2;
times.indType.PATs      = 3;
times.indType.WT        = 4;
times.indType.t1        = 5;
times.indType.t2        = 6;
times.indType.PATfWT    = 7;

tmpLength = min(length(timeEcgBumps(:,1)), length(timePlethPoints(:,1)));
timeEcgBumps = timeEcgBumps(1:tmpLength,:);
timePlethPoints = timePlethPoints(1:tmpLength,:);

times.value = zeros(tmpLength, 7);
times.value(:,1) = timePlethPoints(:,1) - timeEcgBumps(:,1);
times.value(:,2) = timePlethPoints(:,2) - timeEcgBumps(:,1);
times.value(:,3) = timePlethPoints(:,3) - timeEcgBumps(:,1);
times.value(:,4) = timePlethPoints(:,5) - timePlethPoints(:,4);
times.value(:,5) = timePlethPoints(:,1) - timePlethPoints(:,2);
times.value(:,6) = [0; (timePlethPoints(2:end,2) - timePlethPoints(1:end-1,2))];
times.value(:,7) = timePlethPoints(:,5) - timePlethPoints(:,2);
times.value = 1000 * times.value;

times.time = timeEcgBumps(1:tmpLength,1);

end
