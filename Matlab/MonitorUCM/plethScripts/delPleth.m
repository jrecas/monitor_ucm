function [points] = delPleth(timeEcgBumps, timePleth, pleth)

% Obtiene la diferencia de tiempos entre el inicio de un latido y los
% puntos de referencia localizados en la pletismografia, para cada latido.
%
% Entrada
%   timeEcgBumps            Tiempos de los bumps de ECG
%   timePleth               Tiempos de la pletismografia
%   pleth                   Valores de la pletismografia
%
% Salida
%   points.value            Valores de los puntos de la pletismografia
%   points.time             Tiempos de los valores de la pletismografia
%   points.indType.max      Indice del maximo
%   points.indType.foot     Indice del pie
%   points.indType.slope    Indice del punto de maxima pendiente
%   points.indType.wt0      Indice del inicio de 2/3 de la anchura a
%                           media altura
%   points.indType.wt1      Indice del final de 2/3 de la anchura a
%                           media altura
%
% Autor: Jose Manuel Bote
%

points = struct;

points.indType.max      = 1;
points.indType.foot     = 2;
points.indType.slope    = 3;
points.indType.wt0      = 4;
points.indType.wt1      = 5;

% Calcular la primera y segunda derivada de la pletismografia. La primera
% derivada, centrada en el punto, por lo que la expresion es diferente a la
% primera aproximacion. La segunda ya esta centrada en el punto.
plethFirstDerivative = zeros(length(pleth(:,1)), 1);
for i = 2 : length(pleth)-1
    plethFirstDerivative(i) = pleth(i+1) - pleth(i-1);
end
plethSecondDerivative = zeros(length(pleth(:,1)), 1);
for i = 2 : length(pleth)-1
    plethSecondDerivative(i) = pleth(i+1) - 2 * pleth(i) + pleth(i-1);
end

for i = 1 : length(timeEcgBumps) - 1
    
    % Seleccionar el intervalo de pletismografia
    % Calcular el maximo de la pletismografia
    index = (timePleth > timeEcgBumps(i,1)) & (timePleth < timeEcgBumps(i+1,1));
    data = pleth(index);
    timeData = timePleth(index);
    [~, maxPos1] = max(data);
    [maximum, maxPos2] = max(flipud(data));
    maxPos = floor((maxPos1 + (1 + length(data) - maxPos2)) / 2);
    if length(data) > 1
        points.time(i,1) = timeData(maxPos);
        points.value(i,1) = maximum;
    else
        points.time(i,1) = 0;
        points.value(i,1) = 0;
    end
    
    % Calcular el pie de la pletismografia
    index = (timePleth > timeEcgBumps(i,1)) & (timePleth < points.time(i,1));
    data = pleth(index);
    dataSecondDerivative = plethSecondDerivative(index);
    timeData = timePleth(index);
    [~, footPos] = max(flipud(dataSecondDerivative));
    footPos = (1 + length(data) - footPos);
    foot = data(footPos);
    if length(data) > 1
        points.time(i,2) = timeData(footPos);
        points.value(i,2) = data(footPos);
    else
        points.time(i,2) = 0;
        points.value(i,2) = 0;
    end
    
    % Calcular el punto de primera derivada maxima de la pletismografia
    index = (timePleth > timeEcgBumps(i,1)) & (timePleth < points.time(i,1));
    data = pleth(index);
    dataFirstDerivative = plethFirstDerivative(index);
    timeData = timePleth(index);
    [~, slopePos] = max(dataFirstDerivative);
    if length(data) > 1
        points.time(i,3) = timeData(slopePos);
        points.value(i,3) = data(slopePos);
    else
        points.time(i,3) = 0;
        points.value(i,3) = 0;
    end
    
    % Calcular el punto inicial de la anchura 2/3 a media altura de la
    % pletismografia
    limit = maximum - (1 / 3) * (maximum - foot);
    index = (timePleth > timeEcgBumps(i,1)) & (timePleth < points.time(i,1));
    data = pleth(index);
    timeData = timePleth(index);
    if ((length(data) > 1) && (~isempty(limit)))
        index = (data > limit);
        data = data(index);
        timeData = timeData(index);
        if length(data) > 1
            points.time(i,4) = timeData(1);
            points.value(i,4) = data(1);
        else
            points.time(i,4) = 0;
            points.value(i,4) = 0;
        end
    else
        points.time(i,4) = 0;
        points.value(i,4) = 0;
    end
    
    % Calcular el punto final de la anchura 2/3 a media altura de la
    % pletismografia
    index = (timePleth > points.time(i,1)) & (timePleth < timeEcgBumps(i+1,1));
    data = pleth(index);
    timeData = timePleth(index);
    if ((length(data) > 1) && (~isempty(limit)))
        index = (data < limit);
        data = data(index);
        timeData = timeData(index);
        if length(data) > 1
            points.time(i,5) = timeData(1);
            points.value(i,5) = data(1);
        else
            points.time(i,5) = 0;
            points.value(i,5) = 0;
        end
    else
        points.time(i,5) = 0;
        points.value(i,5) = 0;
    end
    
end
