function [] = plotComparison(structData)
%
% Representa la comparacion de la señal en crudo y la señal filtrada.
%
% Entrada
%   structData  Struct de datos
%
% Autor: Jose Manuel Bote
%

figure;
hold on;
plot(structData.time, structData.raw, 'b.-', 'LineWidth', 2);
plot(structData.time, structData.filtered, 'r.-', 'LineWidth', 2);
title('Comparison');
legend('Raw', 'Filtered');
xlabel('Tiempo (s)');
grid();
hold off;

figure;
hold on;
plot(structData.time, structData.raw - mean(structData.raw), 'b.-', 'LineWidth', 2);
plot(structData.time, structData.filtered - mean(structData.filtered), 'r.-', 'LineWidth', 2);
title('Comparison 2');
legend('Raw', 'Filtered');
xlabel('Tiempo (s)');
grid();
hold off;

end
