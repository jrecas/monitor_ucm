function [] = plotRawExperiment(ecg, pleth, bp)
%
% Representa los datos en crudo del experimento.
%
% Entrada
%   ecg     Struct del ECG
%   pleth   Struct de la pletismografia
%   bp      Struct de la presion sanguinea
%
% Autor: Jose Manuel Bote
%

factorEcg = 0.005;

figure;
hold on;
if ~isempty(fieldnames(ecg))
    plot(ecg.time, factorEcg * ecg.raw, 'b.-', 'LineWidth', 2);
    if isfield(ecg,'bumps')
        plot(ecg.bumps.time, factorEcg * ecg.bumps.raw, 'r*', 'LineWidth', 2);
    end
end
if ~isempty(fieldnames(pleth))
    plot(pleth.time, pleth.raw, 'r.-', 'LineWidth', 2);
    if isfield(pleth,'points')
        plot(pleth.points.time, pleth.points.raw, 'b*', 'LineWidth', 2);
    end
end
if ~isempty(fieldnames(bp))
    plot(bp.time, bp.systolic, 'sm--', 'LineWidth', 2);
    plot(bp.time, bp.diastolic, 'sg--', 'LineWidth', 2);
end
title('Plot Raw Experiment');
xlabel('Tiempo (s)');
grid();
hold off;

end
