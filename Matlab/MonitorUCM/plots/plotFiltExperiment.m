function [] = plotFiltExperiment(ecg, pleth, bp)
%
% Representa los datos filtrados del experimento.
%
% Entrada
%   ecg     Struct del ECG
%   pleth   Struct de la pletismografia
%   bp      Struct de la presion sanguinea
%
% Autor: Jose Manuel Bote
%

factorEcg = 0.005;

figure;
hold on;
if (~isempty(fieldnames(ecg)) && isfield(ecg,'filtered'))
    plot(ecg.time, factorEcg * ecg.filtered, 'b.-', 'LineWidth', 2);
    if (isfield(ecg,'bumps') && isfield(ecg.bumps,'filtered'))
        plot(ecg.bumps.time, factorEcg * ecg.bumps.filtered, 'r*', 'LineWidth', 2);
    end
end
if (~isempty(fieldnames(pleth)) && isfield(pleth,'filtered'))
    plot(pleth.time, pleth.filtered, 'r.-', 'LineWidth', 2);
    if (isfield(pleth,'points') && isfield(pleth.points,'filtered'))
        plot(pleth.points.time, pleth.points.filtered, 'b*', 'LineWidth', 2);
    end
end
if ~isempty(fieldnames(bp))
    plot(bp.time, bp.systolic, 'sm--', 'LineWidth', 2);
    plot(bp.time, bp.diastolic, 'sg--', 'LineWidth', 2);
end
title('Plot Filtered Experiment');
xlabel('Tiempo (s)');
grid();
hold off;

end
