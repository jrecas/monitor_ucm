function [misc, time]=getShMiscExp(dataDir,fName,nRows)

    T=0.004;
    
    fid=loadLog(dataDir, 'MISC');
    data = fread(fid, 'int16','b');
    fclose(fid);
    if(isempty(data))
      misc=[];time=[];
      disp(['No ha información de MISC para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end
    
    misc = reshape(data,nRows,length(data)/nRows)';
    time=(0:length(misc)-1)'*T;
end

