function [sName]=getShNameExp(dataDir)
%
%  Obtiene la referencia del Shimmer2 que realizó el experimento
%
%  Autor Joaquín Recas (V1.1)
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una
%    sesión
%  Salidas:
%    -sName: identificador del Shimmer2 RN42-XXX
%

    fid = fopen([dataDir,'logStart.txt']);
    if(fid==-1)
        disp(['No se puede abrir el fichero ', dataDir,'logStart.txt para obtebner el nombre del Shimmer']);
     end
    fgetl(fid);
    tline = fgetl(fid);
    fclose(fid);
    if(size(tline)<16)
        sName=[];
        disp(['El fichero ', dataDir,'logStart.txt no tiene el formato adecuado']);
      return;
    end  
    sName=tline(15:end);
end