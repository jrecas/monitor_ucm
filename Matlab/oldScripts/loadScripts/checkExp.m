function [valido]=checkExp(dataDir, timeLen)
%
%  Indica si un experimento parece válido
%
%  Autor Joaquín Recas (V1.0)
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una
%    sesión
%    -timeLen longitud mínima aceptada [sec]
%  Salidas:
%    -valido: se considera válido el experimento?
%

    valido=false;
    tInic=0;
    tFin=0;
    
    fid1 = fopen([dataDir,'logStart.txt']);
    fid2 = fopen([dataDir,'logEnd.txt']);
    if(fid1==-1 || fid2==-1)
        disp(['No se pueden abrir los ficheros ', dataDir,'logStart.txt o logEnd.txt']);
        return;
    end
     
    %Log start: 2013.02.12-00.12.22
    inicLine = fgetl(fid1);
    %Log end: 2013.02.12-00.12.24
    endLine = fgetl(fid2);
    fclose(fid1);fclose(fid2);
    if(length(inicLine)<27 || length(endLine)<25)
        disp(['Los ficheros ', dataDir,'logStart.txt o logEnd no tienen el formato adecuado']);
        return;
    end
    
    if(length(inicLine)==27 && length(endLine)==25)
      tInic=datenum(inicLine(12:end), 'yyyy.mm.dd-HH.MM');
      tFin=datenum(endLine(10:end), 'yyyy.mm.dd-HH.MM');
    end
    
    if(length(inicLine)==30 && length(endLine)==28)
      tInic=datenum(inicLine(12:end), 'yyyy.mm.dd-HH.MM.SS');
      tFin=datenum(endLine(10:end), 'yyyy.mm.dd-HH.MM.SS');
    end
    
    if((tFin-tInic)*24*60*60 >= timeLen)
      valido=true;
    end
end