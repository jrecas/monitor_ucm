function [sECG, sACC, sOxim, shimmer2, android]=getExpsData(rootDir, dateReg, plotEnable)
%
%  Obtiene los datos de los experimentos referenciados
%
%  Autor Joaquín Recas (V2.0)
%
%  Entradas:
%    -rootDir: directorio donde están todos los experimentos
%    -dateReg: fecha de los que se quieren obtener (permite expresiones regulares)
%  Salidas:
%    sECG = 
%      ecg:       electrocardiografía [mV]
%      timeAbs:   vector de tiempos de Acc. en días comenzando en el primer experimento cargado
%      timeRel:   vector de tiempos en segundos comenzando en 0
%      bumps:     amplitud del pulso detectado puntos R, P y T junto al Onset y Offset para cada uno (indType) [mV]
%      tBumpsAbs: tiempo para el pulso detectado absoluto [dias]
%      tBumpsRel: pulso detectado en segundos comenzando en 0
%      offsetAbs: offset de sicronización en días cada 1.0240s (cada 256 muestras) y número de muestras perdidas
%      offsetRel: offset de sicronización en segundos comenzando en 0
%      indType:   índices del tipo de punto delineado para una fila de bumps
%
%    sACC.sh/And:  Shimmer2/Android
%      acc:     aceleración para (X-Y-Z) [m/s^2]
%      timeAbs: vector de tiempos de Acc. en días comenzando en el primer experimento cargado
%      timeRel: vector de tiempos en segundos comenzando en 0
%      mod:     módulo de la aceleración
%    sOxim = 
%      pleth:   pletismografía
%      spo2:    saturación de oxígeno en sangre [%]
%      timeAbs: vector de tiempos en días comenzando en el primer experimento cargado
%      timeRel: vector de tiempos en segundos comenzando en 0
%
%    -shimmer2: identificador del Shimmer2 RN42-XXX
%       shimmer2.acc.data   =  aceleración en el Shimmer2 (X-Y-Z) [m/s^2]
%       shimmer2.acc.time   =  vector de tiempos de Acc. en días comenzando en el primer experimento cargado
%       shimmer2.acc.mod    =  módulo de la aceleración
%     
%       shimmer2.ecg.data    = electrocardiografía [mV]
%       shimmer2.ecg.time    = vector de tiempos del ECG en días comenzando en el primer experimento cargado
%       shimmer2.ecg.bumps   = pulso detectado puntos R, P y T junto al Onset y Offset para cada uno (indType)
%       shimmer2.ecg.indType = índices del tipo de punto delineado para una fila de bumps
%       shimmer2.ecg.offsets = offset de sicronización en días cada 1.0240s (cada 256 muestras) y número de muestras perdidas
%     
%       shimmer2.spo2.data   = oximetria [%]
%       shimmer2.spo2.time   = vector de tiempos de SpO2 en días comenzando en el primer experimento cargado
%     
%    -android:
%       android.acc.data     = aceleración en el dispositivo Android (X-Y-Z) [m/s^2]
%       android.acc.time     = vector de tiempos de Acc. en días comenzando en el primer experimento cargado
%       android.acc.mod      = módulo de la aceleración
%       android.bp.data      = presión sistólica, diastólica y bpm
%       android.bp.time      = vector de tiempos absolutos
%
%  Ejemplos:
%
%    todos los experimento del 2013.02.08 mostrando las gráficas
%    >> getExpsData('./Upload/', '2013.02.08', true) 
%
%    todos los experimento en el periodo 2013.02.07 - 2013.02.08
%    >> getExpsData('./Upload/', '2013.02.0[78]', false) 
%

    sECG=struct(); sACC=struct(); sOxim=struct();
    shimmer2=struct(); android=struct();

    %rootDir='./Upload/';
    %dateReg='2013.02.0[78]';
    %plotData=true;

    %minExpLength=60;  % Duración mínima del experimento para qeu se considere aceptable
    minExpLength=6;  % Duración mínima del experimento para qeu se considere aceptable

    dList=dir(rootDir);
    i=regexp({dList.name},dateReg);
    %{dList(~cellfun('isempty',i)).name}';

    accSh=[];accShMod=[];accShTime=[];
    ecgSh=[];ecgShTime=[]; ecgShBumps=[]; tEcgShBumps=[]; ecgShOffset=[];
    accAnd=[];accAndMod=[];accAndTime=[];
    spo2Sh=[];spo2ShTime=[];
    bpAnd=[];bpAndTime=[];
    miscSh=[];miscShTime=[];
    indType=[];
    sName=[];
    for expName={dList(~cellfun('isempty',i)).name}
        timeExp=datenum(expName{:}, 'yyyy.mm.dd-HH.MM.SS');
        if (isunix)
            dataDir=[rootDir, expName{:}, '/'];
        elseif (ispc)
            dataDir=[rootDir, expName{:}, '\'];
        end
        
        if(checkExp(dataDir, minExpLength)==false)
          disp(['Datos del experimento: ', expName{:}, ' descartados!!! Registro demadiado corto < ', num2str(minExpLength), 'sec'])
%           continue
        end
        disp(' ');
        disp(['Se importaran los datos del experimento: ', expName{:}])
        
        %%  Shimmer Ecg
        [ecgTmp, timeEcgTmp, bumpsTmp, tBumpsTmp,indType, offsetTmp]=getShEcgExp(dataDir);
        if(isempty(ecgTmp))
          disp(['Datos del experimento: ', expName{:}, ' descartados!!! Falta información de ECG'])
          continue
        end
        timeEcgTmp=timeEcgTmp/(24*60*60)+timeExp;
        tBumpsTmp=tBumpsTmp/(24*60*60)+timeExp;
%        offsetTmp(:,1)=offsetTmp(:,1)/(24*60*60)+timeExp;
        ecgSh(end+1:end+length(ecgTmp),1)=ecgTmp;
        ecgShBumps(end+1:end+length(bumpsTmp),1:9)=bumpsTmp;

        
        ecgShTime(end+1:end+length(timeEcgTmp),1)=timeEcgTmp;
        tEcgShBumps(end+1:end+length(tBumpsTmp),1:9)=tBumpsTmp;
        ecgShOffset(end+1:end+length(offsetTmp),1:2)=offsetTmp;        
        
        
        %%  Shimmer Acc
        sName=getShNameExp(dataDir);
        [accShTmp, modShTmp, timeShAccTmp]=getShAccExp(dataDir, sName);
        timeShAccTmp=timeShAccTmp/(24*60*60)+timeExp;
        accSh(end+1:end+length(accShTmp),1:3)=accShTmp;
        accShMod(end+1:end+length(modShTmp),1)=modShTmp;
        accShTime(end+1:end+length(timeShAccTmp),1)=timeShAccTmp;        
        
        %%  Shimmer SpO2
        [spo2Tmp, timeSpo2Tmp]=getShSpO2Exp(dataDir);
        timeSpo2Tmp=timeSpo2Tmp/(24*60*60)+timeExp;
        spo2Sh(end+1:end+length(spo2Tmp),1:3)=spo2Tmp;
        spo2ShTime(end+1:end+length(timeSpo2Tmp),1)=timeSpo2Tmp;        
        
        %%  Android Acc
        [accAndTmp, modAndTmp, timeAndAccTmp]=getAndAccExp(dataDir);
        timeAndAccTmp=timeAndAccTmp/(24*60*60)+timeExp;
        accAnd(end+1:end+length(accAndTmp),1:3)=accAndTmp;
        accAndMod(end+1:end+length(modAndTmp),1)=modAndTmp;
        accAndTime(end+1:end+length(timeAndAccTmp),1)=timeAndAccTmp;
        
        %%  Android Log para BP
         [bpAndTmp, timeBpAndTmp]=getAndLogBpExp(dataDir);
         bpAnd(end+1:end+length(bpAndTmp),1:3)=bpAndTmp;
         bpAndTime(end+1:end+length(timeBpAndTmp),1)=timeBpAndTmp;
        
        %%  Shimmer Misc
%         nRowns=3;fName='misc1???.log';
%         nRowns=2;fName='misc???.log';
%         nRowns=3;fName='misc???.log';
%         [miscTmp, timeMisc]=getShMiscExp(dataDir,fName,nRowns);
%         timeMisc=timeMisc/(24*60*60)+timeExp;
%         miscSh(end+1:end+length(miscTmp),1:nRowns)=miscTmp;
%         miscShTime(end+1:end+length(timeMisc),1)=timeMisc;
        
    end

    if(~isempty(sName))
      shimmer2.name=sName;
    end

    %% ECG
    shimmer2.ecg.data=ecgSh;
    shimmer2.ecg.time=ecgShTime;
    shimmer2.ecg.bumps=ecgShBumps;
    shimmer2.ecg.tBumps=tEcgShBumps;
    shimmer2.ecg.offsets=ecgShOffset;
    shimmer2.ecg.indType=indType;

    sECG.rawEcg    =shimmer2.ecg.data;          %electrocardiografía Raw
    sECG.rawBumps  =shimmer2.ecg.bumps;         %pulso detectado puntos R, P y T junto al Onset y Offset para cada uno (indType)
    sECG.ecg       =sECG.rawEcg/2069-1;         %electrocardiografía [mV]
    sECG.bumps     =shimmer2.ecg.bumps/2069-1;  %pulso detectado puntos R, P y T junto al Onset y Offset para cada uno (indType)
    sECG.timeAbs   =shimmer2.ecg.time;          %vector de tiempos del ECG en días comenzando en el primer experimento cargado     
    sECG.tBumpsAbs =shimmer2.ecg.tBumps;        %tiempo absoluto para el pulso detectado
    sECG.indType   =shimmer2.ecg.indType;       %índices del tipo de punto delineado para una fila de bumps
    sECG.offsetAbs =shimmer2.ecg.offsets;       %offset de sicronización en días cada 1.0240s (cada 256 muestras) y número de muestras perdidas
    
    inicEcgTime         =sECG.timeAbs(1);
    sECG.timeRel        =(sECG.timeAbs  -inicEcgTime)*(24*60*60);
    sECG.tBumpsRel      =(sECG.tBumpsAbs -inicEcgTime)*(24*60*60);
    sECG.offsetRel      =(sECG.offsetAbs(:,1)-inicEcgTime)*(24*60*60);
    sECG.offsetRel(:,2) =sECG.offsetAbs(:,2);
    if(nargin==3 && plotEnable)
        mSize=8;
        fECG=1;
        figure('Name','BPM-ECG','NumberTitle','off')
        hold on; 
        plot(sECG.timeRel, sECG.ecg*fECG, 'b');

        plot(sECG.offsetRel(:,1), zeros(1,length(sECG.offsetRel)), 'ms', 'markersize', mSize);

        plot(sECG.tBumpsRel(:,sECG.indType.P), sECG.bumps(:,sECG.indType.P)*fECG,'k*', 'markersize', mSize);
        plot(sECG.tBumpsRel(:,sECG.indType.R), sECG.bumps(:,sECG.indType.R)*fECG,'r*', 'markersize', mSize);
        plot(sECG.tBumpsRel(:,sECG.indType.T), sECG.bumps(:,sECG.indType.T)*fECG,'g*', 'markersize', mSize);

        plot(sECG.tBumpsRel(:,sECG.indType.Pon), sECG.bumps(:,sECG.indType.Pon)*fECG,'k.', 'markersize', mSize);
        plot(sECG.tBumpsRel(:,sECG.indType.Pof), sECG.bumps(:,sECG.indType.Pof)*fECG,'k.', 'markersize', mSize);
        plot(sECG.tBumpsRel(:,sECG.indType.Ron), sECG.bumps(:,sECG.indType.Ron)*fECG,'r.', 'markersize', mSize);
        plot(sECG.tBumpsRel(:,sECG.indType.Rof), sECG.bumps(:,sECG.indType.Rof)*fECG,'r.', 'markersize', mSize);
        plot(sECG.tBumpsRel(:,sECG.indType.Ton), sECG.bumps(:,sECG.indType.Ton)*fECG,'g.', 'markersize', mSize);
        plot(sECG.tBumpsRel(:,sECG.indType.Tof), sECG.bumps(:,sECG.indType.Tof)*fECG,'g.', 'markersize', mSize);
        
        title('ECG y delineacion');
        xlabel('Time [sec]');
        legend('ECG', 'Synch', 'P', 'R', 'T');
        hold off; 
        grid;

    end
    
    %% ACC Shimmer
    shimmer2.acc.data=accSh;
    shimmer2.acc.time=accShTime;
    shimmer2.acc.mod=accShMod;

    sACC.sh.acc        =shimmer2.acc.data;         %aceleración en el Shimmer2 (X-Y-Z) [m/s^2]
    sACC.sh.timeAbs    =shimmer2.acc.time;         %vector de tiempos de Acc. en días comenzando en el primer experimento cargado
    sACC.sh.mod        =shimmer2.acc.mod;          %módulo de la aceleración
    inicAccTime        =sACC.sh.timeAbs(1);
    sACC.sh.timeRel    =(sACC.sh.timeAbs  -inicAccTime)*(24*60*60);

    if(nargin==3 && plotEnable)
      figure('Name','ShAcc','NumberTitle','off')
      hold on;
      plot(sACC.sh.timeRel, sACC.sh.acc(:,1), 'b');
      plot(sACC.sh.timeRel, sACC.sh.acc(:,2), 'r');
      plot(sACC.sh.timeRel, sACC.sh.acc(:,3), 'g');
      plot(sACC.sh.timeRel, sACC.sh.mod, 'k');
      hold off; grid;
      xlabel('Time [sec]'); ylabel('Acc [m/s^2]');
      title('Shimmer2 3 Axis Acc');
      legend('X', 'Y', 'Z');
    end
    
    %% ACC Android
    if(~isempty(accAnd))
      android.acc.data=accAnd;
      android.acc.time=accAndTime;
      android.acc.mod=accAndMod;

      sACC.and.acc       =android.acc.data;         %aceleración en el Android (X-Y-Z) [m/s^2]
      sACC.and.timeAbs   =android.acc.time;         %vector de tiempos de Acc. en días comenzando en el primer experimento cargado
      sACC.and.mod       =android.acc.mod;          %módulo de la aceleración

      inicAccTime        =sACC.and.timeAbs(1);
      sACC.and.timeRel   =(sACC.and.timeAbs  -inicAccTime)*(24*60*60);
      if(nargin==3 && plotEnable)
        figure('Name','AndAcc','NumberTitle','off')
        hold on;
        plot(sACC.and.timeRel, sACC.and.acc(:,1), 'b');
        plot(sACC.and.timeRel, sACC.and.acc(:,2), 'r');
        plot(sACC.and.timeRel, sACC.and.acc(:,3), 'g');
        plot(sACC.and.timeRel, sACC.and.mod, 'k');
        hold off; grid;
        xlabel('Time [sec]'); ylabel('Acc [m/s^2]');
        title('Android 3 Axis Acc');
        legend('X', 'Y', 'Z');
      end
    end
    
    %% Datos de SpO2 de Nonin
    if(~isempty(spo2Sh))
      shimmer2.spo2.data=spo2Sh;
      shimmer2.spo2.time=spo2ShTime;

      sOxim.pleth    =shimmer2.spo2.data(:,2);
      sOxim.spo2     =shimmer2.spo2.data(:,3);
      sOxim.timeAbs  =shimmer2.spo2.time;
      sOxim.timeRel  =(sOxim.timeAbs-inicEcgTime)*(24*60*60);

      if(nargin==3 && plotEnable)
        figure('Name','Oxim','NumberTitle','off')
        hold on;
        plot(sOxim.timeRel, sOxim.spo2, 'b.-');
        plot(sOxim.timeRel, sOxim.pleth, 'r.-');
        hold off; grid;
        xlabel('Time [sec]');
        title('Oximetría');
        legend('SpO2', 'Pleth.');
      end
    end


    %%
    if(~isempty(miscSh))
     shimmer2.misc.data=miscSh;
     shimmer2.misc.timeAbs=miscShTime;
     shimmer2.misc.timeRel=(shimmer2.misc.timeAbs  -inicEcgTime)*(24*60*60);
    end
    if(~isempty(bpAnd))
      android.bp.data=bpAnd;
      android.bp.time=bpAndTime;
    end
    
end

