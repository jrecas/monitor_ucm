function [sBP]=getBPfromFile(dirName, name, tInic, tFin)
%
%Cogemos los datos de BP del usuario 'name' con el formato OMRON y los guardamos en sBP:
%  -sBP.legend
%  -sBP.timeAbs
%  -sBP.timeRel
%  -sBP.sys
%  -sBP.dia
%  -sBP.bpm
%  -sBP.miscData
  
  fd=fopen([dirName, name, 'BP.csv']);
  fgetl(fd);         %Name
  fgetl(fd);         %blank
  %header=fgetl(fd);
  
  %Date;Time;SYS (mmHg);DIA (mmHg);Pulse (Pulses/min);Irregular Heartbeat (y or n);Excessive Movement (y or n);Memo (y or n);MsCnt1;MsCnt2;MsCnt3;InputType;MeasureType;Comment
  legend=regexp(fgetl(fd), ';', 'split');
  sBP.legend=legend(6:end);
  
  %29/07/2013;19:18:55;107;69;60;n;n;n;0;0;0;0;2;
  line=fgetl(fd);
  
  sBP.timeAbs=[];
  sBP.sys=[];
  sBP.dia=[];
  sBP.bpm=[];
  data={};
  while(ischar(line))
    tData=datenum(line(1:19), 'dd/mm/yyyy;HH:MM:SS');
    if(tData>tInic && tData<tFin)
      sBP.timeAbs(end+1,1)=tData;

      tmp=regexp(line(21:end), ';', 'split');
      sBP.sys(end+1,1)=str2num(tmp{1});
      sBP.dia(end+1,1)=str2num(tmp{2});
      sBP.bpm(end+1,1)=str2num(tmp{3});

      data=[data; tmp(4:end)];
    end
    
    line=fgetl(fd);
  end
  fclose(fd);
  
  if(~isempty(data))
    sBP.miscData=data;
    sBP.timeRel=(sBP.timeAbs-tInic)*(24*60*60);
  else
    sBP=struct();
  end
end