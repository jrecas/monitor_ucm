function fid=loadLog(dataDir, fileName)

fid = fopen([dataDir, fileName, '.log']);

if(fid==-1)
    if (isunix() == true)
        if(system(['ls ',dataDir, lower(fileName), '???.log'])~=0 || ...
                system(['cat ',dataDir, lower(fileName), '???.log > ',dataDir, fileName, '.log'])~=0 )
            disp(['No hay informacion de ', fileName, ' para el Shimmer2 en el experimento: ', dataDir]);
            fid=-1;
            return
        end
    elseif (ispc() == true)
        if(system(['dir ',dataDir, lower(fileName), '???.log'])~=0 || ...
                system(['type ',dataDir, lower(fileName), '???.log > ',dataDir, fileName, '.log'])~=0 )
            disp(['No hay informacion de ', fileName, ' para el Shimmer2 en el experimento: ', dataDir]);
            fid=-1;
            return
        end
    end
    
    fid = fopen([dataDir, fileName, '.log']);
    if(fid==-1)
        disp(['ERROR: No se puede abrir el fichero ', dataDir, fileName, 'log']);
        fid=-1;
        return;
    end    
end
