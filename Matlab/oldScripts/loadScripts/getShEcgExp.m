function [ecg, time, bumps, tBumps, indType, offset]=getShEcgExp(dataDir)
%
%  El ECG se mide cada 4ms y para pasarlo a mv hay que dividir por 2069
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una sesión
%  Salidas:
%    -ecg:     electrocardiografía RAW
%    -time:    vector de tiempos del ECG en segundos comenzando en 0
%    -bumps:   pulso detectado puntos R,P y T junto al Onset y Offset para cada uno [mV]
%    -tBumps:  tiempo para el pulso detectado [s]
%    -indType: índice del tipo de punto delineado para una fila de bumps
%    -offset:  offset de sicronización cada 1.0240s (cada 256 muestras) y número de muestras perdidas
%

    %T=1;
    
    indType.R   =1; indType.Ron =2; indType.Rof =3;
    indType.P   =4; indType.Pon =5; indType.Pof =6;
    indType.T   =7; indType.Ton =8; indType.Tof =9;    
    
    ecg=[];time=[];bumps=[];tBumps=[];offset=[];
    
    fid=loadLog(dataDir, 'ECG');
    if(fid~=-1)
      T=0.004;
    else
      fid=loadLog(dataDir, 'ECG1K');
      T=0.001;
    end
    if(fid==-1)
      error('NO HAY DATOS DE ECG')
    end
    rawEcg = fread(fid, 'int16','b');
    fclose(fid);
    if(isempty(rawEcg))
      disp(['No ha información de ECG para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end

    
    %ecg = rawEcg/2069-1;
    ecg = rawEcg;
    %ecg = rawEcg(8382200:8400000+tBumps100)/2069;

    fid=loadLog(dataDir, 'OFF');
    data = fread(fid, 'int32','b');
    fclose(fid);
    if(isempty(data))
      ecg=[];
      disp(['No ha información de sincronización para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end    
     
    offset = reshape(data,2,length(data)/2)';
    if(sum(offset(:,2)))
      disp(['Se han perdido muestras de ECG en el registro: ', num2str(sum(offset(:,2)))]);
      
      %Lugares en los que faltan muestras
      inds=find(offset(:,2)~=0);   
      %dec2hex(offset(inds,1)*2)
      %offset(inds,:)
      %figure; plot(0:length(rawEcg)-1, rawEcg, 'b.-', offset(:,1),zeros(length(offset(:,1)),1), 'r.')
      for i=inds'
        %offset(i,1)
        ecg(offset(i-1,1)+1:offset(i,1))=nan;

        %figure; plot( ecg(offset(i-1,1)+1 :offset(i,1))); %Este es el trozo malo
        
        %nOkSamples=offset(i+1,1)-offset(i,1);    %numero de muestras que debería de haber en la ventana
        %nFaltan=offset(i+1,2);
        %inicEcg=offset(i,1)+1;
        %ecg(offset(i,1)+1:offset(i+1,1));        %Este es el trozo malo, que debería de tener 256 muestras
        %figure; plot(ecg(offset(i,1)+1:offset(i+1,1))); %Este es el trozo malo
        %ecg(offset(i,1):offset(i+1,1))=nan;
      end
      
      %figure; plot(0:length(ecg)-1, ecg, 'b.-', offset(:,1),zeros(length(offset(:,1)),1), 'r.')
       nSamp=offset(2:end,1)-offset(1:end-1,1);
       indF=find(nSamp~=256)';
       if(~isempty(indF))
         disp('Faltan datos de sincronización!!!');
       end
%       for i=indF
%         nOkSamples=offset(i+1,1)-offset(i,1);    %numero de muestras que debería de haber en la ventana
%         nFaltan=offset(i+1,2);
%         ecg=[ecg(1:offset(i,1)); nan(nOkSamples,1); ecg(offset(i+1,1)+1:end)];
%         %inicEcg=offset(i,1)+1;
%         %ecg(offset(i,1)+1:offset(i+1,1));        %Este es el trozo malo, que debería de tener 256 muestras
%         %figure; plot(ecg(offset(i,1)+1:offset(i+1,1))); %Este es el trozo malo
%         %ecg(offset(i,1):offset(i+1,1))=nan;
%       end
    end
    
    time=(0:length(ecg)-1)'*T;

    %Pasamos el número de muestra a tiempo
    offset(:,1)=offset(:,1)*T;
    
    %Delineation
    fid=loadLog(dataDir, 'DEL');
    if(fid==-1)
      disp(['No ha información de delineación para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end
    data = fread(fid, 'int32','b');
    fclose(fid);
    
    %figure; plot(rawEcg);
    %dec2hex(cursor_info.DataIndex*2);
    
    data=data+1;
    
    data=data(data<length(ecg));
    data=reshape(data(1:floor(length(data)/9)*9),9,floor(length(data)/9))';%-1;
    ind0=data<=0;

    data(ind0)=1;
    bumps=ecg(data);
    bumps(ind0)=NaN;
    tBumps=(data-1)*T;
    tBumps(ind0)=NaN;
   
end

