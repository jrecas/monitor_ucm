function [accCal, mod, time]=getShAccExp(dataDir, sName)
%
%  La aceleración del Shimmer2 se mide cada 100ms
%
%  Autor Joaquín Recas (V1.1)
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una sesión
%    -sName: Nombre del Shimmer2 para calibración RN42-XXXX
%  Salidas:
%    -accCal: aceleración calibrada (X-Y-Z) [m/s^2]
%    -mod:    módulo de la aceleración
%    -time:   vector de tiempos en segundos comenzando en 0
%

    accCal=[];mod=[];time=[];
    
    fid=loadLog(dataDir, 'ACC');
    if(fid==-1)
      accCal=[];mod=[];time=[];
      disp(['No ha información de acelerometría para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end

    data = fread(fid, 'uint16','b');
    fclose(fid);
    
    adc_acc = reshape(data,3,length(data)/3)';
    %adc_acc(50:end-50);
    %adc_acc(adc_acc>)
    time=(0:length(adc_acc)-1)'*0.1;

    [accCal]=calibrateAcc(adc_acc,sName,1.5);
    
    accCal(abs(accCal)>50)=0;
    mod=sqrt(sum(accCal.^2,2));
end

