function [CalibratedData]=calibrateAcc(UncalibratedData,btId,accRange)
%  Autor Joaquín Recas (V1.0)

%AM -> R    AccelCalParametersAM=[1 0 0; 0 1 0; 0 0 -1];                       % Default Calibration Parameters for Accelerometer (Allignment Matrix)
%SM -> K    AccelCalParametersSM=[38 0 0; 0 38 0; 0 0 38];                     % Default Calibration Parameters for Accelerometer (Sensitivity Matrix)
%OV -> B    AccelCalParametersOV=[2048;2048;2048];                             % Default Calibration Parameters for Accelerometer (Offset Vector) 

B=[];K=[];R=[];

if(nargin==3 && strcmp(btId,'RN42-B5E6') && accRange==1.5)
    %Calibration Paramenter for RN42-B5E6 @ +-1.5g
    B=[ 1915.750133; ...
        2104.839722; ...
        1743.841619];

    K=[ 98.344443   0          0; ...
          0       98.628723    0; ...
          0         0        98.616135];

    R=[  0.999446  0.017634  -0.028210; ...
        -0.005626  0.999123  -0.041494;...
         0.001196  0.004793  -0.999988  ];
end

if(nargin==3 && strcmp(btId,'RN42-9EEC') && accRange==1.5)
    %Calibration Paramenter for RN42-B5E6 @ +-1.5g
    B=[ 1899.252062; ...
        2169.462831; ...
        1532.324418];

    K=[ 98.781902   0          0; ...
          0      100.998173    0; ...
          0         0        99.486716];

    R=[  0.999878  0.006770  -0.014082; ...
        -0.008003  0.999735   0.021603;...
        -0.007917  0.032662  -0.999435  ];
end

if(nargin==3 && strcmp(btId,'RN42-9AA0') && accRange==1.5)
    %Calibration Paramenter for RN42-B5E6 @ +-1.5g
    B=[ 1987.818281; ...
        2113.621159; ...
        1764.096728];

    K=[ 98.759359   0          0; ...
          0       98.670142    0; ...
          0         0        98.440584];

    R=[  0.999551  0.010342  -0.028133; ...
        -0.001734  0.999413  -0.034223;...
        -0.019238  0.034317  -0.999226  ];
end

if(isempty(B))
  disp('Warning: Accelerometer calibration using default parameters!!')
  R=[1 0 0; 0 1 0; 0 0 -1];                       % Default Calibration Parameters for Accelerometer (Allignment Matrix)
  K=[38 0 0; 0 38 0; 0 0 38];                     % Default Calibration Parameters for Accelerometer (Sensitivity Matrix)
  B=[2048;2048;2048];                             % Default Calibration Parameters for Accelerometer (Offset Vector) 
end

%RK_ACC=(R^-1)*(K^-1)  
%B_ACC=B

CalibratedData=((R^-1)*(K^-1)*(UncalibratedData'-B*ones(1,length(UncalibratedData(:,1)))))';

end