function [ecg, time, offset]=getShHrEcgExp(dataDir, fileLog, T)
%
%  El ECG se mide cada 1ms y para pasarlo a mv hay que dividir por 2069
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una sesión
%  Salidas:
%    -ecg:     electrocardiografía RAW
%    -time:    vector de tiempos del ECG en segundos comenzando en 0
%    -offset:  offset de sicronización cada 1.0240s (cada 256 muestras) y número de muestras perdidas
%

    ecg=[];time=[];offset=[];
    
    fid=loadLog(dataDir, fileLog);
    
    rawEcg = fread(fid, 'uint16','b');
    fclose(fid);
    if(isempty(rawEcg))
      disp(['No ha información de ECG para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end

    
    %ecg = rawEcg/2069-1;
    ecg = rawEcg;
    time=(0:length(ecg)-1)'*T;

    fid=loadLog(dataDir, 'OFF');
    data = fread(fid, 'int32','b');
    fclose(fid);
    if(isempty(data))
      %ecg=[];
      disp(['No ha información de sincronización para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end    
     
    offset = reshape(data,2,length(data)/2)';
    if(sum(offset(:,2)))
      disp(['Se han perdido muestras de ECG en el registro: ', num2str(sum(offset(:,2)))]);
      
%       %Lugares en los que faltan muestras
%       inds=find(offset(:,2)~=0);   
%       %dec2hex(offset(inds,1)*2)
%       %offset(inds,:)
%       %figure; plot(0:length(rawEcg)-1, rawEcg, 'b.-', offset(:,1),zeros(length(offset(:,1)),1), 'r.')
%       for i=inds'
%         %offset(i,1)
%         ecg(offset(i-1,1)+1:offset(i,1))=nan;
% 
%         %figure; plot( ecg(offset(i-1,1)+1 :offset(i,1))); %Este es el trozo malo
%         
%         %nOkSamples=offset(i+1,1)-offset(i,1);    %numero de muestras que debería de haber en la ventana
%         %nFaltan=offset(i+1,2);
%         %inicEcg=offset(i,1)+1;
%         %ecg(offset(i,1)+1:offset(i+1,1));        %Este es el trozo malo, que debería de tener 256 muestras
%         %figure; plot(ecg(offset(i,1)+1:offset(i+1,1))); %Este es el trozo malo
%         %ecg(offset(i,1):offset(i+1,1))=nan;
%       end
%       
%       %figure; plot(0:length(ecg)-1, ecg, 'b.-', offset(:,1),zeros(length(offset(:,1)),1), 'r.')
%        nSamp=offset(2:end,1)-offset(1:end-1,1);
%        indF=find(nSamp~=256)';
%        if(~isempty(indF))
%          disp('Faltan datos de sincronización!!!');
%        end
% %       for i=indF
% %         nOkSamples=offset(i+1,1)-offset(i,1);    %numero de muestras que debería de haber en la ventana
% %         nFaltan=offset(i+1,2);
% %         ecg=[ecg(1:offset(i,1)); nan(nOkSamples,1); ecg(offset(i+1,1)+1:end)];
% %         %inicEcg=offset(i,1)+1;
% %         %ecg(offset(i,1)+1:offset(i+1,1));        %Este es el trozo malo, que debería de tener 256 muestras
% %         %figure; plot(ecg(offset(i,1)+1:offset(i+1,1))); %Este es el trozo malo
% %         %ecg(offset(i,1):offset(i+1,1))=nan;
% %       end
    end
    


    %Pasamos el número de muestra a tiempo
    offset(:,1)=offset(:,1)*T;
  
end

