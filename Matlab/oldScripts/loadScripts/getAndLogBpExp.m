function [valuesBP, time]=getAndLogBpExp(dataDir)
%
%  Recoge las medidas de presion sanguínea (BoodPressure) que se han anotado en el android
%
%  Autor Joaquín Recas (V1.1)
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una sesión
%  Salidas:
%    -valuesBP: matriz con DBP, SBP y BPM
%    -time: tiempo absoluto de las medidas en formato Matlab (días desde 00/00/0000 00:00:00h)
%

    valuesBP=[];time=[];

    fid = fopen([dataDir,'BLOODPRESSURE.log']);
    if(fid==-1)
      [status,result]=system(['cat ',dataDir, 'eventLog???.txt > ',dataDir,'BLOODPRESSURE.log']);
      if(status==1)
        disp(['No ha información de registros con presión sanguínea en el experimento: ', dataDir]);
        return;
      end
      fid = fopen([dataDir,'BLOODPRESSURE.log']);
      if(fid==-1)
          disp(['No se puede abrir el fichero ', dataDir,'ACC.log']);
          return;
      end    
    end
    
    line=fgetl(fid);
    while(ischar(line))
      %Timestamp: 2013.03.21-17.01.35
      if(length(line)==30)
        timeBP=datenum(line(12:end), 'yyyy.mm.dd-HH.MM.SS');
        time(end+1,1)=timeBP;
      else
        time=[];
        break;
      end
      
      %Event: 123 80 58
      line=fgetl(fid);
      if(length(line)==16)
      
        valuesBP(end+1,:)=sscanf(line, 'Event: %f %f %f');
      else
        time=[];
        break;
      end
      line=fgetl(fid);
    end
  
    fclose(fid);

    if(isempty(time))
      valuesBP=[];time=[];
      disp('Error en el formato de fichero para Blood Pressure');
    end
end

