function [spo2, time]=getShSpO2Exp(dataDir)
%
%  La oximetría se mide cada 
%
%  Autor Joaquín Recas (V1.1)
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una
%    sesión
%  Salidas:
%    -spo2: oximetria [%]
%    -time: vector de tiempos en segundos comenzando en 0
%

    spo2=[]; time=[];

    fid=loadLog(dataDir, 'SPO2');
    if(fid==-1)
      spo2=[];time=[];
      disp(['No ha información de SpO2 para el Shimmer2 en el experimento: ', dataDir]);
      return;
    end
    data = fread(fid, 'uint8','b');
    fclose(fid);
    spo2 = reshape(data,3,length(data)/3)';
    
    if(sum((spo2(2:end,1)-spo2(1:end-1,1))~=1 & (spo2(2:end,1)-spo2(1:end-1,1))~=-255))
      disp(['Se han perdido muestras de SpO2 en el registro: ', num2str(sum((spo2(2:end,1)-spo2(1:end-1,1))~=1 & (spo2(2:end,1)-spo2(1:end-1,1))~=-255))]);
    end

     
    
    %spo2((spo2(:,2)==127),2)=NaN;
    %spo2((spo2(:,3)==127),3)=NaN;
    
    time=(0:length(spo2)-1)';
    %if(spo2(1,1)==12)
      time=time*0.012;
    %end

end

