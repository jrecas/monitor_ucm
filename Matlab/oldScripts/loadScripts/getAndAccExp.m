function [acc, mod, time]=getAndAccExp(dataDir)
%
%  La aceleración en Android (FreeShimmer v5.3) se mide cada 200ms aprox
%
%  Autor Joaquín Recas (V1.1)
%
%  Entradas:
%    -dataDir directorio donde está el experimento correpsondiente a una
%    sesión
%  Salidas:
%    -acc:  aceleración (X-Y-Z) [m/s^2]
%    -mod:  módulo de la aceleración
%    -time: vector de tiempos en segundos comenzando en 0
%

    acc=[];mod=[];time=[];
    
    fid=loadLog(dataDir, 'AACC');
    if(fid==-1)
      disp(['No ha información de acelerometría para el dispositivo android en el experimento: ', dataDir]);
      return;
    end

    data = fread(fid, 'float','b');
    fclose(fid);
    data = reshape(data,4,length(data)/4)';
    
    time=(data(:,1)-data(1,1))/1e9;
    acc=data(:,2:4);

    mod=sqrt(sum(acc.^2,2));

end

