clear all; close all; clc;
addpath(genpath('..\calibration\'))
% Experimento y directorio
dateReg = '2014.06.13-01.20.41';
rootDir = '..\calibration\';

%
% Calibracion
%
% Obtener datos de ECG y oximetria
[sECG, sACC, sOxim] = getExpsData(rootDir, dateReg, true);
% Filtar ECG
sECG = filterECG(sECG);
% Filtrar PPG
sOxim = lowFIRFilterPleth(sOxim);           % Filtro FIR
%sOxim = lowIIRFilterPleth(sOxim);          % Filtro IIR
% Calcular puntos y tiempos de oximetria
sOxim = calPointsTimes(sECG, sOxim);
% Obtener datos de presion sanguinea
sBP = getBP(dateReg);

if (~isempty(sBP.sistolic) && ~isempty(sBP.sistolic))
    % Promediar oximetria con presion sanguinea
    sBP = getDataAndBP(sBP, sOxim);
    % Estimar el mejor metodo para la presion
    sBP = runModels(sBP);
    sBP = getModel(sBP);
    % Crear calibration.log
    saveCalibrationLog(sBP, sOxim);
end

% Finalizando
save(strcat('calibration-',dateReg(12:end),'.mat'),'sECG','sOxim','sBP');
disp('Calibration finished!!');
