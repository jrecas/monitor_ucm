function highPassEcg=baselineCorrection(ecgData, sFreq, LBo,LBc, plotEnable)
% Se pueden usar estos valores por defecto
%  lBo= .2;
%  lBc= 1.5;

Bo=zeros(round(LBo*sFreq),1);
Bc=zeros(round(LBc*LBo*sFreq),1);

%tic
ecgNoPeaks=morphOp(ecgData, Bo, 'open');
%toc
%tic
ecgBaseLine=morphOp(ecgNoPeaks, Bc, 'close');
%toc

%ecgHF=ecgData-ecgNoPeaks;
highPassEcg=ecgData-ecgBaseLine;
highPassEcg(isnan(highPassEcg))=0;

if(plotEnable==true)
  tEcg=(1:length(ecgData))/sFreq;
  figure; 
  hold on;
  plot(tEcg, ecgData,    'b.-')
  plot(tEcg, ecgNoPeaks, 'c.-')
  plot(tEcg, ecgBaseLine,'k.-')
  plot(tEcg, highPassEcg,'r.-')
  plot(tEcg, ecgData-mean(ecgData),    'b.-')

  legend('Original', 'noPeaks', 'Baseline', 'HF', 'Org-mean')
end
