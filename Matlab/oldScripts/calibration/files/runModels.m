function sBP = runModels(sBP)

%% PEP (Pre-Ejection Period)
% PAT = PEP + PTT
% Es variable, pero en Foo (2005) estiman 160 ms para un adulto
PEP = 160;

%% Modelos
% sBP.modelsX(:,1) = coeficiente de determinación R^2
% sBP.modelsX(:,2) = coeficiente de determinación R^2 ajustado
% sBP.modelsX(:,3) = valor p
% sBP.modelsX(:,4) = ordenada
% sBP.modelsX(:,5) = pendiente 1
% sBP.modelsX(:,6) = pendiente 2 (en ajustes multilineales)

%% Modelo 1. Variante de Cattivelli, Garudadri (2009)
% Modelo 1
% SBP = a1 + b1*PATp + c1*HR
% DBP = a2 + b2*PATp + c2*HR
objFit = fitlm([1e3*sBP.meanTimes.PATp 1./(1e3*sBP.meanTimes.invHR)], sBP.sistolic);
sBP.modelsSistolic(1,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(1,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(1,3) = objFit.coefTest;
sBP.modelsSistolic(1,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(1,5) = objFit.Coefficients.Estimate(2);
sBP.modelsSistolic(1,6) = objFit.Coefficients.Estimate(3);

objFit = fitlm([1e3*sBP.meanTimes.PATp 1./(1e3*sBP.meanTimes.invHR)], sBP.diastolic);
sBP.modelsDiastolic(1,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(1,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(1,3) = objFit.coefTest;
sBP.modelsDiastolic(1,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(1,5) = objFit.Coefficients.Estimate(2);
sBP.modelsDiastolic(1,6) = objFit.Coefficients.Estimate(3);

%% Modelos 2-4. BP vs PAT
% Modelo 2
% SBP = a1 + b1*PATp
% DBP = a2 + b2*PATp
objFit = fitlm(1e3*sBP.meanTimes.PATp, sBP.sistolic);
sBP.modelsSistolic(2,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(2,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(2,3) = objFit.coefTest;
sBP.modelsSistolic(2,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(2,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1e3*sBP.meanTimes.PATp, sBP.diastolic);
sBP.modelsDiastolic(2,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(2,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(2,3) = objFit.coefTest;
sBP.modelsDiastolic(2,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(2,5) = objFit.Coefficients.Estimate(2);

% Modelo 3
% SBP = a1 + b1*PATf
% DBP = a2 + b2*PATf
objFit = fitlm(1e3*sBP.meanTimes.PATf, sBP.sistolic);
sBP.modelsSistolic(3,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(3,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(3,3) = objFit.coefTest;
sBP.modelsSistolic(3,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(3,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1e3*sBP.meanTimes.PATf, sBP.diastolic);
sBP.modelsDiastolic(3,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(3,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(3,3) = objFit.coefTest;
sBP.modelsDiastolic(3,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(3,5) = objFit.Coefficients.Estimate(2);

% Modelo 4
% SBP = a1 + b1*PATs
% DBP = a2 + b2*PATs
objFit = fitlm(1e3*sBP.meanTimes.PATs, sBP.sistolic);
sBP.modelsSistolic(4,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(4,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(4,3) = objFit.coefTest;
sBP.modelsSistolic(4,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(4,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1e3*sBP.meanTimes.PATs, sBP.diastolic);
sBP.modelsDiastolic(4,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(4,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(4,3) = objFit.coefTest;
sBP.modelsDiastolic(4,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(4,5) = objFit.Coefficients.Estimate(2);

%% Modelos 5-7. BP vs 1/(PAT^2)
% Modelo 5
% SBP = a1 + b1/(PATp^2)
% DBP = a2 + b2/(PATp^2)
objFit = fitlm(1./((1e3*sBP.meanTimes.PATp).^2), sBP.sistolic);
sBP.modelsSistolic(5,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(5,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(5,3) = objFit.coefTest;
sBP.modelsSistolic(5,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(5,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1./((1e3*sBP.meanTimes.PATp).^2), sBP.diastolic);
sBP.modelsDiastolic(5,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(5,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(5,3) = objFit.coefTest;
sBP.modelsDiastolic(5,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(5,5) = objFit.Coefficients.Estimate(2);

% Modelo 6
% SBP = a1 + b1/(PATf^2)
% DBP = a2 + b2/(PATf^2)
objFit = fitlm(1./((1e3*sBP.meanTimes.PATf).^2), sBP.sistolic);
sBP.modelsSistolic(6,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(6,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(6,3) = objFit.coefTest;
sBP.modelsSistolic(6,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(6,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1./((1e3*sBP.meanTimes.PATf).^2), sBP.diastolic);
sBP.modelsDiastolic(6,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(6,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(6,3) = objFit.coefTest;
sBP.modelsDiastolic(6,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(6,5) = objFit.Coefficients.Estimate(2);

% Modelo 7
% SBP = a1 + b1/(PATs^2)
% DBP = a2 + b2/(PATs^2)
objFit = fitlm(1./((1e3*sBP.meanTimes.PATs).^2), sBP.sistolic);
sBP.modelsSistolic(7,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(7,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(7,3) = objFit.coefTest;
sBP.modelsSistolic(7,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(7,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1./((1e3*sBP.meanTimes.PATs).^2), sBP.diastolic);
sBP.modelsDiastolic(7,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(7,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(7,3) = objFit.coefTest;
sBP.modelsDiastolic(7,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(7,5) = objFit.Coefficients.Estimate(2);

%% Modelos 8-10. BP vs 1/((PAT-PEP)^2)
% Modelo 8
% SBP = a1 + b1/((PATp-PEP)^2)
% DBP = a2 + b2/((PATp-PEP)^2)
objFit = fitlm(1./((1e3*sBP.meanTimes.PATp-PEP).^2), sBP.sistolic);
sBP.modelsSistolic(8,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(8,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(8,3) = objFit.coefTest;
sBP.modelsSistolic(8,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(8,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1./((1e3*sBP.meanTimes.PATp-PEP).^2), sBP.diastolic);
sBP.modelsDiastolic(8,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(8,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(8,3) = objFit.coefTest;
sBP.modelsDiastolic(8,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(8,5) = objFit.Coefficients.Estimate(2);

% Modelo 9
% SBP = a1 + b1/((PATf-PEP)^2)
% DBP = a2 + b2/((PATf-PEP)^2)
objFit = fitlm(1./((1e3*sBP.meanTimes.PATf-PEP).^2), sBP.sistolic);
sBP.modelsSistolic(9,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(9,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(9,3) = objFit.coefTest;
sBP.modelsSistolic(9,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(9,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1./((1e3*sBP.meanTimes.PATf-PEP).^2), sBP.diastolic);
sBP.modelsDiastolic(9,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(9,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(9,3) = objFit.coefTest;
sBP.modelsDiastolic(9,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(9,5) = objFit.Coefficients.Estimate(2);

% Modelo 10
% SBP = a1 + b1/((PATs-PEP)^2)
% DBP = a2 + b2/((PATs-PEP)^2)
objFit = fitlm(1./((1e3*sBP.meanTimes.PATs-PEP).^2), sBP.sistolic);
sBP.modelsSistolic(10,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(10,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(10,3) = objFit.coefTest;
sBP.modelsSistolic(10,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(10,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1./((1e3*sBP.meanTimes.PATs-PEP).^2), sBP.diastolic);
sBP.modelsDiastolic(10,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(10,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(10,3) = objFit.coefTest;
sBP.modelsDiastolic(10,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(10,5) = objFit.Coefficients.Estimate(2);

%% Modelos 11-13. Awad (2001), Teng (2003) y Yoon (2009)
% Modelo 11
% SBP = a1 + b1*WT
% DBP = a2 + b2*WT
objFit = fitlm(1e3*sBP.meanTimes.WT, sBP.sistolic);
sBP.modelsSistolic(11,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(11,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(11,3) = objFit.coefTest;
sBP.modelsSistolic(11,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(11,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1e3*sBP.meanTimes.WT, sBP.diastolic);
sBP.modelsDiastolic(11,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(11,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(11,3) = objFit.coefTest;
sBP.modelsDiastolic(11,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(11,5) = objFit.Coefficients.Estimate(2);

% Modelo 12
% SBP = a1 + b1*t1
% DBP = a2 + b2*t1
objFit = fitlm(1e3*sBP.meanTimes.t1, sBP.sistolic);
sBP.modelsSistolic(12,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(12,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(12,3) = objFit.coefTest;
sBP.modelsSistolic(12,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(12,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1e3*sBP.meanTimes.t1, sBP.diastolic);
sBP.modelsDiastolic(12,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(12,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(12,3) = objFit.coefTest;
sBP.modelsDiastolic(12,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(12,5) = objFit.Coefficients.Estimate(2);

% Modelo 13
% SBP = a1 + b1*t2
% DBP = a2 + b2*t2
objFit = fitlm(1e3*sBP.meanTimes.t2, sBP.sistolic);
sBP.modelsSistolic(13,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(13,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(13,3) = objFit.coefTest;
sBP.modelsSistolic(13,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(13,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1e3*sBP.meanTimes.t2, sBP.diastolic);
sBP.modelsDiastolic(13,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(13,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(13,3) = objFit.coefTest;
sBP.modelsDiastolic(13,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(13,5) = objFit.Coefficients.Estimate(2);

%% Modelo 14. Propuesta para el TFM (José Manuel Bote).
% Modelo 14
% SBP = a1 + b1*PATfWT
% DBP = a2 + b2*PATfWT
objFit = fitlm(1e3*sBP.meanTimes.PATfWT, sBP.sistolic);
sBP.modelsSistolic(14,1) = objFit.Rsquared.Ordinary;
sBP.modelsSistolic(14,2) = objFit.Rsquared.Adjusted;
sBP.modelsSistolic(14,3) = objFit.coefTest;
sBP.modelsSistolic(14,4) = objFit.Coefficients.Estimate(1);
sBP.modelsSistolic(14,5) = objFit.Coefficients.Estimate(2);

objFit = fitlm(1e3*sBP.meanTimes.PATfWT, sBP.diastolic);
sBP.modelsDiastolic(14,1) = objFit.Rsquared.Ordinary;
sBP.modelsDiastolic(14,2) = objFit.Rsquared.Adjusted;
sBP.modelsDiastolic(14,3) = objFit.coefTest;
sBP.modelsDiastolic(14,4) = objFit.Coefficients.Estimate(1);
sBP.modelsDiastolic(14,5) = objFit.Coefficients.Estimate(2);

%% Mejor modelo
% Elegir el mejor modelo en funcion del coeficiente de determinacion
% ajustado R2.
[value, sBP.bestModel.sisModel] = max(sBP.modelsSistolic(:,2));
sBP.bestModel.sisCoef = [sBP.modelsSistolic(sBP.bestModel.sisModel,4);
    sBP.modelsSistolic(sBP.bestModel.sisModel,5);
    sBP.modelsSistolic(sBP.bestModel.sisModel,6)];

[value, sBP.bestModel.diaModel] = max(sBP.modelsDiastolic(:,2));
sBP.bestModel.diaCoef = [sBP.modelsDiastolic(sBP.bestModel.diaModel,4);
    sBP.modelsDiastolic(sBP.bestModel.diaModel,5);
    sBP.modelsDiastolic(sBP.bestModel.diaModel,6)];