function sECG = filterECG(sECG)

javaPath = '..\utilScripts\ECGDelineatorOf.jar';
javaaddpath(javaPath,'-end');

% delObj = javaObjectEDT('ECGDelineator.ECGDelineator');
filtObj = javaObjectEDT('ECGDelineator.ECGFilter');

sECG.filtEcg = zeros(length(sECG.rawEcg),1);
for i = 1:length(sECG.rawEcg)
    sECG.filtEcg(i) = filtObj.filterSample(sECG.rawEcg(i));
end

sECG.filtEcg(1:end-filtObj.filterDelay) = sECG.filtEcg(filtObj.filterDelay+1:end);

tmp = round(sECG.tBumpsRel * 250 + 1);

if (~isempty(sECG.tBumpsRel(:,1)))
    sECG.filtBumps = nan(length(sECG.tBumpsRel(:,1)),length(sECG.tBumpsRel(1,:)));
    for i = 1:length(tmp(:,1))
        for j = 1:length(tmp(1,:))
            if (~isnan(tmp(i,j)))
                sECG.filtBumps(i,j) = sECG.filtEcg(tmp(i,j));
            end
        end
    end
end
