function [fB]=morphOp(f,B,Op)
  %function [fB]=erosion()
  %f=0:20;
  %B=zeros(1,6);
 
  %Hacemos la longitud de B impar quitando una muestra si fuese necesario
  if(mod(length(B),2)~=1)
    B=B(1:end-1);
  end
  
  N=length(f);
  M=length(B);
  M1=(M-1)/2;
  
  if(strcmp(Op,'open'))        
    %% Open: Elimina picos
    %B es todo 0's
    if(sum(abs(B))==0)
      % Erosion: fB1 = f erosion B
      %fB1=zeros(N,1);
      fB1=nan(N,1);
      
      parfor i=M1+1:(N-M1)
        fB1(i)=min(f(i-M1:i+M1));
      end
      % Dilation: fB1 = fB1 dilation B
      %fB=zeros(N,1);
      fB=nan(N,1);
      parfor i=M1+1:(N-M1)
        fB(i)=max(fB1(i-M1:i+M1));
      end
    %B no es todo 0's
    else
      % Erosion: fB1 = f erosion B
      fB1=zeros(N,1);
      for i=M1+1:(N-M1)
        fB1(i)=min(f(i-M1:i+M1)-B);
      end
      % Dilation: fB1 = fB1 dilation B
      fB=zeros(N,1);
      for i=M1+1:(N-M1)
        fB(i)=max(fB1(i-M1:i+M1)+B);
      end
    end
    
  elseif(strcmp(Op,'close'))
    %% Close: Elimina valles
    %B es todo 0's
    if(sum(abs(B))==0)
      % Dilation: fB1 = f dilation B
      %fB1=zeros(N,1);
      fB1=nan(N,1);
      parfor i=M1+1:(N-M1)
        fB1(i)=max(f(i-M1:i+M1));
      end
      % Erosion: fB1 = fB1 erosion B
      %fB=zeros(N,1);
      fB=nan(N,1);
      parfor i=M1+1:(N-M1)
        fB(i)=min(fB1(i-M1:i+M1));
      end
    %B no es todo 0's
    else
      % Dilation: fB1 = f dilation B
      fB1=zeros(N,1);
      for i=M1+1:(N-M1)
        fB1(i)=max(f(i-M1:i+M1)+B);
      end
      % Erosion: fB1 = fB1 erosion B
      fB=zeros(N,1);
      for i=M1+1:(N-M1)
        fB(i)=min(fB1(i-M1:i+M1)-B);
      end
    end
  else
    error(['Operación morfológica "', Op, '" no implementada']);
  end
 
end
