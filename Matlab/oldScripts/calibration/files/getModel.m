function sBP = getModel(sBP)

[tmp,sBP.bestModel.sisModel] = max(abs(sBP.models(:,1)));
[tmp,sBP.bestModel.diaModel] = max(abs(sBP.models(:,3)));

switch sBP.bestModel.sisModel
    case 1
        x1 = [1e3*sBP.meanTimes.PATp 1./(1e3*sBP.meanTimes.invHR) ones(length(sBP.meanTimes.PATp),1)];
        sBP.bestModel.sisCoef = regress(sBP.sistolic, x1)';
    case 2
        sBP.bestModel.sisCoef = polyfit(1e3*sBP.meanTimes.PATp, sBP.sistolic, 1);
    case 3
        sBP.bestModel.sisCoef = polyfit(1e3*sBP.meanTimes.PATf, sBP.sistolic, 1);
    case 4
        sBP.bestModel.sisCoef = polyfit(1./((1e3*sBP.meanTimes.PATp).^2), sBP.sistolic, 1);
    case 5
        sBP.bestModel.sisCoef = polyfit(1./((1e3*sBP.meanTimes.PATf).^2), sBP.sistolic, 1);
    case 6
        sBP.bestModel.sisCoef = polyfit(1./((1e3*sBP.meanTimes.PATp-160).^2), sBP.sistolic, 1);
    case 7
        sBP.bestModel.sisCoef = polyfit(1./((1e3*sBP.meanTimes.PATf-160).^2), sBP.sistolic, 1);
    case 8
        sBP.bestModel.sisCoef = polyfit(1e3*sBP.meanTimes.WT, sBP.sistolic, 1);
    case 9
        sBP.bestModel.sisCoef = polyfit(1e3*sBP.meanTimes.t1, sBP.sistolic, 1);
    case 10
        sBP.bestModel.sisCoef = polyfit(1e3*sBP.meanTimes.t2, sBP.sistolic, 1);
    case 11
        sBP.bestModel.sisCoef = polyfit(1e3*sBP.meanTimes.PATfWT, sBP.sistolic, 1);
end

switch sBP.bestModel.diaModel
    case 1
        x1 = [1e3*sBP.meanTimes.PATp 1./(1e3*sBP.meanTimes.invHR) ones(length(sBP.meanTimes.PATp),1)];
        sBP.bestModel.diaCoef = regress(sBP.diastolic, x1)';
    case 2
        sBP.bestModel.diaCoef = polyfit(1e3*sBP.meanTimes.PATp, sBP.diastolic, 1);
    case 3
        sBP.bestModel.diaCoef = polyfit(1e3*sBP.meanTimes.PATf, sBP.diastolic, 1);
    case 4
        sBP.bestModel.diaCoef = polyfit(1./((1e3*sBP.meanTimes.PATp).^2), sBP.diastolic, 1);
    case 5
        sBP.bestModel.diaCoef = polyfit(1./((1e3*sBP.meanTimes.PATf).^2), sBP.diastolic, 1);
    case 6
        sBP.bestModel.diaCoef = polyfit(1./((1e3*sBP.meanTimes.PATp-160).^2), sBP.diastolic, 1);
    case 7
        sBP.bestModel.diaCoef = polyfit(1./((1e3*sBP.meanTimes.PATf-160).^2), sBP.diastolic, 1);
    case 8
        sBP.bestModel.diaCoef = polyfit(1e3*sBP.meanTimes.WT, sBP.diastolic, 1);
    case 9
        sBP.bestModel.diaCoef = polyfit(1e3*sBP.meanTimes.t1, sBP.diastolic, 1);
    case 10
        sBP.bestModel.diaCoef = polyfit(1e3*sBP.meanTimes.t2, sBP.diastolic, 1);
    case 11
        sBP.bestModel.diaCoef = polyfit(1e3*sBP.meanTimes.PATfWT, sBP.diastolic, 1);
end