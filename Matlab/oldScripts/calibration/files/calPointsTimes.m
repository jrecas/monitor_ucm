function sOxim = calPointsTimes(sECG, sOxim)

%% Inicializar variables
% Puntos
sOxim.points.maximo = nan(length(sECG.tBumpsRel),2);
sOxim.points.pie = nan(length(sECG.tBumpsRel),2);
sOxim.points.maxPend = nan(length(sECG.tBumpsRel),2);
sOxim.points.wt0 = nan(length(sECG.tBumpsRel),2);
sOxim.points.wt1 = nan(length(sECG.tBumpsRel),2);

% Tiempos
sOxim.times.PATp = nan(length(sECG.tBumpsRel),1);
sOxim.times.PATf = nan(length(sECG.tBumpsRel),1);
sOxim.times.PATs = nan(length(sECG.tBumpsRel),1);
sOxim.times.WT = nan(length(sECG.tBumpsRel),1);
sOxim.times.t1 = nan(length(sECG.tBumpsRel),1);
sOxim.times.t2 = nan(length(sECG.tBumpsRel),1);
sOxim.times.PATfWT = nan(length(sECG.tBumpsRel),1);
sOxim.times.invHR = nan(length(sECG.tBumpsRel),1);

%% Primera y segunda derivada de la PPG
sOxim.firDfiltPleth = zeros(length(sOxim.pleth),1);
sOxim.secDfiltPleth = zeros(length(sOxim.pleth),1);
for i=1:(length(sOxim.filtPleth)-2)
    sOxim.firDfiltPleth(i+1) = sOxim.filtPleth(i+2) - sOxim.filtPleth(i);
    sOxim.secDfiltPleth(i+1) = sOxim.filtPleth(i+2) - 2*sOxim.filtPleth(i+1)...
        + sOxim.filtPleth(i);
end

% Truncar la se�al de PPG como se hace en Android
sOxim.filtPleth = floor(sOxim.filtPleth);

%% Buscar puntos y tiempos
for i = 2:length(sECG.tBumpsRel)-1
    
% PUNTOS: maximo, pie, maxPend, wt0, wt1
    % Intervalo de Pleth entre picos de ondas R y R+1
    index = sOxim.timeRel > sECG.tBumpsRel(i,sECG.indType.R) & sOxim.timeRel < sECG.tBumpsRel(i+1,sECG.indType.R);
    data = sOxim.filtPleth(index);
    time = sOxim.timeRel(index);
    
    % Maximo del intervalo de Pleth
    [maximo1,posMax1] = max(data);
    [maximo2,posMax2] = max(flipud(data));
    posMax = floor((posMax1 + (length(data)+1 - posMax2))/2);
    if (~isempty(maximo1) && ~isempty(maximo2))
        sOxim.points.maximo(i,1) = time(posMax);
        sOxim.points.maximo(i,2) = maximo1;
    end
    
    % Pie del intervalo de Pleth
    if (~isempty(maximo1))
        indexPie = sOxim.timeRel > sECG.tBumpsRel(i,sECG.indType.R) & sOxim.timeRel < time(posMax);
        dataPie = sOxim.filtPleth(indexPie);
        timePie = sOxim.timeRel(indexPie);
        dataDPie = sOxim.secDfiltPleth(indexPie);
        [pie,posPie] = max(dataDPie);
        if (~isempty(pie))
            sOxim.points.pie(i,1) = timePie(posPie);
            sOxim.points.pie(i,2) = dataPie(posPie);
        end
    end

%   % Minimo del intervalo de Pleth
%     if (~isempty(maximo1) && ~isempty(maximo2))
%         indexMin = sOxim.timeRel > sECG.tBumpsRel(i,sECG.indType.R) & sOxim.timeRel < time(posMax);
%         dataMin = sOxim.filtPleth(indexMin);
%         timeMin = sOxim.timeRel(indexMin);
%         [minimo1,posMin1] = min(dataMin);
%         M = 3;
%         if (length(dataMin)-M > 3)
%             for j = length(dataMin)-M:-1:3
%                 minimo1 = dataMin(j);
%                 nextMin = dataMin(j-1);
%                 nextMin2 = dataMin(j-2);
%                 posMin1 = j;
%                 if (minimo1 <= nextMin && minimo1 <= nextMin2)
%                     break
%                 end
%             end
%             if (~isempty(minimo1))
%                 sOxim.points.minimo(i,1) = timeMin(posMin1);
%                 sOxim.points.minimo(i,2) = minimo1;
%             end
%         end
%     end

    % maxPend del intervalo de Pleth
    if (~isempty(maximo1) && ~isempty(pie))
        indexPend = sOxim.timeRel >= timePie(posPie) & sOxim.timeRel <= time(posMax);
        dataPend = sOxim.filtPleth(indexPend);
        timePend = sOxim.timeRel(indexPend);
        dataDPend = sOxim.firDfiltPleth(indexPend);
        [pend,posPend] = max(dataDPend);
        if (~isempty(pend))
            sOxim.points.maxPend(i,1) = timePend(posPend);
            sOxim.points.maxPend(i,2) = dataPend(posPend);
        end
    end
    
    % wt0 y wt1 del intervalo de Pleth
    if (~isempty(maximo1) && ~isempty(pie))
        indexWt1 = sOxim.timeRel > time(posMax) & sOxim.timeRel < sECG.tBumpsRel(i+1,sECG.indType.R);
        dataWt1 = sOxim.filtPleth(indexWt1);
        timeWt1 = sOxim.timeRel(indexWt1);
        cota = sOxim.points.maximo(i,2) - floor((sOxim.points.maximo(i,2)-sOxim.points.pie(i,2))/3);
        wt0 = find(dataPend >= cota, 1, 'first');
        wt1 = find(dataWt1 < cota, 1, 'first');
        if (~isempty(wt0))
            sOxim.points.wt0(i,1) = timePend(wt0);
            sOxim.points.wt0(i,2) = dataPend(wt0);
        end
        if (~isempty(wt1))
            sOxim.points.wt1(i,1) = timeWt1(wt1);
            sOxim.points.wt1(i,2) = dataWt1(wt1);
        end
    end
    
% TIEMPOS: PATp, PATs, PATf, WT, t1, t2, PATfWT
    if (~isempty(sOxim.points.maximo(i)))
        sOxim.times.PATp(i) = sOxim.points.maximo(i,1) - sECG.tBumpsRel(i,sECG.indType.R);
    end
    if (~isempty(sOxim.points.pie(i)))
        sOxim.times.PATf(i) = sOxim.points.pie(i,1) - sECG.tBumpsRel(i,sECG.indType.R);
    end
    if (~isempty(sOxim.points.maxPend(i)))
        sOxim.times.PATs(i) = sOxim.points.maxPend(i,1) - sECG.tBumpsRel(i,sECG.indType.R);
    end
    if (~isempty(sOxim.points.wt0(i)) && ~isempty(sOxim.points.wt1(i)))
        sOxim.times.WT(i) = sOxim.points.wt1(i,1) - sOxim.points.wt0(i,1);
    end
    if (~isempty(sOxim.points.maximo(i)) && ~isempty(sOxim.points.pie(i)))
        sOxim.times.t1(i) = sOxim.points.maximo(i,1) - sOxim.points.pie(i,1);
    end
    if (~isempty(sOxim.points.maximo(i-1)) && ~isempty(sOxim.points.pie(i)))
        sOxim.times.t2(i) = sOxim.points.pie(i,1) - sOxim.points.maximo(i-1,1);
    end
    if (~isempty(sOxim.points.pie(i)) && ~isempty(sOxim.points.wt1(i)))
        sOxim.times.PATfWT(i) = sOxim.points.wt1(i,1) - sOxim.points.pie(i,1);
    end
    sOxim.times.invHR(i) = sECG.tBumpsRel(i+1,sECG.indType.R) - sECG.tBumpsRel(i,sECG.indType.R);
end

%% Medias y desviaciones estandar de los tiempos
% Medias
sOxim.mean.PATp = nanmean(sOxim.times.PATp);
sOxim.mean.PATf = nanmean(sOxim.times.PATf);
sOxim.mean.PATs = nanmean(sOxim.times.PATs);
sOxim.mean.WT = nanmean(sOxim.times.WT);
sOxim.mean.t1 = nanmean(sOxim.times.t1);
sOxim.mean.t2 = nanmean(sOxim.times.t2);
sOxim.mean.PATfWT = nanmean(sOxim.times.PATfWT);
sOxim.mean.invHR = nanmean(sOxim.times.invHR);

% Desviaciones
sOxim.std.PATp = nanstd(sOxim.times.PATp);
sOxim.std.PATf = nanstd(sOxim.times.PATf);
sOxim.std.PATs = nanstd(sOxim.times.PATs);
sOxim.std.WT = nanstd(sOxim.times.WT);
sOxim.std.t1 = nanstd(sOxim.times.t1);
sOxim.std.t2 = nanstd(sOxim.times.t2);
sOxim.std.PATfWT = nanstd(sOxim.times.PATfWT);
sOxim.std.invHR = nanstd(sOxim.times.invHR);

% Intervalos superior e inferior para muestras validas
sOxim.validInterval.PATp = [0.25*sOxim.mean.PATp 1.75*sOxim.mean.PATp];
sOxim.validInterval.PATf = [0.25*sOxim.mean.PATf 1.75*sOxim.mean.PATf];
sOxim.validInterval.PATs = [0.25*sOxim.mean.PATs 1.75*sOxim.mean.PATs];
sOxim.validInterval.WT = [0.25*sOxim.mean.WT 1.75*sOxim.mean.WT];
sOxim.validInterval.t1 = [0.25*sOxim.mean.t1 1.75*sOxim.mean.t1];
sOxim.validInterval.t2 = [0.25*sOxim.mean.t2 1.75*sOxim.mean.t2];
sOxim.validInterval.PATfWT = [0.25*sOxim.mean.PATfWT 1.75*sOxim.mean.PATfWT];
sOxim.validInterval.invHR = [0.25*sOxim.mean.invHR 1.75*sOxim.mean.invHR];

%% ECG + Pletismografia
figure('Name','ECG & Pleth','NumberTitle','off');
hold on;
ampECG = 2069;
offECG = 1000;
mSize = 30;

% Plot
% ECG sin filtrar
plot(sECG.timeRel, sECG.ecg*ampECG+offECG, 'c.-');
plot(sECG.tBumpsRel(:,sECG.indType.R), sECG.bumps(:,sECG.indType.R)*ampECG+offECG, 'm*', 'markersize', 20);
% ECG filtrado
plot(sECG.timeRel, sECG.filtEcg, 'b.-');
%plot(sECG.tBumpsRel(:,sECG.indType.R), sECG.filtBumps(:,sECG.indType.R), 'r*', 'markersize', 20);
% PPG sin filtrar
plot(sOxim.timeRel, sOxim.pleth, 'c.-');
% PPG filtrada
plot(sOxim.timeRel, sOxim.filtPleth, 'r.-');
% Puntos de referencia de la PPG
plot(sOxim.points.maximo(:,1), sOxim.points.maximo(:,2), 'b.', 'markersize', mSize);
plot(sOxim.points.pie(:,1), sOxim.points.pie(:,2), '.', 'color', [0.3 0 0.6], 'markersize', mSize);
plot(sOxim.points.maxPend(:,1), sOxim.points.maxPend(:,2), '.', 'color', [0 0.6 0], 'markersize', mSize);
plot(sOxim.points.wt0(:,1), sOxim.points.wt0(:,2), '.', 'color', [1 0 0.6], 'markersize', mSize);
plot(sOxim.points.wt1(:,1), sOxim.points.wt1(:,2), '.', 'color', [1 0.6 0], 'markersize', mSize);
% Presiones sistolica y diastolica
% if (~isempty(sBP.sistolic) && ~isempty(sBP.diastolic))
%     plot(sBP.timeRel, sBP.sistolic, 'm^--', 'markersize', 10);
%     plot(sBP.timeRel, sBP.diastolic, 'cv--', 'markersize', 10);
% end

% Adds
title('ECG & PPG');
xlabel('Tiempo (s)');
legend('ECG sin filtrar', 'onda R sin filtrar', 'ECG filtrado', 'onda R filtrada', 'PPG sin filtrar', 'PPG filtrada', 'm�ximo PPG', 'pie PPG', 'maxPend PPG', 'wt0 PPG', 'wt1 PPG');
hold off;
grid;
