function saveCalibrationLog(sBP, sOxim)

tmp = cell2mat(struct2cell(sOxim.validInterval));

fid = fopen('calibration.log','w','b');
if (fid <= 2)
    disp('Error calibration.log');
else
    fwrite(fid, uint32(1e3*tmp(:,1)), 'uint32');
    fwrite(fid, uint32(1e3*tmp(:,2)), 'uint32');
    fwrite(fid, sBP.bestModel.sisModel, 'uint32');
    fwrite(fid, sBP.bestModel.diaModel, 'uint32');
    fwrite(fid, sBP.bestModel.sisCoef, 'float32');
    fwrite(fid, sBP.bestModel.diaCoef, 'float32');
end
fclose(fid);