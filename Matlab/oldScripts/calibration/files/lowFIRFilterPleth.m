function sOxim = lowFIRFilterPleth(sOxim)

%% Filtro FIR
% Filtro pasa baja de orden 10 y frecuencia de corte 10, retardo orden/2,
% ventana hamming
orden = 10;         %orden del filtro
fCorte = 10;        %frecuencia de corte deseada
T = 0.012;          %periodo de muestreo
fSamp = 1/T;        %frecuencia de muestreo
num = fir1(orden,fCorte/(fSamp/2),'low',hamming(orden+1));
% num = [-3266, 1826, 35484, 118208, 216833, 261829, 216833, 118208,...
% 35484, 1826, -3266] * 1e-6;

% Filtrar la PPG
sOxim.filtPleth = zeros(length(sOxim.pleth),1);
delay = orden/2;
for i = orden+1:length(sOxim.pleth)
    sum = 0;
    for j = 1:orden+1
        sum = sum + num(j)*sOxim.pleth(i-j+1);
    end
    sOxim.filtPleth(i-delay) = sum;
end
