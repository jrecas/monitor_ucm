function [lowECG]=filterLowPass(signal,fs,fc)
  %Filtra la señal signal, cuyo perido de muestreo es fs, a partir de la frecuencia de corte fc

  %fs=250
  %Orden del filtro paso bajo
  oLPF = 32;
  %fcA=40;
  %fcB=0.05;  No queremos un paso banda....
  
  %FPB orden oLPF y fcA Hz para eliminar el ruido de alta frec.
  %La frecuencia de corte del filtro hay que pasarsela normalizada.
  %Toma valores entre 0 y 1 donde 1 corresponde a Fnyquist = fs/2.
  %The normalized gain of the filter at Wn is -6 dB.
  %If Wn is a two-element vector, Wn = [W1 W2], fir1 returns an order N bandpass filter with passband  W1 < W < W2
  %B = fir1(oLPF,[fcB fcA]/(fs/2));
  B = fir1(oLPF,fc/(fs/2));
  %figure;freqz(B,1);
  
  %Coeficientes del filtro de Fran
  %aB=[-81, 141, 336, 235, -371, -1091, -911, 744, 2737, 2535, -1275, -6352, -6996, 1559, 18156, 34847, 41871, 34847, 18156, 1559, -6996, -6352, -1275, 2535, 2737, 744, -911, -1091, -371, 235, 336, 141, -81];
  %figure;freqz(aB/130000,1);
  %B*aB(1)/B(1)-aB
  
  %filtfilt Zero-phase forward and reverse digital IIR filtering.
  lowECG = filtfilt(B,1,signal);
end