function sOxim = lowIIRFilterPleth(sOxim)

%% Filtro IIR
% Filtro Butterworth pasa baja de orden 10 y frecuencia de corte 10,
% retardo depende de la frecuencia de corte y del orden: ~ 8 muestras
orden = 10;         %orden del filtro
fCorte = 10;         %frecuencia de corte deseada
T = 0.012;          %periodo de muestreo
fSamp = 1/T;        %frecuencia de muestreo
[b,a] = butter(orden,fCorte/(fSamp/2),'low');
% b = [8, 79, 354, 944, 1652, 1982, 1652, 944, 354, 79, 8] * 1e-6
% a = [100, -519, 1281, -1956, 2029, -1485, 774, -283, 69, -10, 1] * 1e-2

% Filtrar la PPG
sOxim.filtPleth = zeros(length(sOxim.pleth),1);
delay = 8;
a(1) = 0;
for i = orden+1:length(sOxim.pleth)
    sum = 0;
    for j = 1:orden+1
        sum = sum + b(j)*sOxim.pleth(i-j+1) - a(j)*sOxim.filtPleth(i-j+1);
    end
    sOxim.filtPleth(i) = sum;
end
sOxim.filtPleth(1:end-delay) = sOxim.filtPleth(delay+1:end);

