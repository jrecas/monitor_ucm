function sBP = getDataAndBP(sBP, sOxim)

%% Plot BP in figure ECGPPG
figure('Name','Blood Pressure','NumberTitle','off');
hold on;
plot(sBP.timeRel, sBP.sistolic, 'rs-', 'markersize', 15, 'LineWidth', 2);
plot(sBP.timeRel, sBP.diastolic, 'bo-', 'markersize', 15, 'LineWidth', 2);
hold off;

title('Blood Pressure');
xlabel('Tiempo (s)');
legend('SBP', 'DBP');
hold off;
grid;

%% Mix data and BP
N = 10;     % Numero de muestras a promediar

tmpPATp = nan(N,1);
tmpPATf = nan(N,1);
tmpPATs = nan(N,1);
tmpWT = nan(N,1);
tmpT1 = nan(N,1);
tmpT2 = nan(N,1);
tmpPATfWT = nan(N,1);
tmpInvHR = nan(N,1);

sBP.meanTimes.PATp = nan(length(sBP.sistolic),1);
sBP.meanTimes.PATf = nan(length(sBP.sistolic),1);
sBP.meanTimes.PATs = nan(length(sBP.sistolic),1);
sBP.meanTimes.WT = nan(length(sBP.sistolic),1);
sBP.meanTimes.t1 = nan(length(sBP.sistolic),1);
sBP.meanTimes.t2 = nan(length(sBP.sistolic),1);
sBP.meanTimes.PATfWT = nan(length(sBP.sistolic),1);
sBP.meanTimes.invHR = nan(length(sBP.sistolic),1);

for i = 1:length(sBP.sistolic)
    indexBP = find(sOxim.points.maximo(:,1) < sBP.timeRel(i));
    if (length(indexBP) >= N)
        indexBP = indexBP(end+1-N:end);
    else break
    end
    
    tmpPATp = sOxim.times.PATp(indexBP);
    tmpPATf = sOxim.times.PATf(indexBP);
    tmpPATs = sOxim.times.PATs(indexBP);
    tmpWT = sOxim.times.WT(indexBP);
    tmpT1 = sOxim.times.t1(indexBP);
    tmpT2 = sOxim.times.t2(indexBP);
    tmpPATfWT = sOxim.times.PATfWT(indexBP);
    tmpInvHR = sOxim.times.invHR(indexBP);

    tmpPATp = tmpPATp.*(tmpPATp >= sOxim.validInterval.PATp(1)...
        & tmpPATp <= sOxim.validInterval.PATp(2));
    tmpPATf = tmpPATf.*(tmpPATf >= sOxim.validInterval.PATf(1)...
        & tmpPATf <= sOxim.validInterval.PATf(2));
    tmpPATs = tmpPATs.*(tmpPATs >= sOxim.validInterval.PATs(1)...
        & tmpPATs <= sOxim.validInterval.PATs(2));
    tmpWT = tmpWT.*(tmpWT >= sOxim.validInterval.WT(1)...
        & tmpWT <= sOxim.validInterval.WT(2));
    tmpT1 = tmpT1.*(tmpT1 >= sOxim.validInterval.t1(1)...
        & tmpT1 <= sOxim.validInterval.t1(2));
    tmpT2 = tmpT2.*(tmpT2 >= sOxim.validInterval.t2(1)...
        & tmpT2 <= sOxim.validInterval.t2(2));
    tmpPATfWT = tmpPATfWT.*(tmpPATfWT >= sOxim.validInterval.PATfWT(1)...
        & tmpPATfWT <= sOxim.validInterval.PATfWT(2));    
    tmpInvHR = tmpInvHR.*(tmpInvHR >= sOxim.validInterval.invHR(1)...
        & tmpInvHR <= sOxim.validInterval.invHR(2));

    sBP.meanTimes.PATp(i) = nanmean(tmpPATp);
    sBP.meanTimes.PATf(i) = nanmean(tmpPATf);
    sBP.meanTimes.PATs(i) = nanmean(tmpPATs);
    sBP.meanTimes.WT(i) = nanmean(tmpWT);
    sBP.meanTimes.t1(i) = nanmean(tmpT1);
    sBP.meanTimes.t2(i) = nanmean(tmpT2);
    sBP.meanTimes.PATfWT(i) = nanmean(tmpPATfWT);    
    sBP.meanTimes.invHR(i) = nanmean(tmpInvHR);
    
end