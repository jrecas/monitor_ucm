function sBP = getBP(dateReg)

file = strcat(dateReg,'\logCalibPressure.txt');
tmpBP = textread(file,'%s','whitespace','\t','endofline','\n');

tmpBP = reshape(tmpBP, 3, length(tmpBP)/3);

initTime = datevec(dateReg, 'yyyy.mm.dd-HH.MM.SS');
sBP.timeAbs = datenum(tmpBP(1,:), 'yyyy.mm.dd-HH.MM.SS');
sBP.sistolic = str2double(tmpBP(2,:))';
sBP.diastolic = str2double(tmpBP(3,:))';

timeBPTmp = datevec(tmpBP(1,:), 'yyyy.mm.dd-HH.MM.SS');
for i = 1:length(sBP.timeAbs)
    sBP.timeRel(i,1) = etime(timeBPTmp(i,:), initTime);
end