function [ecg] = delineationECG(rawECG, fSample, plotEnable)
%
% Es igual que delineationECG, pero modificando el pico R
%
% index = ((ecg.filt_14.diff2 < RPeakTh)...
% index = ((-abs(ecg.filt_14.diff2deli) < RPeakTh)...
%
%
% Y añadiendo un control para el on y el end de ondas negativas P y T (que
% rara vez se dan). Simplemente vemos que esta mas abajo del pico, si el
% openP/openT o el baseline. Habitualemnte sera openP/openT para ondas
% positivas, pero si la onda P o T es negativa y el latido se ha anotado
% como negativo, lo que hay en delineationECG falla. Esta mejora soluciona
% ese caso, que son muy raros de todas formas.
%
% Ppeak_val = ecg.filt_40.value(ecg.bumps(i,ecg.indType.P));
% Pbase_val = ecg.filt_40.openP(ecg.bumps(i,ecg.indType.P));
% if ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.P)) < ecg.filt_40.openP(ecg.bumps(i,ecg.indType.P))
%     Pbase_val = ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.P));
% end
% tolerance = 0.025*abs(Ppeak_val-Pbase_val);
% negativePWave = false;
% if Ppeak_val < Pbase_val
%     negativePWave = true;
% end
%

% Flags para decidir los puntos a delinear
del_QRS = true;
del_Ppeak = true;
del_Pbase = true;
del_Tpeak = true;
del_Tbase = true;

ecg = struct;
ecg.indType.Pon     = 1;
ecg.indType.P       = 2;
ecg.indType.Pend    = 3;
ecg.indType.Qon     = 4;
ecg.indType.Q       = 5;
ecg.indType.Ron     = 6;
ecg.indType.R       = 7;
ecg.indType.Rend    = 8;
ecg.indType.S       = 9;
ecg.indType.Send    = 10;
ecg.indType.Ton     = 11;
ecg.indType.T       = 12;
ecg.indType.Tend    = 13;

if length(rawECG(1,:)) ~= 1
    rawECG = rawECG';
end
ecg.raw.value = rawECG;
ecg.raw.position = (1:length(ecg.raw.value))';


%% Filtrados FIR
% Filtrado FIR paso bajo de frecuencia de corte 40 Hz. Limpia la señal de
% ruido y se utilizara para hallar el inicio y el final de las ondas de
% ECG.
% Si la frecuencia de muestreo es menor del doble de la frecuencia de
% corte, no se puede filtrar, así que no activamos el filtro.
order = 40;
fCut = 40;
if fSample > 2*fCut
    ecg.filt_40.value = FIRFilter(ecg.raw.value, order, fCut, fSample, false);
    ecg.filt_40.value = ecg.filt_40.value((1 + order/2) : end);
else
    ecg.filt_40.value = ecg.raw.value;
end
ecg.filt_40.position = (1:length(ecg.filt_40.value))';

% Filtrado FIR paso bajo de frecuencia de corte 14 Hz. Filtrado agresivo
% para hallar los picos de las ondas de ECG.

%%%%%%%%%% ESTO NO ESTA EN LA DELINEACIÓN ORIGINAL %%%%%%%%%%
% windowSize = round((40/1000) * fSample); % 40 ms de ventana para la media
% b = ones(1, windowSize) ./ windowSize;
% a = 1;
% tmpEcg = filter(b, a, ecg.raw.value);
% tmpEcg = tmpEcg((1 + windowSize/2) : end);
% order = 40;
% fCut = 14;
% if fSample > 2*fCut
%     ecg.filt_14.value = FIRFilter(tmpEcg, order, fCut, fSample, false);
%     ecg.filt_14.value = ecg.filt_14.value((1 + order/2) : end);
% else
%     ecg.filt_14.value = ecg.raw.value;
% end
%%%%%%%%%%%%%%%%%%%%%%% ORIGINAL %%%%%%%%%%%%%%%%%%%%%%%%%%%%
fCut = 14;
if fSample > 2*fCut
    ecg.filt_14.value = FIRFilter(ecg.raw.value, order, fCut, fSample, false);
    ecg.filt_14.value = ecg.filt_14.value((1 + order/2) : end);
else
    ecg.filt_14.value = ecg.raw.value;
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

ecg.filt_14.position = (1:length(ecg.filt_14.value))';
ecg.filt_40.position = (1:length(ecg.filt_14.value))';


%% Derivadas primera y segunda para el filtrado agresivo
% Primera y segunda de ecg.filt_14 con la misma longitud que ecg.filt_14.
ecg.filt_14.diff1 = [nan(1,1); diff(ecg.filt_14.value,1)];
ecg.filt_14.diff2 = [nan(1,1); diff(ecg.filt_14.value,2); nan(1,1)];

%%%%%%%%%% ESTO NO ESTA EN LA DELINEACIÓN ORIGINAL %%%%%%%%%%
% Filtrados para la 1a y 2a derivada
% windowSize = round((40/1000) * fSample); % 40 ms de ventana
% b = ones(1, windowSize) ./ windowSize;
% a = 1;
% ecg.filt_14.diff1 = filter(b, a, ecg.filt_14.diff1);
% ecg.filt_14.diff1 = ecg.filt_14.diff1(1 + floor(windowSize/2) : end); % Eliminar el delay
% ecg.filt_14.diff1 = [ecg.filt_14.diff1; ones(round(windowSize/2), 1)];
% ecg.filt_14.diff2 = filter(b, a, ecg.filt_14.diff2);
% ecg.filt_14.diff2 = ecg.filt_14.diff2(1 + floor(windowSize/2) : end); % Eliminar el delay
% ecg.filt_14.diff2 = [ecg.filt_14.diff2; ones(round(windowSize/2), 1)];
%%%%%%%%%% ESTO NO ESTA EN LA DELINEACIÓN ORIGINAL %%%%%%%%%%

% Vector cuyos elementos son "true" si hay cambio de signo entre elementos
% contiguos en la primera derivada de ecg.filt_14 y "false" en caso
% contrario.
ecg.filt_14.diff1ChangeSign = false(length(ecg.filt_14.diff1(:,1)),1);
temp = [ecg.filt_14.diff1(2:end) .* ecg.filt_14.diff1(1:end-1); 0];
for i = 1 : length(temp)
   if (temp(i) < 0)
       ecg.filt_14.diff1ChangeSign(i) = true;
   end
end


%% Baseline wander y openings para onda P y T
% Baseline general basado en filtro morfologico ("ECG signal
% conditioning by morphological filtering", Sun, Chan, Krishnan).
if del_QRS
    windowOpen = zeros(round(0.2 * fSample), 1);
    windowClose = zeros(round(0.3 * fSample), 1);
    openEcg = opening(ecg.filt_40.value, windowOpen, windowOpen);
    ecg.filt_40.opening = openEcg;
    ecg.filt_40.baseline = closing(openEcg, windowClose, windowClose);
end

% El inicio y el final de las ondas P y T puede ser hallado con la
% misma tecnica del filtro morfologico con modificaciones en los
% coeficientes. Solamente se utilizara el opening para poder eliminar
%los picos P y T.

% La duracion de la onda P es menor de 120 ms (“The signal-averaged P
% wave duration: a rapid and noninvasive marker of risk of atrial
% fibrillation”, Guidera, Steinberg) por lo que el coeficiente del
% opening es 0.12.
if del_Pbase
    windowOpenP = zeros(round(0.12 * fSample), 1);
    ecg.filt_40.openP = opening(ecg.filt_40.value, windowOpenP, windowOpenP);
end

% La duracion de la onda T se situa entre 100 ms y 250 ms ("Noninvasive
% Instrumentation and Measurement in Medical Diagnosis", Northrop) por
% lo que el coeficiente del opening es 0.25.
if del_Tbase
    windowOpenT = zeros(round(0.25 * fSample), 1);
    ecg.filt_40.openT = opening(ecg.filt_40.value, windowOpenT, windowOpenT);
end


%% Umbral para deteccion de pico R
% Para calcular el umbral de deteccion de pico R se usa el minimo de la
% segunda derivada de ecg.filt_14 en una ventana de 2 segundos de duracion
% si la onda R es positiva y el maximo si es negativa (es adaptativo).
sampleWindow = round(2 * fSample);

% En cada ventana se utilizara como umbral el minimo de la segunda derivada
% o el maximo junto con la media de otras 4 ventanas anteriores para
% minimizar efectos bruscos.

% Aumentar el RFactor que multiplica en el umbral reduce los falsos
% positivos y aumenta los falsos negativos. Disminuirlo aumenta los falsos
% positivos y reduce los falsos negativos. Evaluar la mejor opción para
% cada caso segun interes. Rango aproximado: [0.3 - 0.5].
RFactor = 0.33;
meanRPeakTh = zeros(5,1);
RPeakTh = zeros(length(ecg.filt_14.value),1);
ecg.negativeQRS.position = false(length(ecg.filt_14.value),1);

for pos = 0 : floor(length(ecg.filt_14.value) / sampleWindow) - 1;
    startPos    = pos * sampleWindow + 1;
    endPos      = (pos+1) * sampleWindow;
    minimum     = min(ecg.filt_14.diff2(startPos : endPos));
    maximum     = max(ecg.filt_14.diff2(startPos : endPos));
    
    if abs(minimum) > abs(maximum)
        meanRPeakTh = [RFactor * minimum; meanRPeakTh(1:end-1)];
        tempMean = mean(meanRPeakTh);
        %tempMean = RFactor * minimum;
        for k = startPos : endPos
            RPeakTh(k,1) = tempMean;
        end
        
    else
        meanRPeakTh = [-RFactor * maximum; meanRPeakTh(1:end-1)];
        tempMean = mean(meanRPeakTh);
        %tempMean = -RFactor * maximum;
        for k = startPos : endPos
            RPeakTh(k,1) = tempMean;
            if del_QRS
                ecg.filt_40.value(k,1) = 2 * ecg.filt_40.baseline(k,1) - ecg.filt_40.value(k,1);
                ecg.filt_14.value(k,1) = 2 * ecg.filt_40.baseline(k,1) - ecg.filt_14.value(k,1);
            end
            %ecg.filt_14.diff1(k,1) = -ecg.filt_14.diff1(k,1);   % No es necesario para el algoritmo
            ecg.filt_14.diff2(k,1) = -ecg.filt_14.diff2(k,1);
            if del_Pbase
                ecg.filt_40.openP(k,1) = 2 * ecg.filt_40.baseline(k,1) - ecg.filt_40.openP(k,1);
            end
            if del_Tbase
                ecg.filt_40.openT(k,1) = 2 * ecg.filt_40.baseline(k,1) - ecg.filt_40.openT(k,1);
            end
            ecg.negativeQRS.position(k,1) = true;
        end
        
    end
    
end

RPeakTh = RPeakTh(1:length(ecg.filt_14.value));


%% Delineacion de pico R
% Las condiciones para hallar un oico R son que la segunda derivada de
% ecg.filt_40 sea menor que el umbral RPeakTh y que la primera derivada
% cambie de signo.
index = ((-abs(ecg.filt_14.diff2) < RPeakTh) & (ecg.filt_14.diff1ChangeSign == true) &...
    (ecg.filt_40.position < (sampleWindow * floor(length(ecg.filt_14.value) / sampleWindow))));

% Si encontramos ondas R a una distancia menor de fSample / 4 (equivale a
% mas de 240 bpm), eliminimamos una de ellas.
tmpPos = ecg.filt_40.position(index);
for i = 2 : length(tmpPos)
   if (tmpPos(i) - tmpPos(i-1)) < fSample / 4
       if ecg.filt_14.diff2(tmpPos(i)) < ecg.filt_14.diff2(tmpPos(i-1))
           tmpPos(i-1) = NaN;
       else
           tmpPos(i) = NaN;
       end
   end
end
index = ~isnan(tmpPos);
tmpPos = tmpPos(index);
ecg.bumps = nan(length(tmpPos), ecg.indType.Tend);
ecg.bumps(:,ecg.indType.R) = tmpPos;


%% Delineacion del resto de las ondas
for i = 2 : length(ecg.bumps(:,ecg.indType.R)) - 1
    
    if del_QRS == true
        
        %-------------
        % R-ON Y R-OFF
        %-------------
        % Niveles de tolerancia y offset.
        tolerance = 0.05 * abs(ecg.filt_40.value(ecg.bumps(i,ecg.indType.R)) -...
            ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.R)));
        offset = ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.R));

        % Recorremos a la izquierda del pico R hasta llegar al 50% de la
        % amplitud y despues se busca el 5% de la amplitud siempre que la
        % funcion sea estrictamente creciente.
        if ecg.bumps(i,ecg.indType.R) > 1
            tempPosOn = ecg.bumps(i,ecg.indType.R) - 1;
        else
            tempPosOn = ecg.bumps(i,ecg.indType.R);  % Evitar indice < 1
        end
        while (ecg.filt_40.value(tempPosOn) > (offset + 10 * tolerance))
            tempPosOn = tempPosOn - 1;
            if tempPosOn <= 1
                break;  % Evitar salirse del array
            end
        end
        while (tempPosOn > 1) &&...
                (ecg.filt_40.value(tempPosOn) > (offset + tolerance)) &&...
                (ecg.filt_40.value(tempPosOn) > ecg.filt_40.value(tempPosOn - 1))
            tempPosOn = tempPosOn - 1;
            if tempPosOn <= 1
                break;  % Evitar salirse del array
            end
        end
        ecg.bumps(i,ecg.indType.Ron) = tempPosOn;

        % Recorremos a la derecha del pico R hasta llegar al 50% de la
        % amplitud y despues se busca el 5% de la amplitud siempre que la
        % funcion sea estrictamente decreciente.
        if ecg.bumps(i,ecg.indType.R) < length(ecg.filt_40.value)
            tempPosEnd = ecg.bumps(i,ecg.indType.R) + 1;
        else
            tempPosEnd = ecg.bumps(i,ecg.indType.R);  % Evitar indice > length(ecg)
        end
        while (ecg.filt_40.value(tempPosEnd) > (offset + 10 * tolerance))
            tempPosEnd = tempPosEnd + 1;
            if tempPosEnd >= length(ecg.filt_40.value)
                break;  % Evitar salirse del array
            end
        end
        while (tempPosEnd < length(ecg.filt_40.value)) &&...
                (ecg.filt_40.value(tempPosEnd) > (offset + tolerance)) &&...
                (ecg.filt_40.value(tempPosEnd) > ecg.filt_40.value(tempPosEnd + 1))
            tempPosEnd = tempPosEnd + 1;
            if tempPosEnd >= length(ecg.filt_40.value)
                break;  % Evitar salirse del array
            end
        end
        ecg.bumps(i,ecg.indType.Rend) = tempPosEnd;

        %--------------
        % ONDA Q Y Q-ON
        %--------------
        tempPosEnd = ecg.bumps(i,ecg.indType.Ron);
        tempPosOn = tempPosEnd;
        % Hallar el minimo mas cercano.
        while (tempPosOn > 1) &&...
                (ecg.filt_40.value(tempPosOn) >= ecg.filt_40.value(tempPosOn - 1))
            tempPosOn = tempPosOn - 1;
            if tempPosOn <= 1
                break;  % Evitar salirse del array
            end
        end
        tempPosMin = tempPosOn;
        % Hallar el cruce con el baseline siempre que la funcion sea
        % estrictamente decreciente.
        if tempPosOn > 1
            tempPosOn = tempPosOn - 1;  % Evitar indice < 1
        end
        while (tempPosOn > 1) &&...
                (ecg.filt_40.value(tempPosOn) < (offset - tolerance)) &&...
                (ecg.filt_40.value(tempPosOn) < ecg.filt_40.value(tempPosOn - 1))
            tempPosOn = tempPosOn - 1;
            if tempPosOn <= 1
                break;  % Evitar salirse del array
            end
        end
        % Si la duracion de la onda Q es menor de 100 ms y la amplitud de
        % la onda Q es mayor que la tolerancia definida previamente, se
        % considerara una onda Q valida. Si no, el inicio de la onda Q
        % coincidira con el inicio de la onda R.
        if ((tempPosEnd - tempPosOn) < (100 * fSample / 1000)) &&...
                ((tempPosEnd - tempPosOn) > 0) &&...
                (offset - ecg.filt_40.value(tempPosMin)) > tolerance;
            ecg.bumps(i,ecg.indType.Qon) = tempPosOn;
            ecg.bumps(i,ecg.indType.Q) = tempPosMin;
            % Pequeña correccion para el inicio (20 ms)
            for j = tempPosOn : -1 : (tempPosOn - round(20 * fSample / 1000));
                if j < 2
                    break
                end
                if ecg.filt_40.value(j) > ecg.filt_40.value(j - 1)
                    ecg.bumps(i,ecg.indType.Qon) = j;
                    break;
                end
            end
        else
            ecg.bumps(i,ecg.indType.Qon) = ecg.bumps(i,ecg.indType.Ron);
        end

        %---------------
        % ONDA S Y S-OFF
        %---------------
        tempPosOn = ecg.bumps(i,ecg.indType.Rend);
        tempPosEnd = tempPosOn;
        % Hallar el minimo mas cercano.
        while (tempPosEnd < length(ecg.filt_40.value)) &&...
                (ecg.filt_40.value(tempPosEnd) >= ecg.filt_40.value(tempPosEnd + 1))
            tempPosEnd = tempPosEnd + 1;
            if tempPosEnd >= length(ecg.filt_40.value)
                break;  % Evitar salirse del array
            end
        end
        tempPosMin = tempPosEnd;
        % Hallar el cruce con el baseline siempre que la funcion sea
        % estrictamente creciente.
        if tempPosEnd < length(ecg.filt_40.value)
            tempPosEnd = tempPosEnd + 1;
        end
        while (tempPosEnd < length(ecg.filt_40.value)) &&...
                (ecg.filt_40.value(tempPosEnd) < (offset - tolerance)) &&...
                (ecg.filt_40.value(tempPosEnd) < ecg.filt_40.value(tempPosEnd + 1))
            tempPosEnd = tempPosEnd + 1;
            if tempPosEnd >= length(ecg.filt_40.value)
                break;  % Evitar salirse del array
            end
        end
        % Si la duracion de la onda S es menor de 100 ms y la amplitud de
        % la onda S es mayor que la tolerancia definida previamente, se
        % considerara una onda S valida. Si no, el final de la onda S
        % coincidira con el final de la onda R.
        if ((tempPosEnd - tempPosOn) < (100 * fSample / 1000)) &&...
                ((tempPosEnd - tempPosOn) > 0) &&...
                (offset - ecg.filt_40.value(tempPosMin)) > tolerance;
            ecg.bumps(i,ecg.indType.S) = tempPosMin;
            ecg.bumps(i,ecg.indType.Send) = tempPosEnd;
            % Pequeña correccion para el final (20 ms)
            for j = tempPosEnd : (tempPosEnd + round(20 * fSample / 1000));
                if j < length(ecg.filt_40.value)
                    break
                end
                if ecg.filt_40.value(j) > ecg.filt_40.value(j + 1)
                    ecg.bumps(i,ecg.indType.Send) = j;
                    break;
                end
            end
        else
            ecg.bumps(i,ecg.indType.Send) = ecg.bumps(i,ecg.indType.Rend);
        end
    end
    
    if del_Ppeak == true
        %--------
        % ONDA P
        %--------
        % La onda P se busca en una ventana entre 100 ms y 200 ms anterior
        % a un pico R y en la segunda mitad del intervalo entre el pico R
        % actual y el anterior. El pico de la onda P se halla con el minimo
        % de la segunda derivada de ecg.filt_14 si la onda es positiva y
        % con el maximo si es negativa.
        negativePWave = false;
        PFactor = 0.01;
        cond1 = ecg.bumps(i,ecg.indType.R) - (0.1 * fSample);
        tmpCond2a = ecg.bumps(i,ecg.indType.R) - (0.2 * fSample);
        tmpCond2b = 1 + floor((ecg.bumps(i,ecg.indType.R) + ecg.bumps(i-1,ecg.indType.R)) / 2);
        if tmpCond2a > tmpCond2b
            cond2 = tmpCond2a;
        else
            cond2 = tmpCond2b;
        end
        index = (ecg.filt_40.position < cond1) & (ecg.filt_40.position > cond2);
        tmpDif = ecg.filt_14.diff2(index);
        tmpPos = ecg.filt_40.position(index);
        if ~isempty(tmpDif)
            [minimum, posMin] = min(tmpDif);
            [maximum, posMax] = max(tmpDif);
            if abs(minimum) > abs(maximum)
                if abs(minimum) > abs(PFactor * (RPeakTh(ecg.bumps(i,ecg.indType.R)) ./ RFactor))
                    ecg.bumps(i,ecg.indType.P) = tmpPos(posMin);
                else
                    ecg.bumps(i,ecg.indType.P) = NaN;
                end
            else
                if abs(maximum) > abs(PFactor * (RPeakTh(ecg.bumps(i,ecg.indType.R)) ./ RFactor))
                    ecg.bumps(i,ecg.indType.P) = tmpPos(posMax);
                    negativePWave = true;
                else
                    ecg.bumps(i,ecg.indType.P) = NaN;
                end
            end
        else
            ecg.bumps(i,ecg.indType.P) = NaN;
        end
    end
    
    if del_Ppeak && del_Pbase
        %-------------
        % P-ON y P-END
        %-------------
        % Buscar el corte a iquierda y derecha del pico P para ecg.filt_40
        % con ecg.filt_40.openP, estableciendo una tolerancia de 2.5% de la
        % amplitud del pico P.
        if ~isnan(ecg.bumps(i,ecg.indType.P))

            Ppeak_val = ecg.filt_40.value(ecg.bumps(i,ecg.indType.P));
            Pbase_val = ecg.filt_40.openP(ecg.bumps(i,ecg.indType.P));            
            if ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.P)) < ecg.filt_40.openP(ecg.bumps(i,ecg.indType.P))
                Pbase_val = ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.P));
            end
            tolerance = 0.025*abs(Ppeak_val-Pbase_val);
            negativePWave = false;
            if Ppeak_val < Pbase_val
                negativePWave = true;
            end
            
            if negativePWave == false
                threshold_500 = Pbase_val + 20*tolerance;
                threshold_25  = Pbase_val + tolerance;
                % P-on
                tempPosOn = ecg.bumps(i,ecg.indType.P) - 1;
                while (ecg.filt_40.value(tempPosOn) > threshold_25) &&...
                        (ecg.filt_40.value(tempPosOn) > ecg.filt_40.value(tempPosOn - 1) ||...
                        (ecg.filt_40.value(tempPosOn) > threshold_500))
                    tempPosOn = tempPosOn - 1;
                    if tempPosOn <= 1
                        break;  % Evitar salirse del array
                    end
                end
                % P-end
                tempPosEnd = ecg.bumps(i,ecg.indType.P) + 1;
                while (ecg.filt_40.value(tempPosEnd) > threshold_25) &&...
                        (ecg.filt_40.value(tempPosEnd) > ecg.filt_40.value(tempPosEnd + 1) ||...
                        (ecg.filt_40.value(tempPosEnd) > threshold_500))
                    tempPosEnd = tempPosEnd + 1;
                    if tempPosEnd >= length(ecg.filt_40.value)
                        break;  % Evitar salirse del array
                    end
                end
            else
                threshold_500 = Pbase_val + 20*tolerance;
                threshold_25  = Pbase_val + tolerance;
                % P-on
                tempPosOn = ecg.bumps(i,ecg.indType.P) - 1;
                while (ecg.filt_40.value(tempPosOn) < threshold_25) &&...
                        (ecg.filt_40.value(tempPosOn) < ecg.filt_40.value(tempPosOn - 1) ||...
                        (ecg.filt_40.value(tempPosOn) < threshold_500))
                    tempPosOn = tempPosOn - 1;
                    if tempPosOn <= 1
                        break;  % Evitar salirse del array
                    end
                end
                % P-end
                tempPosEnd = ecg.bumps(i,ecg.indType.P) + 1;
                while (ecg.filt_40.value(tempPosEnd) < threshold_25) &&...
                        (ecg.filt_40.value(tempPosEnd) < ecg.filt_40.value(tempPosEnd + 1) ||...
                        (ecg.filt_40.value(tempPosEnd) < threshold_500))
                    tempPosEnd = tempPosEnd + 1;
                    if tempPosEnd >= length(ecg.filt_40.value)
                        break  % Evitar salirse del array
                    end
                end
            end

            if tempPosOn > ecg.bumps(i-1,ecg.indType.R)
                ecg.bumps(i,ecg.indType.Pon) = tempPosOn;
            else
                ecg.bumps(i,ecg.indType.Pon) = NaN;
            end

            if tempPosEnd < ecg.bumps(i,ecg.indType.R)
                ecg.bumps(i,ecg.indType.Pend) = tempPosEnd;
            else
                ecg.bumps(i,ecg.indType.Pend) = NaN;
            end

        else
            ecg.bumps(i,ecg.indType.Pon) = NaN;
            ecg.bumps(i,ecg.indType.Pend) = NaN;

        end
    end
    
    if del_Tpeak
        %--------
        % ONDA T
        %--------
        % La onda T se busca en una ventana entre 200 ms y 400 ms posterior a
        % un pico R y en la primera mitad del intervalo entre el pico R actual
        % y el posterior. El pico de la onda T se halla con el minimo de la
        % segunda derivada de ecg.filt_14 si la onda es positiva y con el
        % maximo si es negativa.
        negativeTWave = false;
        TFactor = 0.01;
        cond1 = ecg.bumps(i,ecg.indType.R) + (0.2 * fSample);
        tmpCond2a = ecg.bumps(i,ecg.indType.R) + (0.4 * fSample);
        tmpCond2b = 1 + floor((ecg.bumps(i+1,ecg.indType.R) + ecg.bumps(i,ecg.indType.R)) / 2);
        if tmpCond2a < tmpCond2b
            cond2 = tmpCond2a;
        else
            cond2 = tmpCond2b;
        end
        index = (ecg.filt_40.position > cond1) & (ecg.filt_40.position < cond2);
        tmpDif = ecg.filt_14.diff2(index);
        tmpPos = ecg.filt_40.position(index);
        if ~isempty(tmpDif)
            [minimum, posMin] = min(tmpDif);
            [maximum, posMax] = max(tmpDif);
            if abs(minimum) > abs(maximum)
                if abs(minimum) > abs(TFactor * (RPeakTh(ecg.bumps(i,ecg.indType.R)) ./ RFactor))
                    ecg.bumps(i,ecg.indType.T) = tmpPos(posMin);
                else
                    ecg.bumps(i,ecg.indType.T) = NaN;
                end
            else
                if abs(maximum) > abs(TFactor * (RPeakTh(ecg.bumps(i,ecg.indType.R)) ./ RFactor))
                    ecg.bumps(i,ecg.indType.T) = tmpPos(posMax);
                    negativeTWave = true;
                else
                    ecg.bumps(i,ecg.indType.T) = NaN;
                end
            end
        else
            ecg.bumps(i,ecg.indType.T) = NaN;
        end
    end
    
    if del_Tpeak && del_Tbase
        %-------------
        % T-ON y T-END
        %-------------
        % Buscar el corte a iquierda y derecha del pico T para ecg.filt_40
        % con ecg.filt_40.openT, estableciendo una tolerancia de 2.5% de la
        % amplitud del pico T.
        if ~isnan(ecg.bumps(i,ecg.indType.T))
            
            Tpeak_val = ecg.filt_40.value(ecg.bumps(i,ecg.indType.T));
            Tbase_val = ecg.filt_40.openT(ecg.bumps(i,ecg.indType.T));
            if ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.T)) < ecg.filt_40.openP(ecg.bumps(i,ecg.indType.T))
                Tbase_val = ecg.filt_40.baseline(ecg.bumps(i,ecg.indType.T));
            end
            tolerance = 0.025*abs(Tpeak_val-Tbase_val);
            negativeTWave = false;
            if Tpeak_val < Tbase_val
                negativeTWave = true;
            end

            if negativeTWave == false
                threshold_500 = Tbase_val + 20*tolerance;
                threshold_25  = Tbase_val + tolerance;
                % T-on
                tempPosOn = ecg.bumps(i,ecg.indType.T) - 1;
                while (ecg.filt_40.value(tempPosOn) > threshold_25) &&...
                        (ecg.filt_40.value(tempPosOn) > ecg.filt_40.value(tempPosOn - 1) ||...
                        (ecg.filt_40.value(tempPosOn) > threshold_500))
                    tempPosOn = tempPosOn - 1;
                    if tempPosOn <= 1
                        break;  % Evitar salirse del array
                    end
                end
                % T-end
                tempPosEnd = ecg.bumps(i,ecg.indType.T) + 1;
                while (ecg.filt_40.value(tempPosEnd) > threshold_25) &&...
                        (ecg.filt_40.value(tempPosEnd) > ecg.filt_40.value(tempPosEnd + 1) ||...
                        (ecg.filt_40.value(tempPosEnd) > threshold_500))
                    tempPosEnd = tempPosEnd + 1;
                    if tempPosEnd >= length(ecg.filt_40.value)
                        break;  % Evitar salirse del array
                    end
                end
            else
                threshold_500 = Tbase_val - 20*tolerance;
                threshold_25  = Tbase_val - tolerance;
                % T-on
                tempPosOn = ecg.bumps(i,ecg.indType.T) - 1;
                while (ecg.filt_40.value(tempPosOn) < threshold_25) &&...
                        (ecg.filt_40.value(tempPosOn) < ecg.filt_40.value(tempPosOn - 1) ||...
                        (ecg.filt_40.value(tempPosOn) < threshold_500))
                    tempPosOn = tempPosOn - 1;
                    if tempPosOn <= 1
                        break;  % Evitar salirse del array
                    end
                end
                % T-end
                tempPosEnd = ecg.bumps(i,ecg.indType.T) + 1;
                while (ecg.filt_40.value(tempPosEnd) < threshold_25) &&...
                        (ecg.filt_40.value(tempPosEnd) < ecg.filt_40.value(tempPosEnd + 1) ||...
                        (ecg.filt_40.value(tempPosEnd) < threshold_500))
                    tempPosEnd = tempPosEnd + 1;
                    if tempPosEnd >= length(ecg.filt_40.value)
                        break  % Evitar salirse del array
                    end
                end
            end

            if tempPosOn > ecg.bumps(i,ecg.indType.R)
                ecg.bumps(i,ecg.indType.Ton) = tempPosOn;
            else
                ecg.bumps(i,ecg.indType.Ton) = NaN;
            end

            if tempPosEnd < ecg.bumps(i+1,ecg.indType.R)
                ecg.bumps(i,ecg.indType.Tend) = tempPosEnd;
            else
                ecg.bumps(i,ecg.indType.Tend) = NaN;
            end

        else
            ecg.bumps(i,ecg.indType.Ton) = NaN;
            ecg.bumps(i,ecg.indType.Tend) = NaN;

        end

    end
end

%% Pulso
ecg.bpm = 60 * fSample ./ (ecg.bumps(2:end,ecg.indType.R) - ecg.bumps(1:end-1,ecg.indType.R));
ecg.bpm = [ecg.bpm; nan];  % Añadimos un valor para tener la misma longitud

%% Intervalos de 2 segundos detectados como negativos
negativeEcgPos = ecg.filt_40.position(ecg.negativeQRS.position);
negativeEcgVal = ecg.filt_40.value(negativeEcgPos);

%% Plot
tmpPos = ecg.bumps;
index = isnan(ecg.bumps);
tmpPos(index) = 1;

if plotEnable == true
    figure;
    hold on,
    grid;
    plot(ecg.raw.value, 'k');
    plot(ecg.filt_40.value, 'b-');
%    if ~isempty(negativeEcgPos)
%        plot(negativeEcgPos, negativeEcgVal, 'g--', 'linewidth', 2);
%    end
%     plot(ecg.filt_14.value, 'k.-', 'linewidth', 2);
%     if del_QRS
%         plot(ecg.filt_40.baseline, 'r');
%     end
%     if del_Pbase
%         plot(ecg.filt_40.openP, 'g');
%     end
%     if del_Tbase
%         plot(ecg.filt_40.openT, 'm');
%     end
%     plot(10 * ecg.filt_14.diff1 + mean(rawECG), 'y.-', 'linewidth', 2);
%     plot(30 * ecg.filt_14.diff2 + mean(rawECG), 'c.-', 'linewidth', 2);
%     plot(RPeakTh, 'm', 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.Pon),      ecg.raw.value(tmpPos(:,ecg.indType.Pon)),    'm.', 'markersize', 20, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.P),        ecg.raw.value(tmpPos(:,ecg.indType.P)),      'mo', 'markersize' ,10, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.Pend),     ecg.raw.value(tmpPos(:,ecg.indType.Pend)),   'm.', 'markersize', 20, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.Qon),      ecg.raw.value(tmpPos(:,ecg.indType.Qon)),    'y.', 'markersize', 20, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.Q),        ecg.raw.value(tmpPos(:,ecg.indType.Q)),      'yo', 'markersize' ,10, 'linewidth', 2);
%    plot(tmpPos(:,ecg.indType.Ron),      ecg.raw.value(tmpPos(:,ecg.indType.Ron)),    'r.', 'markersize', 20, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.R),        ecg.raw.value(tmpPos(:,ecg.indType.R)),      'ro', 'markersize' ,10, 'linewidth', 2);
%    plot(tmpPos(:,ecg.indType.Rend),     ecg.raw.value(tmpPos(:,ecg.indType.Rend)),   'r.', 'markersize', 20, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.S),        ecg.raw.value(tmpPos(:,ecg.indType.S)),      'co', 'markersize' ,10, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.Send),     ecg.raw.value(tmpPos(:,ecg.indType.Send)),   'c.', 'markersize', 20, 'linewidth', 2);
%    plot(tmpPos(:,ecg.indType.Ton),      ecg.raw.value(tmpPos(:,ecg.indType.Ton)),    'g.', 'markersize', 20, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.T),        ecg.raw.value(tmpPos(:,ecg.indType.T)),      'go', 'markersize', 10, 'linewidth', 2);
    plot(tmpPos(:,ecg.indType.Tend),     ecg.raw.value(tmpPos(:,ecg.indType.Tend)),   'g.', 'markersize', 20, 'linewidth', 2);
%     if ~isempty(negativeEcgPos)
%         legend('raw ECG', 'filt ECG (f_c = 40 Hz)', 'Negative QRS', 'P-on', 'P-peak', 'P-end', 'Q-on', 'Q-peak',...
%             'R-on', 'R-peak', 'R-end', 'S-peak', 'S-end', 'T-on', 'T-peak', 'T-end');
%     else
%         legend('raw ECG', 'filt ECG (f_c = 40 Hz)', 'P-on', 'P-peak', 'P-end', 'Q-on', 'Q-peak',...
%             'R-on', 'R-peak', 'R-end', 'S-peak', 'S-end', 'T-on', 'T-peak', 'T-end');
%     end
    legend('raw ECG', 'filt ECG (f_c = 40 Hz)', 'P-on', 'P-peak', 'P-end', 'Q-on', 'Q-peak',...
        'R-peak', 'S-peak', 'S-end', 'T-peak', 'T-end');
    hold off;
end

end
