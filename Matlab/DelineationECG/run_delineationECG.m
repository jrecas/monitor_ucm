% Path and the register
regExp = './databaseExample/sel30m.mat';
files = dir(regExp);

if ~isempty(regexp(files.name,'.mat','once'))
    % Load
    load(regExp);
    % Input parameters
    fSample = 250;      % Sampling rate
    plotEnable = true;  % Plot?
    rawECG = val(1,:);  % Register 1 or register 2 (physionet)
    % Delineate
    ecg1 = delineationECG(val(1,:), fSample, plotEnable);
end
