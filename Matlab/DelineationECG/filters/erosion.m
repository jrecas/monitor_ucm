function [filtData] = erosion(data, window)
%
% Implementa la erosion de un filtro baseline.
%
% Entrada
%   data        Señal de entrada
%   window      Ventana
%
% Salida
%   filtData    Señal filtrada
%
% Autor: Jose Manuel Bote
%

if mod(length(window), 2) ~= 1
    window = window(1:end-1);
end

winSize = length(window);

N = length(data);
M = winSize;
halfM = (M - 1) / 2;

filtData = nan(N,1);

start_i = halfM+1;
end_i   = N-halfM;

if abs(sum(window)) == 0
    for i = start_i : end_i
        filtData(i) = min(data(i-halfM : i+halfM));
    end
else
    for i = start_i : end_i
        filtData(i) = min(data(i-halfM : i+halfM) - window);
    end
end

if length(filtData(:,1)) ~= length(data(:,1))
    filtData = filtData';

end
