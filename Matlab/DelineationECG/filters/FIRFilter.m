function [filtData] = FIRFilter(data, order, fCut, fSample, plotEnable)
%
% Implementacion de un filtro FIR paso bajo.
%
% Entrada
%   data        Señal de entrada
%   order       Orden del filtro
%   fCut        Frecuencia de corte del filtro
%   fSample     Frecuencia de muestreo de la señal
%   plotEnable  Habilitar grafica
%
% Salida
%   filtData    Señal filtrada
%
% Autor: Jose Manuel Bote
%

[b, a] = fir1(order, fCut / (fSample / 2), 'low', blackman(order + 1));

% Bode
if plotEnable == true
    fRange = logspace(-1, 2.7, 1e4);
    figure;
    freqz(b, a, fRange, fSample);
    title('Bode - FIR Filter');
    grid();
end

% Filtrado
filtData = filter(b, a, data);

end
