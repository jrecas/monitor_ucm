function [filtData] = baselineFilter(data, fSample, plotEnable)
%
% Implementa un filtro baseline.
%
% Entrada
%   data        Señal de entrada
%   fSample     Frecuencia de muestreo de la señal
%   plotEnable  Habilitar grafica
%
% Salida
%   filtData    Señal filtrada
%
% Autor: Jose Manuel Bote
%

% Valores por defecto
windowOpenSize = 0.2 * fSample;
windowCloseSize = 1.5 * windowOpenSize;
windowOpen = zeros(round(windowOpenSize), 1);
windowClose = zeros(round(windowCloseSize), 1);

% Baseline
openData = opening(data, windowOpen, windowOpen);
closeData = closing(openData, windowClose, windowClose);
filtData = data - closeData;

% Quitar ruido
%noiseWindow = [0, 1, 5, 1, 0]';

%rmNoiseData1 = closing(filtData, noiseWindow, noiseWindow);
%rmNoiseData2 = opening(filtData, noiseWindow, noiseWindow);
%filtData = 0.5 * (rmNoiseData1 + rmNoiseData2);

if plotEnable == true
    t = (0:length(data)-1) / fSample;
    figure;
    hold on;
    plot(t, data, 'b.-');
    plot(t, filtData, 'r.-');
    title('Baseline filter');
    legend('Raw Data', 'Filtered Data');
    grid();
    hold off;
end

end
