function [filtData] = opening(data, windowErosion, windowDilation)
%
% Implementacion la apertura de un filtro baseline.
%
% Entrada
%   data            Señal de entrada
%   windowErosion   Ventana de erosion
%   windowDilation  Ventana de dilatacion
%
% Salida
%   filtData        Señal filtrada
%
% Autor: Jose Manuel Bote
%

temp = erosion(data, windowErosion);
filtData = dilation(temp, windowDilation);

end
