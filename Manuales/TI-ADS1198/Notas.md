#Cómo usar la placa de evaluación ADS1x98

| Ras Pi | BBP |  ADS1x98 |  Uso   |          Notas        |
|:------:|-----|:--------:|:------:|:----------------------|
| 1      |     | J4-9     | 3.3V   | JP24 en posición 2-3  |
| 2      |     | J4-10    | 5.0V   | JP4 conectado         |
| 6      |     | J4-5     | GND    |                       |
| 7      |     | J3-8     | RST    |                       |
| 13     |     | J3-15    | DRDY   |                       |
| 18     |     | J3-14    | START  | JP22 en posición 2-3  |
| 19     |     | J3-11    | MOSI   |                       |
| 21     |     | J3-13    | MISO   |                       |
| 23     |     | J3-3     | SCLK   |                       |
| 24     |     | J3-1     | CS     | JP21 en posición 1-2  |



Power-Supply Test Points Test Point:

|Test Point | Voltage | Comentario |
|-|-|-|
| TP- 7   |  +5.0V | Alimentación externa (VCC_5v, J4-10) |
| TP- 9   |  +1.8V | No utilizado! |
| TP-10   |  +3.3V | Alimentación externa (VCC_3.3v, J4-9) |
| TP- 5   |  +3.0V | Generado a partir de VCC_5v (es AVDD, ya que JP2=2-3) |
| TP-13   |  +2.5V | No utilizado! |
| TP- 6   |  -2.5V | No utilzado! |
| TP- 8   |    GND | Tierra externa |

Me parece un poco consufo, ya que en el esquemático pone claramente que AVDD puede ser desde +3.0V to +5.0V, y en elquematico se ve que sólo puede ser 3.0 o 2.5v tenerado a partir de VCC_5v, que externo. También dice que DVDD es 3.3V, cuando en las instrucciones de alimentación dice que DVDD debe ser entre 1.8 y 3.0V:

>The ADS1298 can operate from **+3.0V** to +5.0V analog supply (AVDD/AVSS) and +1.8V to **+3.0V** digital supply (DVDD). An additional digital supply of and +1.8V to +3.0V digital supply (DVDD) is required for operation.

En fin, ponemos JP24 en 2-3 de manera que suministramos 3.3V por J4-9 y se convierte en DVDD (Mirar TP-10). Con JP4 cerrado, suministramos VCC_5v por J4-10 y ya estará todo alimentado, ya que poniendo JP2 a 2-3 se genera 3.0v para AVDD y con JP20 en 1-2 AVSS=AGND=0v

Una vez alimentado, nos falta la comunicación serie.

>The ADS1298 digital signals (including SPI interface signals, some GPIO signals, and some of the control signals) are available at connector J3.

| Signal       |   J3   |  J3   |  Signal      |
|--|--|--|--|
|START/**~CS** |   1   |   2   |   CLKSEL     |
| **CLK**      |   3   |   4   |   GND        |
| NC           |   5   |   6   |   GPIO1      |
| ~CS          |   7   |   8   | **~RESETB**  |
| NC           |   9   |  10   |   GND        |
|**DIN-MOSI**  |  11   |  12   |   GPIO2      |
|**DOUT-MISO** |  13   |  14   | NC/**START** |
| **DRDYB**    |  15   |  16   |   NC         |
| NC           |  17   |  18   |   GND        |
| NC           |  19   |  20   |   NC         |

Ojo, ponemos **~CS** en J3-1 con JP21=1-2 y **START** en J3-14 si JP22=2-3. Una vez hecho esto, tenemos los pines de SPI
