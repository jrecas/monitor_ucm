/*
 * Utils.h
 *
 *  Created on: 25/2/2015
 *      Author: Jose Manuel Bote
 */

#ifndef SRC_UTILS_H_
#define SRC_UTILS_H_

#include <stdint.h>

void findMinValPos16(int16_t buffer[], uint16_t bufferSize, int16_t* minVal, uint16_t* minPos);
void findMaxValPos16(int16_t buffer[], uint16_t bufferSize, int16_t* maxVal, uint16_t* maxPos);
void findMinVal16(int16_t buffer[], uint16_t bufferSize, int16_t* minVal);
void findMaxVal16(int16_t buffer[], uint16_t bufferSize, int16_t* maxVal);
void findMinVal32(int32_t buffer[], uint16_t bufferSize, int32_t* minVal);
void findMaxVal32(int32_t buffer[], uint16_t bufferSize, int32_t* maxVal);
void findMinPos16(int16_t buffer[], uint16_t bufferSize, uint16_t* minPos);
void findMaxPos16(int16_t buffer[], uint16_t bufferSize, uint16_t* maxPos);

#endif /* SRC_UTILS_H_ */
