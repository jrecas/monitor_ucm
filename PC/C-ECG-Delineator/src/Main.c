#include "FIRFilter.h"
#include "BaselineFilter.h"
#include "DelineatorECG.h"
#include <stdio.h>
#include <stdlib.h>
#include <time.h>


int main(void){

	clock_t start_t, end_t;

	FILE *fIn;
	FILE *fOut14;	// Ojo al leer el log, que es little-endian en mi maquina
	FILE *fOut40;
	FILE *fOutBL;
	FILE *fOutDel;

	uint8_t buff[2];
	uint16_t raw_sample;
	int32_t FIR_filt_samples[2] = {0};	// Sera un array con 2 variables (de 14 Hz en [0] y de 40 Hz en [1])
	int16_t BL_filt_sample = 0;
	int16_t openingPSample = 0;
	int16_t openingTSample = 0;

	//char* nameFile = "../ECG-DATA/ECG250.log";
	char* nameFile = "../ECG-DATA/exampleQTDB.log";

	if ((fIn = fopen(nameFile, "r")) == NULL) {
		perror("Error al abrir fichero de entrada");
		return(-1);
	}

	if( (fOut14 = fopen("../fECG250_14.log", "w")) == NULL) {
		perror("Error al abrir fichero de salida");
		return(-1);
	}

	if ((fOut40 = fopen("../fECG250_40.log", "w")) == NULL) {
		perror("Error al abrir fichero de salida");
		return(-1);
	}

	if ((fOutBL = fopen("../fECG250_BL.log", "w")) == NULL) {
		perror("Error al abrir fichero de salida");
		return(-1);
	}

	if ((fOutDel =fopen("../fECG250_DEL.log", "w")) == NULL) {
		perror("Error al abrir fichero de salida");
		return(-1);
	}

	start_t = clock();

	while(fread(buff, sizeof(buff), 1, fIn)) {

		raw_sample = (buff[0] << 8) + buff[1];

		filterSampleFIR(raw_sample, FIR_filt_samples);					// Filtrados FIR
		BL_filt_sample = filterSampleBL((int16_t) FIR_filt_samples[1]);	// Filtrado baseline
		openingPSample = openingP((int16_t) FIR_filt_samples[1]);		// Opening para P
		openingTSample = openingT((int16_t) FIR_filt_samples[1]);		// Opening para T

		// Delinear y guardar la delineacion, TRUE si se detecta un nuevo latido
		if (storeSamples(FIR_filt_samples, &BL_filt_sample, &openingPSample, &openingTSample) == TRUE) {
			fwrite(ECGPoints, sizeof(ECGPoints), 1, fOutDel);
		}

		// Guardar muestras filtradas con FIR de 14 Hz y 40 Hz
		fwrite(&FIR_filt_samples[0], sizeof(FIR_filt_samples[0]), 1, fOut14);
		fwrite(&FIR_filt_samples[1], sizeof(FIR_filt_samples[1]), 1, fOut40);

		// Guardar filtro baseline
		fwrite(&BL_filt_sample, sizeof(BL_filt_sample), 1, fOutBL);

	}

	end_t = clock();

	fclose(fIn);
	fclose(fOut14);
	fclose(fOut40);
	fclose(fOutBL);
	fclose(fOutDel);

	printf("Total time taken by CPU: %.3f ms\n", 1000 * (double)(end_t - start_t) / CLOCKS_PER_SEC);
	puts("I'm done!!!");
	return EXIT_SUCCESS;

}
