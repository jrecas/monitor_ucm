/*
 * BaselineFilter.h
 *
 *  Created on: 24/2/2015
 *      Author: Jose Manuel Bote
 */

#ifndef SRC_BASELINEFILTER_H_
#define SRC_BASELINEFILTER_H_

#include <stdint.h>

#define BUFF_OPEN_LEN			49		// Longitud de la ventana de opening (siempre impar)
#define BUFF_CLOSE_LEN			75		// Longitud de la ventana de closing (siempre impar)
#define BUFF_OPEN_CENTER		(BUFF_OPEN_LEN / 2)
#define BUFF_CLOSE_CENTER		(BUFF_CLOSE_LEN / 2)
#define BASELINE_FILTER_DELAY	(2 * BUFF_OPEN_CENTER + 2 * BUFF_CLOSE_CENTER) // 122

#define BUFF_OPEN_P_LEN			29		// Longitud de la ventana de opening para P-on/end (siempre impar)
#define OPENING_P_DELAY			((2 * BUFF_OPEN_P_LEN) / 2)

#define BUFF_OPEN_T_LEN			63		// Longitud de la ventana de opening para T-on/end (siempre impar)
#define OPENING_T_DELAY			((2 * BUFF_OPEN_T_LEN) / 2)

int16_t filterSampleBL(int16_t sample);
int16_t openingP(int16_t sample);
int16_t openingT(int16_t sample);
void erosion(int16_t sample, int16_t buffer[], uint16_t bufferSize, uint16_t bufferIndex, int16_t* minVal, uint16_t* minPos);
void dilation(int16_t sample, int16_t buffer[], uint16_t bufferSize, uint16_t bufferIndex, int16_t* maxVal, uint16_t* maxPos);

#endif /* SRC_BASELINEFILTER_H_ */
