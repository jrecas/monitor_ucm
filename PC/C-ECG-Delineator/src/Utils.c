/*
 * Utils.c
 *
 *  Created on: 25/2/2015
 *      Author: Jose Manuel Bote
 */

#include "Utils.h"


/*
 * Calcula el valor minimo de un buffer y su posicion.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *minVal:		Valor del minimo del buffer
 * @param *minPos:		Posicion del minimo del buffer
 */
void findMinValPos16(int16_t buffer[], uint16_t bufferSize, int16_t* minVal, uint16_t* minPos) {
	*minVal = buffer[0];
	*minPos = 0;
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < *minVal) {
		  *minVal = buffer[i];
		  *minPos = i;
		}
	}
	return;
}


/*
 * Calcula el valor maximo de un buffer y su posicion.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *maxVal:		Valor del maximo del buffer
 * @param *maxPos:		Posicion del maximo del buffer
 */
void findMaxValPos16(int16_t buffer[], uint16_t bufferSize, int16_t* maxVal, uint16_t* maxPos) {
	*maxVal = buffer[0];
	*maxPos = 0;
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > *maxVal) {
			*maxVal = buffer[i];
			*maxPos = i;
		}
	}
	return;
}


/*
 * Calcula el valor minimo de un buffer.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *minVal:		Valor del minimo del buffer
 */
void findMinVal16(int16_t buffer[], uint16_t bufferSize, int16_t* minVal) {
	*minVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < *minVal) {
			*minVal = buffer[i];
		}
	}
	return;
}


/*
 * Calcula el valor maximo de un buffer.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *maxVal:		Valor del maximo del buffer
 */
void findMaxVal16(int16_t buffer[], uint16_t bufferSize, int16_t* maxVal) {
	*maxVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > *maxVal) {
			*maxVal = buffer[i];
		}
	}
	return;
}


/*
 * Calcula el valor minimo de un buffer.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *minVal:		Valor del minimo del buffer
 */
void findMinVal32(int32_t buffer[], uint16_t bufferSize, int32_t* minVal) {
	*minVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < *minVal) {
			*minVal = buffer[i];
		}
	}
	return;
}


/*
 * Calcula el valor maximo de un buffer.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *maxVal:		Valor del maximo del buffer
 */
void findMaxVal32(int32_t buffer[], uint16_t bufferSize, int32_t* maxVal) {
	*maxVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > *maxVal) {
			*maxVal = buffer[i];
		}
	}
	return;
}


/*
 * Calcula la posicion del valor minimo de un buffer.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *minPos:		Posicion del minimo del buffer
 */
void findMinPos16(int16_t buffer[], uint16_t bufferSize, uint16_t* minPos) {
	*minPos = 0;
	int16_t tempMinVal = buffer[*minPos];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < tempMinVal) {
			tempMinVal = buffer[i];
			*minPos = i;
		}
	}
	return;
}


/*
 * Calcula la posicion del valor maximo de un buffer.
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *maxPos:		Posicion del maximo del buffer
 */
void findMaxPos16(int16_t buffer[], uint16_t bufferSize, uint16_t* maxPos) {
	*maxPos = 0;
	int16_t tempMaxVal = buffer[*maxPos];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > tempMaxVal) {
			tempMaxVal = buffer[i];
			*maxPos = i;
		}
	}
	return;
}
