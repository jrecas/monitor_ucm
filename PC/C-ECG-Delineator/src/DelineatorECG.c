/*
 * DelineatorECG.c
 *
 *  Created on: 25/2/2015
 *      Author: Jose Manuel Bote
 */

#include "DelineatorECG.h"
#include <stdlib.h>
#include <string.h>
#include "BaselineFilter.h"
#include "Utils.h"


// 4 segundos de buffer
int16_t buffSamples40[BUFF_LEN] = {0};			// FIR filter 40 Hz int16_t (quitando el baseline)
int16_t buffSamples14[BUFF_LEN] = {0};			// FIR filter 14 Hz int32_t
int16_t buffSamples40_O_P[BUFF_LEN] = {0};		// ECG con opening P
int16_t buffSamples40_O_T[BUFF_LEN] = {0};		// ECG con opening T
uint16_t buffSamplesIndex = 0;					// Indice para buffers
int32_t buffSamplesOverflow = -1;				// Numero de veces que se desborda el buffer (la primera vez suma 1)

uint32_t ECGPoints[NUM_POINTS] = {0};			// Penultima delineacion, esta completa
uint32_t tempECGPoints[NUM_POINTS] = {0};		// La ultima delineacion, cuando esta completa se guarda en ECGPoints
uint8_t flagNewBeatFound = FALSE;				// Nuevo latido encontrado
int16_t buffThresholdR[NUM_WIN_R_TH] = {0};		// Ultimos umbrales para la deteccion de picos R
uint8_t indexThresholdR = 0;					// Indice para el array del umbral para la deteccion de picos R
int16_t meanThresholdR = INT16_MIN;				// Media de los ultimos umbrales para la deteccion de picos R
uint8_t negativeInterval = FALSE;				// Intervalo de ECG con ondas positivas o negativas

boolean delineationFlag = FALSE;				// Flag para delinear
uint16_t delineationIndex = 0;					// Indice en la delineacion


/*
 * Almacena las muestras filtradas en los bufferes correspondientes.
 * @param FIRSamples[2]:	Muestras de los filtros FIR
 * @param *BL_filt_sample: 	Muestra de baseline
 */
boolean storeSamples(int32_t FIRSamples[2], int16_t* BLSample, int16_t* openingPSample, int16_t* openingTSample) {

	// Almacenar las muestras
	buffSamples14[buffSamplesIndex] = FIRSamples[0];	// FIRSamples[0] es la de 14 Hz (amplificada x10)
	buffSamples40[buffSamplesIndex] = FIRSamples[1];	// FIRSamples[1] es la de 40 Hz (amplificada x10)
	buffSamples40[(buffSamplesIndex - BASELINE_FILTER_DELAY) & MASK_BUFF_LEN] -= *BLSample;
	buffSamples40_O_P[(buffSamplesIndex - OPENING_P_DELAY) & MASK_BUFF_LEN] =
			buffSamples40[(buffSamplesIndex - OPENING_P_DELAY) & MASK_BUFF_LEN] - *openingPSample;
	buffSamples40_O_T[(buffSamplesIndex - OPENING_T_DELAY) & MASK_BUFF_LEN] =
			buffSamples40[(buffSamplesIndex - OPENING_T_DELAY) & MASK_BUFF_LEN] - *openingTSample;

	// Si hay una mitad del buffer completado, se preprocesa
	if (buffSamplesIndex == SECOND_HALF_BUFF_COMPLETED) {
		preProccess(SECOND_HALF_BUFF);		// Preprocesar la segunda mitad del buffer
		delineationIndex = BUFF_HALF_LEN;
		delineationFlag = TRUE;
	} else if (buffSamplesIndex == FIRST_HALF_BUFF_COMPLETED) {
		preProccess(FIRST_HALF_BUFF);		// Preprocesar la primera mitad del buffer
		buffSamplesOverflow += 1;
		delineationIndex = 0;
		delineationFlag = TRUE;
	}

	if (delineationFlag == TRUE) {

		do {

			// Muestras anterior, actual y posterior de FIR filter 14 Hz
			int16_t tempSample14_last = buffSamples14[(delineationIndex - 1) & MASK_BUFF_LEN];
			int16_t tempSample14_now  = buffSamples14[ delineationIndex      & MASK_BUFF_LEN];
			int16_t tempSample14_next = buffSamples14[(delineationIndex + 1) & MASK_BUFF_LEN];

			// Evitar discrepancias entre intervalos invertidos (solo puede ocurrir al final del intervalo)
			if ((negativeInterval == TRUE) && (((delineationIndex + 1) & MASK_BUFF_HALF_LEN) == 0)) {
				tempSample14_next *= -1;
			}

			// Segunda derivada
			int16_t tempSampleSD = tempSample14_last - 2 * tempSample14_now + tempSample14_next;

			// Cambio de signo de primera derivada
			int8_t tempSampleFD = (((tempSample14_next - tempSample14_now) & 0x80000000) !=
					((tempSample14_now - tempSample14_last) & 0x80000000)) ? TRUE : FALSE;

			//if ((tempSampleSD < ((33 * meanThresholdR) / 100)) && (tempSampleFD == TRUE)) {
			if ((tempSampleSD < ((21 * meanThresholdR) >> 6)) && (tempSampleFD == TRUE)) {

				uint64_t posNewBeat = delineationIndex + buffSamplesOverflow * BUFF_LEN;

				int16_t tempPeakR_SD = buffSamples14[(tempECGPoints[4] - 1) & MASK_BUFF_LEN]
									- 2 * buffSamples14[tempECGPoints[4] & MASK_BUFF_LEN] + buffSamples14[(tempECGPoints[4] + 1) & MASK_BUFF_LEN];

				if ((posNewBeat - tempECGPoints[4]) < MIN_RR_PEAKS) {			// Distancia minima no valida entre picos R
					if (tempSampleSD < tempPeakR_SD) {
						tempECGPoints[4] = posNewBeat;							// La nueva derivada es menor que la anterior (mayor en abs)
						uint8_t i;
						for (i = 0; i < 4; i++) {
							tempECGPoints[i] = 0;
						}
					}

				} else if ((posNewBeat - tempECGPoints[4]) > MAX_RR_PEAKS) {	// Distancia maxima no valida entre picos R (buffer desbordado)
					tempECGPoints[4] = posNewBeat;								// Almacenamos el ultimo latido, perdiendo el anterior
					uint8_t i;
					for (i = 0; i < 4; i++) {
						tempECGPoints[i] = 0;
					}

				} else {														// Distancia valida entre picos R
					memcpy(ECGPoints, tempECGPoints, sizeof(tempECGPoints));
					uint8_t i;
					for (i = 0; i < NUM_POINTS; i++) {
						tempECGPoints[i] = 0;
					}
					tempECGPoints[4] = posNewBeat;								// Actualizar el nuevo pico R
					delineateECGWaves((uint16_t) (ECGPoints[4] & MASK_BUFF_LEN), delineationIndex);
					flagNewBeatFound = TRUE;

				}

			}

			delineationIndex++;
			if ((delineationIndex & MASK_BUFF_HALF_LEN) == 0) {
				delineationFlag = FALSE;
			}

		} while ((flagNewBeatFound == FALSE) && ((delineationIndex & MASK_BUFF_HALF_LEN) != 0));

	}

	// Rotacion circular
	buffSamplesIndex++;
	if (buffSamplesIndex == BUFF_LEN) {
		buffSamplesIndex = 0;
	}

	// Devolver si se ha encontrado un nuevo latido
	if (flagNewBeatFound == TRUE) {
		flagNewBeatFound = FALSE;
		return TRUE;
	} else {
		return FALSE;
	}

}


/*
 * Preprocesar la mitad correspondiente de los bufferes, hallando el umbral
 * de deteccion de latido e invirtiendo si es necesario.
 * @param halfBuffer:	Mitad correspondiente del buffer
 */
void preProccess(uint8_t halfBuffer) {

	// Hallar en que parte del buffer estamos
	uint16_t startPos;
	uint16_t endPos;
	if (halfBuffer == FIRST_HALF_BUFF) {
		startPos = 0;
		endPos = BUFF_HALF_LEN;
	} else {
		startPos = BUFF_HALF_LEN;
		endPos = BUFF_LEN;
	}

	// Hallar el umbral para la segunda derivada
	int16_t tempBuffSamplesSD[BUFF_HALF_LEN];

	// Calcular la segunda derivada
	// Evitar discrepancias entre intervalos invertidos (solo puede ocurrir al principio del intervalo)
	tempBuffSamplesSD[0] = -2 * buffSamples14[(startPos) & MASK_BUFF_LEN] + buffSamples14[(startPos + 1) & MASK_BUFF_LEN];
	tempBuffSamplesSD[0] += (negativeInterval == TRUE) ?
			 -1 * buffSamples14[(startPos - 1) & MASK_BUFF_LEN] :
			buffSamples14[(startPos - 1) & MASK_BUFF_LEN];

	// Completar el resto de la segunda derivada
	uint16_t i;
	for (i = 1; i < BUFF_HALF_LEN; i++) {
		tempBuffSamplesSD[i] = buffSamples14[(startPos + i - 1) & MASK_BUFF_LEN]
				- 2 * buffSamples14[(startPos + i) & MASK_BUFF_LEN] + buffSamples14[(startPos + i + 1) & MASK_BUFF_LEN];
	}

	// Hallar el maximo y el minimo y elegir el mayor como umbral, invirtiendo si es necesario
	int16_t tempMin = 0;
	int16_t tempMax = 0;
	findMinVal16(tempBuffSamplesSD, BUFF_HALF_LEN, &tempMin);
	findMaxVal16(tempBuffSamplesSD, BUFF_HALF_LEN, &tempMax);
	if (abs(tempMin) >= abs(tempMax)) {
		buffThresholdR[indexThresholdR] = tempMin;
		negativeInterval = FALSE;
	} else {
		buffThresholdR[indexThresholdR] = -tempMax;
		negativeInterval = TRUE;
		uint16_t k;
		for (k = startPos; k < endPos; k++) {
			buffSamples14[k] *= -1;
			buffSamples40[k] *= -1;
			buffSamples40_O_P[k] *= -1;
			buffSamples40_O_T[k] *= -1;
		}
	}

	// Controlar la media de los umbrales para la deteccion de pico R de las ultimas ventanas
	meanThresholdR = 0;
	uint8_t k;
	for (k = 0; k < NUM_WIN_R_TH; k++) {
		meanThresholdR += buffThresholdR[k];
	}
	meanThresholdR = meanThresholdR >> DESP_WIN_R_TH;
	indexThresholdR++;
	if (indexThresholdR == NUM_WIN_R_TH) {
		indexThresholdR = 0;
	}

}


/*
 * Delineacion de cada latido en dos subprocesos.
 * 1. Delinear S y T para el latido anterior.
 * 2. Delinear P y Q para el nuevo latido.
 * @param startPos:	Posicion del pico R anterior
 * @param endPos: 	Posicion del pico R nuevo
 */
void delineateECGWaves(uint16_t startPos, uint16_t endPos) {
	uint8_t overflow = 0;
	if (startPos > endPos) {
		endPos += BUFF_LEN;
		overflow = 1;
	}
	uint16_t midPos = (startPos + endPos) >> 1;	// Posicion media
	delineateSTWaves(&startPos, &midPos, &overflow);
	delineatePQWaves(&midPos, &endPos, &overflow);
}


/*
 * Delineacion de las ondas S y T para el latido anterior.
 * @param *startPos:	Posicion del pico R anterior
 * @param *endPos: 		Posicion hasta donde se espera encontrar el pico T
 */
void delineateSTWaves(uint16_t* startPos, uint16_t* endPos, uint8_t* overflow) {

	/*
	 * Delineacion de onda S
	 */

	// tolerance: 5% amplitud del pico R
	//int32_t tolerance = (5 * abs(buffSamples40[*startPos & MASK_BUFF_LEN])) / 100;
	int16_t tolerance = (13 * abs(buffSamples40[*startPos & MASK_BUFF_LEN])) >> 8;
	int16_t tempThreshold = 10 * tolerance;

	// Buscar el 50% de la amplitud del pico R
	uint16_t infintyLoop = BUFF_LEN;
	while (buffSamples40[*startPos & MASK_BUFF_LEN] > tempThreshold) {
		*startPos += 1;
		infintyLoop--;
		if (infintyLoop == 0) {
		  break;
		}
	}
	//*startPos -= 1;
	if (*startPos >= *endPos) {
		return;
	}

	// Buscar R end
	tempThreshold = tolerance;
	while ((buffSamples40[*startPos & MASK_BUFF_LEN] > tempThreshold) &&
			(buffSamples40[*startPos & MASK_BUFF_LEN] > buffSamples40[(*startPos + 1) & MASK_BUFF_LEN])) {
		*startPos += 1;
	}
	if (*startPos >= *endPos) {
		return;
	}
	uint16_t tempPosOn = *startPos;		// Posicion de R end y de S on
	ECGPoints[5] = *startPos + ((buffSamplesOverflow - *overflow) * BUFF_LEN);

	// Buscar el siguiente minimo a la derecha (S peak)
	infintyLoop = BUFF_LEN;
	while (buffSamples40[*startPos & MASK_BUFF_LEN] >= buffSamples40[(*startPos + 1) & MASK_BUFF_LEN]) {
		*startPos += 1;
		infintyLoop--;
		if (infintyLoop == 0) {
		  break;
		}
	}
	//*startPos -= 1;
	if (*startPos >= *endPos) {
		return;
	}

	tempThreshold = -tolerance;

	if (buffSamples40[*startPos & MASK_BUFF_LEN] < tempThreshold) {	// El minimo es menor que el umbral, es valido

		// Buscar S end
		while ((buffSamples40[*startPos & MASK_BUFF_LEN] < tempThreshold) &&
				(buffSamples40[*startPos & MASK_BUFF_LEN] < buffSamples40[(*startPos + 1) & MASK_BUFF_LEN])) {
			*startPos += 1;
		}
		if (*startPos >= *endPos) {
			return;
		}

		// Onda S valida si es menor de 100 ms de anchura
		if ((*startPos != tempPosOn) && ((*startPos - tempPosOn) < T_S_WAVE)) {
			// Pequeña correccion para S end
			uint16_t tempStop = *startPos + CORRECT_S_END;
			if (tempStop > *endPos) {
				tempStop = *endPos;
			}
			for (; *startPos <= tempStop; (*startPos)++) {
				if (buffSamples40[*startPos & MASK_BUFF_LEN] > buffSamples40[(*startPos + 1) & MASK_BUFF_LEN]) {
					ECGPoints[5] = *startPos + ((buffSamplesOverflow - *overflow) * BUFF_LEN);
					break;
				}
			}
			ECGPoints[5] = *startPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;
		}

	}

	/*
	 * Delineacion de onda T
	 */
	uint8_t negativeTWave = FALSE;

	uint16_t auxPosPeak = ECGPoints[4] & MASK_BUFF_LEN;
	uint16_t auxStart   = auxPosPeak + WINDOW_T_ON;
	uint16_t auxEnd     = (*endPos < (auxPosPeak + WINDOW_T_OFF)) ? *endPos : (auxPosPeak + WINDOW_T_OFF);
	uint16_t auxMinPos  = auxStart;
	uint16_t auxMaxPos  = auxStart;
	int16_t auxMinVal   = 0;
	int16_t auxMaxVal   = 0;

	uint16_t i;
	for (i = auxStart + 1; i < auxEnd; i++) {
		int16_t tempSampleSD = buffSamples14[(i - 1) & MASK_BUFF_LEN]
				- 2 * buffSamples14[i & MASK_BUFF_LEN] + buffSamples14[(i + 1) & MASK_BUFF_LEN];
		if (tempSampleSD < auxMinVal) {
			auxMinVal = tempSampleSD;
			auxMinPos = i;
		}
		if (tempSampleSD > auxMaxVal) {
			auxMaxVal = tempSampleSD;
			auxMaxPos = i;
		}
	}

	if (abs(auxMinVal) >= abs(auxMaxVal)) {
		//if (abs(auxMinVal) > abs(meanThresholdR / 100)) {
		if (abs(auxMinVal) > abs((5 * meanThresholdR) >> 9)) {
			ECGPoints[6] = auxMinPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;
			auxPosPeak = auxMinPos;
		}
	} else {
		//if (abs(auxMaxVal) > abs(meanThresholdR / 100)) {
		if (abs(auxMaxVal) > abs((5 * meanThresholdR) >> 9)) {
			ECGPoints[6] = auxMaxPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;
			negativeTWave = TRUE;
			auxPosPeak = auxMaxPos;
		}
	}

	// T end
	if (ECGPoints[6] > 0) {

		// tolerance: 2.5% de la amplitud del pico T
		//tolerance = (25 * abs(buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN])) / 1000;
		tolerance = (13 * abs(buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN])) >> 9;

		// Buscar T end
		if (negativeTWave == FALSE) {
			tempThreshold = 20 * tolerance;		// limit: baseT + 50% amplitudT
			infintyLoop = BUFF_LEN;
			while (buffSamples40[auxPosPeak & MASK_BUFF_LEN] > tempThreshold) {
				auxPosPeak++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
			tempThreshold = tolerance;			// limit: baseT + 2.5% amplitudT
			infintyLoop = BUFF_LEN;
			while ((buffSamples40[auxPosPeak & MASK_BUFF_LEN] > buffSamples40[(auxPosPeak + 1) & MASK_BUFF_LEN]) &&
					(buffSamples40[auxPosPeak & MASK_BUFF_LEN] > tempThreshold)) {
				auxPosPeak++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
		} else {
			tempThreshold = -20 * tolerance;	// limit: baseT - 50% amplitudT
			infintyLoop = BUFF_LEN;
			while (buffSamples40[auxPosPeak & MASK_BUFF_LEN] < tempThreshold) {
				auxPosPeak++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
			tempThreshold = -tolerance;			// limit: baseT + 2.5% amplitudT
			infintyLoop = BUFF_LEN;
			while ((buffSamples40[auxPosPeak & MASK_BUFF_LEN] < buffSamples40[(auxPosPeak + 1) & MASK_BUFF_LEN]) &&
					(buffSamples40[auxPosPeak & MASK_BUFF_LEN] < tempThreshold)) {
				auxPosPeak++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
		}

		uint64_t auxToff = auxPosPeak + (buffSamplesOverflow - *overflow) * BUFF_LEN;
		if (auxToff < tempECGPoints[4]) {
			ECGPoints[7] = auxToff;				// T off
		}

	}

}


/*
 * Delineacion de P y Q para el nuevo latido.
 * @param *startPos:	Posicion hasta donde se espera encontrar el pico P
 * @param *endPos: 		Posicion del pico R nuevo
 */
void delineatePQWaves(uint16_t* startPos, uint16_t* endPos, uint8_t* overflow) {

	/*
	 * Delineacion de onda Q
	 */

	// tolerance: 5% amplitud del pico R

	//int32_t tolerance = (5 * abs(buffSamples40[*startPos & MASK_BUFF_LEN])) / 100;
	int16_t tolerance = (13 * abs(buffSamples40[*startPos & MASK_BUFF_LEN])) >> 8;
	int16_t tempThreshold = 10 * tolerance;

	// Buscar el 50% de la amplitud del pico R
	uint16_t infintyLoop = BUFF_LEN;
	while (buffSamples40[*endPos & MASK_BUFF_LEN] > tempThreshold) {
		*endPos -= 1;
		infintyLoop--;
		if (infintyLoop == 0) {
		  break;
		}
	}
	//*endPos -= 1;
	if (*startPos >= *endPos) {
		return;
	}

	// Buscar R on
	tempThreshold = tolerance;
	while ((buffSamples40[*endPos & MASK_BUFF_LEN] > tempThreshold) &&
			(buffSamples40[*endPos & MASK_BUFF_LEN] > buffSamples40[(*endPos - 1) & MASK_BUFF_LEN])) {
		*endPos -= 1;
	}
	if (*startPos >= *endPos) {
		return;
	}
	uint16_t tempPosOff = *endPos;		// Posicion de R on y de Q end
	tempECGPoints[3] = *endPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;

	// Buscar el siguiente minimo a la izquierda (Q peak)
	infintyLoop = BUFF_LEN;
	while (buffSamples40[*endPos & MASK_BUFF_LEN] >= buffSamples40[(*endPos - 1) & MASK_BUFF_LEN]) {
		*endPos -= 1;
		infintyLoop--;
		if (infintyLoop == 0) {
		  break;
		}
	}
	//*endPos -= 1;
	if (*startPos >= *endPos) {
		return;
	}

	//tempThreshold = offset - tolerance;
	tempThreshold = -tolerance;

	if (buffSamples40[*endPos & MASK_BUFF_LEN] < tempThreshold) {	// El minimo es menor que el umbral, es valido

		// Buscar Q on
		while ((buffSamples40[*endPos & MASK_BUFF_LEN] < tempThreshold) &&
				(buffSamples40[*endPos & MASK_BUFF_LEN] < buffSamples40[(*endPos - 1) & MASK_BUFF_LEN])) {
			*endPos -= 1;
		}
		if (*startPos >= *endPos) {
			return;
		}

		// Onda Q valida si es menor de 100 ms de anchura
		if ((*endPos != tempPosOff) && ((tempPosOff - *endPos) < T_Q_WAVE)) {
			// Pequeña correccion para Q on
			uint16_t tempStop = *endPos - CORRECT_Q_ON;
			if (tempStop < *startPos) {
				tempStop = *startPos;
			}
			for (; *endPos >= tempStop; (*endPos)--) {
				if (buffSamples40[*endPos & MASK_BUFF_LEN] > buffSamples40[(*endPos - 1) & MASK_BUFF_LEN]) {
					tempECGPoints[3] = *endPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;
					break;
				}
			}
			tempECGPoints[3] = *endPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;
		}

	}

	/*
	 * Delineacion de onda P
	 */
	uint8_t negativePWave = FALSE;

	uint16_t auxPosPeak = (*overflow == 0) ? tempECGPoints[4] & MASK_BUFF_LEN : (tempECGPoints[4] & MASK_BUFF_LEN) + BUFF_LEN;
	uint16_t auxEnd     = auxPosPeak - WINDOW_P_OFF;
	uint16_t auxStart   = (*startPos > (auxPosPeak - WINDOW_P_ON)) ? *startPos : (auxPosPeak - WINDOW_P_ON);
	uint16_t auxMinPos  = auxEnd;
	uint16_t auxMaxPos  = auxEnd;
	int16_t auxMinVal   = 0;
	int16_t auxMaxVal   = 0;

	uint16_t i;
	for (i = auxEnd - 1; i > auxStart; i--) {
		int16_t tempSampleSD = buffSamples14[(i - 1) & MASK_BUFF_LEN]
				- 2 * buffSamples14[i & MASK_BUFF_LEN] + buffSamples14[(i + 1) & MASK_BUFF_LEN];
		if (tempSampleSD < auxMinVal) {
			auxMinVal = tempSampleSD;
			auxMinPos = i;
		}
		if (tempSampleSD > auxMaxVal) {
			auxMaxVal = tempSampleSD;
			auxMaxPos = i;
		}
	}

	if (abs(auxMinVal) >= abs(auxMaxVal)) {
		//if (abs(auxMinVal) > abs(meanThresholdR / 100)) {
		if (abs(auxMinVal) > abs((5 * meanThresholdR) >> 9)) {
			tempECGPoints[1] = auxMinPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;
			auxPosPeak = auxMinPos;
		}
	} else {
		//if (abs(auxMaxVal) > abs(meanThresholdR / 100)) {
		if (abs(auxMaxVal) > abs((5 * meanThresholdR) >> 9)) {
			tempECGPoints[1] = auxMaxPos + (buffSamplesOverflow - *overflow) * BUFF_LEN;
			negativePWave = TRUE;
			auxPosPeak = auxMaxPos;
		}
	}

	// P on y end
	if (tempECGPoints[1] > 0) {

		// tolerance: 2.5% de la amplitud del pico P
		//tolerance = (25 * abs(buffSamples40_O_P[auxPosPeak & MASK_BUFF_LEN])) / 1000;
		tolerance = (13 * abs(buffSamples40_O_P[auxPosPeak & MASK_BUFF_LEN])) >> 9;

		// Buscar P on
		auxStart = auxPosPeak;

		if (negativePWave == FALSE) {
			tempThreshold = 20 * tolerance;		// limit: baseP + 50% amplitudT
			infintyLoop = BUFF_LEN;
			while (buffSamples40[auxStart & MASK_BUFF_LEN] > tempThreshold) {
				auxStart--;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
			tempThreshold = tolerance;			// limit: baseP + 2.5% amplitudT
			infintyLoop = BUFF_LEN;
			while ((buffSamples40[auxStart & MASK_BUFF_LEN] > buffSamples40[(auxStart - 1) & MASK_BUFF_LEN]) &&
					(buffSamples40[auxStart & MASK_BUFF_LEN] > tempThreshold)) {
				auxStart--;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
		} else {
			tempThreshold = -20 * tolerance;	// limit: baseP - 50% amplitudT
			infintyLoop = BUFF_LEN;
			while (buffSamples40[auxStart & MASK_BUFF_LEN] < tempThreshold) {
				auxStart--;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
			tempThreshold = -tolerance;			// limit: baseP + 2.5% amplitudT
			infintyLoop = BUFF_LEN;
			while ((buffSamples40[auxStart & MASK_BUFF_LEN] < buffSamples40[(auxStart - 1) & MASK_BUFF_LEN]) &&
					(buffSamples40[auxStart & MASK_BUFF_LEN] < tempThreshold)) {
				auxStart--;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
		}

		uint64_t auxPon = auxStart + (buffSamplesOverflow - *overflow) * BUFF_LEN;
		if (auxPon > ECGPoints[4]) {
			tempECGPoints[0] = auxPon;			// P on
		}

		// Buscar P off
		auxEnd = auxPosPeak;

		if (negativePWave == FALSE) {
			tempThreshold = 20 * tolerance;		// limit: baseP + 50% amplitudT
			infintyLoop = BUFF_LEN;
			while (buffSamples40[auxEnd & MASK_BUFF_LEN] > tempThreshold) {
				auxEnd++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
			tempThreshold = tolerance;			// limit: baseP + 2.5% amplitudT
			infintyLoop = BUFF_LEN;
			while ((buffSamples40[auxEnd & MASK_BUFF_LEN] > buffSamples40[(auxEnd + 1) & MASK_BUFF_LEN]) &&
					(buffSamples40[auxEnd & MASK_BUFF_LEN] > tempThreshold)) {
				auxEnd++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
		} else {
			tempThreshold = -20 * tolerance;	// limit: baseP - 50% amplitudT
			infintyLoop = BUFF_LEN;
			while (buffSamples40[auxEnd & MASK_BUFF_LEN] < tempThreshold) {
				auxEnd++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
			tempThreshold = -tolerance;			// limit: baseP + 2.5% amplitudT
			infintyLoop = BUFF_LEN;
			while ((buffSamples40[auxEnd & MASK_BUFF_LEN] < buffSamples40[(auxEnd + 1) & MASK_BUFF_LEN]) &&
					(buffSamples40[auxEnd & MASK_BUFF_LEN] < tempThreshold)) {
				auxEnd++;
				infintyLoop--;
				if (infintyLoop == 0) {
				  break;
				}
			}
		}

		uint64_t auxPoff = auxEnd + (buffSamplesOverflow - *overflow) * BUFF_LEN;
		if (auxPoff < tempECGPoints[4]) {
			tempECGPoints[2] = auxPoff;			// P off
		}

	}

}
