/*
 * FIRFilter.c
 *
 *  Created on: 23/2/2015
 *      Author: Jose Manuel Bote
 */

#include "FIRFilter.h"


// Coeficientes multiplicados x1e4
/*static const int16_t COEFFS_14[FIR_CONV_LEN] = {
		0, 0, 0, -1, -5, -12, -23, -36, -47, -52, -40,
		-4, 66, 173, 316, 488, 672, 849, 997, 1095, 1129
};*/

// Coeficientes multiplicados x1e4
/*static const int16_t COEFFS_40[FIR_CONV_LEN] = {
		0, 0, -1, -4, -3, 8, 23, 17, -26, -77, -64, 55,
		199, 187, -91, -468, -521, 121, 1383, 2660, 3200
};*/

enum COEFFS_14 {
	C14_0 = 0,
	C14_1 = 0,
	C14_2 = 0,
	C14_3 = -1,
	C14_4 = -5,
	C14_5 = -12,
	C14_6 = -23,
	C14_7 = -36,
	C14_8 = -47,
	C14_9 = -52,
	C14_10 = -40,
	C14_11 = -4,
	C14_12 = 66,
	C14_13 = 173,
	C14_14 = 316,
	C14_15 = 488,
	C14_16 = 672,
	C14_17 = 849,
	C14_18 = 997,
	C14_19 = 1095,
	C14_20 = 1129
};

enum COEFFS_40 {
	C40_0 = 0,
	C40_1 = 0,
	C40_2 = -1,
	C40_3 = -4,
	C40_4 = -3,
	C40_5 = 8,
	C40_6 = 23,
	C40_7 = 17,
	C40_8 = -26,
	C40_9 = -77,
	C40_10 = -64,
	C40_11 = 55,
	C40_12 = 199,
	C40_13 = 187,
	C40_14 = -91,
	C40_15 = -468,
	C40_16 = -521,
	C40_17 = 121,
	C40_18 = 1383,
	C40_19 = 2660,
	C40_20 = 3200
};

//int16_t samples[FIR_BUFF_LEN] = {0};
//uint8_t buffIndex = FIR_BUFF_LEN - 1;

int16_t samples[FIR_ORDER + 1] = {0};
int16_t *pLast = samples + FIR_ORDER;
//int16_t *pMiddle = samples + FIR_CONV_LEN;

/*
 * Implementacion de ambos filtros FIR (14 Hz y 40 Hz de frecuencia de corte)
 * @param sample: 			// Muestra para ser filtrada
 * @param FIRSamples[2]:	// Array con 2 variables (la muestra de 14 Hz en [0] y 40 Hz en [1])
 */


void filterSampleFIR(int16_t sample, int32_t FIRSamples[2]) {
	
	 // Punteros de incio y final de convolucion
	int16_t *pI, *pJ;
	
	// Desplaza
	pI=samples;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;  *pI=*(pI+1); pI++;
	*pI=sample;
	
	// Convolucion en paralelo
	pI=samples; pJ=pLast;
	int32_t temp14 = 0;
	int32_t temp40 = 0;
	temp14+=((*pI)+(*pJ))*C14_0;   temp40+=((*pI)+(*pJ))*C40_0;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_1;   temp40+=((*pI)+(*pJ))*C40_1;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_2;   temp40+=((*pI)+(*pJ))*C40_2;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_3;   temp40+=((*pI)+(*pJ))*C40_3;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_4;   temp40+=((*pI)+(*pJ))*C40_4;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_5;   temp40+=((*pI)+(*pJ))*C40_5;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_6;   temp40+=((*pI)+(*pJ))*C40_6;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_7;   temp40+=((*pI)+(*pJ))*C40_7;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_8;   temp40+=((*pI)+(*pJ))*C40_8;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_9;   temp40+=((*pI)+(*pJ))*C40_9;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_10;  temp40+=((*pI)+(*pJ))*C40_10;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_11;  temp40+=((*pI)+(*pJ))*C40_11;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_12;  temp40+=((*pI)+(*pJ))*C40_12;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_13;  temp40+=((*pI)+(*pJ))*C40_13;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_14;  temp40+=((*pI)+(*pJ))*C40_14;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_15;  temp40+=((*pI)+(*pJ))*C40_15;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_16;  temp40+=((*pI)+(*pJ))*C40_16;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_17;  temp40+=((*pI)+(*pJ))*C40_17;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_18;  temp40+=((*pI)+(*pJ))*C40_18;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_19;  temp40+=((*pI)+(*pJ))*C40_19;  pI++;
	temp14+= (*pI)       *C14_20;  temp40+= (*pI)       *C40_20;
	FIRSamples[0] = (temp14 >> 9);	// Amplificada para las derivadas (aprox. x20) (">>9" es equivalente a "/512")
	FIRSamples[1] = (temp40 >> 10);	// Amplificada para el baseline (aprox. x10) (">>10" es equivalente a "/1024")
	
	return;

/*	
	// Convolucion por separado
	pI=samples; pJ=pLast;
	int32_t temp14 = 0;
	temp14+=((*pI)+(*pJ))*C14_0;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_1;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_2;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_3;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_4;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_5;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_6;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_7;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_8;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_9;   pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_10;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_11;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_12;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_13;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_14;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_15;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_16;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_17;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_18;  pI++; pJ--;
	temp14+=((*pI)+(*pJ))*C14_19;  pI++;
	temp14+= (*pI)       *C40_20;
	FIRSamples[0] = temp14 >> 9;	// Amplificada para las derivadas (aprox. x20) (">>9" es equivalente a "/512")
	
	pI=samples; pJ=pLast;
	int32_t temp40 = 0;
	temp40+=((*pI)+(*pJ))*C40_0;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_1;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_2;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_3;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_4;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_5;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_6;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_7;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_8;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_9;   pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_10;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_11;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_12;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_13;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_14;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_15;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_16;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_17;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_18;  pI++; pJ--;
	temp40+=((*pI)+(*pJ))*C40_19;  pI++;
	temp40+= (*pI)       *C40_20;
	FIRSamples[1] = temp40 >> 10;	// Amplificada para el baseline (aprox. x10) (">>10" es equivalente a "/1024")
	
	return;
*/	
}

/*
void filterSampleFIR(int16_t sample, int32_t FIRSamples[2]) {
	int16_t *pI, *pJ;
	
	//Desplaza
	for(pI=samples;pI!=pLast;pI++)
	  *pI=*(pI+1);
	*pI=sample;
	
	int16_t *pC14=COEFFS_14, *pC40=COEFFS_40;
	//int16_t temp14 = (*pMiddle)*(*(pC14+FIR_CONV_LEN-1));
	//int16_t temp40 = (*pMiddle)*(*(pC40+FIR_CONV_LEN-1));
	//NOTE podría ser de 16?
	int32_t temp14 = 0;
	int32_t temp40 = 0;
	for(pI=samples, pJ=pLast; pI!=pMiddle; pI++, pJ--, pC14++, pC40++){
	  temp14+=((*pI)+(*pJ))*(*pC14);
	  temp40+=((*pI)+(*pJ))*(*pC40);
	}
	temp14+= (*pMiddle)*(*pC14);
	temp40+= (*pMiddle)*(*pC40);
	
	FIRSamples[0] = temp14 >> 9;	// Amplificada para las derivadas (aprox. x20) (">>9" es equivalente a "/512")
	FIRSamples[1] = temp40 >> 10;	// Amplificada para el baseline (aprox. x10) (">>10" es equivalente a "/1024")
	return;

}*/

/*
void filterSampleFIR(int16_t sample, int32_t FIRSamples[2]) {
	int16_t *pI, *pJ;
	
	//Desplaza
	for(pI=samples;pI!=pLast;pI++)
	  *pI=*(pI+1);
	*pI=sample;
	
	int16_t *pC14=COEFFS_14;
	int32_t temp14 = 0;
	for(pI=samples, pJ=pLast; pI!=pMiddle; pI++, pJ--, pC14++){
	  temp14+=((*pI)+(*pJ))*(*pC14);
	}
	temp14+= (*pMiddle)*(*pC14);
	FIRSamples[0] = temp14 >> 9;	// Amplificada para las derivadas (aprox. x20) (">>9" es equivalente a "/512")
	
	int16_t *pC40=COEFFS_40;
	int32_t temp40 = 0;
	for(pI=samples, pJ=pLast; pI!=pMiddle; pI++, pJ--, pC40++){
	  temp40+=((*pI)+(*pJ))*(*pC40);
	}
	temp40+= (*pMiddle)*(*pC40);
	
	FIRSamples[1] = temp40 >> 10;	// Amplificada para el baseline (aprox. x10) (">>10" es equivalente a "/1024")
	return;

}
*/

/*
void filterSampleFIR(int16_t sample, int32_t FIRSamples[2]) {

	samples[buffIndex] = sample;

	int32_t temp14 = COEFFS_14[FIR_CONV_LEN - 1] * samples[(buffIndex + FIR_CONV_LEN - 1) & FIR_MASK];
	uint8_t i = 0;
	for (i = 0; i < FIR_CONV_LEN - 1; i++) {
		temp14 += COEFFS_14[i] * (samples[(buffIndex + i) & FIR_MASK] + samples[(buffIndex + FIR_ORDER - i) & FIR_MASK]);
	}
	FIRSamples[0] = temp14 >> 9;	// Amplificada para las derivadas (aprox. x20) (">>9" es equivalente a "/512")
	
	int32_t temp40 = COEFFS_40[FIR_CONV_LEN - 1] * samples[(buffIndex + FIR_CONV_LEN - 1) & FIR_MASK];
	uint8_t k = 0;
	for (k = 0; k < FIR_CONV_LEN - 1; k++) {
		temp40 += COEFFS_40[k] * (samples[(buffIndex + k) & FIR_MASK] + samples[(buffIndex + FIR_ORDER - k) & FIR_MASK]);
	}
	FIRSamples[1] = temp40 >> 10;	// Amplificada para el baseline (aprox. x10) (">>10" es equivalente a "/1024")
	
	if (buffIndex == 0) {
		buffIndex = FIR_BUFF_LEN - 1;
	} else {
		buffIndex--;
	}
	
	return;

}
*/
