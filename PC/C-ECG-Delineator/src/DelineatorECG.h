/*
 * DelineatorECG.h
 *
 *  Created on: 25/2/2015
 *      Author: Jose Manuel Bote
 */

#ifndef SRC_DELINEATORECG_H_
#define SRC_DELINEATORECG_H_

#include <stdint.h>

typedef uint8_t boolean;
#define TRUE	1
#define FALSE	0

#define F_SAMPLE			250		// Sampling rate en Hz

#define GROUP_FILT_DELAY	122		// Retardo de todos los filtros en conjunto

#define BUFF_LEN					1024									// Longitud del buffer de unos 4 segundos (sampling rate = 250 Hz)
#define BUFF_HALF_LEN				(BUFF_LEN / 2)							// Mitad de la longitud del buffer
#define MASK_BUFF_LEN				(BUFF_LEN - 1)							// Mascara para hacer el buffer circular
#define MASK_BUFF_HALF_LEN			(BUFF_HALF_LEN - 1)						// Mascara para hacer el buffer circular
#define FIRST_HALF_BUFF				0										// Primera mitad del buffer
#define SECOND_HALF_BUFF			1										// Segunda mitad del buffer
#define FIRST_HALF_BUFF_COMPLETED	(BUFF_HALF_LEN + GROUP_FILT_DELAY - 1)	// Primera mitad del buffer lleno (634)
#define SECOND_HALF_BUFF_COMPLETED	(GROUP_FILT_DELAY - 1)					// Segunda mitad del buffer lleno (122)

#define NUM_POINTS		8							// P (on, peak, end), Q peak, R peak, S peak, T (peak end)
#define MIN_RR_PEAKS	(F_SAMPLE / 4)				// Minima distancia entre picos R permitida (sampling rate / 4) (240 bpm)
#define MAX_RR_PEAKS	(BUFF_HALF_LEN - GROUP_FILT_DELAY)	// Maxima distancia entre picos R permitida (38.5 bpm)
#define NUM_WIN_R_TH	4							// Ventanas utilizadas para calcular el umbral para deteccion de pico R
#define DESP_WIN_R_TH	2							// Desplazamiento de bits para calcular el umbral para deteccion de pico R

#define T_Q_WAVE		((F_SAMPLE * 100) / 1000)	// Periodo maximo admitido (en num de muestras) para una onda Q (100 ms)
#define T_S_WAVE		((F_SAMPLE * 100) / 1000)	// Periodo maximo admitido (en num de muestras) para una onda S (100 ms)
#define	CORRECT_Q_ON	((F_SAMPLE * 5) / 1000)		// Correcion para el inicio de la onda Q de 20 ms
#define	CORRECT_S_END	((F_SAMPLE * 5) / 1000)		// Correcion para el final de la onda S de 20 ms

#define WINDOW_P_ON		50	// Inicio de la busqueda de la onda P (respecto de la posicion del pico R) 200 ms
#define WINDOW_P_OFF		25	// Fin de la busqueda de la onda P (respecto de la posicion del pico R) 100 ms
#define WINDOW_T_ON		50	// Inicio de la busqueda de la onda T (respecto de la posicion del pico R) 200 ms
#define WINDOW_T_OFF		100	// Fin de la busqueda de la onda T (respecto de la posicion del pico R) 400 ms

extern uint32_t ECGPoints[NUM_POINTS];

boolean storeSamples(int32_t FIRSamples[2], int16_t* BLSample, int16_t* OpeningPSample, int16_t* OpeningTSample);
void preProccess(uint8_t halfBuffer);
void delineateECGWaves(uint16_t startPos, uint16_t endPos);
void delineateSTWaves(uint16_t* startPos, uint16_t* endPos, uint8_t* overflow);
void delineatePQWaves(uint16_t* startPos, uint16_t* endPos, uint8_t* overflow);

#endif /* SRC_DELINEATORECG_H_ */
