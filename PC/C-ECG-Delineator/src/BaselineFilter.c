/*
 * Baselinefilter.c
 *
 *  Created on: 24/2/2015
 *      Author: Jose Manuel Bote
 */

#include "BaselineFilter.h"
#include "Utils.h"


// Baseline general de ECG
int16_t buffOE[BUFF_OPEN_LEN] = {0};	// OE: Opening Erosion
int16_t buffOD[BUFF_OPEN_LEN] = {0};	// OD: Opening Dilation
int16_t buffCD[BUFF_CLOSE_LEN] = {0};	// CD: Closing Dilation
int16_t buffCE[BUFF_CLOSE_LEN] = {0};	// CE: Closing Erosion

uint16_t buffIndexOpen = 0;
uint16_t buffIndexClose = 0;

int16_t minValOE = INT16_MAX;
int16_t maxValOD = INT16_MIN;
int16_t maxValCD = INT16_MIN;
int16_t minValCE = INT16_MAX;

uint16_t minPosOE = 0;
uint16_t maxPosOD = 0;
uint16_t maxPosCD = 0;
uint16_t minPosCE = 0;


// Opening para onda P
int16_t buffOE_P[BUFF_OPEN_P_LEN] = {0};	// OE: Opening Erosion
int16_t buffOD_P[BUFF_OPEN_P_LEN] = {0};	// OD: Opening Dilation

uint16_t buffIndexOpen_P = 0;

int16_t minValOE_P = INT16_MAX;
int16_t maxValOD_P = INT16_MIN;

uint16_t minPosOE_P = 0;
uint16_t maxPosOD_P = 0;


// Opening para onda T
int16_t buffOE_T[BUFF_OPEN_T_LEN] = {0};	// OE: Opening Erosion
int16_t buffOD_T[BUFF_OPEN_T_LEN] = {0};	// OD: Opening Dilation

uint16_t buffIndexOpen_T = 0;

int16_t minValOE_T = INT16_MAX;
int16_t maxValOD_T = INT16_MIN;

uint16_t minPosOE_T = 0;
uint16_t maxPosOD_T = 0;



/*
 * Calcula el baseline con las operaciones de opening y closing.
 * A su vez, cada una de ellas, se compone de erosion y dilation.
 * @param sample: 			Muestra para ser filtrada
 * @return minValCE:		Muestra filtrada
 */
int16_t filterSampleBL(int16_t sample) {

	erosion(sample, buffOE, BUFF_OPEN_LEN, buffIndexOpen, &minValOE, &minPosOE);
	dilation(minValOE, buffOD, BUFF_OPEN_LEN, buffIndexOpen, &maxValOD, &maxPosOD);
	dilation(maxValOD, buffCD, BUFF_CLOSE_LEN, buffIndexClose, &maxValCD, &maxPosCD);
	erosion(maxValCD, buffCE, BUFF_CLOSE_LEN, buffIndexClose, &minValCE, &minPosCE);

	buffIndexOpen++;
	if (buffIndexOpen == BUFF_OPEN_LEN) {
		buffIndexOpen = 0;
	}

	buffIndexClose++;
	if (buffIndexClose == BUFF_CLOSE_LEN) {
		buffIndexClose = 0;
	}

	return minValCE;

}


/*
 * Calcula el opening para la onda P.
 * @param sample: 			Muestra para ser filtrada
 * @return maxValOD_P:		Muestra filtrada
 */
int16_t openingP(int16_t sample) {

	erosion(sample, buffOE_P, BUFF_OPEN_P_LEN, buffIndexOpen_P, &minValOE_P, &minPosOE_P);
	dilation(minValOE_P, buffOD_P, BUFF_OPEN_P_LEN, buffIndexOpen_P, &maxValOD_P, &maxPosOD_P);

	buffIndexOpen_P++;
	if (buffIndexOpen_P == BUFF_OPEN_P_LEN) {
		buffIndexOpen_P = 0;
	}

	return maxValOD_P;

}


/*
 * Calcula el opening para la onda T.
 * @param sample: 			Muestra para ser filtrada
 * @return maxValOD_T:		Muestra filtrada
 */
int16_t openingT(int16_t sample) {

	erosion(sample, buffOE_T, BUFF_OPEN_T_LEN, buffIndexOpen_T, &minValOE_T, &minPosOE_T);
	dilation(minValOE_T, buffOD_T, BUFF_OPEN_T_LEN, buffIndexOpen_T, &maxValOD_T, &maxPosOD_T);

	buffIndexOpen_T++;
	if (buffIndexOpen_T == BUFF_OPEN_T_LEN) {
		buffIndexOpen_T = 0;
	}

	return maxValOD_T;

}


/*
 * Erosion.
 * @param sample: 		Muestra para ser filtrada
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *bufferIndex:	Indice para recorrer el buffer
 * @param *minVal:		Valor del minimo del buffer
 * @param *minPos:		Posicion del minimo del buffer
 */
void erosion(int16_t sample, int16_t buffer[], uint16_t bufferSize,
		uint16_t bufferIndex, int16_t* minVal, uint16_t* minPos) {

	buffer[bufferIndex] = sample;

	if (sample <= *minVal) {
		*minVal = sample;
		*minPos = bufferIndex;
	} else if (bufferIndex == *minPos) {
		findMinValPos16(buffer, bufferSize, minVal, minPos);
	}

	return;

}


/*
 * Dilation.
 * @param sample: 		Muestra para ser filtrada
 * @param buffer: 		Buffer de muestras
 * @param bufferSize: 	Tamaño del buffer
 * @param *bufferIndex:	Indice para recorrer el buffer
 * @param *maxVal:		Valor del maximo del buffer
 * @param *maxPos:		Posicion del maximo del buffer
 */
void dilation(int16_t sample, int16_t buffer[], uint16_t bufferSize,
		uint16_t bufferIndex, int16_t* maxVal, uint16_t* maxPos) {

	buffer[bufferIndex] = sample;

	if (sample >= *maxVal) {
		*maxVal = sample;
		*maxPos = bufferIndex;
	} else if (bufferIndex == *maxPos) {
		findMaxValPos16(buffer, bufferSize, maxVal, maxPos);
	}

	return;

}
