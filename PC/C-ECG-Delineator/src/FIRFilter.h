/*
 * FIRFilter.h
 *
 *  Created on: 23/2/2015
 *      Author: Jose Manuel Bote
 */

#ifndef SRC_FIRFILTER_H_
#define SRC_FIRFILTER_H_

#include <stdint.h>

#define FIR_ORDER			40
#define FIR_BUFF_LEN		64
#define FIR_CONV_LEN		(1 + (FIR_ORDER / 2))
#define FIR_MASK			(FIR_BUFF_LEN - 1)
#define FIR_FILTER_DELAY	(FIR_ORDER / 2)

void filterSampleFIR(int16_t sample, int32_t FIR_filt_samples[2]);

#endif /* SRC_FIRFILTER_H_ */
