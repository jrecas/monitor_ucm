package rpredictor;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

public class RPressurePredictor implements IRPressurePredictor {
	protected double ecgSampleRate = 250;
	protected double plethSampleRate = 75;
	protected int trainningSetSize = 0;
	protected String workingDirectory = "c:/experiments/abpDemo";
	protected RConnection connection;
	
	public RPressurePredictor()
	{
		try {
			connection = new RConnection();
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}
	
	public RPressurePredictor(String host)
	{
		try {
			connection = new RConnection(host);
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}
	
	public RPressurePredictor(String host, int port)
	{
		try {
			connection = new RConnection(host, port);
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}
	
	
	@Override
	public int addTrainningTuple(double[] ecg, double[] onsets, double[] rPeaks, double[] ends,
			double[] pleth, double systolic, double diastolic) {
		
		if (trainningSetSize == 0)
		{
			try {
				connection.assign("ecgSignal", ecg);
				connection.assign("onsets", onsets);
				connection.assign("rPeaks", rPeaks);
				connection.assign("ends", ends);
				connection.assign("plethSignal", pleth);
				double [] syst = {systolic};
				connection.assign("abpSyst", syst);
				double [] diast = {diastolic};
				connection.assign("abpDiast", diast);
			} catch (RserveException e) {
				e.printStackTrace();
				return -1;
			} catch (REngineException e) {
				e.printStackTrace();
				return -2;
			}
		}else{
			try {
				connection.assign("tmp", ecg);
				connection.eval("ecgSignal<-rbind(ecgSignal,tmp)");
				connection.assign("tmp", pleth);
				connection.eval("plethSignal<-rbind(plethSignal,tmp)");
				connection.assign("tmp", onsets);
				connection.eval("onsets<-rbind(onsets,tmp)");
				connection.assign("tmp", rPeaks);
				connection.eval("rPeaks<-rbind(rPeaks,tmp)");
				connection.assign("tmp", ends);
				connection.eval("ends<-rbind(ends,tmp)");
				double [] syst = {systolic};
				connection.assign("tmp", syst);
				connection.eval("abpSyst<-rbind(abpSyst,tmp)");
				double [] diast = {diastolic};
				connection.assign("tmp", diast);
				connection.eval("abpDiast<-rbind(abpDiast,tmp)");
			} catch (RserveException e) {
				e.printStackTrace();
				return -1;
		} catch (REngineException e) {
				e.printStackTrace();
				return -2;
			}
		}		
		return ++trainningSetSize;
	}

	@Override
	public double[] predict(double[] ecg, double[] onsets, double[] rPeaks, double[] ends,
			double[] pleth) throws RserveException, REXPMismatchException {
		try {
			connection.assign("pEcgSignal", ecg);
			connection.assign("pPlethSignal", pleth);
			connection.assign("ponsets", onsets);
			connection.assign("prPeaks", rPeaks);
			connection.assign("pends", ends);
		} catch (REngineException e) {
			e.printStackTrace();
		}

		return connection.eval("abpPredict(pEcgSignal, ponsets, prPeaks, pends, " +
				"ecgSampleRate, pPlethSignal, plethSampleRate, model, abpOffset)").asDoubles();
	}

	@Override
	public void setECGSampleRate(double rate) {
		ecgSampleRate = rate;
	}

	@Override
	public int train() {
		try {
			double[] tmpE = {ecgSampleRate};
			connection.assign("ecgSampleRate", tmpE);
			double[] tmpP = {plethSampleRate};
			connection.assign("plethSampleRate", tmpP);
			connection.eval(new String("setwd(\"") + workingDirectory + "\")");
			connection.eval("source(\"abpTrain.r\")");
			connection.eval("abpOffset=0.0");
			connection.eval("model<-abpTrain(ecgSignal, onsets, rPeaks, ends, " +
					"ecgSampleRate, plethSignal, plethSampleRate, abpSyst, abpDiast)");
		} catch (RserveException e) {
			e.printStackTrace();
			return -1;
		} catch (REngineException e) {
			e.printStackTrace();
			return -2;
		}
		return 1;
	}

	@Override
	public void reset() {
		try {
			connection.eval("rm(list=ls(all=TRUE))");
			trainningSetSize = 0;
		} catch (RserveException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void setWorkingDirectory(String path) {
		workingDirectory = path;
	}

	@Override
	public void calibrateABP(double[] ecg, double[] onsets, double[] rPeaks, double[] ends,
			double[] pleth, double systolic, double diastolic) {
		try {
			connection.assign("ecgSignalC", ecg);
			connection.assign("onsetsC", onsets);
			connection.assign("rPeaksC", rPeaks);
			connection.assign("endsC", ends);
			connection.assign("plethSignalC", pleth);
			double [] syst = {systolic};
			connection.assign("abpSystC", syst);
			double [] diast = {diastolic};
			connection.assign("abpDiastC", diast);
			double[] tmpE = {ecgSampleRate};
			connection.assign("ecgSampleRate", tmpE);
			double[] tmpP = {plethSampleRate};
			connection.assign("plethSampleRate", tmpP);
			connection.eval(new String("setwd(\"") + workingDirectory + "\")");
			connection.eval("source(\"abpTrain.r\")");
			connection.eval("abpOffset<-abpCalibrate(ecgSignalC, onsetsC, rPeaksC, endsC, " +
					"ecgSampleRate, plethSignalC, plethSampleRate, abpSystC, abpDiastC)");			
		} catch (RserveException e) {
			e.printStackTrace();
		} catch (REngineException e) {
			e.printStackTrace();
		}
		
	}

	@Override
	public void setPlethSampleRate(double rate) {
		plethSampleRate = rate;
	}
}
