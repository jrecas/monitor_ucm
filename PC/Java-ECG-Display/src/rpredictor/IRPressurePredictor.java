package rpredictor;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RserveException;

public interface IRPressurePredictor {
	// Reset the R environment, erasing all variables
	public void reset();
	
	// Set the ECG sample Rate
	public void setECGSampleRate(double rate);
	
	// Set the Pleth Sample Rate
	public void setPlethSampleRate(double rate);
	
	// Calibrate the ABP for the current person. Must be done once 
	public void calibrateABP(double[] ecg, double[] onsets, double[] rPeaks, double[] ends, 
			double[] pleth, double systolic, double diastolic);
	
	// Add a training tuple. Each tuple represents the association of given ECG and Pleth Signal periods
	// to a ABP value
	public int addTrainningTuple(double[] ecg, double[] onsets, double[] rPeaks, double[] ends,
			double[] pleth, double systolic, double diastolic);
	
	// Train the model using the added tuples
	public int train();
	
	// Predict the ABP for the period using the given ECG and Pleth Signals
	public double[] predict(double[] ecg,  double[] onsets, double[] rPeaks, double[] ends,
			double[] pleth) throws RserveException, REXPMismatchException;
	
	// set the working directory for the R environment
	public void setWorkingDirectory(String path);
}
