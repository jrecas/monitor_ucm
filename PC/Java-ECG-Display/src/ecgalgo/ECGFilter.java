/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecgalgo;

/**
 *
 * @author Fran
 */
public class ECGFilter {
    
    private static final int BUFFER_SIZE_FILTER = 1;
    
    //Sizes for the morphological filter
    private static final int TAMB0 = 50;
    private static final int TAMB0m = 25;
    private static final int TAMBc = 75;
    private static final int TAMBcm = 37;
    private static final int TAMB1 = 5;
    private static final int TAMB2 = 5;
    private static final int TAMBEnt = 150;
    private static final int TAMBFB = 5;
    private static final int TAMBFBm = 2;
    
    private int bufferEntrada[] = new int[BUFFER_SIZE_FILTER];
    private int bufferSalida[] = new int[BUFFER_SIZE_FILTER];
    
    //Numero de bytes leidos del archivo de entrada y almacenados en el buffer
    private int samplesRead;

    //Indice para el buffer de entrada
    private int actEntrada, actSalida;
    
    private int contador = 0;
    
    private int bufferEB0[] = new int[TAMB0];
    private int aBEB0 = TAMB0 - 1, pMinEB0 = 0, p2MinEB0 = -1;

    private int bufferDB0[] = new int[TAMB0];
    private int aBDB0 = TAMB0m, pMaxDB0 = 0, p2MaxDB0 = -1;

    private int bufferDBc[] = new int[TAMBc];
    private int aBDBc = TAMB0m, pMaxDBc = 0, p2MaxDBc = -1;

    private int bufferEBc[] = new int[TAMBc];
    private int aBEBc = TAMB0m, pMinEBc = 0, p2MinEBc = -1;

    private int bufferEnt[] = new int[TAMBEnt];
    private int aBEnt = 0, rBEnt = 0;

    //El vector B1 definido en la tecnica se despliega en memoria
    private int vaux, v2aux;

    private int firstPass = 1;
    
    private static final int ORDER_A = 0;
    private static final int ORDER_B = 32;
    private static final int ORDER = (ORDER_A > ORDER_B ? ORDER_A : ORDER_B);
    private static final int FILTERED_DATA_LENGTH = 128;
    private static final int BUFFER_SIZE_FILTFILT = (ORDER*2+FILTERED_DATA_LENGTH);
    
    //Filter coefficients
    private int a[] = {1};
    private int b[] = {-81,
        141,
        336,
        235,
        -371,
        -1091,
        -911,
        744,
        2737,
        2535,
        -1275,
        -6352,
        -6996,
        1559,
        18156,
        34847,
        41871,
        34847,
        18156,
        1559,
        -6996,
        -6352,
        -1275,
        2535,
        2737,
        744,
        -911,
        -1091,
        -371,
        235,
        336,
        141,
        -81};
    
    private int samplesQueue[] = new int[FILTERED_DATA_LENGTH];
    private int firstAvailableSample = 0;
    private int availableSamples = 0;
    private int filteredData[] = new int[FILTERED_DATA_LENGTH];
    private int availableFilteredData = 0;
    
    private int readDataBuffer[] = new int[FILTERED_DATA_LENGTH];
    private int queuedSamples[] = new int[FILTERED_DATA_LENGTH];
    private int availableQueuedSamples;
    private int aux[] = new int[BUFFER_SIZE_FILTFILT];
    private int aux2[] = new int[BUFFER_SIZE_FILTFILT];
    private int aux3[] = new int[BUFFER_SIZE_FILTFILT];
    private int first=0;//The output of the BW filter is stored in a circular buffer, this is the pointer to the first valid position
    private int filteredDataOffset = 0;
    private int availableBWFilteredData = 0;

    private int firstTime_removeBWSample = 0;
    private int i_removeBWSample=0;
    
    public ECGFilter(){
    
    }
    
    private void reset_variables() {
        aBEB0 = TAMB0 - 1;
        pMinEB0 = 0;
        p2MinEB0 = -1;
        aBDB0 = TAMB0m;
        pMaxDB0 = 0;
        p2MaxDB0 = -1;
        aBDBc = TAMB0m;
        pMaxDBc = 0;
        p2MaxDBc = -1;
        aBEBc = TAMB0m;
        pMinEBc = 0;
        p2MinEBc = -1;
        aBEnt = 0;
        rBEnt = 0;
        firstPass = 1;
    }
    
    private void initBuffers(int i, int value) {
	actEntrada = 0;

	//VICTOR: se incrementa el contador, al ser llamado este procedimiento para cada nuevo sample.
	if(i==0){
		reset_variables();
	}
	else{
		contador++;
	}
	samplesRead = 1;

	//Llenado inicial de los buffers
	//FRAN:morph --> change bufferEntrada[i] by 0
	bufferEB0[i] = value;
	if (value <= bufferEB0[pMinEB0]) {
		p2MinEB0 = pMinEB0;
		pMinEB0 = i;
	} else if (p2MinEB0 != -1 && value <= bufferEB0[p2MinEB0]) {
		p2MinEB0 = i;
	}

	if (i >= TAMB0m) 
            bufferEnt[aBEnt++] = value;
	else {
            bufferDB0[i] = value;			
            if (value >= bufferDB0[pMaxDB0]) {
                    p2MaxDB0 = pMaxDB0;
                    pMaxDB0 = i;
            } else if (value >= bufferDB0[p2MaxDB0])
                    p2MaxDB0 = i;

            bufferDBc[i] = value;
            if (value >= bufferDBc[pMaxDBc]) {
                    p2MaxDBc = pMaxDBc;
                    pMaxDBc = i;
            } else if (value >= bufferDBc[p2MaxDBc])
                    p2MaxDBc = i;

            bufferEBc[i] = value;
            if (value <= bufferEBc[pMinEBc]) {
                    if (pMinEBc != 0)
                            p2MinEBc = pMinEBc;
                    pMinEBc = i;
            } else if (p2MinEBc != -1 && value <= bufferEBc[p2MinEBc])
                    p2MinEBc = i;
	}
	bufferSalida[0] = 0;
    }
    
    private void fill_in_filter(int entrada[]) {
        
	for(int i=0; i<BUFFER_SIZE_FILTER; i++){
            bufferEntrada[i] = entrada[i];
	}
	actEntrada = 0;

	samplesRead = BUFFER_SIZE_FILTER;
	if (contador < (TAMB0*3) + (TAMB0/2) - (TAMBcm - TAMB0m) - 3){
            contador++;
        }
    }
    
    private void storeOutput(int v) {
	bufferSalida[0] = v;
    }
    
    private void proceso() {

	while (actEntrada < samplesRead) {

		if (aBEB0 == pMinEB0) {
			if (bufferEntrada[actEntrada] > bufferEB0[pMinEB0]) {
				if (p2MinEB0 == -1) {
					bufferEB0[aBEB0] = bufferEntrada[actEntrada];

					v2aux = Integer.MAX_VALUE;
					vaux = bufferEB0[TAMB0 - 1];
					pMinEB0 = TAMB0 - 1;
					for (int i = TAMB0 - 2; i >= 0; i--) { 
						if (bufferEB0[i] < vaux) {
							v2aux = vaux;
							vaux = bufferEB0[i];
							p2MinEB0 = pMinEB0;
							pMinEB0 = i;
						} else if (bufferEB0[i] < v2aux) {
							v2aux = bufferEB0[i];
							p2MinEB0 = i;
						}
					}
				} else if (bufferEntrada[actEntrada] > bufferEB0[p2MinEB0]) {
					pMinEB0 = p2MinEB0;
					p2MinEB0 = -1;
				}
			}
		} else if (bufferEntrada[actEntrada] <= bufferEB0[pMinEB0]) {
			p2MinEB0 = pMinEB0;
			pMinEB0 = aBEB0;
		} else if (aBEB0 == p2MinEB0) {
			if (bufferEntrada[actEntrada] > bufferEB0[p2MinEB0]) {
				p2MinEB0 = -1;
			}
		} else if (p2MinEB0 != -1 && bufferEntrada[actEntrada] <= bufferEB0[p2MinEB0])
			p2MinEB0 = aBEB0;

		bufferEnt[aBEnt] = bufferEntrada[actEntrada];
	
		if (aBEnt != TAMBEnt - 1)
			aBEnt++;
		else
			aBEnt = 0;

		bufferEB0[aBEB0] = bufferEntrada[actEntrada++];
		 
		if (aBEB0 != TAMB0 - 1)
			aBEB0++;
		else
			aBEB0 = 0;

		if (aBDB0 == pMaxDB0) {
			if (bufferEB0[pMinEB0] < bufferDB0[pMaxDB0]) {
				if (p2MaxDB0 == -1) {
					bufferDB0[aBDB0] = bufferEB0[pMinEB0];

					v2aux = Integer.MIN_VALUE;
					vaux = bufferDB0[TAMB0 - 1];
					pMaxDB0 = TAMB0 - 1;
					for (int i = TAMB0 - 2; i >= 0; i--) {
						if (bufferDB0[i] > vaux) {
							v2aux = vaux;
							vaux = bufferDB0[i];
							p2MaxDB0 = pMaxDB0;
							pMaxDB0 = i;
						} else if (bufferDB0[i] > v2aux) {
							v2aux = bufferDB0[i];
							p2MaxDB0 = i;
						}
					}
				} else if (bufferEB0[pMinEB0] < bufferDB0[p2MaxDB0]) {
					pMaxDB0 = p2MaxDB0;
					p2MaxDB0 = -1;
				}
			}
		} else if (bufferEB0[pMinEB0] >= bufferDB0[pMaxDB0]) {
			p2MaxDB0 = pMaxDB0;
			pMaxDB0 = aBDB0;
		} else if (aBDB0 == p2MaxDB0) {
			if (bufferEB0[pMinEB0] < bufferDB0[p2MaxDB0]) {
				p2MaxDB0 = -1;
			}
		} else if (p2MaxDB0 != -1 && bufferEB0[pMinEB0] >= bufferDB0[p2MaxDB0])
			p2MaxDB0 = aBDB0;

		bufferDB0[aBDB0] = bufferEB0[pMinEB0];

		if (aBDB0 != TAMB0 - 1){
			aBDB0++;
		}
		else{
			aBDB0 = 0;
		}

		//Paso de datos al buffer DBc si se ha llenado el buffer DB0.
//			if (!firstPass || actEntrada >= TAMB0 + (TAMB0/2) - 1) { // 74
		//VICTOR: he cambiado esto
		if (contador >= TAMB0 + (TAMB0/2) - 1) {

			if (aBDBc == pMaxDBc) {
				if (bufferDB0[pMaxDB0] < bufferDBc[pMaxDBc]) {
					if (p2MaxDBc == -1) {
						bufferDBc[aBDBc] = bufferDB0[pMaxDB0];

						v2aux = Integer.MIN_VALUE;
						vaux = bufferDBc[TAMBc - 1];
						pMaxDBc = TAMBc - 1;
						for (int i = TAMBc - 2; i >= 0; i--) {
							if (bufferDBc[i] > vaux) {
								v2aux = vaux;
								vaux = bufferDBc[i];
								p2MaxDBc = pMaxDBc;
								pMaxDBc = i;
							} else if (bufferDBc[i] > v2aux) {
								v2aux = bufferDBc[i];
								p2MaxDBc = i;
							}
						}
					} else if (bufferDB0[pMaxDB0] < bufferDBc[p2MaxDBc]) {
						pMaxDBc = p2MaxDBc;
						p2MaxDBc = -1;
					}
				}
			} else if (bufferDB0[pMaxDB0] >= bufferDBc[pMaxDBc]) {
				p2MaxDBc = pMaxDBc;
				pMaxDBc = aBDBc;
			} else if (aBDBc == p2MaxDBc) {
				if (bufferDB0[pMaxDB0] < bufferDBc[p2MaxDBc]) {
					p2MaxDBc = -1;
				}
			} else if (p2MaxDBc != -1 && bufferDB0[pMaxDB0] >= bufferDBc[p2MaxDBc])
				p2MaxDBc = aBDBc;

			bufferDBc[aBDBc] = bufferDB0[pMaxDB0];
		
			if (aBDBc != TAMBc - 1)
				aBDBc++;
			else
				aBDBc = 0;
		
			//Paso de datos al buffer EBc si se ha llenado DBc
			//if (!firstPass || actEntrada >= (TAMB0*2) + (TAMB0/2) - 2) { //123
			//VICTOR: he cambiado esto.
			if (contador >= (TAMB0*2) + (TAMB0/2) - 2) {

				if (aBEBc == pMinEBc) {
					if (bufferDBc[pMaxDBc] > bufferEBc[pMinEBc]) {
						if (p2MinEBc == -1) {
							bufferEBc[aBEBc] = bufferDBc[pMaxDBc];

							v2aux = Integer.MAX_VALUE;
							vaux = bufferEBc[TAMBc - 1];
							pMinEBc = TAMBc - 1;
							for (int i = TAMBc - 2; i >= 0; i--) {
								if (bufferEBc[i] < vaux) {
									v2aux = vaux;
									vaux = bufferEBc[i];
									p2MinEBc = pMinEBc;
									pMinEBc = i;
								} else if (bufferEBc[i] < v2aux) {
									v2aux = bufferEBc[i];
									p2MinEBc = i;
								}
							}
						} else if (bufferDBc[pMaxDBc] > bufferEBc[p2MinEBc]) {
							pMinEBc = p2MinEBc;
							p2MinEBc = -1;
						}
					}
				} else if (bufferDBc[pMaxDBc] <= bufferEBc[pMinEBc]) {
					p2MinEBc = pMinEBc;
					pMinEBc = aBEBc;
				} else if (aBEBc == p2MinEBc) {
					if (bufferDBc[pMaxDBc] > bufferEBc[p2MinEBc]) {
						p2MinEBc = -1;
					}
				} else if (p2MinEBc != -1 && bufferDBc[pMaxDBc] <= bufferEBc[p2MinEBc])
					p2MinEBc = aBEBc;

				bufferEBc[aBEBc] = bufferDBc[pMaxDBc];
		
				if (aBEBc != TAMBc - 1)
					aBEBc++;
				else
					aBEBc = 0;
			
				//Paso de datos al buffer FBc1 si se ha llenado EBc
				//if (!firstPass || actEntrada >= (TAMB0*3) + (TAMB0/2) - (TAMBcm - TAMB0m) - 3) {
				//VICTOR: he cambiado esto.
				if (contador >= (TAMB0*3) + (TAMB0/2) - (TAMBcm - TAMB0m) - 3) {
					storeOutput(bufferEnt[rBEnt] - bufferEBc[pMinEBc]);

					if (rBEnt != TAMBEnt - 1)
						rBEnt++;
					else
						rBEnt = 0;
				}
			
			//Si no esta lleno el bufferDBc
			//Llenamos el bufferEBc con los valores de DBc en la diferencia
			} else if (aBDBc <= TAMBcm) {

				storeOutput(bufferEnt[rBEnt] - bufferDBc[aBDBc - 1]);

				if (rBEnt != TAMBEnt - 1)
					rBEnt++;
				else
					rBEnt = 0;
		
				if (bufferDBc[aBDBc - 1] <= bufferEBc[pMinEBc]) {
					p2MinEBc = pMinEBc;
					pMinEBc = aBEBc;
				} else if (p2MinEBc != -1 && bufferDBc[aBDBc - 1] <= bufferEBc[p2MinEBc])
					p2MinEBc = aBEBc;

				bufferEBc[aBEBc] = bufferDBc[aBDBc - 1];
				aBEBc++;
			}
		}
	}//while
    }
    
    private int removeBWSample(int inputSample){
	int read_data[] = new int[BUFFER_SIZE_FILTER];
	int read_data2;

	if (firstTime_removeBWSample == 0){
		if (i_removeBWSample < TAMB0){
			read_data2 = inputSample;
			initBuffers(i_removeBWSample, read_data2);
			i_removeBWSample++;
		}
		else{
			firstTime_removeBWSample = 1;
		}
	}

	read_data[0] = inputSample;
	fill_in_filter(read_data);
	proceso();

	return bufferSalida[0];
    }
    
    private int[] filter(int ordA, int ordB, int a[], int b[], int x[], int length){
	int ord = (ordA>ordB) ? ordA : ordB;
        int y[] = new int[length];

	y[0]=b[0]*x[0]/131072;
	for (int i=1;i<ord+1;i++){
        	y[i]=0;
        	for (int j=0;(j<i+1) & (j<ordB+1);j++){
			//printf("b=%d x=%d\n", b[j],x[i-j]);
        		y[i]=y[i]+b[j]*x[i-j]/131072;
		}
		for (int j=0;(j<i) & (j<ordA);j++){
			y[i]=y[i]-a[j+1]*y[i-j-1];
		}
	}
	
	for (int i=ord+1;i<length;i++){
		y[i]=0;
		for (int j=0;j<ordB+1;j++){
			//printf("b=%d x=%d\n", b[j],x[i-j]);
			y[i]=y[i]+b[j]*x[i-j]/131072;
		}
		for (int j=0;j<ordA;j++){
			y[i]=y[i]-a[j+1]*y[i-j-1];
		}
	}
        
        return y.clone();
    }
    
    private int[] filtfilt(int ordA, int ordB, int a[], int b[], int x[], int length){
        int xAux[] = new int[length];
        int yAux[] = new int[length];
        
        for (int i=0;i<length;i++){
            xAux[i] = x[i];
        }

	yAux = filter(ordA,ordB,a,b,xAux,length);
	
	// reverse the series for FILTFILT
	for (int i=0;i<length;i++){
		xAux[i] = yAux[length-i-1];
	}
	// do FILTER again
	yAux = filter(ordA,ordB,a,b,xAux,length);
	// reverse the series back
	for (int i=0;i<length;i++){
		xAux[i] = yAux[length-i-1];
	}
	for (int i=0;i<length;i++){
		yAux[i]=xAux[i];
	}
        
        return yAux.clone();
    }
    
    private int removeBWSampleDumb(int sample){
        return sample;
    }
    
    public int filterSample(int sample){
        samplesQueue[(firstAvailableSample + availableSamples) % FILTERED_DATA_LENGTH] = sample;
        if(availableSamples<128){
            availableSamples++;
        }
        else{
            firstAvailableSample = (firstAvailableSample + 1) % FILTERED_DATA_LENGTH;
        }
        while(availableFilteredData == 0){
            if (filteredDataOffset >= 140) {
                aux[first] = removeBWSample(samplesQueue[firstAvailableSample]);
                firstAvailableSample = (firstAvailableSample + 1) % FILTERED_DATA_LENGTH;
                availableSamples--;
                availableBWFilteredData++;
                //High-pass filter
                if (filteredDataOffset == (140 + BUFFER_SIZE_FILTFILT)) {
                    for (int j = 0; j < BUFFER_SIZE_FILTFILT; j++) {
                        aux2[j] = aux[j];
                    }
                    aux3 = filtfilt(ORDER_A, ORDER_B, a, b, aux2, BUFFER_SIZE_FILTFILT);
                    availableBWFilteredData = 0;
                    for (int k = 0; k < FILTERED_DATA_LENGTH; k++) {
                        filteredData[k] = aux3[ORDER + k];
                    }
                    availableFilteredData = FILTERED_DATA_LENGTH;
                } else if (filteredDataOffset > (140 + BUFFER_SIZE_FILTFILT)) {
                    if (availableBWFilteredData == FILTERED_DATA_LENGTH) {
                        for (int j = 0; j < BUFFER_SIZE_FILTFILT; j++) {
                            aux2[j] = aux[(first + j) % BUFFER_SIZE_FILTFILT];
                        }
                        aux3 = filtfilt(ORDER_A, ORDER_B, a, b, aux2, BUFFER_SIZE_FILTFILT);
                        availableBWFilteredData = 0;
                        for (int k = 0; k < FILTERED_DATA_LENGTH; k++) {
                            filteredData[k] = aux3[ORDER + k];
                        }
                        availableFilteredData = FILTERED_DATA_LENGTH;
                    }
                } else if (filteredDataOffset <= (140 + BUFFER_SIZE_FILTFILT - FILTERED_DATA_LENGTH)) {
                    //IN(offset) = (data_t)atoi(buffer)*multiplier;
                    filteredData[FILTERED_DATA_LENGTH - 1] = 0;
                    availableFilteredData = 1;
                }
                first = (first + 1) % BUFFER_SIZE_FILTFILT;
            }
            else {
                //IN(offset) = (data_t)atoi(buffer)*multiplier;
                filteredData[FILTERED_DATA_LENGTH - 1] = 0;
                availableFilteredData = 1;
            }
            filteredDataOffset++;
        }
        //IN(offset) = filteredData[FILTERED_DATA_LENGTH - availableFilteredData];
        int filteredSample;
        if(availableFilteredData > 0){
            filteredSample = filteredData[FILTERED_DATA_LENGTH - availableFilteredData];
            availableFilteredData--;
            //System.out.println("Filtered sample - offset=" + filteredDataOffset);
        }
        else{
            filteredSample = 0;
            //System.out.println("No filtered sample available - offset=" + filteredDataOffset);
        }
        return filteredSample;
    }
}
