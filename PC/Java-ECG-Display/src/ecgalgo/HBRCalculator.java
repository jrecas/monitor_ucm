/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecgalgo;

/**
 *
 * @author Fran
 */
public class HBRCalculator {
    
    private static final int WINDOW_SIZE = 10;
    private int window[];
    private int index;
    private boolean ready;
    
    public HBRCalculator(){
        window = new int[WINDOW_SIZE];
        index = 0;
        ready = false;
    }
    
    public int updateHBR(int offset){
        window[index++] = offset;
        if(index==WINDOW_SIZE){
            ready = true;
            index = 0;
        }
        if(ready){
            return 60*250/((window[(index+WINDOW_SIZE-1)%WINDOW_SIZE]-window[index])/(WINDOW_SIZE-1));
        }
        else{
            return 0;
        }
    }
    
}
