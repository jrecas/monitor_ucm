/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecgalgo;

import ecgdisplay.*;

import java.lang.Math;

/*
 *
 * @author Fran
 */
public class ECGDelineator {

    private ComDevice device;
    boolean firstTime;
    private static final int BUFFER_SIZE = 512;
    private static final int H_SIZE = 32;
    private static final int EPS_WINDOW_SIZE = 8;
    private int bufferedSamples[]; //Samples ready to be delineated
    private int bufferedSamplesPointer; //Pointer to the first available sample
    private int bufferedSamplesCount; //Counts how many samples are buffered
    private int in_data[];
    private int h_data[][];	// Note: Starting from 0, i.e. h_1 is at h_data[0]
    private int q_data[][];	// Note: Starting from 0
    private int eps_data[];		// Starting from 0
    private int eps_data_int[][];
    private int offset; // Last valid data real offset (i.e. non-circular)
    private int last_offset = Integer.MAX_VALUE; // Last non-circular offset (EOF)
    private int last_r; // Last detected R peak.
    private int this_r; // Currently detected R peak.
    private int rr = 150; // Averaged detected RR interval, starting with 100bpm as default value
    private int n_valid = 0;
    private int this_qrs_end; // Last end of QRS complex (for blanking)
    private int this_t_end; // Last end of T complex (for blanking as well)
    private int pr1 = 0; // Averaged interval between P (1st modmax) and R, to improve P detection.
    private int pr2 = 0; // Averaged interval between P (2nd modmax) and R, to improve P detection.
    private int epsilon_t, epsilon_p;
    private int epsilon_t_5, epsilon_p_5;
    // Current correct (=non-isolated, non-redundant) maximum and minimum (4) [Li])
    // NB: I don't really really know how to behave if more than 3 points are found
    // within 120ms anyway, so I hardcode the value to 2, and try to do something clever
    private int ok_maxmin[][]; // [.][0] is unused
    private int ok_maxmin_valid = 0; // Number of valid ok_maxmin items
    private int searchback_r = 0;
    //Variables from li2
    int modmax[];
    int until;
    int searchback;
    //End variables from li2
    private static final int delay[] = {0, 0, 2, 6, 14, 30};
    private static final int RR_MOVING_AVERAGE = 4;
    private static final int LI2_BLANKING = 60;
    private static final int LI_2_NEIGHBOUR = 10;
    private static final int DATA_MULTIPLIER = 8;
    private static final int LI_4_NEIGHBOUR = 60; // (160 [ms]). Also used for Gamma.
    private static final int LI_4_NEIGHBOUR_BEFORE = 20; // (80 [ms]). Also used for Gamma.
    private static final double GAMMA_QRS_PRE = 0.06; // 0.06
    private static final double GAMMA_QRS_POST_1 = 0.2; // 0.20
    private static final int Q2_MAXMOD_NR_MAX = 16;
    private static final double EPSILON_P = 0.12; //[Martinez] says 0.02, we take 0.12
    private static final double EPSILON_T = 0.25; //[Martinez] says 0.25...
    private static final int PQ_MAX_INTERVAL = 100; //(340ms)
    private static final int PQ_MIN_INTERVAL = 1; // (4ms)
    private static final double P_COEFF_PR1 = 1.5;
    private static final double P_COEFF_PR2 = 0.6;
    private static final int P_COEFF_PR1_WIDE = 2;
    private static final double P_COEFF_PR2_WIDE = 0.5;
    private static final double P_T_GAMMA = 0.125; //0.125
    private static final double P_XI_ON = 0.5; // 0.5
    private static final double P_XI_END = 0.9; //0.9
    private static final double T_XI_ON = 0.25; //0.25
    private static final double T_XI_END = 0.4; //0.4
    private static final int Q4_MAXMOD_NR_MAX = 16;
    private static final int t_end_rr[] = {375, 300, 250, 214, 188, 167, 150, 125, 100, 83, 75};
    private static final int t_end_qtc_min[] = {830, 769, 708, 668, 648, 607, 567, 526, 465, 425, 405};
    private static final int t_end_qtc_max[] = {1032, 931, 870, 830, 789, 729, 688, 648, 567, 506, 486};
    private static final double QTC_MAX_RATIO = 1.4;
    private static final double QTC_MAX_RATIO_BIS = 1.4;
    private static final double QTC_MAX_RATIO_5 = 1.4;

    public ECGDelineator(ComDevice device) {
        this.device = device;
        firstTime = true;

        bufferedSamples = new int[BUFFER_SIZE];
        bufferedSamplesPointer = 0;
        bufferedSamplesCount = 0;

        in_data = new int[BUFFER_SIZE];
        h_data = new int[4][H_SIZE];
        q_data = new int[5][BUFFER_SIZE];
        eps_data = new int[4];
        eps_data_int = new int[4][EPS_WINDOW_SIZE];
        ok_maxmin = new int[2][5];

        modmax = new int[5];
        searchback = 0;
        ok_maxmin_valid = 0;
        for (int j = 0; j < 2; j++) {
            for (int i = 1; i < 5; i++) {
                ok_maxmin[j][i] = 0;
            }
        }

        modmax[4] = delay[4];
        
        //FRAN: probably, apart from modifying this, I need to include something else, because when I press
        //the button "Reset Delineation", the delineation stops
        this.offset = 0;
        this.last_r = 0;
        this.this_r = 0;
    }

    public void newSample(int sample) {
        bufferedSamples[(bufferedSamplesPointer + bufferedSamplesCount) % BUFFER_SIZE] = sample;
        bufferedSamplesCount++;
        if (bufferedSamplesCount == BUFFER_SIZE) {
            if(firstTime){
                fill_until(BUFFER_SIZE);
                firstTime = false;
            }
            else{
                while(bufferedSamplesCount == BUFFER_SIZE) {
                    li2_iteration();
                }
            }
        }
    }

    private int fetchdata() {
        if (bufferedSamplesCount > 0) {
            in_data[offset % BUFFER_SIZE] = bufferedSamples[bufferedSamplesPointer];
            bufferedSamplesPointer = (bufferedSamplesPointer + 1) % BUFFER_SIZE;
            bufferedSamplesCount--;
            return 1;
        } else {
            return 0;
        }
    }

    private void prefetch(int i) {
        if (i + 2 >= offset && i < last_offset) {
            fill_until(i + 2);
        }
    }

    private int highpass(int d1, int d2) {
        return 2 * (d1 - d2);
    }

    private int lowpass(int d1, int d2, int d3, int d4) {
        return (d1 + 3 * d2 + 3 * d3 + d4) / 8;
    }

    private void filter(int i) {
        if (i >= 24) {
            q_data[0][i % BUFFER_SIZE] = highpass(in_data[i % BUFFER_SIZE], in_data[(i - 1) % BUFFER_SIZE]); // Delay: 0.5
            h_data[0][i % H_SIZE] = lowpass(in_data[i % BUFFER_SIZE], in_data[(i - 1) % BUFFER_SIZE], in_data[(i - 2) % BUFFER_SIZE], in_data[(i - 3) % BUFFER_SIZE]); // Delay: 1.5
            q_data[1][i % BUFFER_SIZE] = highpass(h_data[0][i % H_SIZE], h_data[0][(i - 2) % H_SIZE]); // Delay: 1.5+1=2.5
            h_data[1][i % H_SIZE] = lowpass(h_data[0][i % H_SIZE], h_data[0][(i - 2) % H_SIZE], h_data[0][(i - 4) % H_SIZE], h_data[0][(i - 6) % H_SIZE]); // Delay: 1.5+3=4.5
            q_data[2][i % BUFFER_SIZE] = highpass(h_data[1][i % H_SIZE], h_data[1][(i - 4) % H_SIZE]); // Delay: 4.5+2=6.5
            h_data[2][i % H_SIZE] = lowpass(h_data[1][i % H_SIZE], h_data[1][(i - 4) % H_SIZE], h_data[1][(i - 8) % H_SIZE], h_data[1][(i - 12) % H_SIZE]); // Delay: 4.5+6=10.5
            q_data[3][i % BUFFER_SIZE] = highpass(h_data[2][i % H_SIZE], h_data[2][(i - 8) % H_SIZE]); // Delay: 10.5+4=14.5
            h_data[3][i % H_SIZE] = lowpass(h_data[2][i % H_SIZE], h_data[2][(i - 8) % H_SIZE], h_data[2][(i - 16) % H_SIZE], h_data[2][(i - 24) % H_SIZE]); // Delay: 10.5+12 = 22.5
            q_data[4][i % BUFFER_SIZE] = highpass(h_data[3][i % H_SIZE], h_data[3][(i - 16) % H_SIZE]); // Delay: 22.5+8=30.5
        }
    }

    private int rms(int scale, int offset, int asize) {
        int val = 0;
        int out;
        int i;

        /* TODO: Check overflows! */
        for (i = offset; i < asize; i++) {
            val += q_data[scale - 1][i % BUFFER_SIZE] * q_data[scale - 1][i % BUFFER_SIZE];
        }

        //FRAN: probably it would be better to return a double
        out = (int) Math.sqrt(val / (asize - offset));
        return out;
    }

    private void fill_until(int until) {
        int i;

        while (offset < until) {
            if (fetchdata() == 0) {
                if (last_offset != offset) {
                    last_offset = offset;
                }
                break;
            }

            filter(offset);
            offset++;

            if (offset % BUFFER_SIZE == 0) {
                eps_data[3] = eps_data[3] * 2;
                /* epsilon 1-4 */
                for (i = 1; i <= 4; i++) {
                    int neweps;
                    if (i == 4) {
                        eps_data[i - 1] = eps_data[i - 1] * 2;
                    }
                    neweps = rms(i, offset == BUFFER_SIZE ? delay[i] : offset - BUFFER_SIZE, offset);

                    if (offset == BUFFER_SIZE) { //First data, fill the window with the same value
                        eps_data[i - 1] = neweps;
                    } else {
                        eps_data[i - 1] = (int) Math.sqrt((eps_data[i - 1] * eps_data[i - 1] * (offset - BUFFER_SIZE) + neweps * neweps * BUFFER_SIZE) / offset);
                    }
                }
                eps_data[3] = eps_data[3] / 2;
            }
        }
    }

    private void add_r(int r) {
        last_r = this_r;
        this_r = r;

        int newr = this_r - last_r;
        if (newr > rr * 2) {
            newr = rr * 2; //Fixup for sel232
        }
        if (newr < rr / 2) {
            newr = rr / 2;
        }

        if (n_valid > 0) {
            rr = ((n_valid - 1) * rr + newr) / n_valid;
        }

        if (n_valid < RR_MOVING_AVERAGE) {
            n_valid++;
        }

        if (rr < 50) //Max HBR=300 bpm = 5 beats per second
        {
            rr = 50;
        }

        if (rr > 750) //Min HBR=20 bpm = 1 beat every 3 seconds
        {
            rr = 750;
        }

        this_qrs_end = r;
    }

    private void compute_epsilon_pt() {
        //FRAN: have a look at this: Using the existing EPS(4) doesn't make much difference. (Normally we should recompute the RMS)
        /*epsilon_t = EPS(4)*2*EPSILON_T;
        epsilon_p = EPS(4)*2*EPSILON_P;*/
        epsilon_p = 0;
        epsilon_t = 0;
        epsilon_p_5 = 0;
        epsilon_t_5 = 0;
    }

    private void compute_epsilon_pt_5() {
        /*if (epsilon_t_5 == 0) { // Compute when needed
        int from = off_max(last_r, off_sub(offset, BUFFER_SIZE));
        int rms_rr = rms(5, from, this_r);
        epsilon_t_5 = rms_rr*EPSILON_T;
        epsilon_p_5 = rms_rr*EPSILON_P;
        }*/
        epsilon_p_5 = 0;
        epsilon_t_5 = 0;
    }

    /* Return: 0 if no modmax was found, 1 if it was found in out[0]
     * value in out[1]*/
    private int[] findmodmax(int scale, int offset, int asize, double threshold) {
        int out[] = new int[2];

        prefetch(offset + 2);
        if (offset + 2 >= last_offset || offset > asize) {
            out[0] = 0;
            return out;
        }

        //Current trend, 1 = increasing, 0 = decreasing
        int trend = q_data[scale - 1][(offset + 1) % BUFFER_SIZE] > q_data[scale - 1][offset % BUFFER_SIZE] ? 1 : 0;
        int i;
        int qi;
        int qi_1;

        qi = q_data[scale - 1][(offset + 1) % BUFFER_SIZE];
        for (i = offset + 2; i < asize && i < last_offset; i++) {
            prefetch(i);
            qi_1 = qi;
            qi = q_data[scale - 1][i % BUFFER_SIZE];
            if (qi > qi_1) {
                if (trend == 0 && -qi_1 >= threshold) {
                    out[1] = i - 1;
                    out[0] = 1;
                    return out;
                }
                trend = 1;
            } else {
                if (trend != 0 && qi_1 >= threshold) {
                    out[1] = i - 1;
                    out[0] = 1;
                    return out;
                }
                trend = 0;
            }
        }

        out[0] = 0;
        return out; //No mod max found in the range
    }

    private int abs(int x) {
        return x > 0 ? x : -x;
    }

    //max = 1, find max, else find min
    private int get_minmax(int scale, int from, int to, int max) {
        int i;
        int max_q = from;

        for (i = from + 1; i <= to && i < last_offset; i++) {
            prefetch(i);
            if (max == 1) {
                if (abs(q_data[scale - 1][i % BUFFER_SIZE]) > abs(q_data[scale - 1][max_q % BUFFER_SIZE])) {
                    max_q = i;
                }
            } else {
                if (abs(q_data[scale - 1][i % BUFFER_SIZE]) < abs(q_data[scale - 1][max_q % BUFFER_SIZE])) {
                    max_q = i;
                }
            }
        }

        return max_q;
    }

    //Returns one-two, but checks we do not go below 0.
    private int sub(int one, int two) {
        return one > two ? one - two : 0;
    }

    private int diff(int one, int two) {
        if (one > two) {
            return one - two;
        } else {
            return two - one;
        }
    }

    private int sign(int x) {
        return x > 0 ? 1 : 0;
    }

    private int min(int x, int y) {
        return x > y ? y : x;
    }

    private int max(int x, int y) {
        return x > y ? x : y;
    }

    /* Return: 0 if not found, 1 if found in out[0]
     * value in out[1]*/
    private int[] detect_zero_crossing(int scale, int from, int to) {
        int i;
        int out[] = new int[2];
        /* If from is bigger, start from there, otherwise, start from to. */
        if (abs(q_data[scale - 1][(from + BUFFER_SIZE) % BUFFER_SIZE]) > abs(q_data[scale - 1][(to + BUFFER_SIZE) % BUFFER_SIZE])) {//FRAN: check this (out of bounds (-8))
            for (i = from; i < to; i++) {
                if (sign(q_data[scale - 1][(i + BUFFER_SIZE) % BUFFER_SIZE]) != sign(q_data[scale - 1][(i + BUFFER_SIZE + 1) % BUFFER_SIZE])) {
                    if (abs(q_data[scale - 1][(i + BUFFER_SIZE) % BUFFER_SIZE]) < abs(q_data[scale - 1][(i + BUFFER_SIZE + 1) % BUFFER_SIZE])) {
                        out[1] = i - delay[scale];
                    } else {
                        out[1] = i + 1 - delay[scale];
                    }
                    out[0] = 1;
                    return out;
                }
            }
        } else {
            for (i = to - 1; i >= from; i--) {
                if (sign(q_data[scale - 1][(i + BUFFER_SIZE) % BUFFER_SIZE]) != sign(q_data[scale - 1][(i + BUFFER_SIZE + 1) % BUFFER_SIZE])) {
                    if (abs(q_data[scale - 1][(i + BUFFER_SIZE) % BUFFER_SIZE]) < abs(q_data[scale - 1][(i + BUFFER_SIZE + 1) % BUFFER_SIZE])) {
                        out[1] = i - delay[scale];
                    } else {
                        out[1] = i + 1 - delay[scale];
                    }
                    out[0] = 1;
                    return out;
                }
            }
        }
        out[0] = 0;
        return out;
    }

    private int detect_onset(int scale, int from, double xi_on, int min_ok) {
        int i;

        for (i = from; i > 1; i--) {
            if (abs(q_data[scale - 1][i % BUFFER_SIZE]) < xi_on) {
                return i - delay[scale];
            }
            //Crossing zero necessarly goes below the threshold
            if (sign(q_data[scale - 1][i % BUFFER_SIZE]) != sign(q_data[scale - 1][(i - 1) % BUFFER_SIZE])) {
                return i - delay[scale];
            }//We only consider local minimum if all values are of the same sign
            else if ((min_ok != 0) && sign(q_data[scale - 1][i % BUFFER_SIZE]) == sign(q_data[scale - 1][(i + 1) % BUFFER_SIZE])
                    && abs(q_data[scale - 1][i % BUFFER_SIZE]) <= abs(q_data[scale - 1][(i - 1) % BUFFER_SIZE]) && abs(q_data[scale - 1][i % BUFFER_SIZE]) <= abs(q_data[scale - 1][(i + 1) % BUFFER_SIZE])) {
                return i - delay[scale];
            }
        }
        return 1;
    }

    private int detect_end(int scale, int from, double xi_end, int min_ok) {
        int i;
        for (i = from; i < last_offset - 1; i++) {
            prefetch(i + 1);
            if (abs(q_data[scale - 1][i % BUFFER_SIZE]) < xi_end) {
                return i - delay[scale];
            }
            //Crossing zero necessarly goes below the threshold
            if (sign(q_data[scale - 1][i % BUFFER_SIZE]) != sign(q_data[scale - 1][(i + 1) % BUFFER_SIZE])) {
                return i - delay[scale];
            }
            if ((min_ok != 0) && abs(q_data[scale - 1][i % BUFFER_SIZE]) <= abs(q_data[scale - 1][(i - 1) % BUFFER_SIZE]) && abs(q_data[scale - 1][i % BUFFER_SIZE]) <= abs(q_data[scale - 1][(i + 1) % BUFFER_SIZE])) {
                return i - delay[scale];
            }
        }
        return last_offset - 1;
    }

    /* @init-@end: Research zone for the main peak
     * @limit: for p wave: qrs_onset, for t wave: qrs_offset (we don't want our waves to overlap)
     * @scale: which scale to search at
     * @gamma/xi: see papers
     * @out: p or t? */
    private int pt_detect(int init, int end, int limit, int scale, int epsilon, char out) {
        //Maximal modmax
        int max = 0;
        int max_pos = 0;
        //hreshold to consider a modmax relevant (gamma)
        double thres;
        int i;
        //First and last relevant peak
        int nfirst = end, nlast = init;

        int orig_init = init;

        int q4_maxmod[] = new int[Q4_MAXMOD_NR_MAX];
        int q4_maxmod_nr = 0;
        int found = 0;
        int tmpfound;
        int maxdiff = 0; //Maximum difference/distance (i.e. most significant wave)
        int maxdiff_p = 0; //Maximum difference/distance position

        while (true) {
            int outF[] = findmodmax(scale, init, end, epsilon);
            init = outF[1];
            if (outF[0] == 0) {
                break;
            }

            q4_maxmod[q4_maxmod_nr++] = init;
            if (q4_maxmod_nr == Q4_MAXMOD_NR_MAX) {
                q4_maxmod_nr--;
            }
            if (abs(q_data[scale - 1][init % BUFFER_SIZE]) > max) {
                max = abs(q_data[scale - 1][init % BUFFER_SIZE]);
                max_pos = init;
            }
        }

        if (q4_maxmod_nr == 0) {
            return 0;
        }

        thres = max * P_T_GAMMA;
        boolean exitLoop = false;
        for (i = 0; i < q4_maxmod_nr - 1; i++) {
            int next = i + 1;

            if (abs(q_data[scale - 1][q4_maxmod[i] % BUFFER_SIZE]) < thres) {
                continue;
            }

            while (abs(q_data[scale - 1][q4_maxmod[next] % BUFFER_SIZE]) < thres) {
                next++;
                if (next >= q4_maxmod_nr) {
                    exitLoop = true;
                    break;
                }
            }
            if (exitLoop) {
                break;
            }

            //Ok, we found an interesting pair of max-mod, let's find a 0-crossing at 2^3
            int p;

            int outD[] = detect_zero_crossing(3, q4_maxmod[i] - delay[scale] + delay[3], q4_maxmod[next] - delay[scale] + delay[3]);
            p = outD[1];
            tmpfound = outD[0];

            if (tmpfound == 0) { //Zero not found at scale 2^3, trying again at scale 2^k
                outD = detect_zero_crossing(scale, q4_maxmod[i], q4_maxmod[next]);
                p = outD[1];
                tmpfound = outD[0];
            }

            if (tmpfound != 0) {
                found = 1;
                if (out == 'p') {
                    device.addPoint(p, ECGPoint.Wave.P_WAVE, ECGPoint.Type.TYPE_SEC_PEAK);
                    
                } else {
                    device.addPoint(p, ECGPoint.Wave.T_WAVE, ECGPoint.Type.TYPE_SEC_PEAK);
                }

                int thisdiff = (32 * abs(q_data[scale - 1][q4_maxmod[i] % BUFFER_SIZE] - q_data[scale - 1][q4_maxmod[next] % BUFFER_SIZE])) / (q4_maxmod[next] - q4_maxmod[i]);

                if (out == 'p' || scale == 5) {
                    if (thisdiff > maxdiff) {
                        maxdiff = thisdiff;
                        maxdiff_p = p;
                        nfirst = q4_maxmod[i];
                        nlast = q4_maxmod[next];
                    }
                } else {
                    if (5 * thisdiff > 9 * maxdiff) { // thisdiff/maxdiff > 10/5
                        maxdiff = thisdiff;
                        maxdiff_p = p;
                        nfirst = q4_maxmod[i];
                        nlast = q4_maxmod[next];
                    } else if (9 * thisdiff > 5 * maxdiff) {
                        prefetch(q4_maxmod[next] + 5 - delay[4] + delay[5]);
                        /* 2 diffs are very close, check values at scale 5 */
                        int other = max(
                                abs(q_data[4][get_minmax(5, nfirst - 5 - delay[4] + delay[5],
                                nfirst + 5 - delay[4] + delay[5],
                                sign(q_data[3][nfirst % BUFFER_SIZE])) % BUFFER_SIZE]),
                                abs(q_data[4][get_minmax(5, nlast - 5 - delay[4] + delay[5],
                                nlast + 5 - delay[4] + delay[5],
                                sign(q_data[3][nlast % BUFFER_SIZE])) % BUFFER_SIZE]));
                        int thisMax = max(
                                abs(q_data[4][get_minmax(5, q4_maxmod[i] - 5 - delay[4] + delay[5],
                                q4_maxmod[i] + 5 - delay[4] + delay[5],
                                sign(q_data[3][q4_maxmod[i] % BUFFER_SIZE])) % BUFFER_SIZE]),
                                abs(q_data[4][get_minmax(5, q4_maxmod[next] - 5 - delay[4] + delay[5],
                                q4_maxmod[next] + 5 - delay[4] + delay[5],
                                sign(q_data[3][q4_maxmod[next] % BUFFER_SIZE])) % BUFFER_SIZE]));
                        if (thisMax > other /*||
                                (this == other && nlast == q4_maxmod[i] &&
                                data_sign(Q(4, nfirst)) == data_sign(Q(4, q4_maxmod[next])) &&
                                data_sign(Q(4, nlast)) != data_sign(Q(4, q4_maxmod[next])))*/) { //Bi-phasic T handling
                            maxdiff = thisdiff > maxdiff ? thisdiff : maxdiff;
                            maxdiff_p = p;
                            nfirst = q4_maxmod[i];
                            nlast = q4_maxmod[next];
                        }
                    }
                }
            }
        }
        int p_onset, p_end;

        if (found == 0) {

            if (out == 't') {
                /* We got a big, isolated peak */
                int p;
                int rg_start, rg_stop;

                if (sign(q_data[scale - 1][orig_init % BUFFER_SIZE]) != sign(q_data[scale - 1][max_pos % BUFFER_SIZE])) {
                    rg_start = orig_init;
                    rg_stop = max_pos;
                } else if (sign(q_data[scale - 1][end % BUFFER_SIZE]) != sign(q_data[scale - 1][max_pos % BUFFER_SIZE])) {
                    rg_start = max_pos;
                    rg_stop = end;
                } else {
                    return 0;
                }

                int outD[] = detect_zero_crossing(3, rg_start - delay[scale] + delay[3], rg_stop - delay[scale] + delay[3]);
                p = outD[1];
                tmpfound = outD[0];

                if (tmpfound == 0) { //Zero not found at scale 2^3, trying again at scale 2^k
                    outD = detect_zero_crossing(scale, rg_start, rg_stop);
                    p = outD[1];
                    tmpfound = outD[0];
                    if (tmpfound == 0) {
                        return 0;
                    }
                } else if (rg_start == orig_init) { /* Find the LAST zero-crossing */
                    outD = detect_zero_crossing(3, p + 1 + delay[3], rg_stop - delay[scale] + delay[3]);
                    p = outD[1];
                    int detected = outD[0];
                    while (detected != 0) {
                        outD = detect_zero_crossing(3, p + 1 + delay[3], rg_stop - delay[scale] + delay[3]);
                        p = outD[1];
                        detected = outD[0];
                    }
                }

                maxdiff_p = p;
                nfirst = rg_start;
                nlast = rg_stop;
            } else {
                return 0;
            }
        }

        //P/T onset and end
        double xi_on = (double) abs(q_data[scale - 1][nfirst % BUFFER_SIZE]);
        double xi_end = (double) abs(q_data[scale - 1][nlast % BUFFER_SIZE]);
        if (out == 'p') {
            xi_on = xi_on * P_XI_ON;
            xi_end = xi_end * P_XI_END;
        } else {
            xi_on = xi_on * T_XI_ON;
            xi_end = xi_end * T_XI_END;
        }

        p_onset = detect_onset(scale, nfirst - 1, xi_on, 1);
        p_end = detect_end(scale, nlast + 1, xi_end, 1);

        if (out == 'p') {
            if (p_end > limit) {
            } else {
                device.addPoint(maxdiff_p, ECGPoint.Wave.P_WAVE, ECGPoint.Type.TYPE_PEAK);
                device.addPoint(p_onset, ECGPoint.Wave.P_WAVE, ECGPoint.Type.TYPE_ONSET);
                device.addPoint(p_end, ECGPoint.Wave.P_WAVE, ECGPoint.Type.TYPE_END);
                if (pr1 == 0) {
                    pr1 = this_r - (nfirst - delay[4]);
                    pr2 = this_r - (nlast - delay[4]);
                } else {
                    pr1 = (3 * pr1 + (this_r - (nfirst - delay[4]))) / 4;
                    pr2 = (3 * pr2 + (this_r - (nlast - delay[4]))) / 4;
                }
            }
        } else {
            device.addPoint(maxdiff_p, ECGPoint.Wave.T_WAVE, ECGPoint.Type.TYPE_PEAK);
            device.addPoint(p_end, ECGPoint.Wave.T_WAVE, ECGPoint.Type.TYPE_END);
            this_t_end = p_end;
            if (p_onset < limit) {
            } else {
                device.addPoint(p_onset, ECGPoint.Wave.T_WAVE, ECGPoint.Type.TYPE_ONSET);
            }
        }

        return 1;
    }

    private void p_detect(int qrs_onset) {
        int init1 = max(this_t_end, sub(qrs_onset, min(PQ_MAX_INTERVAL, rr / 2)));
        int end1 = sub(qrs_onset, PQ_MIN_INTERVAL);

        int init = init1 + delay[4];
        int end = end1 + delay[4];

        if (init1 < sub(offset, BUFFER_SIZE)) {
            init1 = offset - BUFFER_SIZE + 1;
        }

        if (pr1 > 0) //FRAN: check this casting to (int) is accurate
        {
            init = max(sub(this_r, (int) (pr1 * P_COEFF_PR1)), init1) + delay[4];
        }
        if (pr2 > 0) //FRAN: check casting
        {
            end = min(sub(this_r, (int) (pr2 * P_COEFF_PR2)), end1) + delay[4];
        }

        if (init >= last_offset) {
            return;
        }

        if (end <= init) {
            return;
        }

        if (pt_detect(init, end, qrs_onset, 4, epsilon_p, 'p') != 0) {
            return;
        }

        compute_epsilon_pt_5();
        if (pt_detect(init - delay[4] + delay[5], end - delay[4] + delay[5], qrs_onset, 5, epsilon_p_5, 'p') != 0) {
            return;
        }

        if (pr1 == 0) {
            return;
        }

        init = max(sub(this_r, pr1 * P_COEFF_PR1_WIDE), init1) + delay[4];
        //FRAN: check casting
        end = min(sub(this_r, (int) (pr2 * P_COEFF_PR2_WIDE)), end1) + delay[4];

        if (pt_detect(init, end, qrs_onset, 4, epsilon_p, 'p') != 0) {
            return;
        }
    }

    private void t_detect(int qrs_onset, int qrs_end) {
        if (n_valid <= 1) {
            //Do not detect T wave on the first beat, since it may detect what should be a QRS
            return;
        }

        int i;
        for (i = 0; i < t_end_rr.length - 1; i++) {
            if (rr >= t_end_rr[i]) {
                break;
            }
        }

        double srr = Math.sqrt(rr);
        /* Whatever happens, we only start detecting the T 40ms after QRS_end */
        int init = qrs_end + 10 + delay[4];
        //init = off_max(qrs_end+15, qrs_onset+((t_end_qtc_min[i]*srr*QTC_MIN_RATIO+64)/128))+delay[4];
        int end = min(last_offset, (int) Math.round(qrs_onset + ((t_end_qtc_max[i] * srr * QTC_MAX_RATIO + 64) / 128) + delay[4]));

        if (init >= last_offset) {
            return;
        }

        // BUG remover... It seems that the end values go crazy sometimes, FIXME!!!
        if (end > init + rr) {
            end = init + rr;
        }

        if (pt_detect(init, end, qrs_end, 4, epsilon_t, 't') == 0) {
            compute_epsilon_pt_5();
            init = qrs_end + 10 + delay[5];
            end = min(last_offset, (int) Math.round(qrs_onset + ((t_end_qtc_max[i] * srr * QTC_MAX_RATIO_5 + 64) / 128) + delay[5]));

            pt_detect(init - delay[4] + delay[5], end - delay[4] + delay[5], qrs_end, 5, epsilon_t_5, 't');
        }
    }

    private void qrs_detect_waves(int nr) {
        int max = max(abs(q_data[1][ok_maxmin[0][2] % BUFFER_SIZE]), abs(q_data[1][ok_maxmin[1][2] % BUFFER_SIZE]));
        double thres = max * GAMMA_QRS_PRE; //gamma_qrs_(pre/post)
        int init = sub(nr + delay[2], LI_4_NEIGHBOUR_BEFORE);
        int end = init + LI_4_NEIGHBOUR; //-LI_4_NEIGHBOUR_BEFORE;

        int q2_maxmod[] = new int[Q2_MAXMOD_NR_MAX];
        int q2_maxmod_nr = 0;
        int i;

        if (init < delay[4]) {
            init += delay[4];
        }

        /* Store potential maxima (need postprocessing once gamma is computed) and get the max value */
        /* NB: Martinez is unclear. Is gamma_QRS_pre computed only on values before the R? */
        while (true) {
            int out[] = findmodmax(2, init, end, thres);
            init = out[1];
            if (out[0] == 0) {
                break;
            }

            q2_maxmod[q2_maxmod_nr++] = init;
            if (q2_maxmod_nr == Q2_MAXMOD_NR_MAX) {
                q2_maxmod_nr--;
            }
            if (abs(q_data[1][init % BUFFER_SIZE]) > max) {
                max = abs(q_data[1][init % BUFFER_SIZE]);
                thres = max * GAMMA_QRS_PRE;
            }
        }

        /* [Martinez] says only QRS complexes with 3 or less waves are considered, this
         * leaves only 2 waves apart from the R. We take the closest ones to the R.
         * (TODO: Is this really clever? What about taking the maximum difference in the 2 modmax?) */
        int waves[] = new int[2]; //Position of the waves
        int peaks[][] = new int[2][2]; //Position of the peaks associated with the waves
        int nr_waves = 0;

        thres = max * GAMMA_QRS_PRE; //NB: My own tuning
        boolean exitLoop = false;
        for (i = 0; i < q2_maxmod_nr - 1 && q2_maxmod[i] < nr + delay[2]; i++) {
            int next = i + 1;

            if (abs(q_data[1][q2_maxmod[i] % BUFFER_SIZE]) < thres) {
                continue;
            }

            while (abs(q_data[1][q2_maxmod[next] % BUFFER_SIZE]) < thres) {
                next++;
                if (q2_maxmod[next] > nr + delay[2] || next >= q2_maxmod_nr) {
                    exitLoop = true;
                    break;
                }
            }
            if (exitLoop) {
                break;
            }

            if (sign(q_data[1][q2_maxmod[i] % BUFFER_SIZE]) == sign(q_data[1][q2_maxmod[next] % BUFFER_SIZE])) {
                continue;
            }

            /* Ok, we found an interesting pair of max-mod, let's find
             * a 0-crossing at 2^1. */
            int qrs;

            int out[] = detect_zero_crossing(1, q2_maxmod[i] - delay[2] + delay[1], q2_maxmod[next] - delay[2] + delay[1]);
            qrs = out[1];
            if (out[0] != 0) {
                int j;
                if (nr_waves < 2) {
                    waves[nr_waves] = qrs;
                    peaks[nr_waves][0] = q2_maxmod[i];
                    peaks[nr_waves][1] = q2_maxmod[next];
                    nr_waves++;
                } else {
                    for (j = 0; j < nr_waves; j++) {
                        if (abs(q_data[1][q2_maxmod[i] % BUFFER_SIZE] - q_data[1][q2_maxmod[next] % BUFFER_SIZE])
                                > abs(q_data[1][peaks[j][0] % BUFFER_SIZE] - q_data[1][peaks[j][1] % BUFFER_SIZE])
                                && qrs != nr) {
                            waves[j] = qrs;
                            peaks[j][0] = q2_maxmod[i];
                            peaks[j][1] = q2_maxmod[next];
                            break;
                        }
                    }
                }
            }
        }
        //pre_out:
        thres = max * GAMMA_QRS_POST_1;
        exitLoop = false;
        for (; i < q2_maxmod_nr - 1; i++) {
            int next = i + 1;

            if (q2_maxmod[next] < nr + delay[2]) {
                continue;
            }

            if (abs(q_data[1][q2_maxmod[i] % BUFFER_SIZE]) < thres) {
                continue;
            }

            /*		if (q2_maxmod[next]-delay[2]-nr > LI_4_NEIGHBOUR_CLOSE)
            thres = max*GAMMA_QRS_POST_2;*/
            while (abs(q_data[1][q2_maxmod[next] % BUFFER_SIZE]) < thres) {
                next++;
                if (next >= q2_maxmod_nr) {
                    exitLoop = true;
                    break;
                }
            }
            if (exitLoop) {
                break;
            }

            if (sign(q_data[1][q2_maxmod[i] % BUFFER_SIZE]) == sign(q_data[1][q2_maxmod[next] % BUFFER_SIZE])) {
                continue;
            }

            /* Ok, we found an interesting pair of max-mod, let's find
             * a 0-crossing at 2^1. */
            int qrs;

            int out[] = detect_zero_crossing(1, q2_maxmod[i] - delay[2] + delay[1], q2_maxmod[next] - delay[2] + delay[1]);
            qrs = out[1];
            if (out[0] != 0) {
                int j;
                if (nr_waves < 2) {
                    waves[nr_waves] = qrs;
                    peaks[nr_waves][0] = q2_maxmod[i];
                    peaks[nr_waves][1] = q2_maxmod[next];
                    nr_waves++;
                } else {
                    for (j = 0; j < nr_waves; j++) {
                        if (abs(q_data[1][q2_maxmod[i] % BUFFER_SIZE] - q_data[1][q2_maxmod[next] % BUFFER_SIZE])
                                > abs(q_data[1][peaks[j][0] % BUFFER_SIZE] - q_data[1][peaks[j][1] % BUFFER_SIZE])
                                && qrs != nr) {
                            waves[j] = qrs;
                            peaks[j][0] = q2_maxmod[i];
                            peaks[j][1] = q2_maxmod[next];
                            break;
                        }
                    }
                }
            }
        }
        /* TODO: Here, we could find out the QRS morphology: QRS, RSR', QR, RS... */

        //post_out:
        int nfirst = ok_maxmin[0][2];
        int nlast = ok_maxmin[1][2];
        int first_wave = nr + delay[2];
        int last_wave = nr + delay[2];
        for (i = 0; i < nr_waves; i++) {
            int j;
            device.addPoint(waves[i], ECGPoint.Wave.QRS_WAVE, ECGPoint.Type.TYPE_SEC_PEAK);
            for (j = 0; j < 2; j++) {
                if (peaks[i][j] < nfirst) {
                    nfirst = peaks[i][j];
                } else if (peaks[i][j] > nlast) {
                    nlast = peaks[i][j];
                }
            }
            if (waves[i] < first_wave) {
                first_wave = waves[i];
            } else if (waves[i] > last_wave) {
                last_wave = waves[i];
            }
        }

        /* QRS onset and end */
        int qrs_onset, qrs_end;


        int xi_on;
        int xi_end;
        int max_q4_onset = get_minmax(4, nfirst - delay[2] + delay[4] - 10, nr + delay[4], 1);
        xi_on = abs(q_data[3][max_q4_onset % BUFFER_SIZE]) * 1 / 4; /* 0.25 */

        int onset_start_q4 = sub(nfirst - delay[2] + delay[4], 2); /* 8ms before the first wave */


        qrs_onset = detect_onset(4, onset_start_q4, xi_on, 0);

        int end_start_q4 = get_minmax(4, nlast - delay[2] + delay[4], nlast - delay[2] + delay[4] + 20, 1);
        xi_end = abs(q_data[3][end_start_q4 % BUFFER_SIZE]) * 7 / 10; /* 0.70 */

        qrs_end = detect_end(4, end_start_q4, xi_end, 0);

        device.addPoint(qrs_onset, ECGPoint.Wave.QRS_WAVE, ECGPoint.Type.TYPE_ONSET);
        device.addPoint(qrs_end, ECGPoint.Wave.QRS_WAVE, ECGPoint.Type.TYPE_END);
        this_qrs_end = qrs_end;

        compute_epsilon_pt();

        p_detect(qrs_onset);

        t_detect(qrs_onset, qrs_end);
    }

    private int check_r_wave() {
        int r;

        if (ok_maxmin_valid == 2) { //2 points
            if (ok_maxmin[1][1] - ok_maxmin[0][1] > LI_4_NEIGHBOUR) {
                // min-max interval too wide, should not happen
                ok_maxmin_valid = 0;
                return 0;
            }

            /* 5) [Li] */
            int out[] = detect_zero_crossing(1, ok_maxmin[0][1], ok_maxmin[1][1]);
            if (out[0] == 1) {
                //FRAN: the R peak was found... add_point(out[1], qrs_wave, type_peak);
                //System.out.println("R peak found\t" + (bufferedSamplesCount+offset-out[1]) + " samples before the last sample");
                device.addPoint(out[1], ECGPoint.Wave.QRS_WAVE, ECGPoint.Type.TYPE_PEAK);
                add_r(out[1]);
                qrs_detect_waves(out[1]);
                ok_maxmin_valid = 0;
                return 1;
            }
        } else {
            //Means an isolated modmax/min has been found
        }

        ok_maxmin_valid = 0;
        return 0;
    }

    private void add_modmax(int modmax[], int searchback) {
        int mmsign;

        if (searchback != 0) {
            mmsign = sign(q_data[2][modmax[3] % BUFFER_SIZE]);
        } else {
            if (q_data[0][modmax[1] % BUFFER_SIZE] > 0 && q_data[1][modmax[2] % BUFFER_SIZE] > 0 && q_data[2][modmax[3] % BUFFER_SIZE] > 0 && q_data[3][modmax[4] % BUFFER_SIZE] > 0) {
                mmsign = 1;
            } else if (q_data[0][modmax[1] % BUFFER_SIZE] < 0 && q_data[1][modmax[2] % BUFFER_SIZE] < 0 && q_data[2][modmax[3] % BUFFER_SIZE] < 0 && q_data[3][modmax[4] % BUFFER_SIZE] < 0) {
                mmsign = 0;
            } else {
                return;
            }
        }

        if (ok_maxmin_valid < 2) {
            /* Enough space, put it in the list */
            for (int i = 0; i < 5; i++) {
                ok_maxmin[ok_maxmin_valid][i] = modmax[i];
            }
            ok_maxmin_valid++;
        } else { /* ok_maxmin_valid == 2 */
            if (sign(q_data[2][ok_maxmin[0][3] % BUFFER_SIZE]) == mmsign) {
                if (sign(q_data[2][ok_maxmin[1][3] % BUFFER_SIZE]) == mmsign) {
                    /* Same sign as element 0 and 1, eliminate 0. */
                    for (int i = 0; i < 5; i++) {
                        ok_maxmin[0][i] = ok_maxmin[1][i];
                    }
                    for (int i = 0; i < 5; i++) {
                        ok_maxmin[1][i] = modmax[i];
                    }
                } else {
                    /* Same sign as element 0. (1 is the "pivot") */
                    int a1l1 = abs(q_data[2][ok_maxmin[0][3] % BUFFER_SIZE]) / (ok_maxmin[1][3] - ok_maxmin[0][3]);
                    int a2l2 = abs(q_data[2][modmax[3] % BUFFER_SIZE]) / (modmax[3] - ok_maxmin[1][3]);

                    if (a1l1 * 5 > a2l2 * 6) { /* 1.2 */
                        /* modmax is redundant */

                    } else if (a2l2 * 5 > a1l1 * 6) {
                        /* 0 is redundant */
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[0][i] = ok_maxmin[1][i];
                        }
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[1][i] = modmax[i];
                        }
                    } else {
                        /* modmax is redundant (following the peak) */
                    }
                }
            } else { /* Sign different from 0 */
                if (sign(q_data[2][ok_maxmin[1][3] % BUFFER_SIZE]) == mmsign) {
                    /* 1 and modmax have the same sign, one of them is redundant */
                    if (modmax[1] - ok_maxmin[1][1] > LI_4_NEIGHBOUR) {
                        /* 0-current interval > 120ms => modmax is isolated */
                        return;
                    }

                    int a1l1 = abs(q_data[2][ok_maxmin[1][3] % BUFFER_SIZE]) / (ok_maxmin[1][3] - ok_maxmin[0][3]);
                    int a2l2 = abs(q_data[2][modmax[3] % BUFFER_SIZE]) / (modmax[3] - ok_maxmin[0][3]);

                    if (a1l1 * 5 > 6 * a2l2) {
                        /* modmax is redundant */
                    } else if (a2l2 * 5 > 6 * a1l1) {
                        /* 1 is redundant */
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[1][i] = modmax[i];
                        }
                    } else {
                        /* modmax is redundant (same side, rules 3 says the furthest is redundant) */
                    }
                } else {
                    /* 0 and 1 have the same sign, one of them is redundant */
                    if (modmax[1] - ok_maxmin[0][1] > LI_4_NEIGHBOUR) {
                        /* 0-current interval > 120ms => 0 is isolated */
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[0][i] = ok_maxmin[1][i];
                        }
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[1][i] = modmax[i];
                        }
                        return;
                    }

                    int a1l1 = abs(q_data[2][ok_maxmin[0][3] % BUFFER_SIZE]) / (modmax[3] - ok_maxmin[0][3]);
                    int a2l2 = abs(q_data[2][ok_maxmin[1][3] % BUFFER_SIZE]) / (modmax[3] - ok_maxmin[1][3]);

                    if (a1l1 * 5 > 6 * a2l2) {
                        /* 1 is redundant */
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[1][i] = modmax[i];
                        }
                    } else if (a2l2 * 5 > 6 * a1l1) {
                        /* 0 is redundant */
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[0][i] = ok_maxmin[1][i];
                        }
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[1][i] = modmax[i];
                        }
                    } else {
                        /* 0 is redundant (same side, rules 3 says the furthest is redundant) */
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[0][i] = ok_maxmin[1][i];
                        }
                        for (int i = 0; i < 5; i++) {
                            ok_maxmin[1][i] = modmax[i];
                        }
                    }
                }
            }
        }
    }

    private void li2_iterate(int scale, int modmax[], int searchback) {
        do { /* Big loop to ensure tail recursion optimization */
            int init = modmax[scale + 1] - delay[scale + 1] + delay[scale];
            int tmp = sub(init, LI_2_NEIGHBOUR);
            int threshold = eps_data[scale - 1];
            int found = 0;

            while (true) {
                int out[] = findmodmax(scale, tmp, init + LI_2_NEIGHBOUR, threshold);
                tmp = out[1];
                if (out[0] == 0) {
                    break;
                }

                /* TODO: NB: Modify this, if closer, a modmax < 1.2*current is good */

                /* NB: This seems not to work well... */
                /* From now we are only interested in modmax > 1.2 the current one */
                //threshold = 1.2*modmax[scale];

                /* NB: I take the nearest one instead */
                if (found == 0 || (diff(modmax[scale], init) > diff(tmp, init))) {
                    modmax[scale] = tmp;
                }

                found = 1;
            }

            if (found == 1) {
                if (scale > 1) {
                    scale = scale - 1;
                    continue;
                } else {
                    /* 3) [Li] */
                    /* NB: Alpha check seems weird (like in 10677 of sel100).. I ignore it, as [Martinez]... */
                    /*double alpha[4];
                    int i;*/

                    add_modmax(modmax, searchback);
                }
            }
            return; /* We come here only is continue has not been called */
        } while (true);
    }

    private int li2_searchback(int until) {
        int modmax[] = new int[5];

        modmax[3] = max(this_r + rr / 2 + delay[3], sub(offset, BUFFER_SIZE)); /* Start from half the period */
        until = until + delay[3];

        while (true) {
            int out[] = findmodmax(3, modmax[3], until, eps_data[2]);
            modmax[3] = out[1];
            if (out[0] == 0) {
                if (check_r_wave() == 0) {
                    searchback_r = max(searchback_r, this_r) + rr;
                    /* Update this_r and last_r even if we didn't find any... */
                    return 0;
                } else {
                    return 1;
                }
            }

            modmax[1] = modmax[3] - delay[3] + delay[1];
            modmax[2] = modmax[3] - delay[3] + delay[2];

            add_modmax(modmax, 1);

            /* Ok, we found a mod max at scale 3, find some at scale 2 in
             * the "neighbourhood" [Li], e.g. 10 samples. */
            //li2_iterate(2, modmax, 1);
        }
    }

    //Returns 1 when the execution is finished, 0 otherwise
    private int li2_iteration() {
        while (true) {
            until = last_offset;

            if (n_valid > 1) { /* We need 2 R before thinking about searching back. */
                searchback = min(max(this_r, searchback_r) + rr * 3 / 2, until);
            } else {
                searchback = min(max(this_r, searchback_r) + 300, until);
            }

            until = min(until, searchback);

            /* If a maxmin is in the list, call check_r_wave after 120ms without any point. */
            if (ok_maxmin_valid > 0) {
                until = min(until, ok_maxmin[ok_maxmin_valid - 1][1] + LI_4_NEIGHBOUR - delay[1] + delay[4]);
            }

            int out[] = findmodmax(4, modmax[4], until, eps_data[3]);
            modmax[4] = out[1];
            if (out[0] == 0) {
                /* No peak for 120ms (or we hit the search back limit), check for an R wave. */
                int r_ok = check_r_wave();

                if ((r_ok == 0) && until == searchback && until != last_offset) {
                    /* We hit the search back limit */
                    li2_searchback(until - rr / 2);
                }

                if (until == last_offset) {
                    return 1;
                } else {
                    /* Blanking */
                    if (r_ok != 0) {
                        modmax[4] = max(until, max(this_qrs_end + LI2_BLANKING, this_t_end) + delay[4]);
                    } else {
                        modmax[4] = until;
                    }
                    return 0;
                }
            }

            /* Ok, we found a mod max at scale 4, find some at scale 3 in
             * the "neighbourhood" [Li], e.g. 10 samples. */
            li2_iterate(3, modmax, 0);
        }
    }
}

/*#define IN(i) in_data[conv_nofetch(i)]
#define H(scale, i) h_data[scale-1][convh(i)]
#define Q(scale, i) q_data[scale-1][conv(i)]
#define Qn(scale, i) q_data[scale-1][conv_nofetch(i)]
#define EPS(scale) eps_data[scale-1]
#define EPS_INT(scale, window) eps_data_int[scale-1][window]*/