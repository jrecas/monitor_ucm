/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*IMPORTANT:
 * For this code to work, we have to copy win32com.dll to c:\windows\system32
 * and javax.comm.properties to <JDKFOLDER>/jre/lib
 */

package ecgdisplay;

import ecgalgo.*;
import java.io.EOFException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;
import java.util.List;
import java.util.Enumeration;

import java.util.logging.Level;
import java.util.logging.Logger;
import gnu.io.*;

import java.util.Calendar;
import java.io.File;
import java.io.*;

import java.text.DecimalFormat;

/**
 *
 * @author nicolas
 */
public class ComDevice {
    
    private static final String  CLASS_NAME = "SerialPortStub";
    
    private String portName;
    private SerialPort port;
    int baudrate;
    private InputStream is;
    private OutputStream os;
    private boolean connected = false;
    private boolean disconnect = false;

    private DataThread dt = null;

    /* buffer of 2^16 points */
    private final int MAX_PTS = 16384;
    private int cpos = 0;
    private int pts[][] = new int[2][MAX_PTS];
    private long realoffset = 0; /* Offset from the node */
    private static final int correction = 283;

    private int hbr = 0;
    private int systolic = 0;
    private int diastolic = 0;
    private int channel = -1;
    private int mult = -1;
    private int arr = 0;
    private int arrCount = 0;
    
    private boolean recording = false;
    private boolean preBuffering = false;
    private boolean buffering = false;
    private int ecgBuffer[] = new int[2500];
    private int ecgBufferCount = 0;
    private int plethBuffer[] = new int[750];
    private int plethBufferCount = 0;
    private long qBuffer[] = new long[100];
    private int qBufferCount = 0;
    private long rBuffer[] = new long[100];
    private int rBufferCount = 0;
    private long sBuffer[] = new long[100];
    private int sBufferCount = 0;
    private FileWriter fwRAWECG = null;
    private FileWriter fwPleth = null;
    private FileWriter fwFilteredECG = null;
    private FileWriter fwPoints = null;
    private long recordingOffset;
    private long bufferingOffset;
    private long delineationOffset;
    private long lastOffset = 0;//Last offset received from the node
    private long samplesSinceLastOffset = 0;
    private ECGFilter ECGfilter = new ECGFilter();
    private boolean filter = true;
    private ECGDelineator delineator;
    private boolean delineate = true;
    private HBRCalculator hbrCalculator;
    private ComDeviceListener listener = null;
    int currentRAW = 0;
    int ecgRAW[], pleth[];
    
    int plethSampleBytes[] = new int[5];
    int plethSample = 0;
    int plethSampleBytesCount = 0;

    //FRAN: changed from private to public
    public ComDevice(String portName) {
        this.portName = portName;
        ecgRAW = new int[correction];
        pleth = new int[correction];
        
        delineationOffset = 0;
        delineator = new ECGDelineator(this);
        hbrCalculator = new HBRCalculator();
        
        this.baudrate = 115200;
        realoffset = 0;
        hbr = 0;
    }

    public void setListener(ComDeviceListener listener) {
        this.listener = listener;
    }
    
    public void setBaudrate(int baudrate){
        this.baudrate = baudrate;
    }

    public boolean isConnected() {
        return connected;
    }

    public String getPortName() {
        return portName;
    }

    public int getPos() {
        return cpos;
    }

    public long getRealOffset() {
        return realoffset;
    }

    public int getHBR() {
        return hbr;
    }
    
    public int getSystolic() {
        return systolic;
    }
    
    public int getDiastolic() {
        return diastolic;
    }

    public int getChannel() {
        return channel;
    }

    public int getMult() {
        return mult;
    }

    public int getPt(int i, int pos) {
        /* TODO: Bound checking */
        return pts[i][(pos+MAX_PTS) % MAX_PTS];
    }
    
    public boolean getFilter(){
        return filter;
    }
    
    public void setFilter(boolean filter){
        this.filter = filter;
    }
    
    public boolean getDelineate(){
        return this.delineate;
    }
    
    public void setDelineate(boolean delineate){
        this.delineate = delineate;
    }
    
    public int getArr(){
        return arr;
    }
    
    public int getBaudrate(){
        return baudrate;
    }
    
    public void resetDelineation(){
        //FRAN: the "-400" is to correct the initial value of cpos
        delineationOffset = cpos-400;
        delineator = new ECGDelineator(this);
        hbrCalculator = new HBRCalculator();
    }
    
    public void startRecording() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        int sec = calendar.get(Calendar.SECOND);

        File dir = new File("save");
        if (!dir.exists()) {
            dir.mkdir();
        }
        String prefix = "save/" + year + "-";
        if (month < 10) {
            prefix = prefix + "0";
        }
        prefix = prefix + month + "-";
        if (day < 10) {
            prefix = prefix + "0";
        }
        prefix = prefix + day + "_";
        if (hour < 10) {
            prefix = prefix + "0";
        }
        prefix = prefix + hour + "-";
        if (min < 10) {
            prefix = prefix + "0";
        }
        prefix = prefix + min + "-";
        if (sec < 10) {
            prefix = prefix + "0";
        }
        prefix = prefix + sec + "_";
        try {
            fwRAWECG = new FileWriter(prefix + "ecg-RAW.txt");
            fwPleth = new FileWriter(prefix + "pleth.txt");
            fwFilteredECG = new FileWriter(prefix + "ecg-Filtered.txt");
            fwPoints = new FileWriter(prefix + "point.txt");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        recordingOffset = lastOffset + samplesSinceLastOffset;
        recording = true;
    }
    
    public void startBuffering() {
        preBuffering = true;
    }
    
    public void stopBuffering(){
        buffering = false;
        System.out.println("DONE!");
        //Send all the buffers to Alair
        
    }

    public void stopRecording() {
        recording = false;

        try {
            fwRAWECG.close();
            fwPleth.close();
            fwFilteredECG.close();
            fwPoints.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public boolean getRecording() {
        return recording;
    }

    public void dumpToFile() {
        FileWriter fw = null;
        try {
            int start = 0;

            if (cpos > MAX_PTS) {
                start = cpos - MAX_PTS + 1;
            }
            fw = new FileWriter("shimmer.pts");

            for (int i = start; i <= cpos; i++) {
                fw.write(String.valueOf(getPt(0, i)+2048) + "\n");
                fw.write(String.valueOf(getPt(1, i)+2048) + "\n");
                fw.write(String.valueOf(getPt(2, i)+2048) + "\n");
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

    }

    public void connect() throws IOException {
        try {
            CommPortIdentifier portId = CommPortIdentifier.getPortIdentifier(this.getPortName());
            port = (SerialPort)portId.open("ComDevice", 0);
            is = port.getInputStream();
            os = port.getOutputStream();
            port.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
            port.setSerialPortParams(this.baudrate, SerialPort.DATABITS_8, SerialPort.STOPBITS_1, SerialPort.PARITY_NONE);
            //printPortStatus();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        
        connected = true;
        realoffset = 0;
        dt = new DataThread();
        dt.start();
    }
    
    void printPortStatus(){
        System.err.println("Baudrate: " + port.getBaudRate());
        System.err.println("Data bits: " + port.getDataBits());
        System.err.println("Stop bits: " + port.getStopBits());
        System.err.println("Parity: " + port.getParity());
    }

    private int readbyte() throws IOException {
        int i = is.read();
        //if (i < 0) throw new EOFException("End of file!");
        return i;
    }
    
    private long convert(long n){
        long data[] = new long[3];
        data[0] = (n>>16) & 0xFF;
        data[1] = (n>>8) & 0xFF;
        data[2] = n & 0xFF;
        
        long aux = data[0] << 16 | data[1] << 8 | data[2];
        if ((aux>>23)!=0){
            return -(16777216-((aux<<1)>>1));
        }
        else{
            return aux;
        }
    }

    private long getUInt32() throws IOException {
        long data[] = new long[4];
        data[0] = readbyte();
        data[1] = readbyte();
        data[2] = readbyte();
        data[3] = readbyte();
        //System.out.print("Bytes: " + data[0] + " " + data[1] + " " + data[2] + " " + data[3] + " -     ");
        return data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
    }

    private int getInt16() throws IOException {
        int data[] = new int[2];
        data[0] = readbyte();
        data[1] = readbyte();
        
        int aux = data[0] << 8 | data[1];
        if ((aux>>15)!=0){
            return -(65536-((aux<<1)>>1));
        }
        else{
            return aux;
        }
    }
    
    private int getInt24() throws IOException {
        int data[] = new int[3];
        data[0] = readbyte();
        data[1] = readbyte();
        data[2] = readbyte();
        
        long aux = data[0] << 16 | data[1] << 8 | data[2];
        if ((aux>>23)!=0){
            return (int)(-(16777216-((aux<<1)>>1))/256);
        }
        else{
            return (int)(aux/256);
        }
    }
    
    private int getInt32() throws IOException {
        int data[] = new int[4];
        data[0] = readbyte();
        data[1] = readbyte();
        data[2] = readbyte();
        data[3] = readbyte();
        
        long aux = data[0] << 24 | data[1] << 16 | data[2] << 8 | data[3];
        if ((aux>>31)!=0){
            return (int)(-(16777216-((aux<<1)>>1)/256));
        }
        else{
            return (int)(aux/256);
        }
    }

    private int getInt8() throws IOException {
        return readbyte();
    }

    public void setChannel(int channel) throws IOException {
        os.write(0x80 | channel);
        os.flush();
    }

    public void setMult(int mult) throws IOException {
        os.write(0x40 | mult);
        os.flush();
    }

    private void fetchData() throws IOException {
        if(disconnect){
            try {
                is.close();
                os.close();
                port.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            connected = false;
            disconnect = false;
            return;
        }
        if (!connected)
            return;
        
        int hdr = getInt8();
        int ecgFiltered;

        do {
            //System.out.println(hdr);
            switch(hdr) {
                case 0xD0: //ECG sample
                    long ecgSample = getUInt32();
                    
                    cpos++;
                    ecgRAW[(currentRAW + correction - 1) % correction] = (int)(convert(ecgSample))/2;
                    pleth[(currentRAW + correction - 1) % correction] = plethSample;
                    ecgFiltered = ECGfilter.filterSample(ecgRAW[(currentRAW + correction - 1) % correction]);
                    
                    if(!this.getFilter()){
                        //Shows the RAW signal
                        pts[0][cpos % MAX_PTS] = ecgRAW[currentRAW];
                    }
                    else{
                        //Shows the filtered signal
                        pts[0][cpos % MAX_PTS] = ecgFiltered;
                    }
                    pts[1][cpos % MAX_PTS] = pleth[currentRAW];
                    
                    currentRAW = (currentRAW + 1) % correction;
                    if (recording) {
                        fwRAWECG.write(String.valueOf(ecgRAW[(currentRAW + correction - 1) % correction]) + "\n");
                        fwFilteredECG.write(String.valueOf(ecgFiltered) + "\n");
                    }
                    if(buffering && ecgBufferCount<2500){
                        ecgBuffer[ecgBufferCount++] = ecgFiltered;
                    }
                    listener.newDataArrived(0);
                    listener.newDataArrived(1);
                    delineator.newSample(ecgFiltered/100);
                    samplesSinceLastOffset++;
                            
                    break;
                case 0xaa: //ECG sample
                	 readbyte();
                	 readbyte();
                	 readbyte();
                    break;

                case 0xDB: //Pleth sample
                    plethSampleBytes[0] = plethSampleBytes[1];
                    plethSampleBytes[1] = plethSampleBytes[2];
                    plethSampleBytes[2] = plethSampleBytes[3];
                    plethSampleBytes[3] = plethSampleBytes[4];
                    plethSampleBytes[4] = readbyte();
                    plethSampleBytesCount++;
                    int sum = (plethSampleBytes[0] + plethSampleBytes[1] + plethSampleBytes[2] + plethSampleBytes[3]) & 0xFF;
                    //System.out.println(plethSampleBytes[4] + " " + sum);
                    if(plethSampleBytes[0]==1 && sum==plethSampleBytes[4] && plethSampleBytesCount>=5){
                        if(plethSampleBytesCount>5){
                            System.err.println("Sample lost: " + plethSampleBytesCount);
                        }
                        plethSampleBytesCount = 0;
                        plethSample = plethSampleBytes[2]*200;
                        if (recording) {
                            fwPleth.write(String.valueOf(plethSampleBytes[2]) + "\n");
                        }
                        if(preBuffering){
                            //If first byte of the frame
                            if((plethSampleBytes[1]&0x01)==1){
                                bufferingOffset = lastOffset + samplesSinceLastOffset;
                                buffering = true;
                                ecgBufferCount = 0;
                                plethBufferCount = 0;
                                qBufferCount = 0;
                                rBufferCount = 0;
                                sBufferCount = 0;
                                System.out.print("Buffering... ");
                                preBuffering = false;
                            }
                            else{
                                System.out.println("Pleth sample discarded");
                            }
                            
                        }
                        if(buffering && plethBufferCount<750){
                            plethBuffer[plethBufferCount++] = plethSampleBytes[2];
                        }
                    }
                            
                    break;
                default:
                    System.out.println("Synchronizing...");
                    continue;
            }
        } while(false);
    }
    
    public void addPoint(int offset, ECGPoint.Wave wave, ECGPoint.Type type) {
        if (type != ECGPoint.Type.TYPE_SEC_PEAK) {
            listener.newPointArrived(0, new ECGPoint((int) (offset + delineationOffset + correction + 118), wave, type));
            if ((wave == ECGPoint.Wave.QRS_WAVE) && (type == ECGPoint.Type.TYPE_PEAK)) {
                hbr = hbrCalculator.updateHBR(offset);
                listener.newQRS(offset);
                //System.out.println("Beat detected! - Offset: " + (offset + delineationOffset + correction + 118));
            }
            if (recording && ((offset + delineationOffset - recordingOffset)>0)) {
                try {
                    fwPoints.write(offset + delineationOffset - recordingOffset + " ");
                    switch (wave) {
                        case P_WAVE:
                            switch (type) {
                                case TYPE_ONSET:
                                    fwPoints.write("(p");
                                    break;
                                case TYPE_PEAK:
                                    fwPoints.write("pp");
                                    break;
                                case TYPE_END:
                                    fwPoints.write("p)");
                                    break;
                            }
                            break;
                        case QRS_WAVE:
                            switch (type) {
                                case TYPE_ONSET:
                                    fwPoints.write("(N");
                                    break;
                                case TYPE_PEAK:
                                    fwPoints.write("NN");
                                    break;
                                case TYPE_END:
                                    fwPoints.write("N)");
                                    break;
                            }
                            break;
                        case T_WAVE:
                            switch (type) {
                                case TYPE_ONSET:
                                    fwPoints.write("(t");
                                    break;
                                case TYPE_PEAK:
                                    fwPoints.write("tt");
                                    break;
                                case TYPE_END:
                                    fwPoints.write("t)");
                                    break;
                            }
                            break;
                    }
                    fwPoints.write("\n");
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            if(buffering){
                if(((offset + delineationOffset - bufferingOffset)>0) && ((offset + delineationOffset - bufferingOffset)<2500)){
                    if (wave == ECGPoint.Wave.QRS_WAVE){
                        if ((type == ECGPoint.Type.TYPE_ONSET) && (qBufferCount<100)){
                            qBuffer[qBufferCount++] = offset + delineationOffset - bufferingOffset;
                        }
                        else if ((type == ECGPoint.Type.TYPE_PEAK) && (rBufferCount<100)){
                            rBuffer[rBufferCount++] = offset + delineationOffset - bufferingOffset;
                        }
                        else if ((type == ECGPoint.Type.TYPE_END) && (sBufferCount<100)){
                            sBuffer[sBufferCount++] = offset + delineationOffset - bufferingOffset;
                        }
                    }
                }
                else if((offset + delineationOffset - bufferingOffset)>=2500){
                    stopBuffering();
                }
            }
        }
    }

    private class DataThread extends Thread {

        @Override
        public void run() {
            try {
                cpos = 400;
                while (connected) {
                    fetchData();
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void close() {
        disconnect = true;
    }
    
}
