/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecgdisplay;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.html.*;

/**
 *
 * @author Fran
 */
public class SwingProgressBar {

    final static int interval = 1000;
    int i;
    JProgressBar pb;
    Timer timer;
    JButton button;
    JPanel panel;
    JFrame frame;

    public SwingProgressBar() {
        frame = new JFrame("Downloading ECG data");
        
        button = new JButton("Cancel");
        button.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                buttonActionPerformed(evt);
            }
        });

        pb = new JProgressBar(0, 100);
        pb.setValue(0);
        pb.setStringPainted(true);

        panel = new JPanel();
        panel.add(button);
        panel.add(pb);

        JPanel panel1 = new JPanel();
        panel1.setLayout(new BorderLayout());
        panel1.add(panel, BorderLayout.NORTH);
        panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        frame.setContentPane(panel1);
        frame.pack();
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
    
    private void buttonActionPerformed(java.awt.event.ActionEvent evt){
        frame.setVisible(false);
    }
}