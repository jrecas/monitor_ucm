/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * ECGFrame.java
 *
 * Created on 29-Nov-2008, 15:05:23
 */
package ecgdisplay;

import ecgalgo.*;
import java.awt.BasicStroke;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import gnu.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.filechooser.*;

import java.io.*;
import java.text.DecimalFormat;

/**
 *
 * @author nicolas
 */
public class ECGFrame extends javax.swing.JFrame implements ComDeviceListener {

    private boolean pause = false;
    private ComDevice mydevice = null;
    private int nLeads = 2;
    private ECGCanvas ecgc[];
    private long startTime = 0;//In number of samples

    /** Creates new form ECGFrame */
    public ECGFrame() {
        try{
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            //UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
            //UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        initComponents();
        //comList.addItem("Press scan");
        ecgc = new ECGCanvas[nLeads];
        for(int i=0; i<nLeads; i++){
            ecgc[i] = new ECGCanvas(0);
            ecgc[i].setLead(i);
            ecgPanel.add(ecgc[i]);
        }
        //ecgc.createBufferStrategy(2);
        setVisible(true);
    }

    public void newDataArrived(int i) {
        ecgc[i].offscreenpaint();
    }

    public void newPointArrived(int i, ECGPoint pt) {
        ecgc[i].offscreenpaint(pt);
    }
    
    //Auxiliary variables required for the stats calculation
    private static final int WINDOW_SIZE = 10;
    private int window[] = new int[WINDOW_SIZE];;
    private int index = 0;
    private boolean ready = false;
    
    public void newQRS(int offset){
        //FRAN: here I receive the offset of a new QRS complex, I have to process 
        //this information and update the real-time stats accordingly
        window[index++] = offset;
        if(index==WINDOW_SIZE){
            ready = true;
            index = 0;
        }
        int hbr;
        if(ready){
            hbr = 60*250/((window[(index+WINDOW_SIZE-1)%WINDOW_SIZE]-window[index])/(WINDOW_SIZE-1));
        }
        else{
            hbr = 0;
        }
        String s = (hbr==0 ? "---" : Integer.toString(hbr));
    }

    private class ECGCanvas extends Canvas {

        int lastw, lasth;
        BufferStrategy strategy = null;
        boolean blank = false;
        int lastpos = -1;
        int lead;
        int offset = 0;
        int additionalOffset = 0;
        int newOffset = 0;
        int calculateOffset = 250;
        boolean offsetInitialized = false;
        int canvasType;//0 for real-time visualization, 1 for SD card and 2 for offline
        double zoom = 1.0;
        
        public ECGCanvas(int canvasType){
            this.canvasType = canvasType;
        }
        
        private void setLead(int lead){
            this.lead = lead;
        }
        
        private int getLead(){
            return lead;
        }

        private int scalept(int pt) {
            int h = getHeight();

            return h / 2 - (pt * (h - 30) / 65536);
        }
        
        public void center(){
            this.calculateOffset = 250;
            this.newOffset = 0;
        }
        
        public void increaseOffset(){
            additionalOffset-=10;
        }
        
        public void decreaseOffset(){
            additionalOffset+=10;
        }
        
        public void zoomIn(){
            zoom+=0.1;
        }
        
        public void zoomOut(){
            zoom-=0.1;
            if(zoom<0){
                zoom = 0;
            }
        }

        public void blank() {
            blank = true;
            for(int i=0; i<nLeads; i++){
                ecgc[i].offscreenpaint();
            }
        }
        
        private double RMS(int lead1, int lead2, int lead3) {
            double res = 0;
            if (lead1 > 0) {
                res += lead1 * lead1;
            } else {
                res -= lead1 * lead1;
            }
            if (lead2 > 0) {
                res += lead2 * lead2;
            } else {
                res -= lead2 * lead2;
            }
            if (lead3 > 0) {
                res += lead3 * lead3;
            } else {
                res -= lead3 * lead3;
            }
            if (res > 0) {
                return Math.sqrt(res / 3);
            } else {
                return -Math.sqrt(-res / 3);
            }
        }
        
        public void offscreenpaint() {
            offscreenpaint(null);
        }

        public void offscreenpaint(ECGPoint pt) {
            if (strategy == null) {
                createBufferStrategy(2);
                strategy = getBufferStrategy();
            }

            if (pause) {
                strategy.show();
                return;
            }

            Graphics2D g = (Graphics2D)strategy.getDrawGraphics();
            int w = getWidth();
            int h = getHeight();
            //g.setColor(Color.WHITE);
            //g.fillRect(0, 0, w, h);

            int cpos = mydevice.getPos();

            if (w > 0 && h > 0) {
                if (blank | lastw != w || lasth != h) {
                    g.setColor(Color.WHITE);
                    g.fillRect(0, 0, w, h);
                    lastw = w;
                    lasth = h;
                    blank = false;
                }

                int from = cpos - w + 100;
                int to = cpos;

                if (mydevice.getDelineate() && pt != null && pt.getPos() > from && pt.getPos() <= to) {
                    String s = "";
                    switch (pt.getWave()) {
                        case QRS_WAVE:
                            g.setColor(Color.RED);
                            s = "QRS";
                            break;
                        case P_WAVE:
                            g.setColor(Color.ORANGE);
                            s = "P";
                            break;
                        case T_WAVE:
                            g.setColor(Color.MAGENTA);
                            s = "T";
                            break;
                    }

                    if (pt.getType() == ECGPoint.Type.TYPE_SEC_PEAK) {
                        Color c = g.getColor();
                        g.setColor(new Color(
                                (c.getRed()+2*255)/3,
                                (c.getGreen()+2*255)/3,
                                (c.getBlue()+2*255)/3));
                        g.setStroke(new BasicStroke(1.0f));
                    } else if (pt.getType() == ECGPoint.Type.TYPE_PEAK) {
                        g.setStroke(new BasicStroke(3.0f));
                    }  else {
                        g.setStroke(new BasicStroke(1.0f));
                    }

                    int ptp = pt.getPos() % w;
                    if (pt.getType() == ECGPoint.Type.TYPE_PEAK) {
                        int end = mydevice.getPt(this.getLead(), pt.getPos());
                        if(mydevice.getPt(this.getLead(), pt.getPos()-1) > end){
                            end = mydevice.getPt(this.getLead(), pt.getPos()-1);
                        }
                        if(mydevice.getPt(this.getLead(), pt.getPos()+1) > end){
                            end = mydevice.getPt(this.getLead(), pt.getPos()+1);
                        }
                        g.drawLine(ptp-1, 30, ptp-1, (int)(((scalept(end*3)-offset)*zoom+additionalOffset)/2)+this.getHeight()/2 - 5);
                    }
                    else{
                        g.drawLine(ptp, 30, ptp, (int)(((scalept(mydevice.getPt(this.getLead(), pt.getPos())*3)-offset)*zoom+additionalOffset)/2)+this.getHeight()/2 - 5);
                    }
                    if (pt.getType() == ECGPoint.Type.TYPE_PEAK) {
                        Font f = new Font(null, Font.PLAIN, 20);
                        g.setFont(f);
                        FontMetrics fm = g.getFontMetrics();
                        g.drawString(s, ptp - fm.stringWidth(s) / 2, 5 + fm.getMaxAscent());
                    }
                }

                g.setStroke(new BasicStroke(1.0f));

                int dfrom = (from + w) % w;
                int dto = to % w;

                g.setColor(Color.WHITE);
                g.fillRect(dfrom - 1, 0, 1, h - 50);

                //FRAN: uncomment this to draw the x axis
                /*g.setColor(Color.BLACK);
                g.drawLine(dto - 1, h / 2, dto, h / 2);

                if (cpos % 250 == 0) {
                    g.drawLine(dto, h / 2 - 10, dto, h / 2 + 10);
                } else if (cpos % 125 == 0) {
                    g.drawLine(dto, h / 2 - 5, dto, h / 2 + 5);
                }*/

                g.setColor(Color.BLUE);

                int pval = mydevice.getPt(this.getLead(), to - 1);
                int nval = mydevice.getPt(this.getLead(), to);

                //g.drawLine(dto - 1, scalept(pval)-300, dto, scalept(nval)-300);
                if(calculateOffset>0){
                    newOffset += scalept(nval*3);
                    calculateOffset--;
                }
                if(calculateOffset == 0){
                    offset=newOffset/250;
                    calculateOffset--;
                    offsetInitialized = true;
                }
                if(offsetInitialized){
                    g.drawLine(dto - 1, (int)(((scalept(pval*3)-offset)*zoom+additionalOffset)/2)+this.getHeight()/2, dto, (int)(((scalept(nval*3)-offset)*zoom+additionalOffset)/2)+this.getHeight()/2);
                }

                if ((cpos % 4) == 0) {
                    g.setColor(Color.WHITE);
                    g.fillRect(0, h - 50, w, 50);
                    g.setColor(Color.BLACK);
                    Font f = new Font(null, Font.PLAIN, 30);
                    g.setFont(f);
                    FontMetrics fm = g.getFontMetrics();
                    int pos = (int)(mydevice.getRealOffset()+mydevice.getPos());
                    int sec = (int)((pos / 250) % 60);
                    int min = (int)((pos / (250*60)) % 60);
                    int hour = (int)(pos / (250*60*60));
                    String s = "";
                    if(hour<10){
                        s = "0";
                    }
                    s += hour + ":";
                    if(min<10){
                        s += "0";
                    }
                    s += min + ":";
                    if(sec<10){
                        s += "0";
                    }
                    s += sec;
                    if(this.getLead()==(nLeads-1)){
                        g.drawString(s, 20, h - 15 + fm.getDescent());
                        
                        s = "HBR: " + (mydevice.getHBR()==0 ? "---" : mydevice.getHBR());
                        int stringWidth = fm.stringWidth(s);
                        g.drawString(s, w/3 - stringWidth / 2, h - 15 + fm.getDescent());
                        
                        s = "BP: " + (mydevice.getSystolic()==0 ? "---" : mydevice.getSystolic()) + "/" + (mydevice.getDiastolic()==0 ? "---" : mydevice.getDiastolic());
                        stringWidth = fm.stringWidth(s);
                        g.drawString(s, w*2/3 - stringWidth / 2, h - 15 + fm.getDescent());
                    }
                    if(this.getLead() == 0){
                            s = "ECG";
                        }
                        if(this.getLead() == nLeads-1){
                            s = "Pleth";
                        }
                    g.drawString(s, w - fm.stringWidth(s) - 10, h - 15 + fm.getDescent());
                }
            }

            if ((cpos % 2) == 0) {
                strategy.show();
            }
        }

        @Override
        public void update(Graphics g) {
            paint(g);
        }

        @Override
        public void paint(Graphics g) {
            if(canvasType == 0){
                if (mydevice == null || !mydevice.isConnected()) {
                    int w = getWidth();
                    int h = getHeight();
                    g.setColor(Color.WHITE);
                    g.fillRect(0, 0, w, h);
                } else {
                    if (strategy == null) {
                        offscreenpaint();
                    }
                    strategy.show();
                }
            }
        }
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        bigPanel = new java.awt.Panel();
        jPanel10 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        topPanel = new javax.swing.JPanel();
        scanButton = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        portList = new javax.swing.JComboBox();
        connectButton = new javax.swing.JButton();
        filteringCheckBox = new javax.swing.JCheckBox();
        delineationCheckBox = new javax.swing.JCheckBox();
        jPanel1 = new javax.swing.JPanel();
        recordingButton = new javax.swing.JButton();
        pauseButton = new javax.swing.JButton();
        resetDelineationButton = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        upButton = new javax.swing.JButton();
        downButton = new javax.swing.JButton();
        centerButton = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        systolicTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        diastolicTextField = new javax.swing.JTextField();
        calibrateButton = new javax.swing.JButton();
        ecgPanel = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("SmartCardia ECG Display");
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        bigPanel.setLayout(new java.awt.BorderLayout());

        jPanel10.setLayout(new java.awt.BorderLayout());

        jPanel2.setLayout(new java.awt.GridLayout(3, 1));

        scanButton.setText("Scan");
        scanButton.setActionCommand("scan");
        scanButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                scanButtonActionPerformed(evt);
            }
        });
        topPanel.add(scanButton);

        jLabel3.setText("Port:");
        topPanel.add(jLabel3);

        portList.setEnabled(false);
        portList.setMinimumSize(new java.awt.Dimension(120, 20));
        portList.setPreferredSize(new java.awt.Dimension(120, 20));
        topPanel.add(portList);

        connectButton.setText("Connect");
        connectButton.setEnabled(false);
        connectButton.setPreferredSize(new java.awt.Dimension(120, 25));
        connectButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                connectButtonActionPerformed(evt);
            }
        });
        topPanel.add(connectButton);

        filteringCheckBox.setSelected(true);
        filteringCheckBox.setText("Filtering");
        filteringCheckBox.setEnabled(false);
        filteringCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                filteringCheckBoxActionPerformed(evt);
            }
        });
        topPanel.add(filteringCheckBox);

        delineationCheckBox.setSelected(true);
        delineationCheckBox.setText("Delineation");
        delineationCheckBox.setEnabled(false);
        delineationCheckBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                delineationCheckBoxActionPerformed(evt);
            }
        });
        topPanel.add(delineationCheckBox);

        jPanel2.add(topPanel);

        recordingButton.setText("Start recording");
        recordingButton.setEnabled(false);
        recordingButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recordingButtonActionPerformed(evt);
            }
        });
        jPanel1.add(recordingButton);

        pauseButton.setText("Pause");
        pauseButton.setEnabled(false);
        pauseButton.setPreferredSize(new java.awt.Dimension(90, 25));
        pauseButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pauseButtonActionPerformed(evt);
            }
        });
        jPanel1.add(pauseButton);

        resetDelineationButton.setText("Reset delineation");
        resetDelineationButton.setEnabled(false);
        resetDelineationButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                resetDelineationButtonActionPerformed(evt);
            }
        });
        jPanel1.add(resetDelineationButton);

        jLabel1.setText("Vertical position:");
        jLabel1.setEnabled(false);
        jPanel1.add(jLabel1);

        upButton.setText("+");
        upButton.setEnabled(false);
        upButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                upButtonActionPerformed(evt);
            }
        });
        jPanel1.add(upButton);

        downButton.setText("-");
        downButton.setEnabled(false);
        downButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                downButtonActionPerformed(evt);
            }
        });
        jPanel1.add(downButton);

        centerButton.setText("Center");
        centerButton.setEnabled(false);
        centerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                centerButtonActionPerformed(evt);
            }
        });
        jPanel1.add(centerButton);

        jPanel2.add(jPanel1);

        jLabel2.setText("Systolic:");
        jLabel2.setEnabled(false);
        jPanel3.add(jLabel2);

        systolicTextField.setEnabled(false);
        systolicTextField.setMinimumSize(new java.awt.Dimension(50, 20));
        systolicTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        jPanel3.add(systolicTextField);

        jLabel4.setText("Diastolic:");
        jLabel4.setEnabled(false);
        jPanel3.add(jLabel4);

        diastolicTextField.setEnabled(false);
        diastolicTextField.setMinimumSize(new java.awt.Dimension(50, 20));
        diastolicTextField.setPreferredSize(new java.awt.Dimension(50, 20));
        jPanel3.add(diastolicTextField);

        calibrateButton.setText("Calibrate");
        calibrateButton.setToolTipText("");
        calibrateButton.setEnabled(false);
        calibrateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calibrateButtonActionPerformed(evt);
            }
        });
        jPanel3.add(calibrateButton);

        jPanel2.add(jPanel3);

        jPanel10.add(jPanel2, java.awt.BorderLayout.NORTH);

        ecgPanel.setMinimumSize(new java.awt.Dimension(271, 200));
        ecgPanel.setPreferredSize(new java.awt.Dimension(500, 500));
        ecgPanel.addMouseWheelListener(new java.awt.event.MouseWheelListener() {
            public void mouseWheelMoved(java.awt.event.MouseWheelEvent evt) {
                ecgPanelMouseWheelMoved(evt);
            }
        });
        ecgPanel.setLayout(new java.awt.GridLayout(2, 1));
        jPanel10.add(ecgPanel, java.awt.BorderLayout.CENTER);

        bigPanel.add(jPanel10, java.awt.BorderLayout.CENTER);

        getContentPane().add(bigPanel);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void connectButtonActionPerformed(java.awt.event.ActionEvent evt) {
        if(mydevice == null){
            mydevice = new ComDevice((String) this.portList.getSelectedItem().toString());
            try {
                connectButton.setEnabled(false);
                pause = false;
                mydevice.setListener(this);
                mydevice.connect();
                for(int i=0; i<nLeads; i++){
                    ecgc[i].blank();
                }
                pauseButton.setEnabled(true);
                connectButton.setEnabled(true);
                resetDelineationButton.setEnabled(true);
                recordingButton.setEnabled(true);
                filteringCheckBox.setEnabled(true);
                delineationCheckBox.setEnabled(true);
                //modeButton.setEnabled(true);
                jLabel1.setEnabled(true);
                //jLabel4.setEnabled(true);
                upButton.setEnabled(true);
                downButton.setEnabled(true);
                centerButton.setEnabled(true);
                jLabel2.setEnabled(true);
                systolicTextField.setEnabled(true);
                jLabel4.setEnabled(true);
                diastolicTextField.setEnabled(true);
                calibrateButton.setEnabled(true);
                connectButton.setText("Disconnect");
            } catch (IOException ex) {
                ex.printStackTrace();
                connectButton.setEnabled(true);
                pauseButton.setEnabled(false);
                resetDelineationButton.setEnabled(false);
                recordingButton.setEnabled(false);
                filteringCheckBox.setEnabled(false);
                delineationCheckBox.setEnabled(false);
                jLabel1.setEnabled(false);
                upButton.setEnabled(false);
                downButton.setEnabled(false);
                centerButton.setEnabled(false);
                jLabel2.setEnabled(false);
                systolicTextField.setEnabled(false);
                jLabel4.setEnabled(false);
                diastolicTextField.setEnabled(false);
                calibrateButton.setEnabled(false);
                connectButton.setText("Connect");
            }
        } else {
            mydevice.close();
            while(mydevice.isConnected());
            mydevice = null;
            connectButton.setEnabled(true);
            pauseButton.setEnabled(false);
            resetDelineationButton.setEnabled(false);
            recordingButton.setEnabled(false);
            filteringCheckBox.setEnabled(false);
            delineationCheckBox.setEnabled(false);
            jLabel1.setEnabled(false);
            upButton.setEnabled(false);
            downButton.setEnabled(false);
            centerButton.setEnabled(false);
            jLabel2.setEnabled(false);
            systolicTextField.setEnabled(false);
            jLabel4.setEnabled(false);
            diastolicTextField.setEnabled(false);
            calibrateButton.setEnabled(false);
            connectButton.setText("Connect");
        }
}

    private void pauseButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pauseButtonActionPerformed
        if (pause) {
            pause = false;
            pauseButton.setText("Pause");
        } else {
            pause = true;
            for(int i=0; i<nLeads; i++){
                ecgc[i].blank();
            }
            pauseButton.setText("Resume");
        }
}//GEN-LAST:event_pauseButtonActionPerformed

    private void recordingButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recordingButtonActionPerformed
        if(mydevice.getRecording()){
            mydevice.stopRecording();
            recordingButton.setText("Start recording");
        }
        else{
            mydevice.startRecording();
            recordingButton.setText("Stop recording");
        }
    }//GEN-LAST:event_recordingButtonActionPerformed

    private void upButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_upButtonActionPerformed
        for(int i=0; i<nLeads; i++){
            ecgc[i].increaseOffset();
        }
    }//GEN-LAST:event_upButtonActionPerformed

    private void downButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_downButtonActionPerformed
        for(int i=0; i<nLeads; i++){
            ecgc[i].decreaseOffset();
        }
    }//GEN-LAST:event_downButtonActionPerformed

    private void scanButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_scanButtonActionPerformed
        // TODO add your handling code here:
        this.scanPorts();
        this.portList.setEnabled(true);
        this.connectButton.setEnabled(true);
    }//GEN-LAST:event_scanButtonActionPerformed

    private void centerButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_centerButtonActionPerformed
        // TODO add your handling code here:
        for(int i=0; i<nLeads; i++){
            ecgc[i].center();
        }
    }//GEN-LAST:event_centerButtonActionPerformed

    private long convert(long n){
        long data[] = new long[3];
        data[0] = (n>>16) & 0xFF;
        data[1] = (n>>8) & 0xFF;
        data[2] = n & 0xFF;
        
        long aux = data[0] << 16 | data[1] << 8 | data[2];
        if ((aux>>23)!=0){
            return -(16777216-((aux<<1)>>1));
        }
        else{
            return aux;
        }
    }
    
    private void ecgPanelMouseWheelMoved(java.awt.event.MouseWheelEvent evt) {//GEN-FIRST:event_ecgPanelMouseWheelMoved
        for(int i=0; i<2; i++){
            if(evt.getUnitsToScroll()<0){
                ecgc[i].zoomIn();
            }
            else{
                ecgc[i].zoomOut();
            }
        }
    }//GEN-LAST:event_ecgPanelMouseWheelMoved

    private void resetDelineationButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_resetDelineationButtonActionPerformed
        mydevice.resetDelineation();
    }//GEN-LAST:event_resetDelineationButtonActionPerformed

    private void filteringCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_filteringCheckBoxActionPerformed
        mydevice.setFilter(!mydevice.getFilter());
        for(int i=0; i<nLeads; i++){
            ecgc[i].center();
        }
    }//GEN-LAST:event_filteringCheckBoxActionPerformed

    private void delineationCheckBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_delineationCheckBoxActionPerformed
        mydevice.setDelineate(!mydevice.getDelineate());
    }//GEN-LAST:event_delineationCheckBoxActionPerformed

    private void calibrateButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calibrateButtonActionPerformed
        //Send signals to Alair
        mydevice.startBuffering();
    }//GEN-LAST:event_calibrateButtonActionPerformed

    public void scanPorts() {
        this.portList.removeAllItems();
        
	Enumeration ports = CommPortIdentifier.getPortIdentifiers();

	if (ports == null) {
	    System.out.println("No comm ports found!");
	    return;
	}

	// print out all ports
	//System.out.println("printing all ports...");
	while (ports.hasMoreElements()) {
            CommPortIdentifier portId = (CommPortIdentifier)ports.nextElement();
            if(portId.getPortType() == CommPortIdentifier.PORT_SERIAL){
                this.portList.addItem(portId.getName());
            }
	}
	//System.out.println("done.");
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new ECGFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private java.awt.Panel bigPanel;
    private javax.swing.JButton calibrateButton;
    private javax.swing.JButton centerButton;
    private javax.swing.JButton connectButton;
    private javax.swing.JCheckBox delineationCheckBox;
    private javax.swing.JTextField diastolicTextField;
    private javax.swing.JButton downButton;
    private javax.swing.JPanel ecgPanel;
    private javax.swing.JCheckBox filteringCheckBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JButton pauseButton;
    private javax.swing.JComboBox portList;
    private javax.swing.JButton recordingButton;
    private javax.swing.JButton resetDelineationButton;
    private javax.swing.JButton scanButton;
    private javax.swing.JTextField systolicTextField;
    private javax.swing.JPanel topPanel;
    private javax.swing.JButton upButton;
    // End of variables declaration//GEN-END:variables
}
