/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ecgdisplay;

/**
 *
 * @author nicolas
 */
public interface ComDeviceListener {
    public void newDataArrived(int i);

    public void newPointArrived(int i, ECGPoint pt);
    
    public void newQRS(int offset);
}
