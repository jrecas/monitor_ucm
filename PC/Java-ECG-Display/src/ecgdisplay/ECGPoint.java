/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package ecgdisplay;

/**
 *
 * @author nicolas
 */
public class ECGPoint {
    public enum Wave { QRS_WAVE, P_WAVE, T_WAVE };
    public enum Type { TYPE_ONSET, TYPE_PEAK, TYPE_END, TYPE_SEC_PEAK };

    private Wave wave;
    private Type type;
    private int pos;

    public ECGPoint(int pos, Wave wave, Type type) {
        this.pos = pos;
        this.wave = wave;
        this.type = type;
    }

    /**
     * @return the wave
     */
    public Wave getWave() {
        return wave;
    }

    /**
     * @return the type
     */
    public Type getType() {
        return type;
    }

    /**
     * @return the pos
     */
    public int getPos() {
        return pos;
    }
}
