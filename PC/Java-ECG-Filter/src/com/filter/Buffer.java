/* 
 * Buffer.java
 */

package com.filter;

/**
 * Implementa un buffer con un indice de escritura y un indice de lectura.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/08/10
 */
public class Buffer {
	
	/** Buffer. */
	protected int[] buffer;
	
	/** Tamaño del buffer. */
	protected final int bufferSize;
	
	/** Indice de escritura. */
	protected int indexWrite;
	
	/** Indice de lectura. */
	protected int indexRead;
	
	/** Indices de comienzo y final de un intervalo. */
	protected int[] indexInterval;
	
	/** Periodo entre datos */
	protected final int T;
	
	/**
	 * Constructor de Buffer.
	 * @param bufSize Tamaño del buffer
	 * @param indWrite Indice de escritura
	 * @param indRead Indice de lectura
	 * @param t Periodo entre datos
	 */
	public Buffer(int bufSize, int indWrite, int indRead, int t) {
		buffer = new int[bufSize];
		bufferSize = bufSize;
		indexWrite = indWrite;
		indexRead = indRead;
		indexInterval = new int[2];
		T = t;
	}
	
	/**
	 * Guardar una muestra (int) en el buffer en la posicion
	 * dada por el indice de escritura.
	 * @param sample Muestra
	 */
	public void writeSample(int sample) {
		buffer[indexWrite] = sample;
		incrementIndexWrite();
	}
	
	/**
	 * Guardar una muestra (double) en el buffer en la posicion
	 * dada por el indice de escritura.
	 * @param sample Muestra
	 */
	public void writeSample(double sample) {
		writeSample((int) sample);
	}
	
	/**
	 * Obtener el valor de una muestra del buffer de una posicion.
	 * @param pos Posicion de la muestra en el buffer
	 * @return Muestra
	 */
	public int getSample(int pos) {
		return buffer[pos];
	}
	
	/**
	 * Extraer una muestra del buffer en la posicion dada por
	 * el indice de lectura.
	 * @return Muestra
	 */
	public int readSample() {
		int sample = buffer[indexRead];
		incrementIndexRead();
		return sample;
	}
	
	/**
	 * Actualizar un nuevo intervalo en el buffer.
	 * @param newPos Nueva posicion final del intervalo
	 */
	public void setInterval(int newPos) {
		indexInterval[0] = indexInterval[1];
		indexInterval[1] = newPos % bufferSize;
	}
	
	/** Incrementar el indice de escritura. */
	protected void incrementIndexWrite() {
		indexWrite++;
		if (indexWrite == bufferSize) {
			indexWrite = 0;
		}
	}
	
	/** Incrementar el indice de lectura. */
	protected void incrementIndexRead() {
		indexRead++;
		if (indexRead == bufferSize) {
			indexRead = 0;
		}
	}
}