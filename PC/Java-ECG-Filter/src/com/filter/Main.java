package com.filter;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		DataInputStream dis = null;
		DataOutputStream dos = null;
		
		FileInputStream fis = null;
		FileOutputStream fos = null;
		
		int fSample = 250;
		
		ECGFilter ecgFilter = new ECGFilter(fSample);
		
		int sample;
		
		//System.out.println("Filtering...");
		
		try {
			
			fis = new FileInputStream("../ECG-DATA/ECG" + String.valueOf(fSample) + ".log");
			fos = new FileOutputStream("fECG" + String.valueOf(fSample) + ".log");
			
			dis = new DataInputStream(fis);
			dos = new DataOutputStream(fos);

			while (true) {
				sample = fSample == 250 ? dis.readShort() : dis.readInt();
				//sample = dis.readInt();
				sample = ecgFilter.filterSample(sample);
				System.out.println(sample);
				dos.writeInt(sample);
			}
			
		} catch (IOException e) {
			// nothing
		}
		try {
			dis.close();
			dos.close();
		} catch (IOException e) {
			// nothing
		}
		
		System.out.println("Finished!");
	}

}
