/* 
 * NotchFilter.java
 */

package com.filter;

/**
 * Filtra la señal mediante un filtro Notch.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/22
 */

public class NotchFilter {
	
	/**
	 * Retardo del filtro (variable de 1 a 5 muestras, pero parece que 1 es
	 * la mejor aproximación para 250 Hz y 3 para 1 kHz). Se deja en 2.
	 */
	public static final int FILTER_DELAY = 2;
	
	/**
	 * Coeficientes del filtro Notch con frecuencia de eliminacion de 50 Hz.
	 * Numerador: b = {b0, b1, b2}. Simétrico: b0 = b2.
	 * Denominador: a = {1, a1, a2}. b1 = a1.
	 * Sólo es necesario almacenar b0, b1 y a2.
	 * x_n = b_n * (x_n - x_(n-2)) + b_(n-1) * (x_(n-1) - y_(n-1))
	 * 		- a_(n-2) * y_(n-2).
	 */
	private double[] filterCoefs;
	
	/** Muestras para implementar el filtro Notch. */
	private double[] xSamples;
	private double[] ySamples;
	
	/** Constructor de NotchFilter. */
	public NotchFilter(int frequency) {
		filterCoefs = new double[3];
		setFilterCoefs(frequency);
		xSamples = new double[2];
		ySamples = new double[2];
	}
	
	/**
	 * Filtrar las muestras (double).
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public double filterSample(double sample) {
		double tempSample = filterCoefs[0] * (sample + xSamples[1]) +
				filterCoefs[1] * (xSamples[0] - ySamples[0]) -
				filterCoefs[2] * ySamples[1];
		xSamples[1] = xSamples[0];
		xSamples[0] = sample;
		ySamples[1] = ySamples[0];
		ySamples[0] = tempSample;
		return tempSample;
	}
	
	/**
	 * Filtrar las muestras (int).
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(int sample) {
		return (int) filterSample((double) sample);
	}
	
	/**
	 * Elegir los coeficientes del filtro en funcion de la frecuencia
	 * de muestreo. Por defecto se elige a 250 Hz.
	 * @param frequency Frecuencia de muestreo
	 */
	private void setFilterCoefs(int frequency) {
		if (frequency == 1000) {
			filterCoefs[0] = 0.863271264002681;
			filterCoefs[1] = -1.642039521920206;
			filterCoefs[2] = 0.726542528005361;
		} else {
			filterCoefs[0] = 0.579192220162268;
			filterCoefs[1] = -0.357960478079794;
			filterCoefs[2] = 0.158384440324536;
		}
	}
}
