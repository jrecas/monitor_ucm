/* 
 * ECGLowFIRFilter.java
 */

package com.filter;

/**
 * Implementa un filtro FIR paso bajo de orden 40 y frecuencia de corte de
 * 16 Hz para filtrar la señal de ECG.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/22
 */

public class ECGLowFIRFilter {
	
	/** Retardo del filtro. */
	public static final int FILTER_DELAY = 20;
	
	/** Coeficientes del filtro. */	
	private int[] filterCoefs;
	
	/** Muestras para implementar el filtro. */
	private long[] xSamples;
	
	/** Constructor de ECGLowFIRFilter1k. */
	public ECGLowFIRFilter(int frequency) {
		filterCoefs = new int[21];
		setFilterCoefs(frequency);
		xSamples = new long[41];
	}
	
	/**
	 * Filtrar las muestras (int) de ECG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(int sample) {
		xSamples[40] = sample;
		long tempSample = filterCoefs[20] * xSamples[20];
		for (int i = 0; i < 20; i++) {
			tempSample += filterCoefs[i] * (xSamples[i] + xSamples[40 - i]);
		}
		for (int i = 0; i < 40; i++) {
			xSamples[i] = xSamples[i + 1];
		}
		return (int) ((double) tempSample * 0.000001);
	}
	
	/**
	 * Filtrar las muestras (double) de ECG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(double sample) {
		return filterSample((int) sample);
	}
	
	/**
	 * Elegir los coeficientes del filtro en funcion de la frecuencia
	 * de muestreo. Por defecto se elige a 250 Hz.
	 * Los coeficientes hay que multiplicarlos por 1E-5.
	 * @param frequency Frecuencia de muestreo
	 */
	private void setFilterCoefs(int frequency) {
		if (frequency == 1000) {
			filterCoefs[0] = 1848;
			filterCoefs[1] = 2171;
			filterCoefs[2] = 2826;
			filterCoefs[3] = 3872;
			filterCoefs[4] = 5354;
			filterCoefs[5] = 7297;
			filterCoefs[6] = 9705;
			filterCoefs[7] = 12560;
			filterCoefs[8] = 15821;
			filterCoefs[9] = 19424;
			filterCoefs[10] = 23286;
			filterCoefs[11] = 27305;
			filterCoefs[12] = 31368;
			filterCoefs[13] = 35352;
			filterCoefs[14] = 39130;
			filterCoefs[15] = 42578;
			filterCoefs[16] = 45581;
			filterCoefs[17] = 48033;
			filterCoefs[18] = 49850;
			filterCoefs[19] = 50967;
			filterCoefs[20] = 51343;
		} else {
			filterCoefs[0] = 1247;
			filterCoefs[1] = 1399;
			filterCoefs[2] = 1476;
			filterCoefs[3] = 1276;
			filterCoefs[4] = 500;
			filterCoefs[5] = -1130;
			filterCoefs[6] = -3717;
			filterCoefs[7] = -7037;
			filterCoefs[8] = -10458;
			filterCoefs[9] = -12948;
			filterCoefs[10] = -13209;
			filterCoefs[11] = -9921;
			filterCoefs[12] = -2039;
			filterCoefs[13] = 10900;
			filterCoefs[14] = 28557;
			filterCoefs[15] = 49710;
			filterCoefs[16] = 72342;
			filterCoefs[17] = 93915;
			filterCoefs[18] = 111762;
			filterCoefs[19] = 123544;
			filterCoefs[20] = 127660;
		}
	}
}
