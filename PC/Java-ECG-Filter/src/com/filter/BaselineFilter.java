/* 
 * BaselineFilter.java
 */

package com.filter;


/**
 * Implementa un filtro para eliminar la componente continua del ECG.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/22
 */

public class BaselineFilter {
	
	/** Ventanas de apertura y cierre. */
	private int openWindow;
	private int closeWindow;
	
	/** Retardo del filtro. */
	private int filterDelay;
	
	/** Objetos erosion y dilatacion de apertura y cierre. */
	private Erosion erosionOpen;
	private Dilation dilationOpen;
	private Dilation dilationClose;
	private Erosion erosionClose;
	
	/** Buffer para almacenar las muestras. */
	private Buffer buffer;
	
	/** Constructor de BaselineFilter1k para eliminar la componente continua. */
	public BaselineFilter(int frequency) {
		setWindows(frequency);
		setDelay(frequency);
		erosionOpen = new Erosion(openWindow);
		dilationOpen = new Dilation(openWindow);
		dilationClose = new Dilation(closeWindow);
		erosionClose = new Erosion(closeWindow);
		buffer = new Buffer(filterDelay + 1, 0, 1, 0);
	}
	
	/**
	 * Obtener el retardo del filtro.
	 * @return Retardo del filtro
	 */
	public int getFilterDelay() {
		return filterDelay;
	}
	
	/**
	 * Filtrar las muestras (int).
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 * @throws IOException 
	 */
	public int filterSample(int sample) {
		buffer.writeSample(sample);
		sample = erosionOpen.write(sample);
		sample = dilationOpen.write(sample);
		sample = dilationClose.write(sample);
		sample = erosionClose.write(sample);
		int tempSample = buffer.readSample();
		return (tempSample + sample);
	}
	
	/**
	 * Filtrar las muestras (double).
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 * @throws IOException 
	 */
	public int filterSample(double sample) {
		return filterSample((int) sample);
	}
	
	/**
	 * Elegir las ventanas para el filtro.
	 * @param frequency Frecuencia de muestreo
	 */
	private void setWindows(int frequency) {
		openWindow = (int) (0.2 * (double) frequency);
		if ((openWindow % 2) == 0) {
			filterDelay--;
		}
		closeWindow = (int) (0.3 * (double) frequency);
		if ((closeWindow % 2) == 0) {
			filterDelay--;
		}
	}
	
	/**
	 * Hallar el retardo del filtro.
	 * @param frequency Frecuencia de muestreo
	 */
	private void setDelay(int frequency) {
		filterDelay = openWindow + closeWindow;
	}
}
