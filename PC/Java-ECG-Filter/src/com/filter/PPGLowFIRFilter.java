/* 
 * PPGLowFIRFilter.java
 */

package com.filter;

/**
 * Implementa un filtro FIR paso bajo de orden 16 y frecuencia de corte de
 * 10 Hz para filtrar la señal de PPG.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/08
 */

public class PPGLowFIRFilter {
	
	/** Retardo del filtro. */
	public static final int FILTER_DELAY = 8;
	
	/**
	 * Coeficientes del filtro a 83.3 Hz de frecuencia de muestreo.
	 * Los coeficientes hay que multiplicarlos por 1E-5.
	 */
	private static final int[] FILTER_COEFS = {
		-789, -4402, -11154, -13576, 5368, 58350, 136997, 209591, 239227
	};
	
	/** Muestras para implementar el filtro. */
	private int[] xSamples;
	
	/** Constructor de ECGLowFIRFilter. */
	public PPGLowFIRFilter() {
		xSamples = new int[17];
	}
	
	/**
	 * Filtrar las muestras (int) de PPG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(int sample) {
		xSamples[16] = sample;
		int tempSample = FILTER_COEFS[8] * xSamples[8];
		for (int i = 0; i < 8; i++) {
			tempSample += FILTER_COEFS[i] * (xSamples[i] + xSamples[16 - i]);
		}
		for (int i = 0; i < 16; i++) {
			xSamples[i] = xSamples[i + 1];
		}
		return (int) ((double) tempSample * 0.000001);
	}
	
	/**
	 * Filtrar las muestras (double) de PPG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(double sample) {
		return filterSample((int) sample);
	}
}
