/* 
 * Erosion.java
 */

package com.filter;

/**
 * Implementa la erosion de un filtro BaseLine.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/09
 */

public class Erosion {
	
	/** Tamaño del buffer. */
	private final int bufferSize;
	
	/** Posicion central del buffer. */
	private final int centerPos;
	
	/** Buffer de muestras. */
	private int[] buffer;
	
	/** Indice de llenado del buffer. */
	private int bufferIndex;
	
	/** Minimo del buffer. */
	private int minimum;
	
	/** Posicion del minimo del buffer. */
	private int minimumPos;
	
	/**
	 * Constructor de Erosion.
	 * @param size Tamaño del buffer
	 */
	public Erosion(int size) {
		if ((size % 2) == 0) {
			size--;
		}
		bufferSize = size;
		centerPos = size / 2;
		buffer = new int[size];
		bufferIndex = 0;
		minimum = Integer.MAX_VALUE;
		minimumPos = 0;
	}
	
	/**
	 * Obtener la posicion central del buffer que coincide con el retardo
	 * del filtro.
	 * @return Posicion central
	 */
	public int getCenterPos() {
		return centerPos;
	}
	
	/**
	 * Escribir una nueva muestra (int) en el buffer y devuelve el minimo.
	 * @param sample Muestra a escribir
	 * @return Minimo
	 */
	public int write(int sample) {
		
		buffer[bufferIndex] = sample;
		
		if (sample <= minimum) {
			minimum = sample;
			minimumPos = bufferIndex;
		} else if (bufferIndex == minimumPos) {
			findMinimum();
		}
		
		bufferIndex++;
		
		if (bufferIndex == bufferSize) {
			bufferIndex = 0;
		}
		
		return minimum;
	}
	
	/**
	 * Escribir una nueva muestra (double) en el buffer y devuelve el minimo.
	 * @param sample Muestra a escribir
	 * @return Minimo
	 */
	public int write(double sample) {
		return write((int) sample);
	}
	
	/**
	 * Encontrar el minimo del buffer.
	 * @return Posicion del minimo
	 */
	private void findMinimum() {
		minimumPos = 0;
		minimum = buffer[minimumPos];
		for (int i = 1; i < bufferSize; i++) {
			if (buffer[i] < minimum) {
				minimum = buffer[i];
				minimumPos = i;
			}
		}
	}
}
