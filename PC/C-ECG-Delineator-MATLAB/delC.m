ecg = readLog('./','exampleQTDB','uint16','b');
ecg40 = readLog('./','fECG250_40','int32','l');
ecg14 = readLog('./','fECG250_14','int32','l');
bl = readLog('./','fECG250_BL','int16','l');
del = readLog('./','fECG250_DEL','uint64','l');
del = reshape(del,8,length(del)/8)';
del = del + 1;

% ecgMat = delineationECG(ecg,250,false);
% index = isnan(ecgMat.bumps);
% ecgMat.bumps(index) = 1;
% ecgMat.bumps(end-5:end,:) = 1;
% ecgMat.bumps = ecgMat.bumps + 20;

hold on;
grid();
%plot(ecg14,'r');
plot(ecg40,'b.-');
%plot(bl(63:end),'r');
plot(del(:,1),ecg40(del(:,1)),'ko','linewidth',2,'markersize',10);
plot(del(:,2),ecg40(del(:,2)),'k*','linewidth',2,'markersize',10);
plot(del(:,3),ecg40(del(:,3)),'ko','linewidth',2,'markersize',10);
plot(del(:,4),ecg40(del(:,4)),'ms','linewidth',2,'markersize',10);
plot(del(:,5),ecg40(del(:,5)),'r*','linewidth',2,'markersize',10);
plot(del(:,6),ecg40(del(:,6)),'y^','linewidth',2,'markersize',10);
plot(del(:,7),ecg40(del(:,7)),'g*','linewidth',2,'markersize',10);
plot(del(:,8),ecg40(del(:,8)),'go','linewidth',2,'markersize',10);

%plot(ecgMat.bumps(:,4),ecg40(ecgMat.bumps(:,4)),'ks','linewidth',2,'markersize',10)
%plot(ecgMat.bumps(:,10),ecg40(ecgMat.bumps(:,10)),'ms','linewidth',2,'markersize',10)