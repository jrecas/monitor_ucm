/*
 * LinearRegression.java
 */

package com.ucm.monitor.maths;

/**
 * Implementa una regresion lineal.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/08/26
 */
public class LinearRegression {
	
	/** Array x. */
	private double[] x;
	
	/** Array y. */
	private double[] y;
	
	/** Numero de datos de x e y. */
	private int N;
	
	/** Coefiente de determinacion R2. */
	private double R2;
	
	/** Coefiente de determinacion R2 ajustado. */
	private double adjR2;
	
	/**
	 * Coeficientes del ajuste lineal.
	 * 0: ordenada, 1: pendiente.
	 */
	private double[] coefficients;
	
	/**
	 * Constructor de LinearRegression.
	 * @param arrayX Array x
	 * @param arrayY Array y
	 */
	public LinearRegression(double[] arrayX, double[] arrayY) {
		x = arrayX;
		y = arrayY;
		N = arrayX.length;
		R2 = 0;
		adjR2 = 0;
		coefficients = new double[2];
		calcCoefficients();
	}
	
	/**
	 * Obtener el coeficientes de determinacion R2.
	 * @return Coeficiente R2
	 */
	public double getR2() {
		return R2;
	}
	
	/**
	 * Obtener el coeficientes de determinacion ajustado R2.
	 * @return Coeficiente R2 ajustado
	 */
	public double getAdjR2() {
		return adjR2;
	}
	
	/**
	 * Obtener los coeficientes del ajuste lineal.
	 * @return Coeficientes del ajuste lineal
	 */
	public double[] getCoefficients() {
		return coefficients;
	}
	
	/** Calcular coeficientes de la regresion lineal. */
	private void calcCoefficients() {
		double meanX = 0;
		double meanY = 0;
		
		for (int i = 0; i < N; i++) {
			meanX += x[i];
			meanY += y[i];
		}
		meanX = meanX / N;
		meanY = meanY / N;
		
		double sumXY = 0;
		double sumXX = 0;
		double sumYY = 0;
		double tempX;
		double tempY;
		for (int i = 0; i < N; i++) {
			tempX = x[i] - meanX;
			tempY = y[i] - meanY;
			sumXY += tempX * tempY;
			sumXX += tempX * tempX;
			sumYY += tempY * tempY;
		}
		R2 = (sumXY * sumXY) / (sumXX * sumYY);
		adjR2 = 1 - ((double) (N - 1) / (N - 2)) * (1 - R2);
		coefficients[1] = sumXY / sumXX;
		coefficients[0] = meanY - coefficients[1] * meanX;
	}
}