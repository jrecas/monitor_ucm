/* 
 * ECGLowFIRFilter.java
 */

package com.ucm.monitor.filter;

/**
 * Implementa un filtro FIR paso bajo de orden 40 y frecuencia de corte de 40 Hz
 * para 1 kHz y 38.885 Hz para 250 Hz (y, sólo para 250 Hz, aprovechar los lóbulos
 * filtrando la señal de la red eléctrica de 50 Hz) filtrando la señal de ECG.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/28
 */

public class ECGLowFIRFilter {
	
	/** Retardo del filtro. */
	public static final int FILTER_DELAY = 20;
	
	/** Coeficientes del filtro. */	
	private int[] filterCoefs;
	
	/** Muestras para implementar el filtro. */
	private long[] xSamples;
	
	/** Constructor de ECGLowFIRFilter1k. */
	public ECGLowFIRFilter(int frequency) {
		filterCoefs = new int[21];
		setFilterCoefs(frequency);
		xSamples = new long[41];
	}
	
	/**
	 * Filtrar las muestras (int) de ECG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(int sample) {
		xSamples[40] = sample;
		long tempSample = filterCoefs[20] * xSamples[20];
		for (int i = 0; i < 20; i++) {
			tempSample += filterCoefs[i] * (xSamples[i] + xSamples[40 - i]);
		}
		for (int i = 0; i < 40; i++) {
			xSamples[i] = xSamples[i + 1];
		}
		return (int) ((double) tempSample * 0.000001);
	}
	
	/**
	 * Filtrar las muestras (double) de ECG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(double sample) {
		return filterSample((int) sample);
	}
	
	/**
	 * Elegir los coeficientes del filtro en funcion de la frecuencia
	 * de muestreo. Por defecto se elige a 250 Hz.
	 * Los coeficientes hay que multiplicarlos por 1E-5.
	 * @param frequency Frecuencia de muestreo
	 */
	private void setFilterCoefs(int frequency) {
		if (frequency == 1000) {
			filterCoefs[0] = -1223;
			filterCoefs[1] = -1446;
			filterCoefs[2] = -1798;
			filterCoefs[3] = -2226;
			filterCoefs[4] = -2598;
			filterCoefs[5] = -2704;
			filterCoefs[6] = -2279;
			filterCoefs[7] = -1026;
			filterCoefs[8] = 1336;
			filterCoefs[9] = 5034;
			filterCoefs[10] = 10202;
			filterCoefs[11] = 16839;
			filterCoefs[12] = 24798;
			filterCoefs[13] = 33774;
			filterCoefs[14] = 43325;
			filterCoefs[15] = 52899;
			filterCoefs[16] = 61884;
			filterCoefs[17] = 69663;
			filterCoefs[18] = 75677;
			filterCoefs[19] = 79479;
			filterCoefs[20] = 80779;
		} else {
			filterCoefs[0] = 817;
			filterCoefs[1] = -398;
			filterCoefs[2] = -1726;
			filterCoefs[3] = -1919;
			filterCoefs[4] = 238;
			filterCoefs[5] = 3953;
			filterCoefs[6] = 5510;
			filterCoefs[7] = 1119;
			filterCoefs[8] = -7857;
			filterCoefs[9] = -13148;
			filterCoefs[10] = -5868;
			filterCoefs[11] = 12747;
			filterCoefs[12] = 27145;
			filterCoefs[13] = 18039;
			filterCoefs[14] = -17523;
			filterCoefs[15] = -54294;
			filterCoefs[16] = -50441;
			filterCoefs[17] = 20999;
			filterCoefs[18] = 144366;
			filterCoefs[19] = 262581;
			filterCoefs[20] = 311320;
		}
	}
}
