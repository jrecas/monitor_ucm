/* 
 * Dilation.java
 */

package com.ucm.monitor.filter;

/**
 * Implementa la dilatacion de un filtro Baseline.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/09
 */

public class Dilation {
	
	/** Tamaño del buffer. */
	private final int bufferSize;
	
	/** Posicion central del buffer. */
	private final int centerPos;
	
	/** Buffer de muestras. */
	private int[] buffer;
	
	/** Indice de llenado del buffer. */
	private int bufferIndex;
	
	/** Maximo del buffer. */
	private int maximum;
	
	/** Posicion del maximo del buffer. */
	private int maximumPos;
	
	/**
	 * Constructor de Dilation.
	 * @param size Tamaño del buffer
	 */
	public Dilation(int size) {
		bufferSize = size;
		centerPos = size / 2;
		buffer = new int[size];
		bufferIndex = 0;
		maximum = Integer.MIN_VALUE;
		maximumPos = 0;
	}
	
	/**
	 * Obtener la posicion central del buffer que coincide con el retardo
	 * del filtro.
	 * @return Posicion central
	 */
	public int getCenterPos() {
		return centerPos;
	}
	
	/**
	 * Escribe una nueva muestra (int) en el buffer y devuelve el maximo.
	 * @param sample Muestra a escribir
	 * @return Maximo
	 */
	public int write(int sample) {
		
		buffer[bufferIndex] = sample;
		
		if (sample >= maximum) {
			maximum = sample;
			maximumPos = bufferIndex;
		} else if (bufferIndex == maximumPos) {
			findMaximum();
		}
			
		bufferIndex++;
		
		if (bufferIndex == bufferSize) {
			bufferIndex = 0;
		}
		
		return maximum;
	}
	
	/**
	 * Escribir una nueva muestra (double) en el buffer y devuelve el minimo.
	 * @param sample Muestra a escribir
	 * @return Minimo
	 */
	public int write(double sample) {
		return write((int) sample);
	}
	
	/**
	 * Encontrar el maximo del buffer.
	 * @return Posicion del maximo
	 */
	private void findMaximum() {
		maximumPos = 0;
		maximum = buffer[maximumPos];
		for (int i = 1; i < bufferSize; i++) {
			if (buffer[i] > maximum) {
				maximum = buffer[i];
				maximumPos = i;
			}
		}
	}
}
