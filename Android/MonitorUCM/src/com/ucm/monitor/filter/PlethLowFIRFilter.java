/* 
 * PlethLowFIRFilter.java
 */

package com.ucm.monitor.filter;

/**
 * Implementa un filtro FIR paso bajo de orden 16 y frecuencia de corte de
 * 10 Hz para filtrar la señal de pleth.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/08
 */

public class PlethLowFIRFilter {
	
	/** Retardo del filtro. */
	public static final int FILTER_DELAY = 8;
	
	/**
	 * Coeficientes del filtro a 83.3 Hz de frecuencia de muestreo (int).
	 * Los coeficientes hay que multiplicarlos por 1E-6.
	 */
	private static final int[] FILTER_COEFS_INT = {
		-789, -4402, -11154, -13576, 5368, 58350, 136997, 209591, 239227
	};
	
	/**
	 * Coeficientes del filtro a 83.3 Hz de frecuencia de muestreo (double).
	 */
	private static final double[] FILTER_COEFS_DOUBLE = {
		-0.000789046984158264, -0.00440166921854980, -0.0111540023617622,
		-0.0135755916510870, 0.00536839775110859, 0.0583500486020483,
		0.136997366151504, 0.209591021373349, 0.239226952675094
	};
	
	/** Muestras int para implementar el filtro. */
	private int[] xSamplesInt;
	
	/** Muestras double para implementar el filtro. */
	private double[] xSamplesDouble;
	
	/** Constructor de ECGLowFIRFilter. */
	public PlethLowFIRFilter() {
		xSamplesInt = new int[17];
		xSamplesDouble = new double[17];
	}
	
	/**
	 * Filtrar las muestras (int) de pleth.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(int sample) {
		xSamplesInt[16] = sample;
		int tempSample = FILTER_COEFS_INT[8] * xSamplesInt[8];
		for (int i = 0; i < 8; i++) {
			tempSample += FILTER_COEFS_INT[i] * (xSamplesInt[i] + xSamplesInt[16 - i]);
		}
		for (int i = 0; i < 16; i++) {
			xSamplesInt[i] = xSamplesInt[i + 1];
		}
		return (int) ((double) tempSample * 0.000001);
	}
	
	/**
	 * Filtrar las muestras (double) de pleth.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public double filterSample(double sample) {
		xSamplesDouble[16] = sample;
		double tempSample = FILTER_COEFS_DOUBLE[8] * xSamplesDouble[8];
		for (int i = 0; i < 8; i++) {
			tempSample += FILTER_COEFS_DOUBLE[i] * (xSamplesDouble[i] + xSamplesDouble[16 - i]);
		}
		for (int i = 0; i < 16; i++) {
			xSamplesDouble[i] = xSamplesDouble[i + 1];
		}
		return tempSample;
	}
}
