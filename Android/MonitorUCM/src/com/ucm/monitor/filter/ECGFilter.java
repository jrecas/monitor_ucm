/* 
 * ECGFilter.java
 */

package com.ucm.monitor.filter;

/**
 * Implementa un conjunto de filtros para filtrar la señal de ECG.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/22
 */

public class ECGFilter {
	
	/** Retardo del conjunto de filtros. */
	public final int filterDelay;
	
	/** Notch filter. */
	//private NotchFilter notchFilter;
	
	/** ECG low FIR filter. */
	private ECGLowFIRFilter ecgLowFirFilter;
	
	/** Baseline Filter. */
	private BaselineFilter baselineFilter;
	
	/** Constructor de ECGFilter. */
	public ECGFilter(int frequency) {
		//notchFilter = new NotchFilter(frequency);
		ecgLowFirFilter = new ECGLowFIRFilter(frequency);
		baselineFilter = new BaselineFilter(frequency);
		filterDelay = ECGLowFIRFilter.FILTER_DELAY +
				baselineFilter.getFilterDelay();
	}
	
	/**
	 * Filtrar las muestras (int) de ECG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(int sample) {
		//sample = (int) notchFilter.filterSample((double) sample);
		sample = ecgLowFirFilter.filterSample(sample);
		sample = baselineFilter.filterSample(sample);
		return sample;
	}
	
	/**
	 * Filtrar las muestras (double) de ECG.
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(double sample) {
		return filterSample((int) sample);
	}
	
}
