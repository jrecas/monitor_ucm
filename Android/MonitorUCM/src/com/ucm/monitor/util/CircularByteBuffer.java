/*
 * Circular Byte Buffer
 * Copyright (C) 2002-2010 Stephen Ostermiller
 * http://ostermiller.org/contact.pl?regarding=Java+Utilities
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * See LICENSE.txt for details.
 */

package com.ucm.monitor.util;

import java.nio.BufferOverflowException;
import java.nio.BufferUnderflowException;
 
public class CircularByteBuffer {
   private int qMaxSize;// max queue size
   private int fp = 0;  // front pointer
   private int rp = 0;  // rear pointer
   private int qs = 0;  // size of queue
   private byte[] q;    // actual queue

   public CircularByteBuffer(int size) {
      qMaxSize = size;
      fp = 0;
      rp = 0;
      qs = 0;
      q = new byte[qMaxSize];
   }
   public void clear() {
     fp = 0;
     rp = 0;
     qs = 0;
   }
   
   public int size() {
	   return qs;
   }

   public byte get() {
      if (!emptyq()) {
         qs--;
         fp = (fp + 1)%qMaxSize;
         return q[fp];
      }
      else {
    	  throw new BufferUnderflowException();
      }
   }

   public byte peek() {
      if (!emptyq()) {
         //qs--;
         //fp = (fp + 1)%qMaxSize;
         return q[(fp + 1)%qMaxSize];
      }
      else {
    	  throw new BufferUnderflowException();
      }
   }

   public void put(byte c) {
      if (!fullq()) {
         qs++;
         rp = (rp + 1)%qMaxSize;
         q[rp] = c;
      }
      else
    	  throw new BufferOverflowException();
   }

   public boolean emptyq() {
      return qs == 0;
   }

   public boolean fullq() {
      return qs == qMaxSize;
   }
   /*
   public void printq() {
      System.out.print("Size: " + qs +
         ", rp: " + rp + ", fp: " + fp + ", q: ");
      for (int i = 0; i < qMaxSize; i++)
         System.out.print("q[" + i + "]=" 
            + q[i] + "; ");
      System.out.println();
   }
   */
}