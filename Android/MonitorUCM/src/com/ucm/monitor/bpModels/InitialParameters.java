/* 
 * InitialParameters.java
 */

package com.ucm.monitor.bpModels;

import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Context;

/**
 * Lee de fichero los parametros iniciales necesarios para estimar las presiones.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/11/10
 */
public class InitialParameters {
	
	/** Datos de entrada */
	private DataInputStream dis;
	
	/**
	 * Numero de modelo elegido en la estimacion de las presiones:
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private int[] models;
	
	/**
	 * Coeficientes del ajuste de los modelos elegidos para las presiones:
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private double[][] coefficients;
	
	/**
	 * Constructor de InitialParameters.
	 * @param mMonitorUCM MonitorUCM
	 */
	public InitialParameters(Context context) {
		models = new int[BPEstimator.NUM_BP];
		coefficients = new double[BPEstimator.NUM_BP][3];
		
		try {
			dis = new DataInputStream(context.getAssets().open("bpModels.log"));
			
			models[0] = dis.readInt();
			models[1] = dis.readInt();
			
			for (int i = 0; i < coefficients.length; i++) {
				for (int j = 0; j < coefficients[0].length; j++) {
					coefficients[i][j] = dis.readDouble();
				}
			}
			
			dis.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}		
	}
	
	/**
	 * Obtener los modelos elegidos en la estimacion de las presiones.
	 * {0: Sistolica, 1: Diastolica}.
	 * @return Modelos elegidos
	 */
	public int[] getModels() {
		return models;
	}
	
	/**
	 * Obtener los coeficientes del ajuste de los modelos elegidos para las presiones.
	 * {0: Sistolica, 1: Diastolica}.
	 * @return Coeficientes de los modelos
	 */
	public double[][] getCoefficients() {
		return coefficients;
	}
}