/*
 * BPEstimator.java
 */

package com.ucm.monitor.bpModels;

import android.content.Context;

import com.ucm.monitor.buffer.ECGBuffer;
import com.ucm.monitor.buffer.PlethBuffer;

/**
 * Implementa los modelos que estiman las presiones sanguineas.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/11/10
 */
public class BPEstimator {
	
	/** Numero de presiones: {SBP, DBP}. */
	public static final int NUM_BP = 2;
	
	/** Numero de tiempos: {PATp, PATf, PATs, WT, t1, t2, PATfWT, 1/HR}. */
	public static final int NUM_TIMES = ECGBuffer.NUM_TIMES + PlethBuffer.NUM_TIMES;
	
	/** Numero de latidos a promediar. */
	public static final int NUM_BEATS = 10;
	
	/**
	 * Extremos de los intervalos de los tiempos validos:
	 * {0: Inferior, 1: Superior}.
	 */
	protected static final int[][] TIME_BOUNDS = {{60, 40, 50, 30, 30, 150, 40, 150},
			{800, 500, 650, 300, 300, 1600, 500, 1600}};
	
	/** Pre-Ejection Period = 100 ms (aproximados) */
	protected static final int PEP = 100;
	
	/** Iterador del numero de latidos a promediar. */
	private int numBeats;
	
	/** Presiones sanguineas estimadas. {0: sistolica, 1: diastolica}. */
	private double[] bp;
	
	/** 
	 * Medias de los ultimos tiempos recogidos:
	 * {PATp, PATf, PATs, WT, t1, t2, PATfWT, 1/HR].
	 */
	private double[] times;
	
	/** Ultimos tiempos recogidos: {PATp, PATf, PATs, WT, t1, t2, PATfWT, 1/HR}. */
	private int[][] lastTimes;
	
	/**
	 * Numero de modelo elegido en la estimacion de las presiones:
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private int[] models;
	
	/**
	 * Coeficientes del ajuste de los modelos elegidos para las presiones:
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private double[][] coefficients;
	
	/**
	 * Constructor de BPEstimator.
	 * @param mMonitorUCM MonitorUCM
	 */
	public BPEstimator(Context context) {
		numBeats = 0;
		bp = new double[NUM_BP];
		times = new double[NUM_TIMES];
		lastTimes = new int[NUM_TIMES][NUM_BEATS];
		InitialParameters initialParameters = new InitialParameters(context);
		models = initialParameters.getModels();
		coefficients = initialParameters.getCoefficients();
	}
	
	/**
	 * Guardar los tiempos y, si es necesario, calcular su media y
	 * estimar la presion.
	 * @param tempTimes Array de tiempos
	 */
	public void setTimes(int[] tempTimes) {
		for (int i = 0; i < tempTimes.length; i++){
			lastTimes[i][numBeats] = tempTimes[i];
		}
		numBeats++;
		if (numBeats == NUM_BEATS) {
			calculateMeanTimes();
			estimateBp();
			numBeats = 0;
		}
	}
	
	/**
	 * Actualizar los modelos elegidos para estimar las presiones.
	 * @param newModels Modelos nuevos
	 */
	public void setModels(int[] newModels) {
		models = newModels;
	}
	
	/**
	 * Actualizar coeficientes de los modelos elegidos para estimar las presiones.
	 * @param newModels Coeficientes nuevos
	 */
	public void setCoefficients(double[][] newCoefficients) {
		coefficients = newCoefficients;
	}
	
	/**
	 * Obtener las presiones estimadas.
	 * @return Presiones estimadas
	 */
	public double[] getBp() {
		return bp;
	}
	
	/**
	 * Obtener la media de los tiempos.
	 * @return Media de los tiempos
	 */
	public double[] getTimes() {
		return times;
	}	
	
	/**
	 * True si numBeats es 0 y, por tanto, hay unos nuevos valores de presiones.
	 * @return numBeats es 0
	 */
	public boolean newBpEstimated() {
		return numBeats == 0;
	}
	
	/** Calcular la media de los ultimos tiempos guardados. */
	private void calculateMeanTimes() {
		int k = 0;
		for (int i = 0; i < lastTimes.length; i++) {
			int tempMean = 0;			
			for (int j = 0; j < NUM_BEATS; j++) {
				if ((lastTimes[i][j] >= TIME_BOUNDS[0][i]) &&
						(lastTimes[i][j] <= TIME_BOUNDS[1][i])) {
					tempMean += lastTimes[i][j];
					k++;
				}
			}
			times[i] = (k == 0) ? tempMean : tempMean / k;
			k = 0;
		}
	}
	
	/** Estimar la presion a partir de diferentes modelos. */
	private void estimateBp() {
		for (int i = 0; i < 2; i++) {
			switch (models[i]) {
			case 1:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[0] + coefficients[i][2] / times[7];
				break;
			case 2:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[0];
				break;
			case 3:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[1];
				break;
			case 4:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[2];
				break;
			case 5:
				bp[i] = coefficients[i][0] + coefficients[i][1] / (times[0] * times[0]);
				break;
			case 6:
				bp[i] = coefficients[i][0] + coefficients[i][1] / (times[1] * times[1]);
				break;
			case 7:
				bp[i] = coefficients[i][0] + coefficients[i][1] / (times[2] * times[2]);
				break;
			case 8:
				bp[i] = coefficients[i][0] + coefficients[i][1] / ((times[0] - PEP) * (times[0]  - PEP));
				break;
			case 9:
				bp[i] = coefficients[i][0] + coefficients[i][1] / ((times[1] - PEP) * (times[1]  - PEP));
				break;
			case 10:
				bp[i] = coefficients[i][0] + coefficients[i][1] / ((times[2] - PEP) * (times[2]  - PEP));
				break;
			case 11:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[3];
				break;
			case 12:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[4];
				break;
			case 13:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[5];
				break;
			case 14:
				bp[i] = coefficients[i][0] + coefficients[i][1] * times[6];
				break;
			}
		}
	}
}
