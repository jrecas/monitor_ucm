package com.ucm.monitor;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.flotandroidchart.flot.FlotChartContainer;
import com.flotandroidchart.flot.FlotDraw;
import com.flotandroidchart.flot.Options;
import com.flotandroidchart.flot.data.SeriesData;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.ucm.monitor.bpModels.Recalibrator;
import com.ucm.monitor.bpModels.BPEstimator;
//import com.ucm.monitor.buffer.ECGBuffer;
//import com.ucm.monitor.buffer.ECGBuffer;
import com.ucm.monitor.buffer.ECGBufferLite;
import com.ucm.monitor.buffer.PlethBuffer;
import com.ucm.monitor.buffer.ECGPlethDelay;
//import com.ucm.monitor.filter.ECGFilter;
import com.ucm.monitor.filter.PlethLowFIRFilter;
import com.ucm.monitor.util.CircularByteBuffer;
import com.ucm.monitor.util.FileReadThread;
import com.ucm.monitor.util.MonitorLog;

import ECGDelineator.ECGFilter;
import ECGDelineator.ECGDelineator;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.TextView;

public class MonitorUCM extends Activity {
	
	private static final String TAG = "UCM_MONITOR";
	
	/* 
	 * Periodos de ECG y pleth (en ms), su relacion y de la delineacion.
	 */
	private static final int T_ECG = 4;
	private static final int T_PLETH = 12;
	private static final int T_RATIO_PLETH_ECG = T_PLETH / T_ECG;
	private static final int T_DEL = 4;
	private static final int T_RATIO_DEL_ECG = T_DEL / T_ECG;
	
	/*
	 * Buffer, filtro, delineador de ECG.
	 * Tamaño del buffers (19.2 ms = 4800 * T_RATIO_DEL_ECG muestras)
	 */
	static final int BUFFER_ECG_SIZE = 200 * 24 * T_RATIO_DEL_ECG;
	//private ECGBuffer ecgBuffer;
	private ECGBufferLite ecgBuffer;
	private ECGFilter ecgFilter;
	//private int ecgFilterDelay;
	private static final int ecgFilterDelay = ECGFilter.filterDelay;
	private ECGDelineator ecgDelineator;
	private static final int DEL_DELAY = 500 * T_RATIO_DEL_ECG;
	
	/*
	 * Buffer y filtro de pleth.
	 * Tamaño del buffers (19.2 ms = 1600 * T_RATIO_DEL_ECG muestras)
	 */
	private static final int BUFFER_PLETH_SIZE = BUFFER_ECG_SIZE / T_RATIO_PLETH_ECG;
	private PlethBuffer plethBuffer;
	private PlethLowFIRFilter plethFilter;
	private int plethFilterDelay;
	
	/* Retardo entre una muestra de ECG y la siguiente muestra de pleth. */
	private ECGPlethDelay ecgPlethDelay;
	
	/* Número de puntos y tiempos utilizados en la delineacion de pleth. */
	//private static final int NUM_POINTS = ECGBuffer.NUM_POINTS + PlethBuffer.NUM_POINTS;
	//private static final int NUM_TIMES = ECGBuffer.NUM_TIMES + PlethBuffer.NUM_TIMES;
	private static final int NUM_POINTS = ECGBufferLite.NUM_POINTS + PlethBuffer.NUM_POINTS;
	private static final int NUM_TIMES = ECGBufferLite.NUM_TIMES + PlethBuffer.NUM_TIMES;
	
	/* Tamaño de la grafica (4.8 ms) de ECG (1200 * RATIO muestras) y pleth (400 muestras). */
	private static final int PLOT_ECG_SIZE = BUFFER_ECG_SIZE / 4;
	private static final int PLOT_PLETH_SIZE = PLOT_ECG_SIZE / T_RATIO_PLETH_ECG;
	
	/* Tamaño de la grafica del historial de datos (300 muestras). */
	private static final int PLOT_HIST_FACTOR = 4 * T_RATIO_DEL_ECG;
	private static final int PLOT_HIST_SIZE = PLOT_ECG_SIZE / PLOT_HIST_FACTOR;
	
	/* Opciones de la libreria flotAndroidChart (parte grafica). */
	private Options options;
	
	/*
	 * Retardo de la grafica, indice de llenado de datos de ECG y pleth e indice
	 * de llenado del historial. Factor de escala para la representacion de ECG.
	 */
	private boolean plotEnable;
	private int indexPlotDelay;
	private int indexPlotEcg;
	private int indexPlotPleth;
	private int indexPlotHist;
	//private static final double FACTOR_PLOT_ECG = 0.005;
	private static final double FACTOR_PLOT_ECG = 0.02;
	
	/* Vector de la series de datos. */
	private Vector<SeriesData> vSeriesData;
	
	/* Series de datos. */
	private SeriesData seriesDataEcg;
	private SeriesData seriesDataPleth;
	private SeriesData seriesPoints;
	private SeriesData seriesPulseHist;
	private SeriesData seriesSbpHist;
	private SeriesData seriesDbpHist;
	private SeriesData seriesCurrentPosHist;
	
	/* Valores de la serie de datos. */
	private double[][] dataEcg;
	private double[][] dataPleth;
	private double[][] points;
	private double[][] pulseHist;
	private double[][] sbpHist;
	private double[][] dbpHist;
	private double[][] currentPosHist;
	
	/* Objeto grafico de alto nivel -> R.id.flotdraw. */
	private FlotChartContainer flotCharCont;
	
	/* Objeto grafico con las series de datos, se mete en flotCharCont. */
	private FlotDraw flotDraw;
	
	/* Modelos para estimar las presiones. */
	private BPEstimator bpEstimator;
	
	/* Recalibracion del sistema. */
	private Recalibrator recalibrator;
	
	/*
	 * Informacion de pulso, oximetria, presiones y tiempos.
	 * Boton de evento de calibracion y boton de calibracion.
	 */
	private TextView textHr;
	private TextView textBp;
	private TextView textSpO2;
	private TextView textTimes;
	private Button eventCalibButton;
	private Button calibButton;
	
	/* Datos recibidos de ECG y pleth. */
	public static final byte ECG_PAQ_LENGTH = 3;
	public static final byte PLETH_PAQ_LENGHT = 2;
	private static final byte ECG_TOKEN = (byte) 0xDA;
	private static final byte PLETH_TOKEN = (byte) 0xAA;
	private byte[] ecgPacket;
	private byte[] plethPacket;
	
	/* Cola donde van los nuevos datos de entrada y su tamaño. */
	private CircularByteBuffer mCircularByteBuffer;
	private static final int BUFFER_SIZE = 4096;
	
	/* Periodo de subida de ficheros y objeto que los maneja. */
	private final int uploadPeriod = 100 * 1000;
	private MonitorLog mMonitorLog = null;
	
	/* Objetos para usar la libreria hoho.android.usbserial. Manejador del driver FTDI. */
	private UsbSerialDriver ftdiDriver = null;
	
	/*
	 * Creates an Executor that uses a single worker thread operating off an unbounded queue.
	 * Sera utilizado para leer constantemente del puerto usb.
	 */
	private final ExecutorService mExecutor = Executors.newSingleThreadExecutor();
	
	/* Este es el objeto que lee constantemente del puerto usb. */
	private SerialInputOutputManager mSerialIoManager;
	
	/* Llamada que hara el mSerialIoManager cada vez que tenga datos. */
	private final SerialInputOutputManager.Listener mListener = new SerialInputOutputManager.Listener() {
		@Override
		public void onRunError(Exception e) {
			Log.d(TAG, "Runner stopped.");
		}
		/* Llegada de nuevos datos, se ejecuta esto en thread de user interface
		 * (si estamos en primer plano sin esperas).
		 */
		@Override
		public void onNewData(final byte[] data) {
			MonitorUCM.this.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					MonitorUCM.this.updateReceivedData(data);
				}
			});
		}
	};
	
	/* En caso de no encontrar ftdi usaremos esto. */
	private FileReadThread	mFileReadThread;
	
	/**
	 * Called when the activity is created (launched by the user), used to initialize the activity. 
	 * Most importantly, here we call setContentView(int)
	 * Always followed by onStart().
	 * The entire lifetime of an activity happens between the first call to onCreate(Bundle) through 
	 * to a single final call to onDestroy()
	 * 
	 * <p> - Get the graphic objects references and assign static variables for them.
	 * <br>- Create the GraphViews once we know the layout (through a LayoutListener)
	 * <br>- Get the BlueThooth reference (finish the application if not found)
	 * <br>- Set screen always on
	 *
	 * @param savedInstanceState the saved instance state
	 */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "onCreate()");
		
		/*
		 * Redirigimos la salida de log a un fichero. He visto que no es necesario,
		 * basta con conectar rapidamente el usb al pc para ver gran parte del log.
		 */
		/*
		File logFile = new File(Environment.getExternalStorageDirectory() + "/UCM_ECG/logcat_" +
				System.currentTimeMillis() + ".txt");
		File logFile = new File(Environment.getExternalStorageDirectory() + "/UCM_ECG/logcat.txt");

		// Logs into a file
		try {
			Runtime.getRuntime().exec("logcat -f " + logFile.getAbsolutePath());
			//Runtime.getRuntime().exec("logcat -s UCM -f " + logFile.getAbsolutePath());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
		
		//ecgFilter = new ECGFilter(1000 / T_ECG);
		//ecgFilterDelay = ecgFilter.filterDelay;
		//ecgBuffer = new ECGBuffer(BUFFER_ECG_SIZE,
		//		(BUFFER_ECG_SIZE - ecgFilterDelay) % BUFFER_ECG_SIZE, 0, T_ECG);
		
		ecgFilter = new ECGFilter();
		ecgBuffer = new ECGBufferLite(BUFFER_ECG_SIZE,
				(BUFFER_ECG_SIZE - ecgFilterDelay) % BUFFER_ECG_SIZE, 0, T_ECG);
		ecgDelineator = new ECGDelineator();
				
		plethFilter = new PlethLowFIRFilter();
		plethFilterDelay = PlethLowFIRFilter.FILTER_DELAY;
		plethBuffer = new PlethBuffer(BUFFER_PLETH_SIZE,
				(BUFFER_PLETH_SIZE - plethFilterDelay) % BUFFER_PLETH_SIZE, 0, T_PLETH);
		
		ecgPlethDelay = new ECGPlethDelay();
		
		options = new Options();
		
		plotEnable = false;
		//indexPlotDelay = ecgFilterDelay;
		indexPlotDelay = DEL_DELAY + ecgFilterDelay;
		indexPlotEcg = 0;
		indexPlotPleth = 0;
		indexPlotHist = 0;
		
		vSeriesData = new Vector<SeriesData>();
		
		seriesDataEcg = new SeriesData();
		seriesDataPleth = new SeriesData();
		seriesPoints = new SeriesData();
		seriesPulseHist = new SeriesData();
		seriesSbpHist = new SeriesData();
		seriesDbpHist = new SeriesData();
		seriesCurrentPosHist = new SeriesData();
		
		dataEcg = new double[PLOT_ECG_SIZE][2];
		dataPleth = new double[PLOT_PLETH_SIZE][2];
		points = new double[NUM_POINTS][2];
		pulseHist = new double[PLOT_HIST_SIZE][2];
		sbpHist = new double[PLOT_HIST_SIZE][2];
		dbpHist = new double[PLOT_HIST_SIZE][2];
		currentPosHist = new double[3][2];
		
		for (int i = 0; i < dataEcg.length; i++) { 
			dataEcg[i][0] = i;
			dataEcg[i][1] = Double.NEGATIVE_INFINITY;
		}
		for (int i = 0; i < dataPleth.length; i++) {
			dataPleth[i][0] = i * T_RATIO_PLETH_ECG;
			dataPleth[i][1] = Double.NEGATIVE_INFINITY;
		}		
		for (int i = 0; i < points.length; i++) {
			points[i][0] = 0;
			points[i][1] = Double.NEGATIVE_INFINITY;
		}
		for (int i = 0; i < pulseHist.length; i++) {
			pulseHist[i][0] = i * PLOT_HIST_FACTOR;
			pulseHist[i][1] = Double.NEGATIVE_INFINITY;
			sbpHist[i][0] = i * PLOT_HIST_FACTOR;
			sbpHist[i][1] = Double.NEGATIVE_INFINITY;
			dbpHist[i][0] = i * PLOT_HIST_FACTOR;
			dbpHist[i][1] = Double.NEGATIVE_INFINITY;
		}
		for (int i = 0; i < currentPosHist.length; i++) {
			currentPosHist[i][0] = 0;
			currentPosHist[i][1] = Double.NEGATIVE_INFINITY;
		}
		
		seriesDataEcg.setData(dataEcg);
		seriesDataPleth.setData(dataPleth);
		seriesPoints.setData(points);
		seriesPulseHist.setData(pulseHist);
		seriesSbpHist.setData(sbpHist);
		seriesDbpHist.setData(dbpHist);
		seriesCurrentPosHist.setData(currentPosHist);
		
		seriesDataEcg.label = "ECG";
		seriesDataPleth.label = "Pleth";
		seriesPoints.label = "Reference points";
		seriesPulseHist.label = "HR (history)";
		seriesSbpHist.label = "Systolic BP (history)";
		seriesDbpHist.label = "Diastolic BP (history)";
		seriesCurrentPosHist.label = "Current value (history)";
		
		seriesDataEcg.series.color = 0x000080;
		seriesDataPleth.series.color = 0xE8000D;
		seriesPoints.series.points.show = true;
		seriesPoints.series.points.radius = 4;
		seriesPoints.series.points.fillColor = 0xFFFF00;
		seriesPoints.series.color = 0x0000FF;
		seriesPulseHist.series.color = 0x80FF80;
		seriesSbpHist.series.color = 0xFFC980;
		seriesDbpHist.series.color = 0xE680FF;
		seriesCurrentPosHist.series.points.show = true;
		seriesCurrentPosHist.series.points.radius = 4;
		seriesCurrentPosHist.series.color = 0x333333;
		
		options.yaxis.min = -40;
		options.yaxis.max = 260;
		options.fps = 25;
		options.legend.noColumns = 4;
		
		vSeriesData.add(seriesDataEcg);
		vSeriesData.add(seriesDataPleth);
		vSeriesData.add(seriesPoints);
		vSeriesData.add(seriesPulseHist);
		vSeriesData.add(seriesSbpHist);
		vSeriesData.add(seriesDbpHist);
		vSeriesData.add(seriesCurrentPosHist);
		
		flotDraw = new FlotDraw(vSeriesData, options, null);
		setContentView(R.layout.main);
		flotCharCont = (FlotChartContainer) this.findViewById(R.id.flotdraw);
		if (flotCharCont != null) {
			flotCharCont.setDrawData(flotDraw);
		}		
		
		final Context context = this;
		bpEstimator = new BPEstimator(context);
		
		recalibrator = null;
		
		/* Evento de calibracion. */
		eventCalibButton = (Button) findViewById(R.id.eventCalibButton);
		eventCalibButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v1) {
				if (recalibrator == null) {
					recalibrator = new Recalibrator(context);
				}
				if (recalibrator.isModeOn()) {
					Toast.makeText(getApplicationContext(),
							"Processing the last event...", Toast.LENGTH_SHORT).show();
				} else {					
					recalibrator.setModeOn();
					
					final int nSample = ecgBuffer.getNSample();
					Toast.makeText(getApplicationContext(),
							"Time saved", Toast.LENGTH_SHORT).show();
					
					/* Obtener la vista de recalibrator.xml. */
					LayoutInflater li = LayoutInflater.from(context);
					View calibrationView = li.inflate(R.layout.event_calibration, null);
					
					/* Crear una alerta. */
					AlertDialog.Builder alertCalibration = new AlertDialog.Builder(context);
					alertCalibration.setView(calibrationView);
					alertCalibration.setTitle("New calibration");
					
					final EditText valueCalibrationSbp =
							(EditText)calibrationView.findViewById(R.id.valueEventCalibSbp);
					final EditText valueCalibrationDbp =
							(EditText)calibrationView.findViewById(R.id.valueEventCalibDbp);
					
					alertCalibration
					.setCancelable(false)
					.setPositiveButton("Save", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int id){
							String strSbp = valueCalibrationSbp.getText().toString();
							String strDbp = valueCalibrationDbp.getText().toString();
							if (strSbp.isEmpty() || strDbp.isEmpty()) {
								recalibrator.setModeOff();
								Toast.makeText(getApplicationContext(),
										"Blood pressure values are incorrect and not saved",
										Toast.LENGTH_SHORT).show();
							} else {								
								int[] tempBp = {Integer.parseInt(strSbp), Integer.parseInt(strDbp)};
								recalibrator.setBp(tempBp);
								try {
									mMonitorLog.logBp(nSample, tempBp);
								} catch (FileNotFoundException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}								
								Toast.makeText(getApplicationContext(),
										"Blood pressure values saved in file", Toast.LENGTH_SHORT).show();
							}
						}
					})
					.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog,int id) {
							recalibrator.setModeOff();
							dialog.cancel();
						}
					});
					
					/* Crear un dialogo de alerta y mostrarlo. */
					AlertDialog alertDialog = alertCalibration.create();
					alertDialog.show();
				}
			}
		});
		
		/* Calibracion del sistema. */
		calibButton = (Button) findViewById(R.id.calibrationButton);
		calibButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (recalibrator == null) {
					recalibrator = new Recalibrator(context);
				}
				if (recalibrator.isModeOn()) {
					Toast.makeText(getApplicationContext(),
							"Processing the last event...", Toast.LENGTH_SHORT).show();
				} else if (!recalibrator.isReady()) {
					Toast.makeText(getApplicationContext(),
							"More events are needed", Toast.LENGTH_SHORT).show();
				} else {
					recalibrator.calibrate();
					bpEstimator.setModels(recalibrator.getModels());
					bpEstimator.setCoefficients(recalibrator.getCoefficients());
					recalibrator = null;
					Toast.makeText(getApplicationContext(),
							"New calibration finished", Toast.LENGTH_SHORT).show();
				}
			}
		});
		
		textHr = (TextView) findViewById(R.id.textHr);
		textBp = (TextView) findViewById(R.id.textBp);
		textSpO2 = (TextView) findViewById(R.id.textSpO2);
		textTimes = (TextView) findViewById(R.id.textTimes);
		textHr.setTextColor(0xFFB3FFB3);
		textBp.setTextColor(0xFFFFFFB3);
		textSpO2.setTextColor(0xFFFFB3B3);
		textTimes.setTextColor(0xFFB3B3FF);
		
		showHr();
		showBp();
		showSpO2();
		showTimes();
		
		ecgPacket = new byte[ECG_PAQ_LENGTH];
		plethPacket = new byte[PLETH_PAQ_LENGHT];
		
		mCircularByteBuffer = new CircularByteBuffer(2 * BUFFER_SIZE);
		
		mFileReadThread = null;
	}
	
	/**
	 *  From onCreate() or onRestart(). When a new activity is started, it is placed on the top of the 
	 *  stack and becomes the running activity, the activity is becoming visible to the user.
	 *  Followed by onResume() if the activity comes to the foreground, or onStop() if it becomes hidden.
	 *  The visible lifetime of an activity happens between a call to onStart() until a corresponding call 
	 *  to onStop(). But it can be hidden!!!
	 *  
	 * <p> - If the BluetoothAdapter is enabled then connectShimmer() if not already connected.
	 * <br>- If not, launch a BT System Intent that will do it. After that, it will call onActivityResult()
	 *       that has to call connectShimmer() if not already connected.
	 */
	public void onStart() {
		super.onStart();
		Log.d(TAG, "onStart()");
	}
	
	/**
	 * Called after your activity has been stopped, when user navigatyes to the activity again, 
	 * prior to it being started again. Always followed by onStart()
	 */
	protected void onRestart() {
		super.onRestart();
		Log.d(TAG, "onRestart()");
	}
	
	/** 
	 * From onStart() or onPause(). Called when the activity will start interacting with the user. 
	 * At this point your activity is at the top of the activity stack, with user input going to it. 
	 * Always followed by onPause().
	 * The foreground lifetime of an activity happens between a call to onResume() until a corresponding
	 * call to onPause()
	 */
	@Override
	protected void onResume() {
		super.onResume();
		Log.d(TAG, "onResume()");
		
		/* Get UsbManager from Android. */
		UsbManager manager = (UsbManager) getSystemService(Context.USB_SERVICE);
		
		/* Find the first available driver. */
		ftdiDriver = UsbSerialProber.findFirstDevice(manager);
		
		if (ftdiDriver == null) {
			Toast.makeText(getApplicationContext(), "No serial device", Toast.LENGTH_SHORT).show();
			Log.e(TAG, "No serial device.");
			try {
				mFileReadThread = new FileReadThread(MonitorUCM.this);
				mMonitorLog = new MonitorLog("", "file", uploadPeriod);
			} catch (FileNotFoundException e) {
				Toast.makeText(getApplicationContext(), "No raw file found", Toast.LENGTH_SHORT).show();
				return;
			}
			mExecutor.submit(mFileReadThread);
			return;
		} else {
			try {
				ftdiDriver.open();
				ftdiDriver.setParameters(115200, 8, UsbSerialDriver.STOPBITS_1,	UsbSerialDriver.PARITY_NONE);
				String app_ver = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionName;
				String nameVersion = getApplicationContext().getString(R.string.app_name) + " v" + app_ver;
				mMonitorLog = new MonitorLog(nameVersion, "ftdi", uploadPeriod);
			} catch (IOException e) {
				Toast.makeText(getApplicationContext(),	"Error setting up device", Toast.LENGTH_SHORT).show();
				Log.e(TAG, "Error setting up device: " + e.getMessage(), e);
				try {
					ftdiDriver.close();
				} catch (IOException e2) {
					// Ignore.
				}
				ftdiDriver = null;
				return;
			} catch (NameNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Toast.makeText(getApplicationContext(),	"Device found and condigured!!", Toast.LENGTH_SHORT).show();
			onDeviceStateChange();
		}
	}
	
	/** 
	 * onPause(). Called when an activity is going into the background, but has not (yet) been killed.
	 * The counterpart to onResume(). After receiving this call you will usually receive a following 
	 * call to onStop() (after the next activity has been resumed and displayed), however in some cases 
	 * there will be a direct call back to onResume() without going through the stopped state.
	 */
	protected void onPause() {
		super.onPause();
		Log.d(TAG, "onPause()");
		
		if (mMonitorLog != null) {
			mMonitorLog.endLogs();
			mMonitorLog = null;
		}
		
		if (mFileReadThread != null) {
			mFileReadThread.stop();
			mFileReadThread = null;
		}
		
		stopIoManager();
		
		if (ftdiDriver != null) {
			try {
				ftdiDriver.close();
			} catch (IOException e) {
				// Ignore.
			}
			ftdiDriver = null;
		}
		finish();
	}
	
	/** 
	 * From onPause. Called when the activity is no longer visible to the user, because another 
	 * activity has been resumed and is covering this one. This may happen either because a new 
	 * activity is being started, an existing one is being brought in front of this one, or this 
	 * one is being destroyed.
	 * Followed by either onRestart() if this activity is coming back to interact with the user, 
	 * or onDestroy() if this activity is going away.
	 */
	@Override
	protected void onStop() {
		super.onStop();
		Log.d(TAG, "onStop()");
	}
	
	/** 
	 * From onStop(). The final call you receive before your activity is destroyed. This can 
	 * happen either because the activity is finishing (someone called finish() on it, or because 
	 * the system is temporarily destroying this instance of the activity to save space. You can 
	 * distinguish between these two scenarios with the isFinishing() method.
	 */
	protected void onDestroy() {
		super.onDestroy();
		Log.d(TAG, "onDestroy()");
	}
	
	/** Stop I/O manager. */
	private void stopIoManager() {
		if (mSerialIoManager != null) {
			Log.i(TAG, "Stopping io manager...");
			mSerialIoManager.stop();
			mSerialIoManager = null;
		}
	}
	
	/** Start I/O manager. */
	private void startIoManager() {
		if (ftdiDriver != null) {
			Log.i(TAG, "Starting io manager...");
			mSerialIoManager = new SerialInputOutputManager(ftdiDriver,	mListener);
			mExecutor.submit(mSerialIoManager);
		}
	}
	
	/** On device sate change. */
	private void onDeviceStateChange() {
		stopIoManager();
		startIoManager();
	}
	
	/** Actualizar los datos recibidas y determinar si son de ECG o de pleth */
	public void updateReceivedData(byte[] data) {
		
		for (int i = 0; i < data.length; i++) {
			mCircularByteBuffer.put(data[i]);
		}
		boolean enoughData = true;
		
		while (!mCircularByteBuffer.emptyq() && enoughData) {			
			/* Parse stream. */
			byte token = mCircularByteBuffer.peek();
			switch (token) {
			case (byte) ECG_TOKEN:
				if (mCircularByteBuffer.size() < ECG_PAQ_LENGTH + 1) {
					//Log.v(TAG, "Faltan datos en el paquete ECG.");
					enoughData = false;
				} else {
					token = mCircularByteBuffer.get();
					ecgPacket[0] = mCircularByteBuffer.get();
					ecgPacket[1] = mCircularByteBuffer.get();
					ecgPacket[2] = mCircularByteBuffer.get();
					if (mMonitorLog != null) {
						try {
							mMonitorLog.logRaw(token, ecgPacket);
							if (T_ECG == 1) {
								mMonitorLog.logEcg1k(ecgPacket);
							} else {
								mMonitorLog.logEcg(ecgPacket);
							}
						} catch (FileNotFoundException e) {
							e.printStackTrace();
							mMonitorLog.endLogs();
							mMonitorLog = null;
						}
					}
					parseECG();
				}
				break;			
			case (byte) PLETH_TOKEN:
				if (mCircularByteBuffer.size() < PLETH_PAQ_LENGHT + 1) {
					//Log.v(TAG, "Faltan datos en el paquete pleth.");
					enoughData = false;
				} else {
					token = mCircularByteBuffer.get();
					plethPacket[0] = mCircularByteBuffer.get();
					plethPacket[1] = mCircularByteBuffer.get();
					if (mMonitorLog != null) {
						try {
							mMonitorLog.logRaw(token, plethPacket);
							mMonitorLog.logPleth(plethPacket);
						} catch (FileNotFoundException e) {
							e.printStackTrace();
							mMonitorLog.endLogs();
							mMonitorLog = null;
						}
					}
					parsePleth();
				}
				break;			
			default:
				token = mCircularByteBuffer.get();
				if (mMonitorLog != null) {
					try {
						mMonitorLog.logRaw(token);
					} catch (FileNotFoundException e) {
						e.printStackTrace();
						mMonitorLog.endLogs();
						mMonitorLog = null;
					}
					Log.w(TAG, "Token unknown: " + Integer.toHexString(0xff & token));
				}
			}
		}
	}
	
	/** Procesar la muestra de pleth recibida. */
	private void parsePleth() {
		int tmpSample = (plethPacket[0] & 0xFF);
		double plethSample = plethFilter.filterSample((double) tmpSample);
		plethBuffer.writeSample(plethSample);
		plethBuffer.writeSampleSpO2(plethPacket[1] & 0xFF);
		if (plotEnable) {
			plotPlethSample();
		}
	}
	
	/** Procesar la muestra de ECG recibida. */
	private void parseECG() {
		
		int ecgSample =
				((ecgPacket[0] & 0xFF) << 16) |
				((ecgPacket[1] & 0xFF) << 8) |
				(ecgPacket[2] & 0xFF);
		ecgSample = ecgSample >> 2;
		ecgSample = ecgFilter.filterSample(ecgSample);
		ecgBuffer.writeSample(ecgSample);
		
		int[] bump = null;
		bump = ecgDelineator.newSample(ecgSample);
		
		//if (ecgBuffer.newBumpsFound()) {
		//	int[] newBumps = ecgBuffer.getBumps();
		//	processPleth(newBumps);
		if (bump != null) {
			processPleth(bump[0]);
			if (mMonitorLog != null) {
				try {
					//int[] bump = ecgBuffer.getAbsBumps();
					int[] tmpBump = new int[bump.length];
					for (int i = 0; i < bump.length; i++) {
						tmpBump[i] = bump[i] - ecgFilterDelay;
					}
					mMonitorLog.logDelEcg(tmpBump);
					int[] points = plethBuffer.getAbsPoints();
					int[] tmpPoints = new int[points.length];
					for (int i = 0; i < points.length; i++) {
						tmpPoints[i] = points[i] - plethFilterDelay;
					}
					mMonitorLog.logDelPleth(tmpPoints);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			int[] times = getTimes();
			bpEstimator.setTimes(times);
			if ((recalibrator != null) && (recalibrator.needsTimes())) {
				recalibrator.setTimes(times);
			}			
			if (bpEstimator.newBpEstimated()) {
				showBp();
				plotHistory();
			}			
			showHr();
			showSpO2();
			showTimes();
			plotReferencePoints();
		}
		
		if (plotEnable) {
			plotEcgSample();
		} else {
			indexPlotDelay--;
			if (indexPlotDelay == 0) {
				plotEnable = true;
				Toast.makeText(getApplicationContext(), "Start monitor", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	/**
	 * Pinta una nueva muestra de pleth del buffer de pleth.
	 */
	private void plotPlethSample() {
		seriesDataPleth.datapoints.points.set(2 * indexPlotPleth + 1,
				(double) plethBuffer.readSample());
		indexPlotPleth++;
		if (indexPlotPleth == PLOT_PLETH_SIZE) {
			indexPlotPleth = 0;
		}
	}
	
	/** Pinta una nueva muestra de ECG del buffer de ECG. */
	private void plotEcgSample() {
		seriesDataEcg.datapoints.points.set(2 * indexPlotEcg + 1,
				((double) ecgBuffer.readSample()) * FACTOR_PLOT_ECG);
		indexPlotEcg++;
		if (indexPlotEcg == PLOT_ECG_SIZE) {
			indexPlotEcg = 0;
		}
	}
	
	/**
	 * Procesa el intervalo valido de pleth.
	 * @param bumps ondas de ECG
	 */
	/*private void processPleth(int[] newBumps) {
		int newRWave = ecgBuffer.getNewRWave();
		plethBuffer.setInterval(newRWave / T_RATIO_PLETH_ECG);
		ecgPlethDelay.setNewSamplesDelay(newRWave % T_RATIO_PLETH_ECG);
		plethBuffer.processPleth(T_ECG * ecgPlethDelay.getOldSamplesDelay());
	}*/
	private void processPleth(int bump) {		
		if (bump <= ecgFilterDelay) {
			return;
		}		
		ecgBuffer.setInterval(bump - ecgFilterDelay);		
		if (ecgBuffer.sameRBumps()) {
			return;
		}		
		int newRBump = ecgBuffer.getRBumps()[1];
		plethBuffer.setInterval(newRBump / T_RATIO_PLETH_ECG);
		ecgPlethDelay.setNewSamplesDelay(newRBump % T_RATIO_PLETH_ECG);
		plethBuffer.processPleth(T_ECG * ecgPlethDelay.getOldSamplesDelay());
	}
	
	/**
	 * Obtener tiempos: {PATp, PATf, PATs, WT, t1, t2, PATfWT, invHR}.
	 * @return times Tiempos
	 */
	private int[] getTimes() {
		int[] times = new int[NUM_TIMES];
		int[] temp = plethBuffer.getTimes();
		for (int i = 0; i < temp.length; i++) {
			times[i] = temp[i];
		}
		times[times.length - 1] = ecgBuffer.getInvHR();
		return times;
	}
	
	/**
	 * Mostrar la informacion de la fecuencia cardiaca.
	 * @param invHr Frecuencia cardiaca
	 */
	private void showHr() {
		int invHr = ecgBuffer.getInvHR();
		textHr.setText("HR (BPM)\n\n");
		if (invHr != 0.0) {
			textHr.append(String.valueOf(Math.round(60000.0 / invHr)));
		} else {
			textHr.append("-");
		}
	}
	
	/** Mostrar la informacion de la oximetria. */
	private void showSpO2() {
		textSpO2.setText("OXIMETRY\n\n");
		textSpO2.append(String.valueOf(plethBuffer.getMeanSpO2()) + " %");
	}
	
	/**
	 * Mostrar la informacion de los tiempos.
	 * @param times Tiempos
	 */
	private void showTimes() {
		int[] times = getTimes();
		textTimes.setText("TIMES (ms)\n\n");
		textTimes.append("PATp: " + String.valueOf(times[0]) + "\n");
		textTimes.append("PATf: " + String.valueOf(times[1]) + "\n");
		textTimes.append("PATs: " + String.valueOf(times[2]) + "\n");
		textTimes.append("WT: " + String.valueOf(times[3]) + "\n");
		textTimes.append("t1: " + String.valueOf(times[4]) + "\n");
		textTimes.append("t2: " + String.valueOf(times[5]) + "\n");
		textTimes.append("PATfWT: " + String.valueOf(times[6]) + "\n");
		textTimes.append("1/HR: " + String.valueOf(times[7]));
	}
	
	/** Mostrar la informacion de la presion sanguinea. */
	private void showBp() {
		double[] bp = bpEstimator.getBp();
		textBp.setText("BP (mmHg)\n\n");
		if (Double.isInfinite(bp[0]) == false) {
			textBp.append("Systolic: " + String.valueOf(Math.round(bp[0])) + "\n");
		} else {
			textBp.append("Systolic: -\n");
		}
		if (Double.isInfinite(bp[1]) == false) {
			textBp.append("Diastolic: " + String.valueOf(Math.round(bp[1])));
		} else {
			textBp.append("Diastolic: -");
		}
	}
	
	/** Representar graficamente los puntos de referencia hallados en la delineacion de pleth. */
	private void plotReferencePoints() {
		int[] tempPoints = plethBuffer.getPoints();
		for (int i = 0; i < tempPoints.length; i++) {
			seriesPoints.datapoints.points.set(2 * i,
					(double) (tempPoints[i] % PLOT_PLETH_SIZE) * T_RATIO_PLETH_ECG);
			seriesPoints.datapoints.points.set(2 * i + 1,
					(double) plethBuffer.getSample(tempPoints[i]));
		}
		/*int[] newBumps = ecgBuffer.getBumps();
		for (int i = 0; i < newBumps.length; i++) {
			if (newBumps[i] != -1) {
				seriesPoints.datapoints.points.set(2 * (tempPoints.length + i),
						(double) (newBumps[i] % PLOT_ECG_SIZE));
				seriesPoints.datapoints.points.set(2 * (tempPoints.length + i) + 1,
						((double) ecgBuffer.getSample(newBumps[i]) * FACTOR_PLOT_ECG));
			} else {
				seriesPoints.datapoints.points.set(2 * (tempPoints.length + i), -1.0);
			}
		}*/
		int newBump = ecgBuffer.getRBumps()[0];
		seriesPoints.datapoints.points.set(2 * tempPoints.length,
				(double) (newBump % PLOT_ECG_SIZE));
		seriesPoints.datapoints.points.set(2 * tempPoints.length + 1,
				((double) ecgBuffer.getSample(newBump) * FACTOR_PLOT_ECG));
	}
	
	/**
	 * Representar graficamente los historiales de la frecuencia cardiaca
	 * y de las presiones sanguineas.
	 */
	private void plotHistory() {
		double invHr = bpEstimator.getTimes()[NUM_TIMES - 1];
		double tempHr = 0;
		if (invHr != 0.0) {
			tempHr = Math.round(60000.0 / invHr);
		}
		seriesPulseHist.datapoints.points.set(2 * indexPlotHist + 1, tempHr);
		seriesCurrentPosHist.datapoints.points.set(1, tempHr);
		
		double[] bp = bpEstimator.getBp();
		if (Double.isInfinite(bp[0]) == true) {
			bp[0] = 0;			
		}
		if (Double.isInfinite(bp[1]) == true) {
			bp[1] = 0;			
		}
		seriesSbpHist.datapoints.points.set(2 * indexPlotHist + 1, bp[0]);
		seriesCurrentPosHist.datapoints.points.set(3, bp[0]);
		seriesDbpHist.datapoints.points.set(2 * indexPlotHist + 1, bp[1]);
		seriesCurrentPosHist.datapoints.points.set(5, bp[1]);
		
		for (int i = 0; i < currentPosHist.length; i++) {
			seriesCurrentPosHist.datapoints.points.set(2 * i, (double) (indexPlotHist * PLOT_HIST_FACTOR));
		}
		
		indexPlotHist++;
		if (indexPlotHist == PLOT_HIST_SIZE) {
			indexPlotHist = 0;
		}
	}
}