/* 
 * PlethBuffer.java
 */

package com.ucm.monitor.buffer;

/**
 * Implementa un buffer con muestras de pleth.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/11/09
 */
public class PlethBuffer extends Buffer {
	
	/** Numero de puntos de referencia. */
	public static final int NUM_POINTS = 5;
	
	/**
	 * Numero de tiempos correspondientes a puntos pleth-pleth o pleth/ECG:
	 * {PATp, PATf, PATs, WT, t1, t2, PATfWT}.
	 */
	public static final int NUM_TIMES = 7;
	
	/**
	 * Posiciones de los puntos de referencia en el intervalo de pleth.
	 * {max, foot, slope, wt0, wt1}.
	 */
	private int[] points;
	
	/** Posicion del ultimo maximo hallado. */
	private int oldMax;
	
	/**
	 * Posicion de los tiempos en el intervalo de pleth.
	 * {PATp, PATf, PATs, WT, t1, t2, PATfWT, 1/HR}.
	 */
	private int[] times;
	
	/** Ultimas muestras de pleth. Necesarias para las derivadas. */
	private int[] lastSamples;
	
	/** Muestras de las derivadas de la pleth. */
	private int[] firstDerivative;
	private int[] secondDerivative;
	
	/** Indice del llenado de ambas derivadas. */
	private int indexDerivative;
	
	/** Muestras de oximetria. */
	private int[] spO2;
	
	/** Media de la oximetria en el intervalo de pleth. */
	private int meanSpO2;
	
	/**
	 * Constructor de PlethBuffer.
	 * @param bufSize Tamaño del buffer
	 * @param indWrite Indice de escritura
	 * @param indRead Indice de lectura
	 * @param t Periodo entre datos
	 */
	public PlethBuffer(int bufSize, int indWrite, int indRead, int t) {
		super(bufSize, indWrite, indRead, t);
		indexInterval[0] = -1;
		indexInterval[1] = -1;
		points = new int[NUM_POINTS];
		oldMax = 0;
		times = new int[NUM_TIMES];
		spO2 = new int[bufSize];
		firstDerivative = new int[bufSize];
		secondDerivative = new int[bufSize];
		indexDerivative = (indWrite + bufSize - 1) % bufSize;
		lastSamples = new int[2];
	}
	
	/**
	 * Guardar una muestra (int) en el buffer de pleth
	 * y hallar su primera y segunda derivada.
	 * @param sample Muestra a guardar
	 */
	public void writeSample(int sample) {
		nSample++;
		nSampleArray[indexWrite] = nSample;
		writeDerivatives(sample);
		writeInBuffer(sample);
	}
	
	/**
	 * Guardar una muestra (double) en el buffer de pleth
	 * y hallar su primera y segunda derivada.
	 * @param sample Muestra a guardar
	 */
	public void writeSample(double sample) {
		nSample++;
		nSampleArray[indexWrite] = nSample;
		writeDerivatives((int) (1000000 * sample));
		writeInBuffer((int) sample);
	}
	
	/**
	 * Guardar una muestra en el array de oximetria.
	 * @param sample Muestra de oximetria
	 */
	public void writeSampleSpO2(int sample) {
		spO2[indexWrite] = sample;
	}
	
	/**
	 * Obtener la posicion de los puntos de referencia en el intervalo de pleth
	 * en funcion del buffer: max, foot, slope, wt0, wt1.
	 * @return Posiciones de los puntos de referencia
	 */
	public int[] getPoints() {
		return points;
	}
	
	/**
	 * Obtener la posicion de los puntos de referencia en el intervalo de pleth
	 * en funcion del numero de muestras recibidas: max, foot, slope, wt0, wt1.
	 * @return Posiciones de los puntos de referencia
	 */
	public int[] getAbsPoints() {
		int[] tmpAbsPoints = new int[points.length];
		for (int i = 0; i < tmpAbsPoints.length; i++) {
			tmpAbsPoints[i] = nSampleArray[points[i]];
		}
		return tmpAbsPoints;
	}
	
	/**
	 * Posicion de los tiempos en el intervalo de pleth:
	 * PATp, PATf, PATs, WT, t1, t2, PATfWT, 1/HR.
	 * @return Posiciones de los tiempos
	 */
	public int[] getTimes() {
		return times;
	}
	
	/**
	 * Media de la oximetria en cada intervalo de pleth.
	 * @return Media de la oximetria en el intervalo
	 */
	public int getMeanSpO2() {
		return meanSpO2;
	}
	
	/**
	 * Procesar el intervalo de pleth.
	 * @param delay Retardo entre la onda R de ECG y la siguiente muestra de pleth.
	 */
	public void processPleth(int delay) {
		if (indexInterval[0] >= 0 && indexInterval[1] >= 0) {
			findPoints();
			findTimes(delay);
			calculateMeanSpO2();
		}
	}
	
	/**
	 * Guardar la primera y la segunda derivada.
	 */
	private void writeDerivatives(int sample) {
		firstDerivative[indexDerivative] =
				sample - lastSamples[1];
		secondDerivative[indexDerivative] = 
				sample - 2 * lastSamples[0] + lastSamples[1];
		lastSamples[1] = lastSamples[0];
		lastSamples[0] = sample;
		incrementIndexDerivative();
	}
	
	/**
	 * Guardar en el buffer de pleth.
	 */
	private void writeInBuffer(int sample) {
		buffer[indexWrite] = sample;
		incrementIndexWrite();
	}
	
	/** Incremetar el indice de llenado de ambas derivadas. */
	private void incrementIndexDerivative() {
		indexDerivative++;
		if (indexDerivative == bufferSize) {
			indexDerivative = 0;
		}
	}
	
	/** Hallar los puntos de referencia en el intervalo de pleth. */
	private void findPoints() {
    	oldMax = points[0];
       	findMax();
       	findFoot();
       	findSlopePoint();
       	int bound = buffer[points[0]] - (buffer[points[0]] - buffer[points[1]]) / 3;
       	findUpperBound(bound);
       	findLowerBound(bound);
	}
	
	/**
	 * Hallar los tiempos.
	 * @param delay Retardo entre la onda R de ECG y la siguiente muestra de pleth.
	 */
	private void findTimes(int delay) {
		times[0] = (points[0] >= indexInterval[0])
				? (points[0] - indexInterval[0]) * T - delay
				: (bufferSize + points[0] - indexInterval[0]) * T - delay;
		times[1] = (points[1] >= indexInterval[0])
				? (points[1] - indexInterval[0]) * T - delay
				: (bufferSize + points[1] - indexInterval[0]) * T - delay;
		times[2] = (points[2] >= indexInterval[0])
				? (points[2] - indexInterval[0]) * T - delay
				: (bufferSize + points[2] - indexInterval[0]) * T - delay;
		times[3] = (points[4] >= points[3])
				? (points[4] - points[3]) * T
				: (bufferSize + points[4] - points[3]) * T;
		times[4] = (points[0] >= points[1])
				? (points[0] - points[1]) * T
				: (bufferSize + points[0] - points[1]) * T;
		times[5] = (points[1] >= oldMax)
				? (points[1] - oldMax) * T
				: (bufferSize + points[1] - oldMax) * T;
		times[6] = (points[4] >= points[1])
				? (points[4] - points[1]) * T
				: (bufferSize + points[4] - points[1]) * T;
	}
	
	/** Hallar el maximo (max). */
	private void findMax() {
		points[0] = indexInterval[0];
		if (indexInterval[0] == indexInterval[1]) {
			return;
		}
		int end = indexInterval[1];
		if (indexInterval[0] > end) {
			end = end + bufferSize;
		}
		
		int temp = buffer[indexInterval[0]];
		int max1 = indexInterval[0];
		for (int i = indexInterval[0]; i <= end; i++) {
			if (buffer[i % bufferSize] > temp) {
				max1 = i;
				temp = buffer[i % bufferSize];
			}
		}
		
		temp = buffer[end % bufferSize];
		int max2 = end;
		for (int i = end; i >= indexInterval[0]; i--) {
			if (buffer[i % bufferSize] > temp) {
				max2 = i;
				temp = buffer[i % bufferSize];
			}
		}
		
		points[0] = ((max1 + max2) / 2) % bufferSize;
	}
	
	/** Hallar el pie (foot). */
	private void findFoot() {
		points[1] = indexInterval[0];
		int end = points[0];
		if (indexInterval[0] > end) {
			end = end + bufferSize;
		}
		
		int temp = secondDerivative[indexInterval[0]];	
		for (int i = indexInterval[0]; i < end; i++) {
			if (secondDerivative[i % bufferSize] > temp) {
				points[1] = i;
				temp = secondDerivative[i % bufferSize];
			}
		}
		points[1] = points[1] % bufferSize;
	}
	
	/** Hallar el punto de maxima primera derivada (slope). */
	private void findSlopePoint() {
		points[2] = points[1];
		int end = points[0];
		if (points[1] > end) {
			end = end + bufferSize;
		}
		
		int temp = firstDerivative[points[1]];	
		for (int i = points[1]; i < end; i++) {
			if (firstDerivative[i % bufferSize] > temp) {
				points[2] = i;
				temp = firstDerivative[i % bufferSize];
			}
		}
		points[2] = points[2] % bufferSize;
	}
	
	/**
	 * Hallar el punto inicial del intervalo de anchura 2/3 de la altura (wt0).
	 * @param limit Limite superior
	 */
	private void findUpperBound(int limit) {
		points[3] = points[1];
		while (buffer[points[3]] < limit) {
			points[3]++;
			if (points[3] == bufferSize) {
				points[3] = 0;
			}
		}
	}
	
	/**
	 * Hallar el punto final del intervalo de anchura 2/3 de la altura (wt1).
	 * @param limit Limite inferior
	 */
	private void findLowerBound(int limit) {
		points[4] = points[0];
		while (buffer[points[4]] > limit) {
			points[4]++;
			if (points[4] == bufferSize) {
				points[4] = 0;
			}
		}
	}
	
	/** Hallar la media de la oximetria en un intervalo de pleth. */
	private void calculateMeanSpO2() {
		int end = indexInterval[1];
		if (indexInterval[0] > end) {
			end = end + bufferSize;
		}
		
		int sum = 0;
		int count = 0;
		int tmpSpO2 = 0;
		for (int i = indexInterval[0]; i < end; i++) {
			tmpSpO2 = spO2[i % bufferSize];
			if (tmpSpO2 <= 100) {
				sum += tmpSpO2;
				count++;
			}
		}

		meanSpO2 = (count == 0) ? 0 : sum / count;
	}
}
