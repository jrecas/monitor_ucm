/* 
 * ECGPlethDelay.java
 */

package com.ucm.monitor.buffer;

/**
 * Implementa el retardo entre la onda R de ECG y la siguiente muestra de pleth.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/08/10
 */
public class ECGPlethDelay {
	
	/**
	 * Numero de muestras de retardo entre las dos ultimas ondas R de ECG y
	 * las siguientes muestras de pleth.
	 */
	private int[] samplesDelay;
	
	/** Constructor de ECGPlethDelay. */
	public ECGPlethDelay() {
		samplesDelay = new int[2];
	}
	
	/**
	 * Actualizar el numero de muestras de retardo entre las ultimas ondas R de ECG
	 * y las siguientes muestras de pleth.
	 * @param numSamples Numero de muestras de ECG de diferencia
	 */
	public void setNewSamplesDelay(int numSamples) {
		samplesDelay[0] = samplesDelay[1];
	   	samplesDelay[1] = numSamples;
	}
	
	/**
	 * Obtener el numero de muestras de retardo del inicio del intervalo.
	 * @return Numero de muestras entre ECG y pleth
	 */
	public int getOldSamplesDelay() {
		return samplesDelay[0];
	}
}