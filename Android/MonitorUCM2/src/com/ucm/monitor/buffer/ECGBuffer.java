/* 
 * ECGBuffer.java
 */

package com.ucm.monitor.buffer;

/**
 * Implementa un buffer con muestras de ECG.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/08/10
 */
public class ECGBuffer extends Buffer {
	
	/** Numero de puntos de referencia. */
	public static final int NUM_POINTS = 3;
	
	/** Numero de tiempos correspondientes a ECG: 1/HR. */
	public static final int NUM_TIMES = 1;
	
	/** Ultimas muestras de ECG. Necesarias para las derivadas. */
	private int[] lastSamples;
	
	/** Muestras de las derivadas del ECG. */
	private int[] firstDerivative;
	private int[] secondDerivative;
	private int[] thirdDerivative;
	
	/** Cambios de signos en las derivadas. */
	private boolean[] changeSignFirstDerivative;
	private boolean[] changeSignThirdDerivative;
	
	/** Indice del llenado de ambas derivadas. */
	private int indexDerivative;
	
	/**
	 * Posiciones del ultimo complejo QRS detectado.
	 * 0: Onda Q. 1: Onda R. 2: Onda S.
	 */
	private int[] bumps;
	
	/** Posiciones de la ultima onda R detectada. */
	private int newRWave;
	
	/** Nuevo complejo QRS encontrado. */
	private boolean newBumpsFound; 
	
	/** Valor umbral para deteccion de onda R. */
	private int thresholdSecDer;
	private int thresholdAmpR;
	
	/** Valores umbrales temporales para deteccion de onda R. */
	private int tmpThresholdSecDer;
	private int tmpThresholdAmpR;
	
	/** Ventana de muestras para actualizar el valor umbral. */
	private final int windowThreshold;
	
	/** Contador para el valor umbral. */
	private int nSamplesThreshold;
	
	/**
	 * Constructor de ECGBuffer.
	 * @param bufSize Tamaño del buffer
	 * @param indWrite Indice de escritura
	 * @param indRead Indice de lectura
	 * @param t Periodo entre datos
	 */
	public ECGBuffer(int bufSize, int indWrite, int indRead, int t) {
		super(bufSize, indWrite, indRead, t);
		newRWave = -1;
		bumps = new int[3];
		for (int i = 0; i < bumps.length; i++) {
			bumps[i] = -1;
		}
		newBumpsFound = false;
		lastSamples = new int[4];
		firstDerivative = new int[bufSize];
		secondDerivative = new int[bufSize];
		thirdDerivative = new int[bufSize];
		changeSignFirstDerivative = new boolean[bufSize];
		changeSignThirdDerivative = new boolean[bufSize];
		indexDerivative = (indWrite + bufSize - 2) % bufSize;
		thresholdSecDer = Integer.MIN_VALUE;
		thresholdAmpR = Integer.MAX_VALUE;
		tmpThresholdSecDer = 0;
		tmpThresholdAmpR = 0;
		windowThreshold = 2000 / t;
		nSamplesThreshold = 0;
	}
	
	/**
	 * Guardar una muestra (int) en el buffer de ECG
	 * y hallar su primera y segunda derivada.
	 * @param sample Muestra a guardar
	 */
	public void writeSample(int sample) {
		nSample++;
		nSampleArray[indexWrite] = nSample;
		writeDerivatives(sample);
		writeInBuffer(sample);
		updateThresholds();
		searchQRSComplex();
		incrementIndexDerivative();
	}
	
	/**
	 * Guardar una muestra (double) en el buffer de ECG
	 * y hallar su primera y segunda derivada.
	 * @param sample Muestra a guardar
	 */
	public void writeSample(double sample) {
		nSample++;
		nSampleArray[indexWrite] = nSample;
		writeDerivatives((int) (1000000 * sample));
		writeInBuffer((int) sample);
		updateThresholds();
		searchQRSComplex();
		incrementIndexDerivative();
	}
	
	/**
	 * Obtener la posicion de la ultima onda R de ECG.
	 * @return Posicion de la ultima onda R de ECG
	 */
	public int getNewRWave() {
		return newRWave;
	}
	
	/**
	 * Obtener las posiciones del ultimo complejo QRS de ECG en funcion del buffer.
	 * Contiene la penultima onda R detectada.
	 * @return Posiciones de las ultimas ondas QRS de ECG
	 */
	public int[] getBumps() {
		newBumpsFound = false;
		return bumps;
	}
	
	/**
	 * Obtener las posiciones del ultimo complejo QRS de ECG en funcion del numero
	 * de muestras recibidas. Contiene la penultima onda R detectada.
	 * @return Posiciones de las ultimas ondas QRS de ECG
	 */
	public int[] getAbsBumps() {
		newBumpsFound = false;
		int[] tmpAbsBumps = new int[bumps.length];
		for (int i = 0; i < tmpAbsBumps.length; i++) {
			tmpAbsBumps[i] = bumps[i] == -1 ? -1 : nSampleArray[bumps[i]];
		}
		return tmpAbsBumps;
	}
	
	/**
	 * Saber si se ha encontrado un nuevo complejo QRS de ECG.
	 * @return Onda R encontrada.
	 */
	public boolean newBumpsFound() {
		return newBumpsFound;
	}
	
	/**
	 * Obtener el periodo (1/HR) entre las dos ultimas ondas R de ECG.
	 * @return Periodo 1/HR
	 */
	public int getInvHR() {
		if (newRWave >= bumps[1]) {
			return (newRWave - bumps[1]) * T; 
		}
		return (bufferSize + newRWave - bumps[1]) * T;
	}
	
	/**
	 * Guardar en el buffer de ECG.
	 */
	private void writeInBuffer(int sample) {
		buffer[indexWrite] = sample;
		incrementIndexWrite();
	}
	
	/**
	 * Guardar la primera y la segunda derivada.
	 */
	private void writeDerivatives(int sample) {
		firstDerivative[indexDerivative] =
				lastSamples[0] - lastSamples[1];
		secondDerivative[indexDerivative] = 
				lastSamples[0] - 2 * lastSamples[1] + lastSamples[2];
		thirdDerivative[indexDerivative] =
				sample - 2 * lastSamples[0] + 2 * lastSamples[2] - lastSamples[3];
		lastSamples[3] = lastSamples[2];
		lastSamples[2] = lastSamples[1];
		lastSamples[1] = lastSamples[0];
		lastSamples[0] = sample;
		updateChangeSignDerivatives();
	}
	
	/** True si existe un cambio de signo en la derivada. */
	private void updateChangeSignDerivatives() {
		int temp = (indexDerivative + bufferSize - 1) % bufferSize;
		if (((firstDerivative[indexDerivative] < 0) && (firstDerivative[temp] > 0))
				|| ((firstDerivative[indexDerivative] > 0) && (firstDerivative[temp] < 0))) {
			changeSignFirstDerivative[indexDerivative] = true;
		} else {
			changeSignFirstDerivative[indexDerivative] = false;
		}
		if (((thirdDerivative[indexDerivative] < 0) && (thirdDerivative[temp] > 0))
				|| ((thirdDerivative[indexDerivative] > 0) && (thirdDerivative[temp] < 0))) {
			changeSignThirdDerivative[indexDerivative] = true;
		} else {
			changeSignThirdDerivative[indexDerivative] = false;
		}
	}
	
	/** Incremetar el indice de llenado de ambas derivadas. */
	private void incrementIndexDerivative() {
		indexDerivative++;
		if (indexDerivative == bufferSize) {
			indexDerivative = 0;
		}
	}

	/** Actualizar el valor umbral para la deteccion de la onda R. */
	private void updateThresholds() {
		if (secondDerivative[indexDerivative] < tmpThresholdSecDer) {
			tmpThresholdSecDer = secondDerivative[indexDerivative];
		}
		if (buffer[indexDerivative] > tmpThresholdAmpR) {
			tmpThresholdAmpR = buffer[indexDerivative];
		}
		nSamplesThreshold++;
		if (nSamplesThreshold == windowThreshold) {
			thresholdSecDer = tmpThresholdSecDer / 2;
			thresholdAmpR = tmpThresholdAmpR / 2;
			tmpThresholdSecDer = 0;
			tmpThresholdAmpR = 0;
			nSamplesThreshold = 0;
		}
	}
	
	/**
	 * Buscar la onda R con el cambio de signo de la primera derivada del ECG.
	 * @param sample Muestra
	 */
	private void searchQRSComplex() {
		if ((secondDerivative[indexDerivative] < thresholdSecDer) &&
				(buffer[indexDerivative] > thresholdAmpR) &&
				(changeSignFirstDerivative[indexDerivative] == true)) {
			indexInterval[0] = bumps[1];
			bumps[1] = newRWave;
			newRWave = indexDerivative;
			indexInterval[1] = indexDerivative;
			if (bumps[1] >= 0) {
				searchQWave();
				searchSWave();
				newBumpsFound = true;
			}
		}
	}
	
	/**
	 * Buscar la onda Q con el cambio de signo de la primera derivada del ECG
	 * a la izquierda de la onda R y siendo negativo el valor de ECG.
	 * @param sample Muestra
	 */
	private void searchQWave() {
		int temp = (bumps[1] + bufferSize - 1) % bufferSize;
		while (changeSignFirstDerivative[temp] == false) {
			temp--;
			if (temp < 0) {
				temp = bufferSize - 1;
			}
		}
		if (buffer[temp] < 0) {
			bumps[0] = temp;
		} else {
			bumps[0] = -1;
		}
	}
	
	/**
	 * Buscar la onda S con el cambio de signo de la primera derivada del ECG
	 * a la izquierda de la onda R y siendo negativo el valor de ECG.
	 * @param sample Muestra
	 */
	private void searchSWave() {
		int temp = (bumps[1] + 1) % bufferSize;
		while (changeSignFirstDerivative[temp] == false) {
			temp++;
			if (temp == bufferSize) {
				temp = 0;
			}
		}
		if (buffer[temp] < 0) {
			bumps[2] = temp;
		} else {
			bumps[2] = -1;
		}
	}
}