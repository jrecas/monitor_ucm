/* 
 * ECGBuffer.java
 */

package com.ucm.monitor.buffer;

/**
 * Implementa un buffer con muestras de ECG.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/08/10
 */
public class ECGBufferLite extends Buffer {
	
	/** Numero de puntos de referencia. */
	public static final int NUM_POINTS = 1;
	
	/** Numero de tiempos correspondientes a ECG: 1/HR. */
	public static final int NUM_TIMES = 1;
	
	/**
	 * Constructor de ECGBuffer.
	 * @param bufSize Tamaño del buffer
	 * @param indWrite Indice de escritura
	 * @param indRead Indice de lectura
	 * @param t Periodo entre datos
	 */
	public ECGBufferLite(int bufSize, int indWrite, int indRead, int t) {
		super(bufSize, indWrite, indRead, t);
	}
	

	/**
	 * Obtener las posiciones de las dos ultimas ondas R de ECG.
	 * {0: Onda R anterior, 1: Onda R siguiente}.
	 * @return int[] Posiciones de las ultimas dos ondas R de ECG
	 */
	public int[] getRBumps() {
		return indexInterval;
	}
	
	/**
	 * Obtener el periodo (1/HR) entre las dos ultimas ondas R de ECG.
	 * @return Periodo 1/HR
	 */
	public int getInvHR() {
		if (indexInterval[1] >= indexInterval[0]) {
			return (indexInterval[1] - indexInterval[0]) * T; 
		}
		return (bufferSize + indexInterval[1] - indexInterval[0]) * T;
	}
	
	/**
	 * Determinar si las ondas R tienen la misma posicion.
	 * @return boolean
	 */
	public boolean sameRBumps() {
		return (indexInterval[0] == indexInterval[1]);
	}
}
