package com.ucm.monitor.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import android.os.Environment;
import android.text.format.Time;
import android.util.Log;

public class MonitorLog {
	
	public static final String TAG = "MONITOR_LOG";
	
	/** The storage directory. */
	public static final String ROOT_DIR = "/MONITOR_UCM/";
	private File rootDir;
	
	/** The upload directory. */
	public static final String	UPLOAD_DIR = "/MONITOR_UCM/Upload/";
	private File uploadDir;
	private static int UPLOAD_TIME;
	
	/** Date format for logs. */
	private static final String dateFormat = "%Y.%m.%d-%H.%M.%S";
	
	/** Buffered file log list. */
	private ArrayList<BufferedFileLog> bFileLogList;
	
	/** The BufferedFileLog for raw data. */
	private BufferedFileLog buffFileRaw = null;
	private BufferedFileLog buffFileEcg = null;
	private BufferedFileLog buffFileEcg1k = null;
	private BufferedFileLog buffFileDelEcg = null;
	private BufferedFileLog buffFilePleth= null;
	private BufferedFileLog buffFileDelPleth = null;
	private BufferedFileLog buffFileBp = null;
	
	public MonitorLog(String version, String deviceName, int uploadPeriod) {		
		UPLOAD_TIME = uploadPeriod;
		
		// Get external storage state
		String state = Environment.getExternalStorageState();
		
		// External media should be mounted
		if (Environment.MEDIA_MOUNTED.equals(state)) {
			// Fetch current time
			Time now = new Time();
			now.setToNow();
			rootDir = new File(Environment.getExternalStorageDirectory() +
					ROOT_DIR + now.format(dateFormat) + "/");
			uploadDir = new File(Environment.getExternalStorageDirectory() +
					UPLOAD_DIR + now.format(dateFormat) + "/");
			Log.d(TAG, rootDir.getAbsolutePath());
			
			rootDir.mkdirs();
			uploadDir.mkdirs();
			
			try {
				BufferedWriter logFile = new BufferedWriter(new FileWriter(uploadDir.toString() +
						"/logStart.txt"));
				logFile.write("Log start: " + now.format(dateFormat) + "\n" + "Device name: " +
						deviceName + "\n" + version);
				logFile.close();
				logFile = null;
			} catch(IOException e) {
				e.printStackTrace();
			}
			
			bFileLogList = new ArrayList<BufferedFileLog>();
		}
	}
	
	public void logRaw(byte token, byte[] packet) throws FileNotFoundException {
		logRaw(token);
		buffFileRaw.write(packet, packet.length, true);
	}
	
	public void logRaw(byte token) throws FileNotFoundException{
		if (buffFileRaw == null) {
			buffFileRaw = new BufferedFileLog(rootDir + "/raw", uploadDir.toString() + "/", UPLOAD_TIME);
			bFileLogList.add(buffFileRaw);
		}
		buffFileRaw.write(token, false);
	}
	
	public void logEcg(byte[] packet) throws FileNotFoundException {
		if (buffFileEcg == null) {
			buffFileEcg = new BufferedFileLog(rootDir + "/ecg", uploadDir.toString() + "/", UPLOAD_TIME);
			bFileLogList.add(buffFileEcg);
		}
		buffFileEcg.write(packet, packet.length, true);
	}
	
	public void logEcg1k(byte[] packet) throws FileNotFoundException {
		if (buffFileEcg1k == null) {
			buffFileEcg1k = new BufferedFileLog(rootDir + "/ecg1k", uploadDir.toString() + "/", UPLOAD_TIME);
			bFileLogList.add(buffFileEcg1k);
		}
		buffFileEcg1k.write(packet, packet.length, true);
	}
	
	
	public void logDelEcg(int[] data) throws FileNotFoundException {
		if (buffFileDelEcg == null) {
			buffFileDelEcg = new BufferedFileLog(rootDir + "/delEcg", uploadDir.toString() + "/", UPLOAD_TIME);
			bFileLogList.add(buffFileDelEcg);
		}
		for (int i = 0; i < data.length; i++) {
			buffFileDelEcg.writeInt(data[i], true);
		}
	}
	
	public void logPleth(byte[] packet) throws FileNotFoundException {
		if (buffFilePleth == null) {
			buffFilePleth = new BufferedFileLog(rootDir + "/pleth", uploadDir.toString() + "/", UPLOAD_TIME);
			bFileLogList.add(buffFilePleth);
		}
		buffFilePleth.write(packet, packet.length, true);
	}
	
	public void logDelPleth(int[] data) throws FileNotFoundException {
		if (buffFileDelPleth == null) {
			buffFileDelPleth = new BufferedFileLog(rootDir + "/delPleth", uploadDir.toString() + "/", UPLOAD_TIME);
			bFileLogList.add(buffFileDelPleth);
		}
		for (int i = 0; i < data.length; i++) {
			buffFileDelPleth.writeInt(data[i], true);
		}
	}
	
	public void logBp(int nSample, int[] data) throws FileNotFoundException {
		if (buffFileBp == null) {
			buffFileBp = new BufferedFileLog(rootDir + "/bp", uploadDir.toString() + "/", UPLOAD_TIME);
			bFileLogList.add(buffFileBp);
		}
		buffFileBp.writeInt(nSample, true);
		for (int i = 0; i < data.length; i++) {
			buffFileBp.writeInt(data[i], true);
		}
	}
	
	public void endLogs() {
		Iterator<BufferedFileLog> bFileLogIt = bFileLogList.iterator();
		while (bFileLogIt.hasNext()) {
			bFileLogIt.next().close();
		}
		
		// It should be empty....
		if (rootDir != null) {
			rootDir.delete();
		} else {
			Log.w(TAG, "Warning tmp dir: "+ rootDir.getName() +" not empty!!");
		}
		
		try {
			Time now = new Time();
			now.setToNow();
			BufferedWriter logFile = new BufferedWriter(new FileWriter(uploadDir.toString() + "/logEnd.txt"));
			logFile.write("Log end: " + now.format(dateFormat) + "\n");
			logFile.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}
