package com.ucm.monitor.util;

import java.io.DataInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import com.ucm.monitor.MonitorUCM;

import android.os.Handler;

public class FileReadThread implements Runnable {
	
	//private FileInputStream rawFile;
	private DataInputStream	rawFile;
	
	private int nMuestras = 4;
	
	private byte[] rawData =
			new byte[(MonitorUCM.ECG_PAQ_LENGTH) * nMuestras + MonitorUCM.PLETH_PAQ_LENGHT * (nMuestras >> 2)];
	
	private Handler mHandler;
	
	private MonitorUCM mMonitorUCM;
	
	/// FIXME Esto es una chapuza, habria que pasarle un listener y no una referencia al objeto completo....
	public FileReadThread(MonitorUCM _mMonitorUCM) throws FileNotFoundException {
		
		mMonitorUCM = _mMonitorUCM;
		mHandler = new Handler();
		
		//rawFile = new FileInputStream(new File(Environment.getExternalStorageDirectory() + "/UCM_ECG/RAW.log"));
		try {
			rawFile = new DataInputStream(mMonitorUCM.getAssets().open("RAW.log"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Override
	public void run(){
		try {
			if (!mMonitorUCM.isFinishing()) {
				int readData;
				for (int i = 0 ; i < rawData.length; i++) {
					if ((readData = rawFile.read()) == -1) {
						stop();
						return; //EOF
					}
					rawData[i] = (byte) (readData & 0xFF);
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		mMonitorUCM.updateReceivedData(rawData);
		mHandler.postDelayed(this, 4 * nMuestras);
	}
	
	/// FIXME lo paro por las bravas, dara excepcion...
	public void stop() {
		if (rawFile != null) {
			try {
				rawFile.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}