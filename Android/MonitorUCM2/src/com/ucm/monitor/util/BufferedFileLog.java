package com.ucm.monitor.util;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import android.util.Log;

/**
 * The Class BufferedFileLog.
 * 
 * @author Joaquin Recas
 */
public class BufferedFileLog {
	
	/** The Constant DEV_TAG, Tag for debugger. */
	private static final String TAG = "BuffFile";
	
	/** Move period in ms. */
	private int MOVE_PERIOD;

	/** The root directory. */
	private String ROOT_DIR;

	/** The destination folder. */
	private String DEST_FOLDER;

	/** The BufferedOutputStream. */
	private BufferedOutputStream cBuffStream;
	
	/** The current file. */
	private File cFile;
	
	/** Byte array used in write(byte). */
	private byte[] byteArr = new byte[1];
	
	/** The index name. */
	private int indName;
	
	/** The flush index. */
	private int indFlush = 0;
	
	private long inicTime = 0;
	
	/**
	 * Instantiates a new buffered file log.
	 * 
	 * @param fileRoot: the file root
	 * @param destFolder: the dest folder
	 * @param moveSamples: the move samples
	 * @throws FileNotFoundException
	 */
	public BufferedFileLog(String fileRoot, String destFolder, int movePeriod) throws FileNotFoundException {
		ROOT_DIR = fileRoot;
		DEST_FOLDER = destFolder;
		MOVE_PERIOD = movePeriod;
		indName = 0;
		newFile();
	}
	
	/**
	 * New file.
	 * 
	 * @throws FileNotFoundException
	 */
	private void newFile() throws FileNotFoundException {
		inicTime = System.currentTimeMillis();
		cFile = new File(ROOT_DIR + String.format("%03d", indName) + ".log");
		cBuffStream = new BufferedOutputStream(new FileOutputStream(cFile));
		indFlush = 0;
	}
	
	/**
	 * Write.
	 * 
	 * @param buffer: the buffer
	 * @param length: the length
	 * @param mayFlush: may flush
	 * @throws FileNotFoundException
	 */
	public synchronized void write(byte[] buffer, int length, boolean mayFlush) throws FileNotFoundException {
		try {
			cBuffStream.write(buffer, 0, length);
		} catch(IOException e) {
			e.printStackTrace();
		}
		indFlush += length;
		if (mayFlush && (System.currentTimeMillis() - inicTime) >= MOVE_PERIOD) {
			closeAndMove();
			indName++;
			newFile();
		}
	}
	
	/**
	 * Write.
	 * 
	 * @param token: the token
	 * @throws FileNotFoundException
	 */
	public void write(byte token, boolean mayFlush) throws FileNotFoundException {
		byteArr[0] = token;
		write(byteArr, 1, mayFlush);
	}
	
	/**
	 * Write int.
	 * 
	 * @param integer: the integer
	 * @throws FileNotFoundException
	 */
	public void writeInt(int integer, boolean mayFlush) throws FileNotFoundException {
		/* 
		 * An integer is 4B --> putInt(int value) Writes the given int
		 *  to the current position and increases the position by 4.
		 */
		byte[] byteArr = ByteBuffer.allocate(4).putInt(integer).array();
		write(byteArr, 4, mayFlush);
	}
	
	/**
	 * Write float.
	 * 
	 * @param integer: the integer
	 * @throws FileNotFoundException
	 */
	public void writeFloat(float floatValue, boolean mayFlush) throws FileNotFoundException {
		/* 
		 * A float is 4B --> putFloat(float value) Writes the given float
		 * to the current position and increases the position by 4.
		 */
		byte[] byteArr = ByteBuffer.allocate(4).putFloat(floatValue).array();
		write(byteArr, 4, mayFlush);
	}
	
	/** Close and move. */
	public void closeAndMove() {
		try {
			cBuffStream.flush();
			cBuffStream.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
		
		//File empty?
		if (indFlush == 0) {
			cFile.delete();
		} else if (cFile.renameTo(new File(DEST_FOLDER + cFile.getName())) == false) {
			Log.e(TAG, "Unable to move file: " + cFile.toString() + " to: " + DEST_FOLDER + cFile.getName());
		}
	}
	
	/** Close. */
	public void close() {
		closeAndMove();
	}
}
