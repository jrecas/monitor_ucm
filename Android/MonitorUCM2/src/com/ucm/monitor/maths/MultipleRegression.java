/*
 * MultipleRegression.java
 */

package com.ucm.monitor.maths;

/**
 * Implementa una regresion lineal multiple.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/08/26
 */
public class MultipleRegression {
	
	/** Array x1. */
	private double[] x1;
	
	/** Array x2. */
	private double[] x2;
	
	/** Array y. */
	private double[] y;
	
	/** Numero de datos de x1, x2 e y. */
	private int n;
	
	/** Coefiente de determinacion R2. */
	private double r2;
	
	/** Coefiente de determinacion R2 ajustado. */
	private double adjR2;
	
	/**
	 * Coeficientes del ajuste lineal.
	 * 0: ordenada, 1: pendiente.
	 */
	private double[] coefficients;
	
	/**
	 * Constructor de LinearRegression.
	 * @param arrayX Array x
	 * @param arrayY Array y
	 */
	public MultipleRegression(double[] arrayX1, double[] arrayX2, double[] arrayY) {
		x1 = arrayX1;
		x2 = arrayX2;
		y = arrayY;
		n = arrayY.length;
		r2 = 0.0;
		adjR2 = 0.0;
		coefficients = new double[3];
		calcCoefficients();
	}
	
	/**
	 * Obtener el coeficiente de determinacion r2.
	 * @return Coeficiente r2
	 */
	public double getR2() {
		return r2;
	}
	
	/**
	 * Obtener el coeficiente de determinacion ajustado r2.
	 * @return Coeficiente r2 ajustado
	 */
	public double getAdjR2() {
		return adjR2;
	}
	
	/**
	 * Obtener los coeficientes del ajuste lineal.
	 * @return Coeficientes del ajuste lineal
	 */
	public double[] getCoefficients() {
		return coefficients;
	}
	
	/** Calcular los coeficientes de la regresion lineal. */
	private void calcCoefficients() {		
		double sumX1 = 0.0;
		double sumX2 = 0.0;
		double sumY = 0.0;
		double sumX1X1 = 0.0;
		double sumX1X2 = 0.0;
		double sumX2X2 = 0.0;
		double sumX1Y = 0.0;
		double sumX2Y = 0.0;
		double sumYY = 0.0;
		
		for (int i = 0; i < n; i++) {
			sumX1 += x1[i];
			sumX2 += x2[i];
			sumY += y[i];
			sumX1X1 += x1[i] * x1[i];
			sumX1X2 += x1[i] * x2[i];
			sumX2X2 += x2[i] * x2[i];
			sumX1Y += x1[i] * y[i];
			sumX2Y += x2[i] * y[i];
			sumYY += y[i] * y[i];
		}
		
		double meanX1 = sumX1 / n;
		double meanX2 = sumX2 / n;
		double meanY = sumY / n;
		
		double tempX1X1 = (sumX1X1 - n * meanX1 * meanX1);
		double tempX2X2 = (sumX2X2 - n * meanX2 * meanX2);
		double tempX1X2 = (sumX1X2 - n * meanX1 * meanX2);
		double tempX1Y = (sumX1Y - n * meanX1 * meanY);
		double tempX2Y = (sumX2Y - n * meanX2 * meanY);
		double tempYY = n * meanY * meanY;
		
		double tempDen = tempX1X1 * tempX2X2 - tempX1X2 * tempX1X2;
		
		coefficients[1] = (tempX2X2 * tempX1Y - tempX1X2 * tempX2Y) / tempDen;
		coefficients[2] = (tempX1X1 * tempX2Y - tempX1X2 * tempX1Y) / tempDen;
		coefficients[0] = meanY - coefficients[1] * meanX1 - coefficients[2] * meanX2;
		
		r2 = (coefficients[0] * sumY + coefficients[1] * sumX1Y + coefficients[2] * sumX2Y
				- tempYY) / (sumYY - tempYY);
		adjR2 = 1.0 - ((double) (n - 1) / (n - 3)) * (1.0 - r2);
	}
}