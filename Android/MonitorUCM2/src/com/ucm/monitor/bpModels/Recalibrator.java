/*
 * Recalibrator.java
 */

package com.ucm.monitor.bpModels;

import java.util.Vector;
import android.content.Context;

//import com.ucm.monitor.MonitorUCM;
import com.ucm.monitor.maths.LinearRegression;
import com.ucm.monitor.maths.MultipleRegression;

/**
 * Actualiza los valores de la calibracion.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/11/10
 */
public class Recalibrator {
	
	/** Numero de latidos que son tomados por cada valor de presion de referencia. */
	public static final int NUM_BEATS = 10;
	
	/** Activar modo calibracion */
	private boolean modeOn;	
	
	/** Contador del numero de latidos. */
	private int numBeats;
	
	/**
	 * Conjunto de presiones tomadas con un sistema de referencia.
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private Vector<double[]> bpVector;
	
	/**
	 * Ultimas presiones de referencia tomadas.
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private double[] bp;
	
	/**
	 * Conjunto de tiempos correspondientes a cada conjunto de presiones de referencia.
	 * {PATp, PATf, PATs, WT, t1, t2, PATfWT, 1/HR}.
	 */
	private Vector<double[]> timesVector;	
	
	/**
	 * Ultimos tiempos correspondientes a las ultimas presiones de referencia.
	 * {PATp, PATf, PATs, WT, t1, t2, PATfWT, 1/HR}.
	 */
	private int[][] times;
	
	/** Las ultimas presiones estan disponibles para ser guardadas. */
	private boolean bpAvailable;
	
	/** La media de los ultimos tiempos estan disponibles para ser guardados. */
	private boolean timesAvailable;
	
	/**
	 * Numero de modelo elegido en la estimacion de las presiones:
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private int[] models;
	
	/**
	 * Coeficientes del ajuste de los modelos elegidos para las presiones:
	 * {0: Sistolica, 1: Diastolica}.
	 */
	private double[][] coefficients;
	
	/**
	 * Constructor de Recalibrator
	 * @param mMonitorUCM MonitorUCM
	 */
	public Recalibrator(Context context) {
		modeOn = false;
		numBeats = 0;
		bpVector = new Vector<double[]>();
		bp = new double[BPEstimator.NUM_BP];
		timesVector = new Vector<double[]>();
		times = new int[BPEstimator.NUM_TIMES][NUM_BEATS];
		bpAvailable = false;
		timesAvailable = false;
		models = new int[BPEstimator.NUM_BP];
		coefficients = new double[BPEstimator.NUM_BP][3];
	}
	
	/**
	 * Guardar las ultimas presiones de referencia.
	 * @param bpVector Presiones
	 */
	public void setBp(int[] newBp) {
		bp[0] = (double) newBp[0];
		bp[1] = (double) newBp[1];
		bpAvailable = true;
		addData();
	}
	
	/**
	 * Guardar los tiempos correspondientes a las ultimas presiones.
	 * @param timesVector Tiempos
	 */
	public void setTimes(int[] newTimes) {
		for (int i = 0; i < BPEstimator.NUM_TIMES; i++) {
			times[i][numBeats - 1] = newTimes[i];
		}
		numBeats--;
		if (numBeats == 0) {
			timesAvailable = true;
			addData();
		}
	}
	
	/**
	 * Obtener los modelos elegidos en la estimacion de las presiones.
	 * {0: Sistolica, 1: Diastolica}.
	 * @return Modelos elegidos
	 */
	public int[] getModels() {
		return models;
	}
	
	/**
	 * Obtener los coeficientes del ajuste de los modelos elegidos para las presiones.
	 * {0: Sistolica, 1: Diastolica}.
	 * @return Coeficientes de los modelos
	 */
	public double[][] getCoefficients() {
		return coefficients;
	}
	
	/** Activar el modo calibracion. */
	public void setModeOn() {
		numBeats = NUM_BEATS;
		modeOn = true;
	}
	
	/** Desactivar el modo calibracion. */
	public void setModeOff() {		
		modeOn = false;
		numBeats = 0;
		bp = new double[BPEstimator.NUM_BP];
		times = new int[BPEstimator.NUM_TIMES][NUM_BEATS];
		bpAvailable = false;
		timesAvailable = false;
	}
	
	/**
	 * True si el modo calibracion esta activado.
	 * @return Modo activado
	 */
	public boolean isModeOn() {
		return modeOn;
	}
	
	/**
	 * True si la calibracion puede ser llevada a cabo. Son necesarios al menos
	 * 3 conjuntos de valores de presion-tiempo.
	 * @return La calibracion es posible
	 */
	public boolean isReady() {
		return bpVector.size() >= 3;
	}
	
	/**
	 * True si el recalibrador necesita mas muestras de tiempos.
	 * @return Necesita mas muestras de tiempos
	 */
	public boolean needsTimes() {
		return numBeats != 0;
	}
	
	/** Calibracion del sistema. Actualiza los modelos elegidos y sus coeficientes. */
	public void calibrate() {
		
		double[][] tmpBp = new double[bpVector.size()][BPEstimator.NUM_BP];
		for (int i = 0; i < bpVector.size(); i++) {
			tmpBp[i] = bpVector.elementAt(i);
		}
		double[][] tempBp = new double[BPEstimator.NUM_BP][bpVector.size()];		
		for (int i = 0; i < tmpBp.length; i++) {
			for (int j = 0; j < tmpBp[0].length; j++) {
				tempBp[j][i] = tmpBp[i][j];
			}
		}
		
		double[][] tmpTimes = new double[timesVector.size()][BPEstimator.NUM_TIMES];
		for (int i = 0; i < timesVector.size(); i++) {
			tmpTimes[i] = timesVector.elementAt(i);
		}
		double[][] tempTimes = new double[BPEstimator.NUM_TIMES][timesVector.size()];
		for (int i = 0; i < tmpTimes.length; i++) {
			for (int j = 0; j < tmpTimes[0].length; j++) {
				tempTimes[j][i] = tmpTimes[i][j];
			}
		}
		
		/* tempInvHR */
		double[] tempInvHr = new double[timesVector.size()];
		for (int i = 0; i < tempTimes[BPEstimator.NUM_TIMES - 1].length; i++) {
			tempInvHr[i] = 1 / tempTimes[BPEstimator.NUM_TIMES - 1][i];
		}
		
		/*
		 * tempTimesSquares
		 * 0: 1/PATp^2, 1: 1/PATf^2, 2: 1/PATs^2,
		 * 3: 1/(PATp-PEP)^2, 4: 1/(PATf-PEP)^2, 5: 1/(PATs-PEP)^2
		 */
		double[][] tempTimesSquares = new double[6][timesVector.size()];
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < timesVector.size(); j++) {
				tempTimesSquares[i][j] = 1 / (tempTimes[i][j] * tempTimes[i][j]);
				tempTimesSquares[i + 3][j] = 1 / ((tempTimes[i][j] - (double) BPEstimator.PEP) *
						(tempTimes[i][j] - (double) BPEstimator.PEP));
			}
		}
		
		
		for (int i = 0; i < BPEstimator.NUM_BP; i++) {			
			MultipleRegression multRegress = new MultipleRegression(
					tempTimes[0], tempInvHr, tempBp[i]);
			
			double maxCorrelation = multRegress.getAdjR2();
			models[i] = 1;
			coefficients[i][0] = (double) multRegress.getCoefficients()[0];
			coefficients[i][1] = (double) multRegress.getCoefficients()[1];
			coefficients[i][2] = (double) multRegress.getCoefficients()[2];
			
			LinearRegression[] regress = new LinearRegression[13];
			regress[0] = new LinearRegression(tempTimes[0], tempBp[i]);
			regress[1] = new LinearRegression(tempTimes[1], tempBp[i]);
			regress[2] = new LinearRegression(tempTimes[2], tempBp[i]);
			regress[3] = new LinearRegression(tempTimesSquares[0], tempBp[i]);
			regress[4] = new LinearRegression(tempTimesSquares[1], tempBp[i]);
			regress[5] = new LinearRegression(tempTimesSquares[2], tempBp[i]);
			regress[6] = new LinearRegression(tempTimesSquares[3], tempBp[i]);
			regress[7] = new LinearRegression(tempTimesSquares[4], tempBp[i]);
			regress[8] = new LinearRegression(tempTimesSquares[5], tempBp[i]);
			regress[9] = new LinearRegression(tempTimes[3], tempBp[i]);
			regress[10] = new LinearRegression(tempTimes[4], tempBp[i]);
			regress[11] = new LinearRegression(tempTimes[5], tempBp[i]);
			regress[12] = new LinearRegression(tempTimes[6], tempBp[i]);
						
			for (int j = 0; j < regress.length; j++) {
				if (regress[j].getAdjR2() > maxCorrelation) {
					maxCorrelation = regress[j].getAdjR2();
					models[i] = j + 2;
					coefficients[i][0] = (double) regress[j].getCoefficients()[0];
					coefficients[i][1] = (double) regress[j].getCoefficients()[1];
				}
			}
		}
		
		bpVector.clear();
		timesVector.clear();
	}
	
	/** Guardar ultimos valores de presiones y tiempos. */
	private void addData() {
		if (bpAvailable && timesAvailable) {
			bpVector.addElement(bp);
			double[] tmpTimes = calculateMeanTimes();
			timesVector.addElement(tmpTimes);
			setModeOff();
		}
	}
	
	/**
	 * Calcular las medias de los valores de tiempos.
	 * @return Medias de los tiempos
	 */
	private double[] calculateMeanTimes() {
		double[] tempTimes = new double[BPEstimator.NUM_TIMES];
		int k = 0;
		for (int i = 0; i < BPEstimator.NUM_TIMES; i++) {
			int tempMean = 0;
			for (int j = 0; j < NUM_BEATS; j++) {
				if ((times[i][j] >= BPEstimator.TIME_BOUNDS[0][i]) &&
						(times[i][j] <= BPEstimator.TIME_BOUNDS[1][i])) {
					tempMean += times[i][j];
					k++;
				}
			}
			tempTimes[i] = (k == 0) ? tempMean : tempMean / k;
			k = 0;
		}
		return tempTimes;
	}
}