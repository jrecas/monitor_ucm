/* 
 * LowFIRFilter.java
 */

package com.ucm.monitor.filter;

/**
 * Implementa de forma generica de un filtro FIR paso bajo,
 * de orden y frecuencia dados segun los coeficientes pasados.
 * 
 * @author	Jose Manuel Bote
 * @since	2014/10/10
 */

public class LowFIRFilter {
	
	/** Retardo del filtro. */
	public final int filterDelay;
	
	/** Orden del filtro. */
	private final int filterOrder;
	
	/**
	 * Coeficientes del filtro FIR.
	 * Solo se necesitan la mitad porque el filtro es simetrico.
	 */
	private final double[] filterCoefficients;
	
	/**
	 * Variable necesaria para agilizar las iteraciones.
	 * Coincide con la mitad del orden del filtro,
	 * con la longitud del array de coeficientes menos 1 y
	 * con el retardo del filtro.
	 */
	private final int N;
	
	/** Muestras para implementar el filtro. */
	private double[] xSamples;
	
	/** Constructor de LowFIRFilter. */
	public LowFIRFilter(int order, double[] coefficients) {
		filterDelay = order / 2;
		filterOrder = order;
		filterCoefficients = coefficients;
		N = order / 2;
		xSamples = new double[order + 1];
	}
	
	/**
	 * Filtrar las muestras (double).
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public double filterSample(double sample) {
		xSamples[filterOrder] = sample;
		double tempSample = filterCoefficients[N] * xSamples[N];
		for (int i = 0; i < N; i++) {
			tempSample += filterCoefficients[i] * (xSamples[i] + xSamples[N]);
		}
		for (int i = 0; i < filterOrder; i++) {
			xSamples[i] = xSamples[i + 1];
		}
		return tempSample;
	}
	
	/**
	 * Filtrar las muestras (int).
	 * @param sample Muestra sin filtrar
	 * @return Muestra filtrada
	 */
	public int filterSample(int sample) {
		return (int) filterSample((double) sample);
	}
}
