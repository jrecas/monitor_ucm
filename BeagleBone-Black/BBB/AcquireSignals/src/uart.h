/*
 * uart.h
 *
 *  Created on: 10/5/2015
 *      Author: Jose Manuel Bote
 */

#ifndef _UART_
#define _UART_

#include <termios.h>
#include <stdint.h>

#define UART_BUFFERSIZE 128

typedef struct {
	int fid;
	struct termios options;
	uint8_t buffer[UART_BUFFERSIZE];
	int rxLength;
} uart;


/*
 * Configure the UART port for the Beglebone Black
 */
int configureUART(uart* uartPort, char* device);

/*
 * Read a byte from the UART port, return the number of read bytes
 */
int readUART(uart* uartPort);


#endif
