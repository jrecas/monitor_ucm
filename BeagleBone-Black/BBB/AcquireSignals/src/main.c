/*
 * BeagleBone Black
 *
 * Created on: 08/05/2015
 *     Author: Jose Manuel Bote
 *
 * Compile in BBB by: gcc -Wall -o AcquireSignals AcquireSignals.c -lpruio
 */

#include <fcntl.h>
#include <limits.h>
#include <pruio.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include "boolean.h"
#include "noninSensor.h"
#include "pipeFile.h"

#define	ACTIVE_DEVICES (PRUIO_ACT_ADC | PRUIO_ACT_PRU1)  // Activate/deactivate BBB PRUSS systems (see activateDevice in pruio.h)
#define AVERAGE        16                                 // Average for each measurement (1,2,4,8 or 16)
#define OPEN_DELAY     100                                // Allow the voltages to stabilize
#define SAMPLE_DELAY   10                                // Width of the start of conversion signal

#define ADC_CHANNELS      2            // Number of ADC channels (must be in accordance with ADC_MASK)
#define ADC_SAMP_CHANNEL  12           // Number of samples for each ADC channel (always even: 2,4,6...)
#define ADC_MASK          0b000000110  // Open the AIN-X with bit(X+1): bit-0 is not used, bit-1 is AIN-0, bit-2 AIN-1, etc.
#define ADC_TIMER         4E6          // Sampling rate (in ns): 1E9==1Hz, 1E7==100Hz 1E6==1kHz...
#define ADC_SAMPLES_MOD   0            // Size in bits of the output sample: 0->12-bits, 1->13-bits, ..., 4->16-bits

#define PACKET_TAG 0xB5

#define ADC_SAMP_BUF_SIZE      (ADC_CHANNELS*ADC_SAMP_CHANNEL)  // Length of the ADC samples buffer
#define ADC_SAMP_HALF_BUF_SIZE (ADC_SAMP_BUF_SIZE >> 1)         // Half-length of the ADC samples buffer
#define ADC_HALF_SAMP_CHANNEL  (ADC_SAMP_CHANNEL / 2)           // Half of the samples for each ADC channel (always even: 2,4,6...

/*
 * Configure the needed modules
 */
int configureModules(pruIo* io, pipeFile* pipePython, noninSensor* nonin);


/*
 * Main
 */
int main(void) {

	printf("Start program...\n");

	// PruIo driver
	pruIo* io = pruio_new(ACTIVE_DEVICES, AVERAGE, OPEN_DELAY, SAMPLE_DELAY);

	// Nonin sensor
	noninSensor nonin = {
			.uartPort = {
					.fid = -1,
					//.options,
					.buffer = {0}
			},
			.packet = {0},
			.frame = {0},
			.numFrames = 0,
			.tmpSpO2 = 0
	};

	// Pipe to Python program (Bluetooth module)
	pipeFile pipePython = {
			.name = "fifo.tmp",
			//.stat,
			.fid = -1
	};

	// File to store the bytes of the output signals
	FILE* outputFile;
	time_t now;
	now = time(NULL);
	struct tm *t = localtime(&now);
	char dateTime[30];
	strftime(dateTime, sizeof(dateTime), "../output/%y%m%d-%H%M%S.log", t);
	outputFile = fopen(dateTime, "wb");

	// The delay for which the program will sleep
	struct timespec delay = {
			.tv_sec = 0,
			.tv_nsec = ADC_TIMER  // ADC sampling rate in ns
	};

	// Configure pruIo driver, the nonin sensor and the pipe to Python
	printf("Configuring modules...\n");
	if (configureModules(io, &pipePython, &nonin)) {
		exit(EXIT_FAILURE);
	}

	printf("Acquiring data...\n");
	while (TRUE) {

		// Nonin sensor
		readNonin(&nonin);

		// ADC: the first or the second half of the buffer is full
		if ((io->DRam[0] >= 0) && (io->DRam[0] < ADC_SAMP_HALF_BUF_SIZE)) {  // The second half is full

			/*
			// Read the ADC samples
			int j;
			//printf("Values from j=%u to j=%u\n", ADC_SAMP_HALF_BUF_SIZE, ADC_SAMP_BUF_SIZE);
			for (j = ADC_SAMP_HALF_BUF_SIZE; j < ADC_SAMP_BUF_SIZE; j++) {
				printf("%u\n", io->Adc->Value[j]);
				//printf("Index=%u \tValue=%u\n", j, io->Adc->Value[j]);
				//printf("Pleth: %u\tZ: %u\tX: %u\tY: %u\n", io->Adc->Value[j], io->Adc->Value[j+1], io->Adc->Value[j+2], io->Adc->Value[j+3]);
			}
			fflush(stdout);
			*/

			// Create the packet
			uint8_t packet[1 + 2*ADC_SAMP_HALF_BUF_SIZE + NONIN_PACKET_SIZE] = {PACKET_TAG};
			memcpy(&packet[1], &(io->Adc->Value[ADC_SAMP_HALF_BUF_SIZE]), 2*ADC_SAMP_HALF_BUF_SIZE);
			memcpy(&packet[1 + 2*ADC_SAMP_HALF_BUF_SIZE], nonin.packet, NONIN_PACKET_SIZE);
			fwrite(packet, sizeof(packet), 1, outputFile);
			fflush(outputFile);

			// Try to open the Python pipe
			if (pipePython.fid < 1) {
				pipePython.fid = open(pipePython.name, O_WRONLY | O_NONBLOCK);
				//printf("Pipe open\n");
			}

			// Send data if the Python pipe is open
			if (pipePython.fid > 0) {
				int writeSuccess = write(pipePython.fid, packet, sizeof(packet));
				if (writeSuccess < 0) {
					close(pipePython.fid);
					pipePython.fid = -1;
				} else if (writeSuccess > PIPE_BUF) {
					printf("Warning!! Write in FIFO is not atomic\n");
				}
			}

			// Calculate the time of sleep
			delay.tv_nsec = (ADC_HALF_SAMP_CHANNEL - (io->DRam[0] / ADC_CHANNELS)) * ADC_TIMER;

		} else if (io->DRam[0] >= ADC_SAMP_HALF_BUF_SIZE && (io->DRam[0] < ADC_SAMP_BUF_SIZE)) {  // The first half is full

			/*
			int j;
			//printf("Values from j=0 to j=%u\n", ADC_SAMP_HALF_BUF_SIZE);
			for (j = 0; j < ADC_SAMP_HALF_BUF_SIZE; j++) {
				printf("%u\n", io->Adc->Value[j]);
				//printf("Index=%u \tValue=%u\n", j, io->Adc->Value[j]);
				//printf("Pleth: %u\tZ: %u\tX: %u\tY: %u\n", io->Adc->Value[j], io->Adc->Value[j+1], io->Adc->Value[j+2], io->Adc->Value[j+3]);
			}
			fflush(stdout);
			*/

			// Create the packet
			uint8_t packet[1 + 2*ADC_SAMP_HALF_BUF_SIZE + NONIN_PACKET_SIZE] = {PACKET_TAG};
			memcpy(&packet[1], io->Adc->Value, 2*ADC_SAMP_HALF_BUF_SIZE);
			memcpy(&packet[1 + 2*ADC_SAMP_HALF_BUF_SIZE], nonin.packet, NONIN_PACKET_SIZE);
			fwrite(packet, sizeof(packet), 1, outputFile);
			fflush(outputFile);

			// Try to open the Python pipe
			if (pipePython.fid < 1) {
				pipePython.fid = open(pipePython.name, O_WRONLY | O_NONBLOCK);
				//printf("Pipe open\n");
			}

			// Send data if the Python pipe is open
			if (pipePython.fid > 0) {
				int writeSuccess = write(pipePython.fid, packet, sizeof(packet));
				if (writeSuccess < 0) {
					close(pipePython.fid);
					pipePython.fid = -1;
				} else if (writeSuccess > PIPE_BUF) {
					printf("Warning!! Write in FIFO is not atomic\n");
				}
			}

			// Calculate the time of sleep
			delay.tv_nsec = (ADC_SAMP_CHANNEL - (io->DRam[0] / ADC_CHANNELS)) * ADC_TIMER;

		}

		// Sleep nPeriods * ADC_TIMER, is dynamically reconfigured depending on the
		// needed time by the last part of the program
		nanosleep(&delay, NULL);

	}

	fclose(outputFile);
	pruio_destroy(io);  // Destroy the driver structure
	exit(EXIT_SUCCESS);

}


/*
 * Configure the needed modules
 */
int configureModules(pruIo* io, pipeFile* pipePython, noninSensor* nonin) {

	// PruIo driver for ADC
	printf("Configuring PruIo...\n");

	// Before calling PruIo::config(), set the clock value to 0 to disable a subsystem or 2 to enable it
	printf("\tEnabling/disabling subsystems... ");
	io->Adc->Conf->ClVa = 2;  // Enabled (it should be already activated, but for safety)
	//io->Gpio->Conf[0]->ClVa = 0;
	//io->Gpio->Conf[1]->ClVa = 0;
	//io->Gpio->Conf[2]->ClVa = 0;
	//io->Gpio->Conf[3]->ClVa = 0;
	//io->PwmSS->Conf[0]->ClVa = 0;
	//io->PwmSS->Conf[1]->ClVa = 0;
	//io->PwmSS->Conf[2]->ClVa = 0;
	printf("OK\n");

	printf("\tConfiguring the new driver... ");
	if (pruio_config(io, ADC_SAMP_CHANNEL, ADC_MASK, ADC_TIMER, ADC_SAMPLES_MOD)) {
		printf("Failed: %s\n", io->Errr);
		return -1;
	} else {
		printf("OK\n");
	}
	printf("PruIo configured\n");

	// Nonin sensor
	printf("Configuring Nonin sensor... ");
	if (!configureNonin(nonin)) {
		printf("ERROR: Nonin sensor cannot be configured\n");
		//return FALSE;
	}
	printf("Nonin sensor configured\n");

	// Bluetooth
	printf("Enabling Bluetooth connection...\n");
	if (system("python BTConnection.py &")) {  // In background
		printf("Bluetooth connection cannot be created\n");
	}
	printf("Bluetooth connection enabled\n");


	// Pipe
	printf("\tStarting pipe... ");
	if (createPipe(pipePython)) {
		printf("Failed\n");
	} else {
		printf("OK\n");
	}

	// Ring buffer mode for pruIo
	printf("\tStarting ring-buffer mode... ");
	if (pruio_rb_start(io)) {
		printf("Failed: (%s)\n", io->Errr);
		return -1;
	}
	printf("OK\n");

	printf("\tStarting measuring...\n");
	printf("\tADC Sampling rate = %.2f Hz\tchannels = %i\n", 1e9/ADC_TIMER, io->Adc->ChAz);
	return 0;

}
