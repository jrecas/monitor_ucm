/*
 * nonin.c
 *
 *  Created on: 10/5/2015
 *      Author: Jose Manuel Bote
 */

#include <stdio.h>
#include "noninSensor.h"


/*
 * Configure the Nonin sensor for the Beglebone Black (UART2)
 */
int configureNonin(noninSensor* nonin) {
	if (!configureUART(&(nonin->uartPort), UART_DEVICE)) {
		return -1;
	} else {
		return 0;
	}
}

/*
 * Control the packets and frames sent by the Nonin sensor
 */
void parseNoninData(noninSensor* nonin, uint8_t byte) {

	int i;
	int crc = 0;

	// Update frame
	for (i = 0; i < 4; i++) {
		nonin->frame[i] = nonin->frame[i + 1];
		crc += nonin->frame[i];
	}
	nonin->frame[4] = byte;

#if SERIAL_DATA_FORMAT == 2

	// If it is a complete frame and bit7 of Status is 1
	if (nonin->frame[0] == 0x01 && (nonin->frame[1] & 0x80) == 0x80 &&
			(crc & 0xFF) == nonin->frame[4]) {

		// Shift the last values
		int j;
		for (j = 3; j < NONIN_PACKET_SIZE; j++) {
			nonin->packet[j-3] = nonin->packet[j];
		}
		nonin->packet[NONIN_PACKET_SIZE - 3] = 0;  // MSB (There is not MSB, so always 0)
		nonin->packet[NONIN_PACKET_SIZE - 2] = nonin->frame[2];  // LSB (Only from 0 to 255)

		// Status is OK and bit "SYNC" is 1, it is the frame number 1
		if ((nonin->frame[1] & 0x81) == 0x81) {
			// Final of a packet, update the SpO2 stored value
			if (nonin->numFrames == 25) {
				nonin->packet[NONIN_PACKET_SIZE - 1] = nonin->tmpSpO2;
			}
			nonin->numFrames = 1;

		} else {
			// The SpO2 value is in frame 9, in byte 3
			if (nonin->numFrames == 9) {
				nonin->tmpSpO2 = nonin->frame[3] & 0x7F;
			}
			// Update the number of frames if it is synchronized
			if (nonin->numFrames > 0 && nonin->numFrames < 25) {
				nonin->numFrames++;
			} else {
				nonin->numFrames = 0;
				nonin->tmpSpO2 = 0;
			}
		}

		//printf("%i\t%i\t%i\t%i\t%i\n", nonin->frame[0], nonin->frame[1], nonin->frame[2], nonin->frame[3], nonin->frame[4]);
		//printf("%i\n", (((nonin->frame[1] & 0xFF) << 8) | (nonin->frame[2])));
		//printf("%i\t%i\t%i\t%i\t%i\t%i\n", nonin->packet[0], nonin->packet[1], nonin->packet[2], nonin->packet[3], nonin->packet[4], nonin->packet[5]);
		//fflush(stdout);

	}

#elif SERIAL_DATA_FORMAT == 7

	// If it is a complete frame and bit7 of Status is 1
	if ((crc & 0xFF) == nonin->frame[4] && (nonin->frame[0] & 0x80) == 0x80) {

		// Shift the last values
		int j;
		for (j = 3; j < NONIN_PACKET_SIZE; j++) {
			nonin->packet[j-3] = nonin->packet[j];
		}
		nonin->packet[NONIN_PACKET_SIZE - 3] = nonin->frame[1];  //MSB
		nonin->packet[NONIN_PACKET_SIZE - 2] = nonin->frame[2];  //LSB

		// Status is OK and bit "SYNC" is 1, it is the frame number 1
		if ((nonin->frame[0] & 0x81) == 0x81) {
			// Final of a packet, update the SpO2 stored value
			if (nonin->numFrames == 25) {
				nonin->packet[NONIN_PACKET_SIZE - 1] = nonin->tmpSpO2;
			}
			nonin->numFrames = 1;

		} else {
			// The SpO2 value is in frame 9, in byte 3
			if (nonin->numFrames == 9) {
				nonin->tmpSpO2 = nonin->frame[3] & 0x7F;
			}
			// Update the number of frames if it is synchronized
			if (nonin->numFrames > 0 && nonin->numFrames < 25) {
				nonin->numFrames++;
			} else {
				nonin->numFrames = 0;
				nonin->tmpSpO2 = 0;
			}
		}

		//printf("%i\t%i\t%i\t%i\t%i\n", nonin->frame[0], nonin->frame[1], nonin->frame[2], nonin->frame[3], nonin->frame[4]);
		//printf("%i\n", (((nonin->frame[1] & 0xFF) << 8) | (nonin->frame[2])));
		//printf("%i\t%i\t%i\t%i\t%i\t%i\n", nonin->packet[0], nonin->packet[1], nonin->packet[2], nonin->packet[3], nonin->packet[4], nonin->packet[5]);
		//fflush(stdout);

	}

#endif

}


/*
 * Read the UART port if contains bytes
 */
void readNonin(noninSensor* nonin) {
	int numBytes = readUART(&(nonin->uartPort));
	if (numBytes) {
		int i;
		for (i = 0; i < numBytes; i++) {
			parseNoninData(nonin, nonin->uartPort.buffer[i]);
		}
	}
}
