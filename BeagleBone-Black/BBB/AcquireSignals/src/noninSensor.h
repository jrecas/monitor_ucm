/*
 * nonin.h
 *
 *  Created on: 10/5/2015
 *      Author: Jose Manuel Bote
 */

#ifndef _NONINSENSOR_
#define _NONINSENSOR_

#include <stdint.h>
#include "uart.h"
#include "boolean.h"

#define UART_DEVICE "/dev/ttyO2"

#define NONIN_NUM_PACKETS 2
#define NONIN_PACKET_SIZE (3*NONIN_NUM_PACKETS)
#define SERIAL_DATA_FORMAT 2

typedef struct {
	uart uartPort;
	uint8_t packet[NONIN_PACKET_SIZE]; // {pleth(MSB), pleth(LSB), SpO2}
	uint8_t frame[5];  // {status, pleth(MSB), pleth(LSB), [variable], chk}
	int numFrames;
	uint8_t tmpSpO2;
} noninSensor;


/*
 * Configure the Nonin sensor for the Beglebone Black (UART2)
 */
int configureNonin(noninSensor* nonin);

/*
 * Control the packets and frames sent by the Nonin sensor
 */
void parseNoninData(noninSensor* nonin, uint8_t byte);

/*
 * Read the UART port if contains bytes
 */
void readNonin(noninSensor* nonin);


#endif
