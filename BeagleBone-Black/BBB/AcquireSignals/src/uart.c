/*
 * uart.c
 *
 *  Created on: 10/5/2015
 *      Author: Jose Manuel Bote
 */

#include "uart.h"

#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>


/*
 * Configure the UART port for the Beglebone Black
 */
int configureUART(uart* uartPort, char* device) {

	/*
	 * Open the UART
	 * Flags (defined in fcntl.h):
	 *     O_RDONLY:   Open for reading only
	 *     O_WRONLY:   Open for writing only
	 *     O_RDWR:     Open for reading and writing
	 *     O_NONBLOCK: Enable nonblocking mode
	 *     O_NDELAY:   Similar to enable nonblocking mode
	 *     O_NOCTTY:   If the file is a terminal, the terminal will not be allocated
	 */
	uartPort->fid = open(device, O_RDONLY | O_NONBLOCK);
	if (uartPort->fid < 0) {
		return -1;
	}

	/*
	 * Configure the UART
	 * Flags (defined in /usr/include/termios.h):
	 *     BaudRate: B1200,    B2400,    B4800,    B9600,    B19200,   B38400,   B57600,
	 *               B115200,  B230400,  B460800,  B500000,  B576000,  B921600,  B1000000,
	 *               B1152000, B1500000, B2000000, B2500000, B3000000, B3500000, B4000000
	 *     CSize:    CS5, CS6, CS7, CS8 (8 bits)
	 *     CLOCAL:   Ignore modem status lines
	 *     CREAD:    Enable receiver
	 *     IGNPAR:   Ignore characters with parity errors
	 *     PARENB:   Parity enable
	 *     PARODD:   Odd parity (else even)
	 */
	uartPort->options.c_cflag = B9600 | CS8 | CLOCAL | CREAD;
	uartPort->options.c_iflag = IGNPAR;
	uartPort->options.c_oflag = 0;  // Raw output
	uartPort->options.c_lflag = 0;

	// Clean the modem line and activate the settings for the port
	tcflush(uartPort->fid, TCIFLUSH);
	tcsetattr(uartPort->fid, TCSANOW, &(uartPort->options));

	return 0;

}


/*
 * Read a byte from the UART port, return the number of read bytes
 */
int readUART(uart* uartPort) {
	int rxLength = read(uartPort->fid, uartPort->buffer, UART_BUFFERSIZE);
	//printf("%i\n", rxLength);
	return rxLength;
}
