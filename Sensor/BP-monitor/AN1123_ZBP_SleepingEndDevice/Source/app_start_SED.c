/*****************************************************************************
 *
 * MODULE:             ZigBee Pro Application Template
 *
 * COMPONENT:          app_start_SED.c
 *
 * AUTHOR:             gpfef
 *
 * DESCRIPTION:
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_sware/Application_Notes/JN-AN-1123-ZBPro-Application-Template/Tags/ZBPRO_APPLICATION_TEMPLATE_1v7_RC5/AN1123_ZBP_SleepingEndDevice/Source/app_start_SED.c $
 *
 * $Revision: 29686 $
 *
 * $LastChangedBy: nxp29741 $
 *
 * $LastChangedDate: 2011-03-02 14:49:57 +0000 (Wed, 02 Mar 2011) $
 *
 * $Id: app_start_SED.c 29686 2011-03-02 14:49:57Z nxp29741 $
 *
 *****************************************************************************
 *
 * This software is owned by Jennic and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on Jennic products. You, and any third parties must reproduce
 * the copyright and warranty notice and any other legend of ownership on each
 * copy or partial copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS". JENNIC MAKES NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
 * ACCURACY OR LACK OF NEGLIGENCE. JENNIC SHALL NOT, IN ANY CIRCUMSTANCES,
 * BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT LIMITED TO, SPECIAL,
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON WHATSOEVER.
 *
 * Copyright Jennic Ltd. 2009 All rights reserved
 *
 ****************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "pwrm.h"
#include "pdm.h"
#include "dbg.h"
#include "dbg_uart.h"
#include "rnd_pub.h"
#include "appapi.h"
#include "wapi.h"
#include "app_timer_driver.h"


#ifdef OVERLAYS_BUILD
#include "ovly.h"
#endif
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_APP
#define TRACE_APP   FALSE
#endif
#ifndef TRACE_OVERLAYS
#define TRACE_OVERLAYS FALSE
#endif
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

PRIVATE void vInitialiseApp(void);
PRIVATE void vUnclaimedInterrupt(void);

#ifdef OVERLAYS_BUILD
PRIVATE void vGrabLock(void);
PRIVATE void vReleaseLock(void);
PRIVATE void vOverlayEvent(OVLY_teEvent eEvent, OVLY_tuEventData *puData);
#endif
/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

extern void *stack_low_water_mark;

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

PRIVATE PWRM_DECLARE_CALLBACK_DESCRIPTOR(PreSleep);
PRIVATE PWRM_DECLARE_CALLBACK_DESCRIPTOR(Wakeup);

// encryption key for PDM
PRIVATE const tsReg128 g_sKey = { 0x97DF1788, 0x0B75E016, 0xAFE6C36D, 0x2545E2DA };
#ifdef OVERLAYS_BUILD
OVLY_tsInitData sInitData;
uint32 u32MutexCount;
PUBLIC bool bOvlyInit = FALSE;
#endif

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

PWRM_CALLBACK(PreSleep)
{
    DBG_vPrintf(TRACE_APP, "APP: Going to sleep (CB)\n");
    vAppApiSaveMacSettings();
#ifdef OVERLAYS_BUILD
    bOvlyInit = FALSE;
#endif
}

PWRM_CALLBACK(Wakeup)
{
    /* Initialise the debug diagnostics module to use UART0 at 115K Baud;
     * Do not use UART 1 if LEDs are used, as it shares DIO with the LEDS
     */
    DBG_vUartInit(DBG_E_UART_0, DBG_E_UART_BAUD_RATE_115200);
    DBG_vPrintf(TRACE_APP, "\n\nAPP: Woken up (CB)\n");

    /* Restore Mac settings (turns radio on) */
    vMAC_RestoreSettings();

    /* Restart JenOS */
    OS_vRestart();

    /* Activate the appropriate application task */
    //OS_eActivateTask(APP_taskSleepingEndDevice);
}


/****************************************************************************
 *
 * NAME: vAppMain
 *
 * DESCRIPTION:
 * Entry point for application from a cold start.
 *
 * RETURNS:
 * Never returns.
 *
 ****************************************************************************/
PUBLIC void vAppMain(void)
{
    //Initialise the debug diagnostics module to use UART0 at 115K Baud;
    DBG_vUartInit(DBG_E_UART_0, DBG_E_UART_BAUD_RATE_115200);
    DBG_vPrintf(TRACE_APP, "\n\nAPP: Power Up\n");

    /*
     * Initialise the stack overflow exception to trigger if the end of the
     * stack is reached. See the linker command file to adjust the allocated
     * stack size.
     */
    vAHI_SetStackOverflow(TRUE, (uint32)&stack_low_water_mark);


    /*
     * Catch resets due to watchdog timer expiry. Comment out to harden code.
     */
    if (bAHI_WatchdogResetEvent())
    {
        DBG_vPrintf(TRACE_APP, "APP: Watchdog timer has reset device!\n");
        vAHI_WatchdogStop();
        while (1);
    }

    //Clock initialization (32MHz)
	vAHI_SelectClockSource(FALSE, FALSE);
	bAHI_SetClockRate(3);

    /* initialise ROM based software modules */
    u32AppApiInit(NULL, NULL, NULL, NULL, NULL, NULL);

    //Light green LED
    wapi_LedOn();

	//Configure Interrupt from button (SW1) in DIO12
	vAHI_DioSetPullup(1<<BUTTON, 0);
	vAHI_DioSetDirection(1<<BUTTON, 0);
	vAHI_DioInterruptEnable(1<<BUTTON, 0);
	vAHI_DioInterruptEdge(0, 1<<BUTTON);
	vAHI_DioSetPullup(1<<BUTTON, 0);

	//Enable the ADS1298
	vAHI_DioSetPullup((1 << AVDD_END), 0);
	vAHI_DioSetDirection(0, (1 << AVDD_END));//AVDD_END output direction
	vAHI_DioSetOutput((1 << AVDD_END), 0); //AVDD_END output high to enable the regulator that provides power to the ADS1298

	//Configure ADC (temporary location of the call)
	ConfigADC();

	//Set single-lead mode
	wapi_set1Lead();

	//Init UART1
	wapi_StartUart1();

	//Disable the I2C (accelerometer)
	vAHI_SiMasterDisable();
	vAHI_DioSetPullup(0, (1 << 14));
	vAHI_DioSetDirection((1 << 14), 0);
	vAHI_DioSetPullup(0, (1 << 15));
	vAHI_DioSetDirection((1 << 15), 0);

	//Clear pending DIO changed bits before starting the OS
	uint32 u32IntSource = u32AHI_DioWakeStatus();
	uint8 u8WakeInt = u8AHI_WakeTimerFiredStatus();

	/* start the RTOS */
	OS_vStart(vInitialiseApp, vUnclaimedInterrupt);

    /* idle task commences here */

    while (TRUE)
    {
        /* Re-load the watch-dog timer. Execution must return through the idle
         * task before the CPU is suspended by the power manager. This ensures
         * that at least one task / ISR has executed with in the watchdog period
         * otherwise the system will be reset.
         */
        vAHI_WatchdogRestart();
        /*
         * suspends CPU operation when the system is idle or puts the device to
         * sleep if there are no activities in progress
         */
        //PWRM_vManagePower();
    }
}

void vAppRegisterPWRMCallbacks(void)
{
    PWRM_vRegisterPreSleepCallback(PreSleep);
    PWRM_vRegisterWakeupCallback(Wakeup);
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************
 *
 * NAME: vInitialiseApp
 *
 * DESCRIPTION:
 * Initialises JenOS modules and application.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vInitialiseApp(void)
{
#ifdef OVERLAYS_BUILD
    sInitData.u32ImageOffset =  0;
    sInitData.prGetMutex = vGrabLock;
    sInitData.prReleaseMutex = vReleaseLock;
    sInitData.prOverlayEvent = &vOverlayEvent;
    OVLY_bInit(&sInitData);
#endif
    /* Initialise the debug diagnostics module to use UART0 at 115K Baud;
     * Do not use UART 1 if LEDs are used, as it shares DIO with the LEDS
     */
    DBG_vUartInit(DBG_E_UART_0, DBG_E_UART_BAUD_RATE_115200);

    /* Initialise JenOS modules. Initialise Power Manager even on non-sleeping nodes
     * as it allows the device to doze when in the idle task
     */
    PWRM_vInit(E_AHI_SLEEP_OSCON_RAMON);

    // Initialise hardware timer
	DBG_vPrintf(TRACE_APP, "Initialise timer driver... \n");
	APP_vTimerDriverInitialise();

	/* start Counter timer */
	DBG_vPrintf(TRACE_APP, "Start APP_tmr_Counter... \n");
	//OS_eStartSWTimer(APP_tmrRestart, APP_TIME_MS(1000), NULL);

    /* Initialise the Persistent Data Manager to use the top 64K sector of
     * an 8-sector 512kB device
     */
    /*
     *  Initialise the PDM, use an application supplied key (g_sKey),
     *  The key value can be set to the desired value here, or the key in eFuse can be used.
     *  To use the key stored in eFuse set the pointer to the key to Null, and remove the
     *  key structure here.
     */
    //PDM_vInit(7, 1, 64 * 1024 , NULL, mutexMEDIA, NULL, &g_sKey);

    /* Initialise Protocol Data Unit Manager */
    //PDUM_vInit();

    /* initialise application */
    //APP_vInitialiseSleepingEndDevice();
}



/****************************************************************************
 *
 * NAME: vUnclaimedInterrupt
 *
 * DESCRIPTION:
 * Catches any unexpected interrupts
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vUnclaimedInterrupt(void)
{
    DBG_vPrintf(TRACE_APP, "Unclaimed Interrupt\n");
    while(1);
}

/****************************************************************************
 *
 * NAME: APP_isrBusErrorException
 *
 * DESCRIPTION:
 * Catches any bus error exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrBusErrorException)
{
    DBG_vPrintf(TRACE_APP, "Bus Error Exception\n");
    DBG_vDumpStack();
    while(1);
}

/****************************************************************************
 *
 * NAME: APP_isrAlignmentException
 *
 * DESCRIPTION:
 * Catches any address alignment exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrAlignmentException)
{
    DBG_vPrintf(TRACE_APP, "Address Alignment Exception\n");
    DBG_vDumpStack();
    while(1);
}

/****************************************************************************
 *
 * NAME: APP_isrIllegalInstructionException
 *
 * DESCRIPTION:
 * Catches any illegal instruction exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrIllegalInstructionException)
{
    DBG_vPrintf(TRACE_APP, "Illegal Instruction Exception\n");
    DBG_vDumpStack();
    while(1);
}

/****************************************************************************
 *
 * NAME: APP_isrStackOverflowException
 *
 * DESCRIPTION:
 * Catches any stack overflow exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrStackOverflowException)
{
    DBG_vPrintf(TRACE_APP, "Stack Overflow Exception\n");
    DBG_vDumpStack();
    while(1);
}

/****************************************************************************
 *
 * NAME: APP_isrUnimplementedModuleException
 *
 * DESCRIPTION:
 * Catches any unimplemented module exceptions.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
OS_ISR(APP_isrUnimplementedModuleException)
{
    DBG_vPrintf(TRACE_APP, "Unimplemented Module Exception\n");
    while(1);
}

#ifdef OVERLAYS_BUILD
/****************************************************************************
 *
 * NAME: vGrabLock
 *
 * DESCRIPTION:
 *  Implements counting semaphore protection (grab routine)
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vGrabLock(void)
{
    if (u32MutexCount == 0)
    {
        OS_eEnterCriticalSection(mutexMEDIA);
    }
    u32MutexCount++;
}


/****************************************************************************
 *
 * NAME: vReleaseLock
 *
 * DESCRIPTION:
 * Implements counting semaphore protection (release routine)
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vReleaseLock(void)
{
    u32MutexCount--;
    if (u32MutexCount == 0)
    {
        OS_eExitCriticalSection(mutexMEDIA);
    }
}

/****************************************************************************
 *
 * NAME: vOverlayEvent
 *
 * DESCRIPTION:
 * Provides debug capability in the overlays.
 *
 * RETURNS:
 * void
 *
 ****************************************************************************/
PRIVATE void vOverlayEvent(OVLY_teEvent eEvent, OVLY_tuEventData *puData)
{
    switch (eEvent)
    {
        case OVLY_E_EVENT_LOAD:
            DBG_vPrintf(TRACE_OVERLAYS, "\nLoad %d %x > %d %x",
                    puData->sLoad.u16ReturnPage,
                    puData->sLoad.u32ReturnAddress,
                    puData->sLoad.u16TargetPage,
                    puData->sLoad.u32TargetAddress);
            break;

        case OVLY_E_EVENT_READ:
            DBG_vPrintf(TRACE_OVERLAYS, "\nRead Off %x %d ",
                    puData->sRead.u32Offset,
                    puData->sRead.u16Length);
            break;

        case OVLY_E_EVENT_INTERRUPTED:
            DBG_vPrintf(TRACE_OVERLAYS, "\nAbort");
            break;

        case OVLY_E_EVENT_ERROR_SIZE:
            DBG_vPrintf(TRACE_OVERLAYS, "\nError Size");
            break;

        case OVLY_E_EVENT_ERROR_INDEX:
            DBG_vPrintf(TRACE_OVERLAYS, "\nError Index");
            break;

        case OVLY_E_EVENT_ERROR_CHECKSUM:
            DBG_vPrintf(TRACE_OVERLAYS, "\nError checksum");
            break;

        case OVLY_E_EVENT_FAILED:
            DBG_vPrintf(TRACE_OVERLAYS, "\nLOAD FAILED");
            while(1);
            break;
    }
}
#endif
/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
