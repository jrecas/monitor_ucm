/*****************************************************************************
 *
 * MODULE:             ZigBee Pro Application Template
 *
 * COMPONENT:          app_syscon.c
 *
 * AUTHOR:             GPfef
 *
 * DESCRIPTION:       Split system controller interrupt into actions for wake timers, DIO and comparator
 *
 * $HeadURL: https://www.collabnet.nxp.com/svn/lprf_sware/Application_Notes/JN-AN-1123-ZBPro-Application-Template/Tags/ZBPRO_APPLICATION_TEMPLATE_1v7_RC5/AN1123_ZBP_SleepingEndDevice/Source/app_syscon.c $
 *
 * $Revision: 27512 $
 *
 * $LastChangedBy: fbhai $
 *
 * $LastChangedDate: 2010-11-09 15:26:01 +0000 (Tue, 09 Nov 2010) $
 *
 * $Id: app_syscon.c 27512 2010-11-09 15:26:01Z fbhai $
 *
 *****************************************************************************
 *
 * This software is owned by Jennic and/or its supplier and is protected
 * under applicable copyright laws. All rights are reserved. We grant You,
 * and any third parties, a license to use this software solely and
 * exclusively on Jennic products. You, and any third parties must reproduce
 * the copyright and warranty notice and any other legend of ownership on each
 * copy or partial copy of the software.
 *
 * THIS SOFTWARE IS PROVIDED "AS IS". JENNIC MAKES NO WARRANTIES, WHETHER
 * EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE,
 * ACCURACY OR LACK OF NEGLIGENCE. JENNIC SHALL NOT, IN ANY CIRCUMSTANCES,
 * BE LIABLE FOR ANY DAMAGES, INCLUDING, BUT NOT LIMITED TO, SPECIAL,
 * INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON WHATSOEVER.
 *
 * Copyright Jennic Ltd. 2009 All rights reserved
 *
 ****************************************************************************/

/****************************************************************************/
/***        Include files                                                 ***/
/****************************************************************************/

#include <jendefs.h>
#include "os.h"
#include "os_gen.h"
#include "DBG.h"
#include "pwrm.h"
#include "AppHardwareApi.h"
#include "wapi.h"
#include "app_timer_driver.h"

#if OVERLAYS_BUILD
#include "ovly.h"
#endif
/****************************************************************************/
/***        Macro Definitions                                             ***/
/****************************************************************************/

#ifndef TRACE_APP_SYSCON
#define TRACE_APP_SYSCON            FALSE
#endif

extern bool bOvlyInit;
/****************************************************************************/
/***        Type Definitions                                              ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Function Prototypes                                     ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Variables                                            ***/
/****************************************************************************/

/****************************************************************************/
/***        Local Variables                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        Exported Functions                                            ***/
/****************************************************************************/

//////////////////////////  jrecas  ///////////////////////////////////
#define NAN       0
#define OXIM           0xAA
#define OXIM_PAQ_LEN    (3+1)
#define uint8_t uint8
#define int8_t  int8
uint8_t spo2Decimate=0;
//#define NONIN_DECIMATE       ( (uint8_t) ECG_PERIOD_MS*PLETH_PERIOD_MS )    //Sample freq for pleth = 12ms
//#define NONIN_DECIMATE       ( (uint8_t) PLETH_PERIOD_MS/ECG_PERIOD_MS )    //Sample freq for pleth = 12ms

#define NONIN_DECIMATE         ( (uint8_t)             12/             4 )    //Sample freq for pleth = 12ms
uint8_t spo2Pkg[OXIM_PAQ_LEN]     = {OXIM,  12,0,0};               //Hedaer, sample period [ms], pleth (8b), spo2(8b)


uint8_t frame[5]={0,0,0,0,0};
uint8_t pleth=NAN;
int8_t  fNumber=-1, spo2=NAN, spo2Tmp=NAN;


//Al recibir un dato se genera una interrupci�n que llamar� a esta funcion
void noninDataInterrupt(uint8_t data){
  uint8_t i, crc=0;
  //We put the new byte at the end of the frame and compute crc
  for(i=0;i<4;i++){
    frame[i]=frame[i+1];
    crc+=frame[i];
  }
  frame[4]=data;                 //Clear interrupt flag & read data
  //frame[4]=RXBUF0;

  //Hemos recibido el �ltimo byte de un frame??
  if((crc%256)==frame[4] && frame[0]==1 && frame[1]>127){
  //if(frame[0]==1 && frame[1]>127){
    pleth=frame[2];
    //Primer frame del paquete (synch=1)
    if((frame[1]&0x81)==0x81){
      //toggleYellowLED();
      //Si estabamos sincronizados y tenemos un SpO2 temporal correcto-> actualizamos SpO2
      if(fNumber==25 && spo2Tmp!=NAN){
        //toggleRedLED();
        spo2=spo2Tmp;
      }
      //Estamos sincronizados
      fNumber=1;
    }
    //No es primera trama
    else{
      //El frame tiene el campo SpO2-D??
      if(fNumber==9)
        spo2Tmp=frame[3];
      //Actualizamos el frame, incrementamos si estamos sincronizados
      if(fNumber>0 && fNumber<25)
        fNumber++;
      else{
        fNumber=-1;
        spo2Tmp=NAN;
      }
    }
    //toggleGreenLED();
  }
}
////////////////////////// (jrecas) ///////////////////////////////////



// The interrupt handler
void vUartISR(uint32 u32DeviceId, uint32 u32ItemBitmap) {
	//DBG_vPrintf(TRUE, "vUartISR!\n");

	if ((u32DeviceId==E_AHI_DEVICE_UART0) && (u32ItemBitmap & (E_AHI_UART_INT_RXDATA | E_AHI_UART_INT_TIMEOUT))) {
		while (u8AHI_UartReadLineStatus(E_AHI_UART_0) & E_AHI_UART_LS_DR) {
			char data = u8AHI_UartReadData(E_AHI_UART_0);
			//DBG_vPrintf(TRACE_APP, "%c", data);
			vAHI_UartWriteData(E_AHI_UART_1, (uint8)data);
		}
		u8AHI_UartReadData(E_AHI_UART_0); // in case no data
	}
	else{
		//DBG_vPrintf(TRACE_APP, "UART1\n");
		if ((u32DeviceId==E_AHI_DEVICE_UART1) && (u32ItemBitmap & (E_AHI_UART_INT_RXDATA | E_AHI_UART_INT_TIMEOUT))) {
			while (u8AHI_UartReadLineStatus(E_AHI_UART_1) & E_AHI_UART_LS_DR) {
				char data = u8AHI_UartReadData(E_AHI_UART_1);
				//DBG_vPrintf(TRACE_APP, "%c", data);
				//////////////////////////  jrecas ////////////////////////////////////
				//jrecas Esto env�a la oximetr�a, lo cambio por mi funcion
				/*OS_eEnterCriticalSection(mutexUart);
				vAHI_UartWriteData(E_AHI_UART_0, (uint8)(0xDB));
				vAHI_UartWriteData(E_AHI_UART_0, (uint8)data);
				OS_eExitCriticalSection(mutexUart);*/
				noninDataInterrupt(data);
				////////////////////////// (jrecas) ///////////////////////////////////
			}
			u8AHI_UartReadData(E_AHI_UART_1); // in case no data
		}
		//DBG_vPrintf(TRACE_APP, "End UART1\n");
	}
}

OS_ISR(APP_UART1) {
	uint8 u8IntStatus;

	u8IntStatus = u8AHI_UartReadInterruptStatus(E_AHI_UART_1);
	//DBG_vPrintf(TRACE_APP, "1: %02x\n", u8IntStatus);
	vUartISR(E_AHI_DEVICE_UART1, ((u8IntStatus >> 1) & 0x7));
}

OS_TASK(APP_taskGprsSend){
	uint8 dumbByte;

	OS_eCollectMessage(APP_gprsSend, (void*)&dumbByte);
}

/****************************************************************************
 *
 * NAME: APP_SysConISR
 *
 * DESCRIPTION:
 * Interrupt
 *
 ****************************************************************************/

OS_ISR(APP_isrSysCon) {
#if OVERLAYS_BUILD
	if (!bOvlyInit) {
		OVLY_vReInit();
		bOvlyInit = TRUE;
	}
#endif
	//Clear pending DIO changed bits by reading register
	uint32 u32IntSource = u32AHI_DioWakeStatus();
	uint8 u8WakeInt = u8AHI_WakeTimerFiredStatus();

	//DBG_vPrintf(TRACE_APP,  "u32IntSource == %d; u8WakeInt == %d, E_AHI_DIO8_INT = %d\n", u32IntSource, u8WakeInt, E_AHI_DIO8_INT);

	if (u8WakeInt & E_AHI_WAKE_TIMER_MASK_1) {
		//Wake timer interrupt got us here
		DBG_vPrintf(TRACE_APP_SYSCON, "APP: Wake Timer 1 Interrupt\n");
		PWRM_vWakeInterruptCallback();
	}

	if (u8WakeInt & E_AHI_WAKE_TIMER_MASK_0) {
		DBG_vPrintf(TRACE_APP_SYSCON, "APP: Wake Timer 0 Interrupt\n");
	}

	if(u32IntSource & E_AHI_DIO12_INT) {
		DBG_vPrintf(TRACE_APP, "Button\n");
	}

	if (u32IntSource & E_AHI_DIO8_INT) {
		int i = 0;
		int32 stat_new;
		int32 ch[8];

		wapi_StartSpiADC();
		SendCommandADC(0x12);
		vAHI_SpiContinuous(TRUE, 23);
		while (bAHI_SpiPollBusy());
		stat_new = u32AHI_SpiReadTransfer32();

		for (i = 0; i < 3; i++) {
			ch[i] = 0;
			vAHI_SpiContinuous(TRUE, 23);//FRAN: check this... what is the meaning of the arguments?
			while (bAHI_SpiPollBusy());
			ch[i] = u32AHI_SpiReadTransfer32();//FRAN: check this... why I read 32 bits instead of 24?
		}

		wapi_StopSpiADC();

		//Send ECG signal (ch[1]) through the UART (critical section)
		//////////////////////////  jrecas  ///////////////////////////////////
		//jrecas Aqu� se env�an los datos de ecg
		OS_eEnterCriticalSection(mutexUart);
		vAHI_UartWriteData(E_AHI_UART_0, (uint8)(0xDA));
		//vAHI_UartWriteData(E_AHI_UART_0, (uint8)((ch[1]>>24) & 0xFF)); esto no vale para nada
		vAHI_UartWriteData(E_AHI_UART_0, (uint8)((ch[1]>>16) & 0xFF));
		vAHI_UartWriteData(E_AHI_UART_0, (uint8)((ch[1]>>8) & 0xFF));
		//vAHI_UartWriteData(E_AHI_UART_0, (uint8)(ch[1] & 0xFF));     Fran divide por 2 para eveitar desbordamientos en el filtro, veamos si dividimos por 256
		//Send SpO2 from Nonin every NONIN_DECIMATE
		if(!spo2Decimate) {
			spo2Pkg[2]=pleth;
			spo2Pkg[3]=spo2;
			for(i=0; i<OXIM_PAQ_LEN; i++)
			    vAHI_UartWriteData(E_AHI_UART_0, spo2Pkg[i]);
				//sendCharBT(spo2Pkg[i]);
		}
		OS_eExitCriticalSection(mutexUart);
		spo2Decimate=(spo2Decimate+1)%NONIN_DECIMATE;

		////////////////////////// (jrecas) ///////////////////////////////////
	}
}

/****************************************************************************/
/***        Local Functions                                               ***/
/****************************************************************************/

/****************************************************************************/
/***        END OF FILE                                                   ***/
/****************************************************************************/
