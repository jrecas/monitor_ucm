/*
 * wapi.h
 *
 *  Created on: 1-dec-2010
 *      Author: Giacomo Paci
 */

#ifndef WAPI_H_
#define WAPI_H_

#ifdef LOG_UART_TO_RAM
#define MAX_BUFFER_BYTE 10000


#define write_to_uart_buffer(x) \
	do{											\
		uart0ToRamBuff[Ram_Buff_index++]=x;			\
		if(Ram_Buff_index>=MAX_BUFFER_BYTE){		\
			Ram_Buff_index=0;					\
		}										\
	} while(0)
#else
#define write_to_uart_buffer(x) \
	do{											\
	} while(0)
#endif

#ifdef LOG_DATA_TO_RAM
#define MAX_BUFFER_BYTE_DATA 2000

#define write_to_data_buffer(x) \
	do{											\
		dataToRamBuff[Ram_Buff_index_data++]=x;			\
		if(Ram_Buff_index_data>=MAX_BUFFER_BYTE_DATA){		\
			Ram_Buff_index_data=0;					\
		}										\
	} while(0)
#else
#define write_to_uart_buffer(x) \
	do{											\
	} while(0)
#endif

//DIO pins
#define START_ADC 3
#define DRDY_ADC 8
#define RESET_ADC 9
#define PWDN_ADC 10
#define AVDD_END 11
#define BUTTON 12

//ADS1298
PUBLIC void wapi_StartSpiADC(void);
PUBLIC void wapi_StopSpiADC(void);
uint8 ReadRegADC(uint8 reg);
void WriteRegADC(uint8 reg, uint8 value);
void SendCommandADC(uint8 data);
void ConfigADC();

//SD card
PUBLIC void wapi_StartSpiSD(void);
PUBLIC void wapi_StopSpiSD(void);
PUBLIC uint32 wapi_TransferSpiSD(uint8 charlength, uint32 outdata);

//LED
PUBLIC void wapi_LedOn(void);
PUBLIC void wapi_LedOff(void);
PUBLIC void wapi_LedToggle(void);

//uart0 api
PUBLIC void wapi_StartUart0(void);
PUBLIC void wapi_StopUart0(void);
PUBLIC bool_t wapi_sendUart0string(char *in_string, uint32 in_length);
bool_t wapi_sendUart0char(char inchar);

//uart1 api
PUBLIC void wapi_StartUart1(void);
PUBLIC void wapi_StopUart1(void);
PUBLIC bool_t wapi_sendUart1string(char *in_string, uint32 in_length);
bool_t wapi_sendUart1char(char inchar);

//1- or 3-lead mode
PUBLIC void wapi_set1Lead(void);
PUBLIC void wapi_set3Lead(void);

#endif /* WAPI_H_ */
