#include <jendefs.h>
#include <AppHardwareApi.h>
#include "wapi.h"
#include "os.h"
#include "os_gen.h"
#include "dbg.h"
#include "dbg_uart.h"

volatile uint8 gpios = 0x80;//Stores the status of the GPIO pins of the ADS1298

//ADS1298 control
//Enable the SPI
void wapi_StartSpiADC(void) {
	vAHI_DioSetPullup(0, 1);
	vAHI_DioSetOutput(0, 1);
	vAHI_DioSetDirection(0, 1);

	vAHI_SpiConfigure(0, FALSE, FALSE, TRUE, 2, FALSE, FALSE);
	vAHI_SpiSelect(0);

}

//Disable the SPI
void wapi_StopSpiADC(void) {
	vAHI_DioSetPullup(0, 1);
	vAHI_DioSetOutput(1, 0);
	vAHI_DioSetDirection(0, 1);

	vAHI_SpiStop();
}

//Write register
void WriteRegADC(uint8 reg, uint8 value){
		uint16 write_command = 0x0;

		DBG_vPrintf(TRACE_APP,  "Writing Register....");
		write_command = 0x4000 | (reg<<8);
		vAHI_SpiStartTransfer(15, write_command);
		vAHI_SpiWaitBusy();
		vAHI_SpiStartTransfer(7, value);
		vAHI_SpiWaitBusy();
		DBG_vPrintf(TRACE_APP,  "Done\n");
}

//Read register
uint8 ReadRegADC(uint8 reg){
		uint16 read_command = 0x0;
		uint32 data_read = 0x0;
		volatile uint32 k;

		DBG_vPrintf(TRACE_APP,  "Reading Register....");
		read_command = 0x2000 | (reg<<8);
		vAHI_SpiStartTransfer(15, read_command);
		vAHI_SpiWaitBusy();
		vAHI_SpiStartTransfer(7, 0x0);
		vAHI_SpiWaitBusy();
		for(k=0;k<10000; k++);
		data_read = u32AHI_SpiReadTransfer32();
		DBG_vPrintf(TRACE_APP,  "Read data = %02x\r\n", data_read);

		return data_read;
}

//Send command
void SendCommandADC(uint8 command){
	vAHI_SpiStartTransfer(7, command);
	vAHI_SpiWaitBusy();
}

void ConfigADC() {
	uint32 data_read = 0x0;
	volatile uint32 k;

	wapi_StartSpiADC();

	vAHI_DioSetPullup(1<<START_ADC, 0);
	vAHI_DioSetOutput(0, 1<<START_ADC);
	vAHI_DioSetDirection(0, 1<<START_ADC);

	//Configure interrupt from ADC
	vAHI_DioSetPullup(1<<DRDY_ADC, 0);
	vAHI_DioSetDirection(1<<DRDY_ADC, 0);
	vAHI_DioInterruptEnable(1<<DRDY_ADC, 0);
	vAHI_DioInterruptEdge(1<<DRDY_ADC, 0);
	vAHI_DioSetPullup(1<<DRDY_ADC, 0);

	vAHI_DioSetPullup(1<<PWDN_ADC, 0);
	vAHI_DioSetOutput(1<<PWDN_ADC, 0);
	vAHI_DioSetDirection(0, 1<<PWDN_ADC);

	for(k=0;k<(uint32)100000; k++);

	vAHI_DioSetPullup(1<<RESET_ADC, 0);
	vAHI_DioSetOutput(1<<RESET_ADC, 0);
	vAHI_DioSetDirection(0, 1<<RESET_ADC);

	vAHI_DioSetPullup(1<<RESET_ADC, 0);
	vAHI_DioSetOutput(0, 1<<RESET_ADC);
	vAHI_DioSetDirection(0, 1<<RESET_ADC);

	for(k=0;k<(uint32)100000; k++);

	vAHI_DioSetPullup(1<<RESET_ADC, 0);
	vAHI_DioSetOutput(1<<RESET_ADC, 0);
	vAHI_DioSetDirection(0, 1<<RESET_ADC);

	for(k=0;k<(uint32)100000; k++);

	SendCommandADC(0x0A); //STOP

	SendCommandADC(0x11); //SDATAC

	//With this register, the sampling frequency is set:
	//For low power mode:
	//0x00=16kHz, 0x01=8kHz, 0x02=4kHz, 0x03=2kHz, 0x04=1kHz, 0x05=500Hz, 0x06=250Hz
	WriteRegADC(0x1, 0x06); //jRecas tocar aqu�!!
	WriteRegADC(0x2, 0x10);
	WriteRegADC(0x3, 0xcc);
	//WriteRegADC(0x4, 0x00);//Lead-off detection disabled
	WriteRegADC(0x4, 0x03);//Lead-off detection enabled
	WriteRegADC(0x5, 0x00);//Gain=6
	WriteRegADC(0x6, 0x00);//Gain=6
	WriteRegADC(0x7, 0x00);//Gain=6
	WriteRegADC(0x8, 0x00);//Gain=6
	WriteRegADC(0x9, 0x00);//Gain=6
	WriteRegADC(0xA, 0x00);//Gain=6
	WriteRegADC(0xB, 0x00);//Gain=6
	WriteRegADC(0xC, 0x00);//Gain=6
	WriteRegADC(0xD, 0x00);
	WriteRegADC(0xE, 0x00);
	//WriteRegADC(0xF, 0x00);//Lead-off detection disabled
	WriteRegADC(0xF, 0x06);//Lead-off detection enabled
	//WriteRegADC(0x10, 0x00);//Lead-off detection disabled
	WriteRegADC(0x10, 0x06);//Lead-off detection enabled
	WriteRegADC(0x11, 0x00);
	WriteRegADC(0x12, 0x00);
	WriteRegADC(0x13, 0x00);
	WriteRegADC(0x14, gpios);
	WriteRegADC(0x15, 0x00);
	WriteRegADC(0x16, 0x00);
	//WriteRegADC(0x17, 0x00);//Lead-off detection disabled
	WriteRegADC(0x17, 0x02);//Lead-off detection enabled

	data_read = ReadRegADC(0x1);
	data_read = ReadRegADC(0x2);
	data_read = ReadRegADC(0x3);
	data_read = ReadRegADC(0x4);
	data_read = ReadRegADC(0x5);
	data_read = ReadRegADC(0x6);

	SendCommandADC(0x10); //RDATAC
	SendCommandADC(0x08); //START

	wapi_StopSpiADC();
}

//SD card control
//Enable SPI
PUBLIC void wapi_StartSpiSD(void) {
	vAHI_DioSetPullup(0, 2);
	vAHI_DioSetOutput(0, 2);
	vAHI_DioSetDirection(0, 2);
	vAHI_SpiConfigure(0, FALSE, FALSE, FALSE, 0, FALSE, FALSE); //sd-card at 16Mbps
	//vAHI_SpiConfigure(0, FALSE, FALSE, FALSE, 1, FALSE, FALSE); //sd-card at 8Mbps
	vAHI_SpiSelect(0);
}

//Disable SPI
PUBLIC void wapi_StopSpiSD(void) {
	vAHI_DioSetPullup(0, 2);// Disable DIO0 pull-up
	vAHI_DioSetOutput(2, 0);// DIO0 output direction
	vAHI_DioSetDirection(0, 2);// DIO0 output low --> chip select activated

	vAHI_SpiStop();
}

//Transfer data to the SD card
PUBLIC uint32 wapi_TransferSpiSD(uint8 charlength, uint32 outdata) {
	volatile uint32 i, indata;
	wapi_StartSpiSD();
	for (i = 0; i < 32; i++)
		;
	vAHI_SpiStartTransfer(charlength, outdata);
	vAHI_SpiWaitBusy();
	indata = u32AHI_SpiReadTransfer32();
	for (i = 0; i < 32; i++)
		;
	wapi_StopSpiSD();
	return indata;
}

//API for UART0 control
//Enable Uart0, resetting all the data inside the receiving and sending fifo
PUBLIC void wapi_StartUart0(void) {
	//    vAHI_DioSetPullup(0, (1<<4));  // CTS
	//    vAHI_DioSetPullup(0, (1<<5));  // RTS
	vAHI_DioSetPullup(0, (1 << 6));
	vAHI_DioSetPullup(0, (1 << 7));
	vAHI_UartSetRTSCTS(E_AHI_UART_0, FALSE); // two line uart
	//	vAHI_UartSetRTSCTS(E_AHI_UART_0,TRUE);  // four line uart
	vAHI_UartEnable(E_AHI_UART_0);
	vAHI_UartReset(E_AHI_UART_0, TRUE, TRUE);// This function resets the Transmit and Receive Buffer FIFOs of the specified UART.


	vAHI_UartSetInterrupt(E_AHI_UART_0, FALSE, FALSE, FALSE, FALSE,
			E_AHI_UART_FIFO_LEVEL_1);//FRAN: at the beginning (streaming) I disable the interrupts

	//  Qui imposto l'interrupt dell'UART che si genera quando viene ricevuto un byte

	vAHI_UartSetControl(E_AHI_UART_0, E_AHI_UART_EVEN_PARITY,
			E_AHI_UART_PARITY_DISABLE, E_AHI_UART_WORD_LEN_8,
			E_AHI_UART_1_STOP_BIT, E_AHI_UART_RTS_LOW);

	vAHI_UartSetBaudRate(E_AHI_UART_0,E_AHI_UART_RATE_115200); //set transmission to 115200Kbps
	//vAHI_UartSetBaudDivisor(E_AHI_UART_0, 1); //set transmission to 1Mbps
	//vAHI_UartSetClocksPerBit(E_AHI_UART_0, 15); //set transmission to 1Mbps
	//vAHI_UartSetClocksPerBit(E_AHI_UART_0, 7); //set transmission to 2Mbps
	//	vAHI_UartSetAutoFlowCtrl(E_AHI_UART_0,10,FALSE,TRUE,TRUE); // four line uart CTS RTS automatic control
}

// Enable Uart1, resetting all the data inside the receiving and sending fifo
PUBLIC void wapi_StartUart1(void) {
	vAHI_DioSetPullup(0, (1 << 19));
	vAHI_DioSetPullup(0, (1 << 20));
	vAHI_UartSetRTSCTS(E_AHI_UART_1,FALSE); // two line uart
	vAHI_UartEnable(E_AHI_UART_1);
	vAHI_UartReset(E_AHI_UART_1, TRUE, TRUE);// This function resets the Transmit and Receive Buffer FIFOs of the specified UART.

	vAHI_UartSetInterrupt(E_AHI_UART_1, FALSE, FALSE, FALSE, TRUE,	E_AHI_UART_FIFO_LEVEL_1);

	vAHI_UartSetControl(E_AHI_UART_1, E_AHI_UART_EVEN_PARITY,
			E_AHI_UART_PARITY_DISABLE, E_AHI_UART_WORD_LEN_8,
			E_AHI_UART_1_STOP_BIT, E_AHI_UART_RTS_LOW);

	//set transmission to 115200bps
	vAHI_UartSetBaudRate(E_AHI_UART_1,E_AHI_UART_RATE_9600);
}

// Disable Uart0, resetting all the data inside the receiving and sending fifo
PUBLIC void wapi_StopUart0(void) {
	vAHI_UartDisable(E_AHI_UART_0);
}

// Disable Uart1, resetting all the data inside the receiving and sending fifo
PUBLIC void wapi_StopUart1(void) {
	vAHI_UartDisable(E_AHI_UART_1);
}

// send string to Uart0
PUBLIC bool_t wapi_sendUart0string(char *in_string, uint32 in_length) {
	uint32 char_count = 0;
	volatile uint32 i;
	uint8 txfifo_check;
	bool_t check = TRUE;
	bool_t right_going = TRUE;
#ifdef BLUETOOTH
	extern bool_t bt_cnx_on;
	if(bt_cnx_on) {
		wapi_sendUart1string(in_string, in_length);
	}
#endif
	while ((char_count < in_length) && right_going) {
		txfifo_check = u8AHI_UartReadTxFifoLevel(E_AHI_UART_0);
		if (txfifo_check < 15) {
			write_to_uart_buffer(in_string[char_count]);
			vAHI_UartWriteData(E_AHI_UART_0, in_string[char_count]);
			check = TRUE;
			char_count++;
		} else {
			right_going = FALSE;
			if (check == TRUE) {
				for (i = 0; (i < 1000) && (u8AHI_UartReadTxFifoLevel(
						E_AHI_UART_0) > 0); i++)
					;
				right_going = TRUE;
				check = FALSE;
			}
		}
	}
	//wait until all data have delivered or time out
	char_count = 0;
	while ((txfifo_check = u8AHI_UartReadTxFifoLevel(E_AHI_UART_0)) > 0
			&& (char_count < 100)) {
		for (i = 0; (i < 1000) && (u8AHI_UartReadTxFifoLevel(E_AHI_UART_0) > 0); i++)
			;
		char_count++;
	}
	return right_going;
}

// send string to Uart1
PUBLIC bool_t wapi_sendUart1string(char *in_string, uint32 in_length) {
	uint32 char_count = 0;
	volatile uint32 i;
	uint8 txfifo_check;
	bool_t check = TRUE;
	bool_t right_going = TRUE;
	while ((char_count < in_length) && right_going) {
		txfifo_check = u8AHI_UartReadTxFifoLevel(E_AHI_UART_1);
		if (txfifo_check < 15) {
			vAHI_UartWriteData(E_AHI_UART_1, in_string[char_count]);
			check = TRUE;
			char_count++;
		} else {
			right_going = FALSE;
			if (check == TRUE) {
				for (i = 0; (i < 1000) && (u8AHI_UartReadTxFifoLevel(
						E_AHI_UART_1) > 0); i++)
					;
				right_going = TRUE;
				check = FALSE;
			}
		}
	}
	//wait until all data have delivered or time out
	char_count = 0;
	while ((txfifo_check = u8AHI_UartReadTxFifoLevel(E_AHI_UART_1)) > 0
			&& (char_count < 100)) {
		for (i = 0; (i < 1000) && (u8AHI_UartReadTxFifoLevel(E_AHI_UART_1) > 0); i++)
			;
		char_count++;
	}
	return right_going;
}

// send string to Uart0
PUBLIC bool_t wapi_sendUart0char(char inchar) {
	volatile uint32 i;
#ifdef BLUETOOTH
	extern bool_t bt_cnx_on;
	if(bt_cnx_on) {
		wapi_sendUart1char(inchar);
	}
#endif
	for (i = 0; (i < 1000) && (u8AHI_UartReadTxFifoLevel(E_AHI_UART_0) > 14); i++)
		;
	if (u8AHI_UartReadTxFifoLevel(E_AHI_UART_0) < 15) {
		//		write_to_uart_buffer(inchar);
		vAHI_UartWriteData(E_AHI_UART_0, inchar);
		return TRUE;
	}
	return FALSE;
}

// send string to Uart1
PUBLIC bool_t wapi_sendUart1char(char inchar) {
	volatile uint32 i;
	for (i = 0; (i < 1000) && (u8AHI_UartReadTxFifoLevel(E_AHI_UART_1) > 14); i++)
		;
	if (u8AHI_UartReadTxFifoLevel(E_AHI_UART_1) < 15) {
		write_to_uart_buffer(inchar);
		vAHI_UartWriteData(E_AHI_UART_1, inchar);
		return TRUE;
	}
	return FALSE;
}

//LED controls (in this version of the board the LED is connected to GPIO1 (ADS1298)
//LED on
PUBLIC void wapi_LedOn(void) {
	wapi_StartSpiADC();
	gpios &= 0xEF;
	WriteRegADC(0x14, gpios);
	wapi_StopSpiADC();
}

//LED off
PUBLIC void wapi_LedOff(void) {
	wapi_StartSpiADC();
	gpios |= 0x10;
	WriteRegADC(0x14, gpios);
	wapi_StopSpiADC();
}

//LED toggle
PUBLIC void wapi_LedToggle(void) {
	wapi_StartSpiADC();
	if(gpios & 0x10){
		gpios &= 0xEF;
	}
	else{
		gpios |= 0x10;
	}
	WriteRegADC(0x14, gpios);
	wapi_StopSpiADC();
}

//1- or 3-lead mode
//Set single-lead operation
PUBLIC void wapi_set1Lead(void){
	wapi_StartSpiADC();
	gpios |= 0x80;
	WriteRegADC(0x14, gpios);
	wapi_StopSpiADC();
}

//Set 3-lead operation
PUBLIC void wapi_set3Lead(void){
	wapi_StartSpiADC();
	gpios &= 0x7F;
	WriteRegADC(0x14, gpios);
	wapi_StopSpiADC();
}
