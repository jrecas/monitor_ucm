/*
 * DelineatorECG.h
 *
 *  Created on: 30/03/2015
 *      Author: Jose Manuel Bote
 */

#ifndef SRC_DELINEATORECG_H_
#define SRC_DELINEATORECG_H_

#include <stdint.h>

typedef uint16_t boolean;
#define TRUE                  1
#define FALSE                 0

#ifdef FULLMODE
	#define F_SAMPLE          250                         // Sampling rate (Hz)
#else
	#define F_SAMPLE          50                          // Sampling rate (Hz)
#endif

#define GROUP_FILT_DELAY      122                         // Delay of the filter group

#define BUFF_LEN              1024                        // The length of the buffers (2^n and covering more or less 4 seconds)
#define BUFF_HALF_LEN         (BUFF_LEN / 2)              // Half of the length of the buffers
#define MASK_BUFF_LEN         (BUFF_LEN - 1)              // Mask of 1's to make the buffers as circular
#define MASK_BUFF_HALF_LEN    (BUFF_HALF_LEN - 1)         // Mask of 1's to make the half of the buffers as circular
#define FIRST_HALF_BUFF       0                           // First half of the buffer
#define SECOND_HALF_BUFF      1                           // Second half of the buffer
#define FIRST_HALF_BUFF_COMPLETED   (BUFF_HALF_LEN + GROUP_FILT_DELAY - 1)  // First half of the buffer completed
#define SECOND_HALF_BUFF_COMPLETED  (GROUP_FILT_DELAY - 1)  // Second half of the buffer completed

#define NUM_POINTS            8                           // P (on, peak, end), Q peak, R peak, S peak, T (peak end)
#define MIN_RR_PEAKS          (F_SAMPLE / 4)              // Minimum distance between R peaks allowed (F_SAMPLE/4 == 240 bpm)
#define MAX_RR_PEAKS          (BUFF_HALF_LEN - GROUP_FILT_DELAY)  // Maximum distance between R peaks allowed (now 38.5 bpm)
#define NUM_WIN_R_TH          3                           // Number of windows - 1 used to calculate the threshold to detect R peaks
#define DESP_WIN_R_TH         2                           // Shift bits to calculate the threshold to detect R peaks

#define T_Q_WAVE              ((F_SAMPLE * 100) / 1000)   // Maximum period allowed (in num of samples) for a Q wave (100 ms)
#define T_S_WAVE              ((F_SAMPLE * 100) / 1000)   // Maximum period allowed (in num of samples) for a S wave (100 ms)
#define	CORRECT_Q_ON          ((F_SAMPLE * 5) / 1000)     // Correction of 20 ms for the Q on
#define	CORRECT_S_END         ((F_SAMPLE * 5) / 1000)     // Correction of 20 ms for the S end

#define WINDOW_P_ON           50                          // Inicio de la busqueda de la onda P (respecto de la posicion del pico R) 200 ms
#define WINDOW_P_OFF          25                          // Fin de la busqueda de la onda P (respecto de la posicion del pico R) 100 ms
#define WINDOW_T_ON           50                          // Inicio de la busqueda de la onda T (respecto de la posicion del pico R) 200 ms
#define WINDOW_T_OFF          100                         // Fin de la busqueda de la onda T (respecto de la posicion del pico R) 400 ms

typedef enum {
	FULL_MODE,
	SAVE_MODE
} delineationMode;

extern uint32_t ECGPoints[NUM_POINTS];
extern delineationMode mode;

boolean storeSamples(int16_t FIRSamples[2], int16_t BLSample, int16_t openingPSample, int16_t openingTSample);
void preProccess(uint16_t halfBuffer);
void delineateECGWaves(uint16_t startPos, uint16_t endPos);
void delineateSTWaves(uint16_t startPos, uint16_t endPos, uint16_t overflow);
void delineatePQWaves(uint16_t startPos, uint16_t endPos, uint16_t overflow);


#endif /* SRC_DELINEATORECG_H_ */
