/*
 * FIRFilter.h
 *
 *  Created on: 11/04/2015
 *      Author: Jose Manuel Bote
 */

#ifndef SRC_FIRFILTER_H_
#define SRC_FIRFILTER_H_

#include <stdint.h>

#if defined(FIR20)
	#define FIR_ORDER  20  // The order of the filter
#elif defined(FIR16)
	#define FIR_ORDER  16  // The order of the filter
#elif defined(FIR10)
	#define FIR_ORDER  10  // The order of the filter
#else
	#define FIR_ORDER  40  // The order of the filter
#endif

#define FIR_FILTER_DELAY  (FIR_ORDER / 2)  // The delay of the filter


void filterSampleFIR(int16_t sample, int16_t FIR_filt_samples[2]);


#endif /* SRC_FIRFILTER_H_ */
