/*
 * BaselineFilter.h
 *
 *  Created on: 30/03/2015
 *      Author: Jose Manuel Bote
 */

#ifndef SRC_BASELINEFILTER_H_
#define SRC_BASELINEFILTER_H_

#include <stdint.h>

#define BUFF_OPEN_LEN           49                           // The length of the opening window (always odd)
#define BUFF_CLOSE_LEN          75                           // The length of the closing window (always odd)
#define BUFF_OPEN_CENTER        (BUFF_OPEN_LEN / 2)          // The center of the opening window
#define BUFF_CLOSE_CENTER       (BUFF_CLOSE_LEN / 2)         // The center of the closing window
#define BASELINE_FILTER_DELAY   (2 * BUFF_OPEN_CENTER + 2 * BUFF_CLOSE_CENTER)  // The global delay of the filter

#define BUFF_OPEN_P_LEN         29                           // The length of the opening window for P-on/end (always odd)
#define OPENING_P_DELAY        ((2 * BUFF_OPEN_P_LEN) / 2)

#define BUFF_OPEN_T_LEN         63                           // The length of the opening window for T-on/end (always odd)
#define OPENING_T_DELAY         ((2 * BUFF_OPEN_T_LEN) / 2)


int16_t filterSampleBL(int16_t sample);
int16_t openingP(int16_t sample);
int16_t openingT(int16_t sample);
void erosion(int16_t sample, int16_t buffer[], uint16_t bufferSize, uint16_t bufferIndex, int16_t *minVal, uint16_t *minPos);
void dilation(int16_t sample, int16_t buffer[], uint16_t bufferSize, uint16_t bufferIndex, int16_t *maxVal, uint16_t *maxPos);


#endif /* SRC_BASELINEFILTER_H_ */
