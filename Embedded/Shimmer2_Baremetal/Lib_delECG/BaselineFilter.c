/*
 * Baselinefilter.c
 *
 *  Created on: 30/03/2015
 *      Author: Jose Manuel Bote
 */

#include "BaselineFilter.h"
#include "Utils.h"


#ifdef BASELINE
// Baseline wander
int16_t  buffOE[BUFF_OPEN_LEN]  = {0};  // OE: Opening Erosion
int16_t  buffOD[BUFF_OPEN_LEN]  = {0};  // OD: Opening Dilation
int16_t  buffCD[BUFF_CLOSE_LEN] = {0};  // CD: Closing Dilation
int16_t  buffCE[BUFF_CLOSE_LEN] = {0};  // CE: Closing Erosion

uint16_t buffIndexOpen  = 0;
uint16_t buffIndexClose = 0;

int16_t  minValOE = INT16_MAX;
int16_t  maxValOD = INT16_MIN;
int16_t  maxValCD = INT16_MIN;
int16_t  minValCE = INT16_MAX;

uint16_t minPosOE = 0;
uint16_t maxPosOD = 0;
uint16_t maxPosCD = 0;
uint16_t minPosCE = 0;
#endif

#ifdef OP_P
// Opening for P wave
int16_t  buffOE_P[BUFF_OPEN_P_LEN] = {0};  // OE: Opening Erosion
int16_t  buffOD_P[BUFF_OPEN_P_LEN] = {0};  // OD: Opening Dilation

uint16_t buffIndexOpen_P = 0;

int16_t  minValOE_P = INT16_MAX;
int16_t  maxValOD_P = INT16_MIN;

uint16_t minPosOE_P = 0;
uint16_t maxPosOD_P = 0;
#endif

#ifdef OP_T
// Opening for T wave
int16_t  buffOE_T[BUFF_OPEN_T_LEN] = {0};  // OE: Opening Erosion
int16_t  buffOD_T[BUFF_OPEN_T_LEN] = {0};  // OD: Opening Dilation

uint16_t buffIndexOpen_T = 0;

int16_t  minValOE_T = INT16_MAX;
int16_t  maxValOD_T = INT16_MIN;

uint16_t minPosOE_T = 0;
uint16_t maxPosOD_T = 0;
#endif



#ifdef BASELINE
/*
 * Baseline wander using opening and closing. Both are made with erosion and dilation.
 * @param sample     Sample to be filtered
 * @return minValCE  Filtered sample
 */
int16_t filterSampleBL(int16_t sample) {

	erosion(sample, buffOE, BUFF_OPEN_LEN, buffIndexOpen, &minValOE, &minPosOE);
	dilation(minValOE, buffOD, BUFF_OPEN_LEN, buffIndexOpen, &maxValOD, &maxPosOD);
	dilation(maxValOD, buffCD, BUFF_CLOSE_LEN, buffIndexClose, &maxValCD, &maxPosCD);
	erosion(maxValCD, buffCE, BUFF_CLOSE_LEN, buffIndexClose, &minValCE, &minPosCE);

	buffIndexOpen++;
	if (buffIndexOpen == BUFF_OPEN_LEN) {
		buffIndexOpen = 0;
	}

	buffIndexClose++;
	if (buffIndexClose == BUFF_CLOSE_LEN) {
		buffIndexClose = 0;
	}

	return minValCE;

}
#endif


#ifdef OP_P
/*
 * Opnening for P wave.
 * @param sample       New sample to be filtered
 * @return maxValOD_P  Filtered sample
 */
int16_t openingP(int16_t sample) {

	erosion(sample, buffOE_P, BUFF_OPEN_P_LEN, buffIndexOpen_P, &minValOE_P, &minPosOE_P);
	dilation(minValOE_P, buffOD_P, BUFF_OPEN_P_LEN, buffIndexOpen_P, &maxValOD_P, &maxPosOD_P);

	buffIndexOpen_P++;
	if (buffIndexOpen_P == BUFF_OPEN_P_LEN) {
		buffIndexOpen_P = 0;
	}

	return maxValOD_P;

}
#endif


#ifdef OP_T
/*
 * Opnening for T wave.
 * @param sample       New sample to be filtered
 * @return maxValOD_T  Filtered sample
 */
int16_t openingT(int16_t sample) {

	erosion(sample, buffOE_T, BUFF_OPEN_T_LEN, buffIndexOpen_T, &minValOE_T, &minPosOE_T);
	dilation(minValOE_T, buffOD_T, BUFF_OPEN_T_LEN, buffIndexOpen_T, &maxValOD_T, &maxPosOD_T);

	buffIndexOpen_T++;
	if (buffIndexOpen_T == BUFF_OPEN_T_LEN) {
		buffIndexOpen_T = 0;
	}

	return maxValOD_T;

}
#endif


/*
 * Erosion.
 * @param sample       New sample to be filtered
 * @param buffer       Buffer with samples
 * @param bufferSize   The size of the buffer
 * @param bufferIndex  Index of a sample in the buffer
 * @param *minVal      Minimum value in the buffer
 * @param *minPos      Position of the minimum value in the buffer
 */
void erosion(int16_t sample, int16_t buffer[], uint16_t bufferSize, uint16_t bufferIndex, int16_t *minVal, uint16_t *minPos) {

	buffer[bufferIndex] = sample;

	if (sample <= *minVal) {
		*minVal = sample;
		*minPos = bufferIndex;
	} else if (bufferIndex == *minPos) {
		findMinValPosBL16(buffer, bufferSize, minVal, minPos);
	}

	return;
}


/*
 * Dilation.
 * @param sample       New sample to be filtered
 * @param buffer       Buffer with samples
 * @param bufferSize   The size of the buffer
 * @param bufferIndex  Index of a sample in the buffer
 * @param *maxVal      Maximum value in the buffer
 * @param *maxPos      Position of the maximum value in the buffer
 */
void dilation(int16_t sample, int16_t buffer[], uint16_t bufferSize, uint16_t bufferIndex, int16_t *maxVal, uint16_t *maxPos) {

	buffer[bufferIndex] = sample;

	if (sample >= *maxVal) {
		*maxVal = sample;
		*maxPos = bufferIndex;
	} else if (bufferIndex == *maxPos) {
		findMaxValPosBL16(buffer, bufferSize, maxVal, maxPos);
	}

	return;

}
