/*
 * FIRFilter.c
 *
 *  Created on: 11/04/2015
 *      Author: Jose Manuel Bote
 */

#include "FIRFilter.h"
#include "DelineatorECG.h"

/*
 * All of the following coefficients are multiplied by a factor (power of 2) for wich the output sample
 * (considering 12-bit input sample) has, in the worst cases, less or equal than 30 bits (we could increase
 * the factor up to the output sample had 32 bits, but is not neccessary). Then, the final output sample has
 * to be truncated to 16 bits.
 * The coefficients which have been multiplied by 10 are intended for less operations (some coefficients are 0).
 * The coefficients which have been multiplied by 17-18 are intended for more precission.
 */

#if defined(FIR40)
	/*
	 * Coefficients for an order-40 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 1024 (2^10) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_40_FIR_14 {
		COEFFS_40_FIR_14_0  =   0,  COEFFS_40_FIR_14_1  =  0,  COEFFS_40_FIR_14_2  =   0,  COEFFS_40_FIR_14_3  =   0,
		COEFFS_40_FIR_14_4  =  -1,  COEFFS_40_FIR_14_5  = -1,  COEFFS_40_FIR_14_6  =  -2,  COEFFS_40_FIR_14_7  =  -4,
		COEFFS_40_FIR_14_8  =  -5,  COEFFS_40_FIR_14_9  = -5,  COEFFS_40_FIR_14_10 =  -4,  COEFFS_40_FIR_14_11 =   0,
		COEFFS_40_FIR_14_12 =   7,  COEFFS_40_FIR_14_13 = 18,  COEFFS_40_FIR_14_14 =  32,  COEFFS_40_FIR_14_15 =  50,
		COEFFS_40_FIR_14_16 =  69,  COEFFS_40_FIR_14_17 = 87,  COEFFS_40_FIR_14_18 = 102,  COEFFS_40_FIR_14_19 = 112,
		COEFFS_40_FIR_14_20 = 116
	};
	/*
	 * Coefficients for an order-40 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_40_FIR_14 {
		COEFFS_40_FIR_14_0  =     0,  COEFFS_40_FIR_14_1  =     2,  COEFFS_40_FIR_14_2  =     1,  COEFFS_40_FIR_14_3  =   -16,
		COEFFS_40_FIR_14_4  =   -64,  COEFFS_40_FIR_14_5  =  -157,  COEFFS_40_FIR_14_6  =  -298,  COEFFS_40_FIR_14_7  =  -468,
		COEFFS_40_FIR_14_8  =  -621,  COEFFS_40_FIR_14_9  =  -677,  COEFFS_40_FIR_14_10 =  -526,  COEFFS_40_FIR_14_11 =   -50,
		COEFFS_40_FIR_14_12 =   860,  COEFFS_40_FIR_14_13 =  2263,  COEFFS_40_FIR_14_14 =  4142,  COEFFS_40_FIR_14_15 =  6391,
		COEFFS_40_FIR_14_16 =  8810,  COEFFS_40_FIR_14_17 = 11131,  COEFFS_40_FIR_14_18 = 13065,  COEFFS_40_FIR_14_19 = 14348,
		COEFFS_40_FIR_14_20 = 14797
	};*/

	/*
	 * Coefficients for an order-40 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 1024 (2^10) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_40_FIR_40 {
		COEFFS_40_FIR_40_0  =   0,  COEFFS_40_FIR_40_1  =  0,  COEFFS_40_FIR_40_2  =   0,  COEFFS_40_FIR_40_3  =   0,
		COEFFS_40_FIR_40_4  =   0,  COEFFS_40_FIR_40_5  =  1,  COEFFS_40_FIR_40_6  =   2,  COEFFS_40_FIR_40_7  =   2,
		COEFFS_40_FIR_40_8  =  -3,  COEFFS_40_FIR_40_9  = -8,  COEFFS_40_FIR_40_10 =  -7,  COEFFS_40_FIR_40_11 =   6,
		COEFFS_40_FIR_40_12 =  20,  COEFFS_40_FIR_40_13 = 19,  COEFFS_40_FIR_40_14 =  -9,  COEFFS_40_FIR_40_15 = -48,
		COEFFS_40_FIR_40_16 = -53,  COEFFS_40_FIR_40_17 = 12,  COEFFS_40_FIR_40_18 = 142,  COEFFS_40_FIR_40_19 = 272,
		COEFFS_40_FIR_40_20 = 328
	};
	/*
	 * Coefficients for an order-40 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_40_FIR_40 {
		COEFFS_40_FIR_40_0  =     0,  COEFFS_40_FIR_40_1  =     1,  COEFFS_40_FIR_40_2  =   -15,  COEFFS_40_FIR_40_3  =   -52,
		COEFFS_40_FIR_40_4  =   -39,  COEFFS_40_FIR_40_5  =   109,  COEFFS_40_FIR_40_6  =   302,  COEFFS_40_FIR_40_7  =   226,
		COEFFS_40_FIR_40_8  =  -336,  COEFFS_40_FIR_40_9  = -1006,  COEFFS_40_FIR_40_10 =  -834,  COEFFS_40_FIR_40_11 =   720,
		COEFFS_40_FIR_40_12 =  2611,  COEFFS_40_FIR_40_13 =  2448,  COEFFS_40_FIR_40_14 = -1192,  COEFFS_40_FIR_40_15 = -6139,
		COEFFS_40_FIR_40_16 = -6825,  COEFFS_40_FIR_40_17 =  1590,  COEFFS_40_FIR_40_18 = 18124,  COEFFS_40_FIR_40_19 = 34870,
		COEFFS_40_FIR_40_20 = 41941
	};*/

#elif defined(FIR20)
	/*
	 * Coefficients for an order-20 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 1024 (2^10) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_20_FIR_14 {
		COEFFS_20_FIR_14_0 =   0,  COEFFS_20_FIR_14_1 =   0,  COEFFS_20_FIR_14_2  =   1,  COEFFS_20_FIR_14_3 =  4,
		COEFFS_20_FIR_14_4 =  12,  COEFFS_20_FIR_14_5 =  28,  COEFFS_20_FIR_14_6  =  53,  COEFFS_20_FIR_14_7 = 84,
		COEFFS_20_FIR_14_8 = 116,  COEFFS_20_FIR_14_9 = 140,  COEFFS_20_FIR_14_10 = 148
	};
	/*
	 * Coefficients for an order-20 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 262144 (2^18) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_20_FIR_14 {
		COEFFS_20_FIR_14_0 =     0,  COEFFS_20_FIR_14_1 =    -3,  COEFFS_20_FIR_14_2  =   174,  COEFFS_20_FIR_14_3 =   982,
		COEFFS_20_FIR_14_4 =  3099,  COEFFS_20_FIR_14_5 =  7215,  COEFFS_20_FIR_14_6  = 13583,  COEFFS_20_FIR_14_7 = 21593,
		COEFFS_20_FIR_14_8 = 29678,  COEFFS_20_FIR_14_9 = 35747,  COEFFS_20_FIR_14_10 = 38007
	};*/

	/*
	 * Coefficients for an order-20 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 1024 (2^10) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_20_FIR_40 {
		COEFFS_20_FIR_40_0 =   0,  COEFFS_20_FIR_40_1 =   0,  COEFFS_20_FIR_40_2  =   2,  COEFFS_20_FIR_40_3 = 3,
		COEFFS_20_FIR_40_4 =  -3,  COEFFS_20_FIR_40_5 = -21,  COEFFS_20_FIR_40_6  = -32,  COEFFS_20_FIR_40_7 = 9,
		COEFFS_20_FIR_40_8 = 125,  COEFFS_20_FIR_40_9 = 264,  COEFFS_20_FIR_40_10 = 328
	};
	/*
	 * Coefficients for an order-20 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_20_FIR_40 {
		COEFFS_20_FIR_40_0 =     0,  COEFFS_20_FIR_40_1 =    16,  COEFFS_20_FIR_40_2  =   206,  COEFFS_20_FIR_40_3 =  414,
		COEFFS_20_FIR_40_4 =  -347,  COEFFS_20_FIR_40_5 = -2699,  COEFFS_20_FIR_40_6  = -4098,  COEFFS_20_FIR_40_7 = 1202,
		COEFFS_20_FIR_40_8 = 16033,  COEFFS_20_FIR_40_9 = 33834,  COEFFS_20_FIR_40_10 = 41952
	};*/

#elif defined(FIR16)
	/*
	 * Coefficients for an order-16 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 1024 (2^10) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_16_FIR_14 {
		COEFFS_16_FIR_14_0 =   0,  COEFFS_16_FIR_14_1 =  1,  COEFFS_16_FIR_14_2 =   5,  COEFFS_16_FIR_14_3 =  17,
		COEFFS_16_FIR_14_4 =  41,  COEFFS_16_FIR_14_5 = 79,  COEFFS_16_FIR_14_6 = 123,  COEFFS_16_FIR_14_7 = 159,
		COEFFS_16_FIR_14_8 = 173,
	};
	/*
	 * Coefficients for an order-16 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 262144 (2^18) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_16_FIR_14 {
		COEFFS_16_FIR_14_0 =     0,  COEFFS_16_FIR_14_1 =   165,  COEFFS_16_FIR_14_2 =  1198,  COEFFS_16_FIR_14_3 =  4265,
		COEFFS_16_FIR_14_4 = 10580,  COEFFS_16_FIR_14_5 = 20299,  COEFFS_16_FIR_14_6 = 31570,  COEFFS_16_FIR_14_7 = 40802,
		COEFFS_16_FIR_14_8 = 44386,
	};*/

	/*
	 * Coefficients for an order-16 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_16_FIR_40 {
		COEFFS_16_FIR_40_0 =   0,  COEFFS_16_FIR_40_1 = 0,  COEFFS_16_FIR_40_2 =  -1,  COEFFS_16_FIR_40_3 = -11,
		COEFFS_16_FIR_40_4 = -21,  COEFFS_16_FIR_40_5 = 8,  COEFFS_16_FIR_40_6 = 114,  COEFFS_16_FIR_40_7 = 259,
		COEFFS_16_FIR_40_8 = 328,
	};
	/*
	 * Coefficients for an order-16 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_16_FIR_40 {
		COEFFS_16_FIR_40_0 =     0,  COEFFS_16_FIR_40_1 =  60,  COEFFS_16_FIR_40_2 =  -115,  COEFFS_16_FIR_40_3 = -1368,
		COEFFS_16_FIR_40_4 = -2736,  COEFFS_16_FIR_40_5 = 968,  COEFFS_16_FIR_40_6 = 14621,  COEFFS_16_FIR_40_7 = 33106,
		COEFFS_16_FIR_40_8 = 42000,
	};*/

#elif defined(FIR10)
	/*
	 * Coefficients for an order-10 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 1024 (2^10) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_10_FIR_14 {
		COEFFS_10_FIR_14_0 =   0,  COEFFS_10_FIR_14_1 =   7,  COEFFS_10_FIR_14_2 = 42,  COEFFS_10_FIR_14_3 = 120,
		COEFFS_10_FIR_14_4 = 214,  COEFFS_10_FIR_14_5 = 257
	};
	/*
	 * Coefficients for an order-10 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_10_FIR_14 {
		COEFFS_10_FIR_14_0 =     0,  COEFFS_10_FIR_14_1 =   926,  COEFFS_10_FIR_14_2 = 5439,  COEFFS_10_FIR_14_3 = 15404,
		COEFFS_10_FIR_14_4 = 27335,  COEFFS_10_FIR_14_5 = 32862
	};*/

	/*
	 * Coefficients for an order-10 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
	enum COEFFS_10_FIR_40 {
		COEFFS_10_FIR_40_0 =   0,  COEFFS_10_FIR_40_1 =  -3,  COEFFS_10_FIR_40_2 = 3,  COEFFS_10_FIR_40_3 = 81,
		COEFFS_10_FIR_40_4 = 253,  COEFFS_10_FIR_40_5 = 355
	};
	/*
	 * Coefficients for an order-10 low-pass FIR filter with cut-off frequency 40 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 250 Hz
	 */
/*	enum COEFFS_10_FIR_40 {
		COEFFS_10_FIR_40_0 =     0,  COEFFS_10_FIR_40_1 =  -305,  COEFFS_10_FIR_40_2 = 379,  COEFFS_10_FIR_40_3 = 10417,
		COEFFS_10_FIR_40_4 = 32386,  COEFFS_10_FIR_40_5 = 45407
	};*/

#endif

#if defined(FIR10)
	/*
	 * This enum is used in SAVE_MODE
	 * Coefficients for an order-10 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 262144 (2^17) and rounded to convert them in integers.
	 * fSample = 50 Hz
	 */
/*	enum COEFFS_10_FIR_14_SAVEMODE {
		COEFFS_10_FIR_14_0_SAVEMODE =   0,  COEFFS_10_FIR_14_1_SAVEMODE =   2,  COEFFS_10_FIR_14_2_SAVEMODE = -18,
		COEFFS_10_FIR_14_3_SAVEMODE = -31,  COEFFS_10_FIR_14_4_SAVEMODE = 272,  COEFFS_10_FIR_14_5_SAVEMODE = 574
	};*/
	/*
	 * This enum is used in SAVE_MODE
	 * Coefficients for an order-10 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 262144 (2^17) and rounded to convert them in integers.
	 * fSample = 50 Hz
	 */
	enum COEFFS_10_FIR_14_SAVEMODE {
		COEFFS_10_FIR_14_0_SAVEMODE =     0,  COEFFS_10_FIR_14_1_SAVEMODE =   287,  COEFFS_10_FIR_14_2_SAVEMODE = -2358,
		COEFFS_10_FIR_14_3_SAVEMODE = -3916,  COEFFS_10_FIR_14_4_SAVEMODE = 34813,  COEFFS_10_FIR_14_5_SAVEMODE = 73420
	};

#else
	/*
	 * This enum is used in SAVE_MODE
	 * Coefficients for an order-40 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 1024 (2^10) and rounded to convert them in integers.
	 * fSample = 50 Hz
	 */
/*	enum COEFFS_40_FIR_14_SAVEMODE {
		COEFFS_40_FIR_14_0_SAVEMODE  =   0,  COEFFS_40_FIR_14_1_SAVEMODE  =   0,  COEFFS_40_FIR_14_2_SAVEMODE  =   0,
		COEFFS_40_FIR_14_3_SAVEMODE  =   0,  COEFFS_40_FIR_14_4_SAVEMODE  =   0,  COEFFS_40_FIR_14_5_SAVEMODE  =   1,
		COEFFS_40_FIR_14_6_SAVEMODE  =  -1,  COEFFS_40_FIR_14_7_SAVEMODE  =  -3,  COEFFS_40_FIR_14_8_SAVEMODE  =   4,
		COEFFS_40_FIR_14_9_SAVEMODE  =   4,  COEFFS_40_FIR_14_10_SAVEMODE = -11,  COEFFS_40_FIR_14_11_SAVEMODE =  -2,
		COEFFS_40_FIR_14_12_SAVEMODE =  21,  COEFFS_40_FIR_14_13_SAVEMODE =  -7,  COEFFS_40_FIR_14_14_SAVEMODE = -34,
		COEFFS_40_FIR_14_15_SAVEMODE =  30,  COEFFS_40_FIR_14_16_SAVEMODE =  47,  COEFFS_40_FIR_14_17_SAVEMODE = -84,
		COEFFS_40_FIR_14_18_SAVEMODE = -58,  COEFFS_40_FIR_14_19_SAVEMODE = 317,  COEFFS_40_FIR_14_20_SAVEMODE = 573
	};*/
	/*
	 * This enum is used in SAVE_MODE
	 * Coefficients for an order-40 low-pass FIR filter with cut-off frequency 14 Hz.
	 * The coefficients are multiplied by 131072 (2^17) and rounded to convert them in integers.
	 * fSample = 50 Hz
	 */
	enum COEFFS_40_FIR_14_SAVEMODE {
		COEFFS_40_FIR_14_0_SAVEMODE  =     0,  COEFFS_40_FIR_14_1_SAVEMODE  =     4,  COEFFS_40_FIR_14_2_SAVEMODE  =      5,
		COEFFS_40_FIR_14_3_SAVEMODE  =   -53,  COEFFS_40_FIR_14_4_SAVEMODE  =    13,  COEFFS_40_FIR_14_5_SAVEMODE  =    176,
		COEFFS_40_FIR_14_6_SAVEMODE  =  -146,  COEFFS_40_FIR_14_7_SAVEMODE  =  -361,  COEFFS_40_FIR_14_8_SAVEMODE  =    538,
		COEFFS_40_FIR_14_9_SAVEMODE  =   486,  COEFFS_40_FIR_14_10_SAVEMODE = -1349,  COEFFS_40_FIR_14_11_SAVEMODE =   -245,
		COEFFS_40_FIR_14_12_SAVEMODE =  2653,  COEFFS_40_FIR_14_13_SAVEMODE =  -889,  COEFFS_40_FIR_14_14_SAVEMODE =  -4336,
		COEFFS_40_FIR_14_15_SAVEMODE =  3794,  COEFFS_40_FIR_14_16_SAVEMODE =  6064,  COEFFS_40_FIR_14_17_SAVEMODE = -10715,
		COEFFS_40_FIR_14_18_SAVEMODE = -7374,  COEFFS_40_FIR_14_19_SAVEMODE = 40571,  COEFFS_40_FIR_14_20_SAVEMODE =  73402
	};

#endif


int16_t samples[FIR_ORDER + 1] = {0};   // The samples stored to be used for the filter
int16_t *pLast = samples + FIR_ORDER;   // A pointer to the last position in the samples array

#if defined(FIR40)
/*
 * Filter samples with an order of 40. Both filters are done at the same time. fSample = 250 Hz.
 * @param sample          New sample to be filtered
 * @param FIRSamples[2]   The ouput of the filter. For the 14-Hz filter, the output is stored in [0] and
 *                        for the 40-Hz filter in [1]
 */
void filterSampleFIR(int16_t sample, int16_t FIRSamples[2]) {

	// Pointers as indexes
	int16_t *pIni, *pEnd;

	// Shift the samples
	pIni = samples;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni = sample;

	// Convolution at the same time, a casting to int32_t is neccessary to avoid overflow
	pIni = samples;
	pEnd = pLast;
	int32_t tempSum14 = 0;
	int32_t tempSum40 = 0;

	if (mode == SAVE_MODE) {

		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_0_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_1_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_2_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_3_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_4_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_5_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_6_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_7_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_8_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_9_SAVEMODE;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_10_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_11_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_12_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_13_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_14_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_15_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_16_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_17_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_18_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_19_SAVEMODE;  pIni++;
		tempSum14+=((int32_t) (*pIni)         )*COEFFS_40_FIR_14_20_SAVEMODE;

	} else {

		#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_0;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_0;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_1;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_1;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_2;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_2;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_3;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_3;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_4;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_4;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_5;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_5;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_6;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_6;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_7;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_7;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_8;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_8;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_9;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_9;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_10;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_10;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_11;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_11;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_12;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_12;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_13;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_13;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_14;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_14;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_15;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_15;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_16;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_16;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_17;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_17;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_18;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_18;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_19;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_40_19;  pIni++;
			tempSum14+=((int32_t) (*pIni)         )*COEFFS_40_FIR_14_20;  tempSum40+=((int32_t) (*pIni)         )*COEFFS_40_FIR_40_20;

		#else
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_0;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_1;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_2;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_3;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_4;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_5;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_6;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_7;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_8;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_9;   pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_10;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_11;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_12;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_13;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_14;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_15;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_16;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_17;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_18;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_40_FIR_14_19;  pIni++;
			tempSum14+=((int32_t) (*pIni)         )*COEFFS_40_FIR_14_20;

		#endif

	}

	// Store the filtered samples but in int16_t type
if (mode == SAVE_MODE) {
		FIRSamples[0] = (int16_t) (tempSum14 >> 15);  // Amplified for the derivatives (x4)
	} else {
		FIRSamples[0] = (int16_t) (tempSum14 >> 8);  // Amplified for the derivatives (x4)
		#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
			FIRSamples[1] = (int16_t) (tempSum40 >> 8);  // Amplified for the baseline (x4)
		#endif
	}

	return;

}

#elif defined(FIR20)
/*
 * Filter samples with an order of 20. Both filters are done at the same time. fSample = 250 Hz.
 * @param sample          New sample to be filtered
 * @param FIRSamples[2]   The ouput of the filter. For the 14-Hz filter, the output is stored in [0] and
 *                        for the 40-Hz filter in [1]
 */
void filterSampleFIR(int16_t sample, int16_t FIRSamples[2]) {

	// Pointers as indexes
	int16_t *pIni, *pEnd;

	// Shift the samples
	pIni = samples;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni = sample;

	// Convolution at the same time, a casting to int32_t is neccessary to avoid overflow
	pIni = samples;
	pEnd = pLast;
	int32_t tempSum14 = 0;

	#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
		int32_t tempSum40 = 0;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_0;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_0;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_1;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_1;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_2;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_2;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_3;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_3;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_4;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_4;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_5;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_5;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_6;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_6;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_7;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_7;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_8;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_8;   pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_9;   tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_40_9;   pIni++;
		tempSum14+=((int32_t) (*pIni)         )*COEFFS_20_FIR_14_10;  tempSum40+=((int32_t) (*pIni)         )*COEFFS_20_FIR_40_10;

	#else
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_0;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_1;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_2;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_3;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_4;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_5;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_6;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_7;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_8;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_20_FIR_14_9;  pIni++;
		tempSum14+=((int32_t) (*pIni)         )*COEFFS_20_FIR_14_10;

	#endif

	// Store the filtered samples but in int16_t type
	FIRSamples[0] = (int16_t) (tempSum14 >> 8);  // Amplified for the derivatives (x4)
	#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
		FIRSamples[1] = (int16_t) (tempSum40 >> 8);  // Amplified for the baseline (x4)
	#endif
	return;

}

#elif defined(FIR16)
/*
 * Filter samples with an order of 16. Both filters are done at the same time. fSample = 250 Hz.
 * @param sample          New sample to be filtered
 * @param FIRSamples[2]   The ouput of the filter. For the 14-Hz filter, the output is stored in [0] and
 *                        for the 40-Hz filter in [1]
 */
void filterSampleFIR(int16_t sample, int16_t FIRSamples[2]) {

	// Pointers as indexes
	int16_t *pIni, *pEnd;

	// Shift the samples
	pIni = samples;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;
	*pIni = sample;

	// Convolution at the same time, a casting to int32_t is neccessary to avoid overflow
	pIni = samples;
	pEnd = pLast;
	int32_t tempSum14 = 0;

	#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
		int32_t tempSum40 = 0;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_0;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_0;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_1;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_1;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_2;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_2;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_3;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_3;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_4;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_4;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_5;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_5;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_6;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_6;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_7;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_40_7;  pIni++;
		tempSum14+=((int32_t) (*pIni)         )*COEFFS_16_FIR_14_8;  tempSum40+=((int32_t) (*pIni)         )*COEFFS_16_FIR_40_8;

	#else
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_0;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_1;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_2;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_3;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_4;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_5;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_6;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_16_FIR_14_7;  pIni++;
		tempSum14+=((int32_t) (*pIni)         )*COEFFS_16_FIR_14_8;

	#endif

	// Store the filtered samples but in int16_t type
	FIRSamples[0] = (int16_t) (tempSum14 >> 8);  // Amplified for the derivatives (x4)
	#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
		FIRSamples[1] = (int16_t) (tempSum40 >> 8);  // Amplified for the baseline (x4)
	#endif

	return;

}

#elif defined(FIR10)
/*
 * Filter samples with an order of 10. Both filters are done at the same time. fSample = 250 Hz.
 * @param sample          New sample to be filtered
 * @param FIRSamples[2]   The ouput of the filter. For the 14-Hz filter, the output is stored in [0] and
 *                        for the 40-Hz filter in [1]
 */
void filterSampleFIR(int16_t sample, int16_t FIRSamples[2]) {

	// Pointers as indexes
	int16_t *pIni, *pEnd;

	// Shift the samples
	pIni = samples;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;  *pIni=*(pIni+1); pIni++;
	*pIni = sample;

	// Convolution at the same time, a casting to int32_t is neccessary to avoid overflow
	pIni = samples;
	pEnd = pLast;
	int32_t tempSum14 = 0;
	int32_t tempSum40 = 0;

	if (mode == SAVE_MODE) {

		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_0_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_1_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_2_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_3_SAVEMODE;  pIni++; pEnd--;
		tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_4_SAVEMODE;  pIni++;
		tempSum14+=((int32_t)((*pIni)        ))*COEFFS_10_FIR_14_5_SAVEMODE;

	} else {

		#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_0;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_40_0;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_1;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_40_1;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_2;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_40_2;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_3;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_40_3;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_4;  tempSum40+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_40_4;  pIni++;
			tempSum14+=((int32_t) (*pIni)         )*COEFFS_10_FIR_14_5;  tempSum40+=((int32_t) (*pIni)         )*COEFFS_10_FIR_40_5;

		#else
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_0;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_1;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_2;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_3;  pIni++; pEnd--;
			tempSum14+=((int32_t)((*pIni)+(*pEnd)))*COEFFS_10_FIR_14_4;  pIni++;
			tempSum14+=((int32_t) (*pIni)         )*COEFFS_10_FIR_14_5;

		#endif
	}

	// Store the filtered samples but in int16_t type
	if (mode == SAVE_MODE) {
		FIRSamples[0] = (int16_t) (tempSum14 >> 15);  // Amplified for the derivatives (x4)
	} else {
		FIRSamples[0] = (int16_t) (tempSum14 >> 8);  // Amplified for the derivatives (x4)
		#if (defined(BASELINE) || defined(OP_P) || defined(OP_T))
			FIRSamples[1] = (int16_t) (tempSum40 >> 8);  // Amplified for the baseline (x4)
		#endif
	}

	return;

}

#endif
