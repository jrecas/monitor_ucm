/*
 * Utils.c
 *
 *  Created on: 30/03/2015
 *      Author: Jose Manuel Bote
 */

#include "Utils.h"


/*
 * Find the minimum value and its position in a buffer with int16-samples, with a minimum bound
 * given by the old minimum value. Used in baseline filter.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *minVal      The minimum value in the buffer
 * @param *minPos      The position of the minimum value in the buffer
 */
void findMinValPosBL16(int16_t buffer[], uint16_t bufferSize, int16_t *minVal, uint16_t *minPos) {
	int16_t tempOldMinVal = *minVal;
	*minVal = buffer[0];
	*minPos = 0;
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < *minVal) {
			*minVal = buffer[i];
			*minPos = i;
		} else if (buffer[i] == tempOldMinVal) {	// There is a minimum bound given by the old minimum value
			*minVal = buffer[i];
			*minPos = i;
			break;	// The minimum bound is reached, there is no lower value
		}
	}
	return;
}


/*
 * Find the maximum value and its position in a buffer with int16-samples, with a maximum bound
 * given by the old maximum value. Used in baseline filter.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *maxVal      The maximum value in the buffer
 * @param *maxPos      The position of the maximum value in the buffer
 */
void findMaxValPosBL16(int16_t buffer[], uint16_t bufferSize, int16_t *maxVal, uint16_t *maxPos) {
	int16_t tempOldMaxVal = *maxVal;
	*maxVal = buffer[0];
	*maxPos = 0;
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > *maxVal) {
			*maxVal = buffer[i];
			*maxPos = i;
		} else if (buffer[i] == tempOldMaxVal) {	// There is a maximum bound given by the old maximum value
			*maxVal = buffer[i];
			*maxPos = i;
			break;	// The maximum bound is reached, there is no bigger value
		}
	}
	return;
}


/*
 * Find the minimum value and its position in a buffer with int16-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *minVal      The minimum value in the buffer
 * @param *minPos      The position of the minimum value in the buffer
 */
void findMinValPos16(int16_t buffer[], uint16_t bufferSize, int16_t *minVal, uint16_t *minPos) {
	*minVal = buffer[0];
	*minPos = 0;
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < *minVal) {
			*minVal = buffer[i];
			*minPos = i;
		}
	}
	return;
}


/*
 * Find the maximum value and its position in a buffer with int16-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *maxVal      The maximum value in the buffer
 * @param *maxPos      The position of the maximum value in the buffer
 */
void findMaxValPos16(int16_t buffer[], uint16_t bufferSize, int16_t *maxVal, uint16_t *maxPos) {
	*maxVal = buffer[0];
	*maxPos = 0;
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > *maxVal) {
			*maxVal = buffer[i];
			*maxPos = i;
		}
	}
	return;
}


/*
 * Find the minimum value in a buffer with int16-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *minVal      The minimum value in the buffer
 */
void findMinVal16(int16_t buffer[], uint16_t bufferSize, int16_t *minVal) {
	*minVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < *minVal) {
			*minVal = buffer[i];
		}
	}
	return;
}


/*
 * Find the maximum value in a buffer with int16-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *maxVal      The maximum value in the buffer
 */
void findMaxVal16(int16_t buffer[], uint16_t bufferSize, int16_t *maxVal) {
	*maxVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > *maxVal) {
			*maxVal = buffer[i];
		}
	}
	return;
}


/*
 * Find the minimum value in a buffer with int32-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *minVal      The minimum value in the buffer
 */
void findMinVal32(int32_t buffer[], uint16_t bufferSize, int32_t *minVal) {
	*minVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < *minVal) {
			*minVal = buffer[i];
		}
	}
	return;
}


/*
 * Find the maximum value in a buffer with int32-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *maxVal      The maximum value in the buffer
 */
void findMaxVal32(int32_t buffer[], uint16_t bufferSize, int32_t *maxVal) {
	*maxVal = buffer[0];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > *maxVal) {
			*maxVal = buffer[i];
		}
	}
	return;
}


/*
 * Find the position of the minimum in a buffer with int16-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *minPos      The position of the minimum value in the buffer
 */
void findMinPos16(int16_t buffer[], uint16_t bufferSize, uint16_t *minPos) {
	*minPos = 0;
	int16_t tempMinVal = buffer[*minPos];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] < tempMinVal) {
			tempMinVal = buffer[i];
			*minPos = i;
		}
	}
	return;
}


/*
 * Find the position of the maximum in a buffer with int16-samples.
 * @param buffer       A buffer with samples
 * @param bufferSize   The size of the buffer
 * @param *maxPos      The position of the maximum value in the buffer
 */
void findMaxPos16(int16_t buffer[], uint16_t bufferSize, uint16_t *maxPos) {
	*maxPos = 0;
	int16_t tempMaxVal = buffer[*maxPos];
	uint16_t i;
	for (i = 1; i < bufferSize; i++) {
		if (buffer[i] > tempMaxVal) {
			tempMaxVal = buffer[i];
			*maxPos = i;
		}
	}
	return;
}
