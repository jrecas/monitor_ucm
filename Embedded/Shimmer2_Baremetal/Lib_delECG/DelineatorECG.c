/*
 * DelineatorECG.c
 *
 *  Created on: 30/03/2015
 *      Author: Jose Manuel Bote
 */

#include "DelineatorECG.h"
#include <stdlib.h>
#include <string.h>
#include "BaselineFilter.h"
#include "Utils.h"


int16_t  buffSamples14[BUFF_LEN] = {0};          // 14 Hz FIR filter
#ifdef BASELINE
	int16_t buffSamples40[BUFF_LEN] = {0};       // 40 Hz FIR filter (with the baseline wander removed)
#endif
#ifdef OP_P
	int16_t buffSamples40_O_P[BUFF_LEN] = {0};  // ECG with opening P
#endif
#ifdef OP_T
	int16_t buffSamples40_O_T[BUFF_LEN] = {0};   // ECG with opening T
#endif

uint16_t buffSamplesIndex = 0;                   // Index for FIR buffers
int32_t  buffSamplesOverflow = -1;               // Number of times that the buffer is overflowed (initizalized at -1)

uint32_t ECGPoints[NUM_POINTS] = {0};            // Penultimate delineation, it is completed
uint32_t tempECGPoints[NUM_POINTS] = {0};        // Last delineation, if completed it is stored in ECGPoints
uint16_t flagNewBeatFound = FALSE;               // Last beat found
int16_t  buffThresholdR[NUM_WIN_R_TH + 1] = {0}; // Last thresholds for the R peak detection stored in an array
uint16_t indexThresholdR = 0;                    // Index for the array for the R peak detection
int16_t  meanThresholdR = INT16_MIN;             // Mean for the last threshold for the R peak detection
uint16_t negativeInterval = FALSE;               // If the last ECG interval contains negative QRS complexes

boolean  delineationFlag = FALSE;                // Flag to delineate
uint16_t delineationIndex = 0;                   // Index in the delineation process

#ifdef FULLMODE
	delineationMode mode = FULL_MODE;            // Delineation mode (FULL_MODE == 250 Hz, SAVE_MODE == 50 Hz)
#else
	delineationMode mode = SAVE_MODE;            // Delineation mode (FULL_MODE == 250 Hz, SAVE_MODE == 50 Hz)
#endif


/*
 * Stores the filtered samples in the buffers.
 * @param FIRSamples[2]      Samples from the FIR filters (0: 14 Hz, 1: 40 Hz)
 * @param BL_filt_sample     Baseline sample
 * @param openingPSample     Opening P wave sample
 * @param openingTSample     Opening T wave sample
 */
boolean storeSamples(int16_t FIRSamples[2], int16_t BLSample, int16_t openingPSample, int16_t openingTSample) {

	// Stores the samples
	buffSamples14[buffSamplesIndex] = FIRSamples[0];  // 14 Hz, amplified x8
	if (mode == FULL_MODE) {
		#ifdef BASELINE
			buffSamples40[buffSamplesIndex] = FIRSamples[1];  // 40 Hz, amplified x4
			buffSamples40[(buffSamplesIndex - BASELINE_FILTER_DELAY) & MASK_BUFF_LEN] -= BLSample;
		#endif
		#ifdef OP_P
			buffSamples40_O_P[buffSamplesIndex] = FIRSamples[1];
			buffSamples40_O_P[(buffSamplesIndex - OPENING_P_DELAY) & MASK_BUFF_LEN] -= openingPSample;
		#endif
		#ifdef OP_T
			buffSamples40_O_T[buffSamplesIndex] = FIRSamples[1];
			buffSamples40_O_T[(buffSamplesIndex - OPENING_T_DELAY) & MASK_BUFF_LEN] -= openingTSample;
		#endif
	}

	// If a half-buffer is completed, it is preproccessed
	if (buffSamplesIndex == SECOND_HALF_BUFF_COMPLETED) {
		preProccess(SECOND_HALF_BUFF);      // Preproccessing the second half of the buffer
		delineationIndex = BUFF_HALF_LEN;
		delineationFlag = TRUE;
	} else if (buffSamplesIndex == FIRST_HALF_BUFF_COMPLETED) {
		preProccess(FIRST_HALF_BUFF);       // Preproccessing the first half of the buffer
		buffSamplesOverflow += 1;
		delineationIndex = 0;
		delineationFlag = TRUE;
	}

	// Delineate the part of the buffer, new beat is delineated for each new sample stored.
	if (delineationFlag) {

		do {

			// Three samples (last, current and next) to calculate the first and the second derivative
			// using the 14 Hz filtered samples
			int16_t tempSample14_last = buffSamples14[(delineationIndex-1) & MASK_BUFF_LEN];
			int16_t tempSample14_curr = buffSamples14[ delineationIndex    & MASK_BUFF_LEN];
			int16_t tempSample14_next = buffSamples14[(delineationIndex+1) & MASK_BUFF_LEN];

			// If negative beats are found in the previous ECG, the samples have changed their sign so
			// it is needed to be taken into account (it may only happen at the begginng of the interval)
			if ((negativeInterval) && !((delineationIndex+1) & MASK_BUFF_HALF_LEN)) {
				tempSample14_next *= -1;
			}

			// The second derivative
			int16_t tempSampleSD = tempSample14_last - 2*tempSample14_curr + tempSample14_next;

			// Change of sign of the first derivative
			int16_t tempSampleFD = (((tempSample14_next - tempSample14_curr) & 0x8000) !=
					((tempSample14_curr - tempSample14_last) & 0x8000)) ? TRUE : FALSE;

			if ((tempSampleSD < ((21*meanThresholdR)>>6)) && tempSampleFD) {  // Equivalent to 33%

				uint32_t posNewBeat = delineationIndex + buffSamplesOverflow * BUFF_LEN;
				int16_t tempPeakR_SD = buffSamples14[(tempECGPoints[4] - 1) & MASK_BUFF_LEN]
						- 2 * buffSamples14[tempECGPoints[4] & MASK_BUFF_LEN] + buffSamples14[(tempECGPoints[4]+1) & MASK_BUFF_LEN];

				// Minimum distance is not valid between R peaks
				if ((posNewBeat - tempECGPoints[4]) < MIN_RR_PEAKS) {
					// The new value is lower than the last one (greater in abs)
					if (tempSampleSD < tempPeakR_SD) {
						tempECGPoints[0] = 0;
						tempECGPoints[1] = 0;
						tempECGPoints[2] = 0;
						tempECGPoints[3] = 0;
						tempECGPoints[4] = posNewBeat;
					}

				// Maximum distance between R peaks is not valid (samples missed)
				// The current beat is stored, missing the last one
				} else if ((posNewBeat - tempECGPoints[4]) > MAX_RR_PEAKS) {
					tempECGPoints[0] = 0;
					tempECGPoints[1] = 0;
					tempECGPoints[2] = 0;
					tempECGPoints[3] = 0;
					tempECGPoints[4] = posNewBeat;

				// The distance between R peaks is valid
				} else {
					memcpy(ECGPoints, tempECGPoints, sizeof(tempECGPoints));
					tempECGPoints[0] = 0;
					tempECGPoints[1] = 0;
					tempECGPoints[2] = 0;
					tempECGPoints[3] = 0;
					tempECGPoints[4] = posNewBeat;  // Updating the last beat
					tempECGPoints[5] = 0;
					tempECGPoints[6] = 0;
					tempECGPoints[7] = 0;
					if (mode == FULL_MODE) {
						#if (defined(P_PEAK) || defined(T_PEAK) || defined(BASELINE) || defined(OP_P) || defined(OP_T))
							delineateECGWaves((uint16_t) (ECGPoints[4] & MASK_BUFF_LEN), delineationIndex);  // Delineate
						#endif
					}
					flagNewBeatFound = TRUE;

				}
			}

			delineationIndex++;
			delineationFlag = (delineationIndex & MASK_BUFF_HALF_LEN);

		} while (!flagNewBeatFound && (delineationIndex & MASK_BUFF_HALF_LEN));

	}

	// Circular index for circular buffer
	buffSamplesIndex = (buffSamplesIndex+1) & MASK_BUFF_LEN;

	// Return if a new beat has been delineated
	if (flagNewBeatFound) {
		flagNewBeatFound = FALSE;
		return TRUE;
	} else {
		return FALSE;
	}

}


/*
 * Preproccess half of the buffer, finding the threshold for R peaks detection and
 * inverting the signal if needed
 * @param halfBuffer    Half of the buffer
 */
void preProccess(uint16_t halfBuffer) {

	// Preproccess the first half of the buffer
	if (!halfBuffer) {

		// Calculate the second derivate and its minimum and maximum
		// If negative beats are found in the previous ECG, the samples have changed their sign so
		// it is needed to be taken into account (it may only happen at the begginng of the interval)
		int16_t	tempSecDer_i = (negativeInterval) ?
			-buffSamples14[BUFF_LEN-1] - 2*buffSamples14[0] + buffSamples14[1] :
			 buffSamples14[BUFF_LEN-1] - 2*buffSamples14[0] + buffSamples14[1];

		int16_t tempSecDerMin = tempSecDer_i;
		int16_t tempSecDerMax = tempSecDer_i;
		uint16_t i;
		for (i = 1; i < BUFF_HALF_LEN; i++) {
			tempSecDer_i = buffSamples14[i-1] - 2*buffSamples14[i] + buffSamples14[i+1];
			if (tempSecDer_i < tempSecDerMin) {
				tempSecDerMin = tempSecDer_i;
			}
			if (tempSecDer_i > tempSecDerMax) {
				tempSecDerMax = tempSecDer_i;
			}
		}

		// Choose the greater value in abs as threshold, inverting if needed
		if (abs(tempSecDerMin) >= abs(tempSecDerMax)) {
			buffThresholdR[indexThresholdR] = tempSecDerMin;
			negativeInterval = FALSE;
		} else {
			buffThresholdR[indexThresholdR] = -tempSecDerMax;
			negativeInterval = TRUE;
			uint16_t k;
			for (k = 0; k < BUFF_HALF_LEN; k++) {
				buffSamples14[k] *= -1;
				if (mode == FULL_MODE) {
					#ifdef BASELINE
						buffSamples40[k] *= -1;
					#endif
					#ifdef OP_P
						buffSamples40_O_P[k] *= -1;
					#endif
					#ifdef OP_T
						buffSamples40_O_T[k] *= -1;
					#endif
				}
			}
		}

	// Preproccess the second half of the buffer
	} else {

		// Calculate the second derivate and its minimum and maximum
		// If negative beats are found in the previous ECG, the samples have changed their sign so
		// it is needed to be taken into account (it may only happen at the begginng of the interval)
		int16_t	tempSecDer_i = (negativeInterval) ?
			-buffSamples14[BUFF_HALF_LEN-1] - 2*buffSamples14[BUFF_HALF_LEN] + buffSamples14[BUFF_HALF_LEN+1] :
			 buffSamples14[BUFF_HALF_LEN-1] - 2*buffSamples14[BUFF_HALF_LEN] + buffSamples14[BUFF_HALF_LEN+1];

		int16_t tempSecDerMin = tempSecDer_i;
		int16_t tempSecDerMax = tempSecDer_i;
		uint16_t i;
		for (i = BUFF_HALF_LEN+1; i < BUFF_LEN-1; i++) {
			tempSecDer_i = buffSamples14[i-1] - 2*buffSamples14[i] + buffSamples14[i+1];
			if (tempSecDer_i < tempSecDerMin) {
				tempSecDerMin = tempSecDer_i;
			}
			if (tempSecDer_i > tempSecDerMax) {
				tempSecDerMax = tempSecDer_i;
			}
		}
		// Avoiding using a mask only for the last access to the array
		tempSecDer_i = buffSamples14[BUFF_LEN-2] - 2*buffSamples14[BUFF_LEN-1] + buffSamples14[0];
		if (tempSecDer_i < tempSecDerMin) {
			tempSecDerMin = tempSecDer_i;
		}
		if (tempSecDer_i > tempSecDerMax) {
			tempSecDerMax = tempSecDer_i;
		}

		// Choose the greater value in abs as threshold, inverting if needed
		if (abs(tempSecDerMin) >= abs(tempSecDerMax)) {
			buffThresholdR[indexThresholdR] = tempSecDerMin;
			negativeInterval = FALSE;
		} else {
			buffThresholdR[indexThresholdR] = -tempSecDerMax;
			negativeInterval = TRUE;
			uint16_t k;
			for (k = BUFF_HALF_LEN; k < BUFF_LEN; k++) {
				buffSamples14[k]     *= -1;
				if (mode == FULL_MODE) {
					#ifdef BASELINE
						buffSamples40[k] *= -1;
					#endif
					#ifdef OP_P
						buffSamples40_O_P[k] *= -1;
					#endif
					#ifdef OP_T
						buffSamples40_O_T[k] *= -1;
					#endif
				}
			}
		}
	}

	// The mean of the n-last threshold is used for the R peaks detection, if 0 then uses the last value
	// without calculate the mean, it is used specially for the first times.
	if (!meanThresholdR) {
		buffThresholdR[0] = buffThresholdR[indexThresholdR];
		buffThresholdR[1] = buffThresholdR[indexThresholdR];
		buffThresholdR[2] = buffThresholdR[indexThresholdR];
		buffThresholdR[3] = buffThresholdR[indexThresholdR];
		meanThresholdR = buffThresholdR[indexThresholdR];
	} else {
		meanThresholdR = (buffThresholdR[0] + buffThresholdR[1] + buffThresholdR[2] + buffThresholdR[3]) >> DESP_WIN_R_TH;
	}
	indexThresholdR = (indexThresholdR+1) & NUM_WIN_R_TH;

}


#if (defined(P_PEAK) || defined(T_PEAK) || defined(BASELINE) || defined(OP_P) || defined(OP_T))
/*
 * Delineation for each beat in two subproccess.
 * 1. Delineate S and T waves for the previous beat.
 * 2. Delineate P and Q waves for the current beat.
 * @param startPos  Position of the previous R peak
 * @param endPos    Position of the current R peak
 */
void delineateECGWaves(uint16_t startPos, uint16_t endPos) {
	uint16_t overflow = FALSE;
	if (startPos > endPos) {
		endPos += BUFF_LEN;
		overflow = TRUE;
	}
	uint16_t midPos = (startPos + endPos) >> 1;  // Mean position
	#if (defined(T_PEAK) || defined(BASELINE) || defined(OP_T))
		delineateSTWaves(startPos, midPos, overflow);
	#endif
	#if (defined(P_PEAK) || defined(BASELINE) || defined(OP_P))
		delineatePQWaves(midPos, endPos, overflow);
	#endif
}
#endif


#if (defined(BASELINE) || defined(T_PEAK) || defined(OP_T))
/*
 * Delineate the S and T wave for the previous beat.
 * @param startPos    Position of the previous R peak
 * @param endPos      Position of the expected T peak
 * @param overflow    If the buffer has been overflowed
 */
void delineateSTWaves(uint16_t startPos, uint16_t endPos, uint16_t overflow) {

#ifdef BASELINE
	/*
	 * Delineate the S wave
	 */

	// Tolerance: 5% of the R peak amplitude
	int16_t tolerance = (13 * abs(buffSamples40[startPos & MASK_BUFF_LEN])) >> 8;  // Equivalent to 5%
	int16_t tempThreshold = 10*tolerance;

	// Find the 50% of the R peak amplitude
	uint16_t infintyLoop = BUFF_LEN;
	while ((buffSamples40[startPos & MASK_BUFF_LEN] > tempThreshold) && infintyLoop) {
		startPos += 1;
		infintyLoop--;
	}
	//startPos -= 1;
	if (startPos >= endPos) {
		return;
	}

	// Find the R end
	tempThreshold = tolerance;
	while ((buffSamples40[startPos & MASK_BUFF_LEN] > tempThreshold) &&
			(buffSamples40[startPos & MASK_BUFF_LEN] > buffSamples40[(startPos+1) & MASK_BUFF_LEN])) {
		startPos += 1;
	}
	if (startPos >= endPos) {
		return;
	}
	uint16_t tempPosOn = startPos;  // Position of the R end and the S end (they are the same points)
	ECGPoints[5] = startPos + ((buffSamplesOverflow - overflow) * BUFF_LEN);

	// Find the minimum to the right (the S peak)
	infintyLoop = BUFF_LEN;
	while ((buffSamples40[startPos & MASK_BUFF_LEN] >= buffSamples40[(startPos+1) & MASK_BUFF_LEN]) && infintyLoop) {
		startPos += 1;
		infintyLoop--;
	}
	//startPos -= 1;
	if (startPos >= endPos) {
		return;
	}

	tempThreshold = -tolerance;
	if (buffSamples40[startPos & MASK_BUFF_LEN] < tempThreshold) {  // The minimum is lower than the threshold, is valid

		// Find the S end
		while ((buffSamples40[startPos & MASK_BUFF_LEN] < tempThreshold) &&
				(buffSamples40[startPos & MASK_BUFF_LEN] < buffSamples40[(startPos+1) & MASK_BUFF_LEN])) {
			startPos += 1;
		}
		if (startPos >= endPos) {
			return;
		}

		// The S wave is valid if its width is lower than 100 ms
		if ((startPos != tempPosOn) && ((startPos - tempPosOn) < T_S_WAVE)) {
			// Small correction for the S end
			uint16_t tempStop = startPos + CORRECT_S_END;
			if (tempStop > endPos) {
				tempStop = endPos;
			}
			for (; startPos <= tempStop; (startPos)++) {
				if (buffSamples40[startPos & MASK_BUFF_LEN] > buffSamples40[(startPos+1) & MASK_BUFF_LEN]) {
					ECGPoints[5] = startPos + ((buffSamplesOverflow - overflow) * BUFF_LEN);
					break;
				}
			}
			ECGPoints[5] = startPos + ((buffSamplesOverflow - overflow) * BUFF_LEN);
		}

	}
#endif

#ifdef T_PEAK
	/*
	 * Delineate the T wave
	 */
	uint16_t negativeTWave = FALSE;
	uint16_t auxPosPeak    = ECGPoints[4] & MASK_BUFF_LEN;
	uint16_t auxStart      = auxPosPeak + WINDOW_T_ON;
	uint16_t auxEnd        = (endPos < (auxPosPeak + WINDOW_T_OFF)) ? endPos : (auxPosPeak + WINDOW_T_OFF);
	uint16_t auxMinPos     = auxStart;
	uint16_t auxMaxPos     = auxStart;
	int16_t  auxMinVal     = 0;
	int16_t  auxMaxVal     = 0;

	uint16_t i;
	for (i = auxStart+1; i < auxEnd; i++) {
		int16_t tempSampleSD = buffSamples14[(i-1) & MASK_BUFF_LEN]
				- 2*buffSamples14[i & MASK_BUFF_LEN] + buffSamples14[(i+1) & MASK_BUFF_LEN];
		if (tempSampleSD < auxMinVal) {
			auxMinVal = tempSampleSD;
			auxMinPos = i;
		}
		if (tempSampleSD > auxMaxVal) {
			auxMaxVal = tempSampleSD;
			auxMaxPos = i;
		}
	}

	if (abs(auxMinVal) >= abs(auxMaxVal)) {
		if (abs(auxMinVal) > abs((5*meanThresholdR) >> 9)) {  // Equivalent to 1%
			ECGPoints[6] = auxMinPos + (buffSamplesOverflow - overflow) * BUFF_LEN;
			auxPosPeak = auxMinPos;
		}
	} else {
		if (abs(auxMaxVal) > abs((5*meanThresholdR) >> 9)) {  // Equivalent to 1%
			ECGPoints[6] = auxMaxPos + (buffSamplesOverflow - overflow) * BUFF_LEN;
			negativeTWave = TRUE;
			auxPosPeak = auxMaxPos;
		}
	}
#endif

#ifdef OP_T
	// T end
	if (ECGPoints[6] > 0) {

		// Tolerance: 2.5% of the T peak amplitude
		int16_t tolerance = (13 * abs(buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN])) >> 9;  // Equivalent to 5%

		// Find the T end
		if (!negativeTWave) {
			int16_t tempThreshold = 20*tolerance;
			uint16_t infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN] > tempThreshold) && infintyLoop) {  // limit: T baseline + 50% of the T amplitude
				auxPosPeak++;
				infintyLoop--;
			}
			tempThreshold = tolerance;
			infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN] > buffSamples40_O_T[(auxPosPeak+1) & MASK_BUFF_LEN]) &&
					(buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN] > tempThreshold) &&
					infintyLoop) {  // limit: T baseline + 2.5% of the T amplitude
				auxPosPeak++;
				infintyLoop--;
			}
		} else {
			int16_t tempThreshold = -20*tolerance;
			uint16_t infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN] < tempThreshold) && infintyLoop) {  // limit: T baseline - 50% of the T amplitude
				auxPosPeak++;
				infintyLoop--;
			}
			tempThreshold = -tolerance;
			infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN] < buffSamples40_O_T[(auxPosPeak+1) & MASK_BUFF_LEN]) &&
					(buffSamples40_O_T[auxPosPeak & MASK_BUFF_LEN] < tempThreshold) &&
					infintyLoop) {  // limit: T baseline - 2.5% of the T amplitude
				auxPosPeak++;
				infintyLoop--;
			}
		}

		uint32_t auxToff = auxPosPeak + (buffSamplesOverflow - overflow) * BUFF_LEN;
		if (auxToff < tempECGPoints[4]) {
			ECGPoints[7] = auxToff;
		}

	}
#endif

}
#endif


#if (defined(BASELINE) || defined(P_PEAK) || defined(OP_P))
/*
 * Delineate the P and Q waves for the current beat.
 * @param *startPos    Position of the expected P peak
 * @param *endPos      Position of the current R peak
 * @param *overflow    If the buffer has been overflowed
 */
void delineatePQWaves(uint16_t startPos, uint16_t endPos, uint16_t overflow) {

#ifdef BASELINE
	/*
	 * Delineate the Q wave
	 */

	// Tolerance: 5% of the R peak amplitude;
	int16_t tolerance = (13 * abs(buffSamples40[startPos & MASK_BUFF_LEN])) >> 8;  // Equivalent to 5%
	int16_t tempThreshold = 10*tolerance;

	// Find the 50% of the R peak amplitude
	uint16_t infintyLoop = BUFF_LEN;
	while ((buffSamples40[endPos & MASK_BUFF_LEN] > tempThreshold) && infintyLoop) {
		endPos -= 1;
		infintyLoop--;
	}
	//endPos -= 1;
	if (startPos >= endPos) {
		return;
	}

	// Buscar R on
	tempThreshold = tolerance;
	while ((buffSamples40[endPos & MASK_BUFF_LEN] > tempThreshold) &&
			(buffSamples40[endPos & MASK_BUFF_LEN] > buffSamples40[(endPos-1) & MASK_BUFF_LEN])) {
		endPos -= 1;
	}
	if (startPos >= endPos) {
		return;
	}
	uint16_t tempPosOff = endPos;  // Position of the R on and the Q end (they are the same points)
	tempECGPoints[3] = endPos + (buffSamplesOverflow - overflow) * BUFF_LEN;

	// Find the minimum to the left (the Q peak)
	infintyLoop = BUFF_LEN;
	while ((buffSamples40[endPos & MASK_BUFF_LEN] >= buffSamples40[(endPos-1) & MASK_BUFF_LEN]) && infintyLoop) {
		endPos -= 1;
		infintyLoop--;
	}
	//endPos -= 1;
	if (startPos >= endPos) {
		return;
	}

	tempThreshold = -tolerance;

	if (buffSamples40[endPos & MASK_BUFF_LEN] < tempThreshold) {  // The minimum is lower than the threshold, is valid

		// Find the Q on
		while ((buffSamples40[endPos & MASK_BUFF_LEN] < tempThreshold) &&
				(buffSamples40[endPos & MASK_BUFF_LEN] < buffSamples40[(endPos-1) & MASK_BUFF_LEN])) {
			endPos -= 1;
		}
		if (startPos >= endPos) {
			return;
		}

		// The Q wave is valid if its width is lower than 100 ms
		if ((endPos != tempPosOff) && ((tempPosOff - endPos) < T_Q_WAVE)) {
			// Small correction for the Q on
			uint16_t tempStop = endPos - CORRECT_Q_ON;
			if (tempStop < startPos) {
				tempStop = startPos;
			}
			for (; endPos >= tempStop; (endPos)--) {
				if (buffSamples40[endPos & MASK_BUFF_LEN] > buffSamples40[(endPos-1) & MASK_BUFF_LEN]) {
					tempECGPoints[3] = endPos + (buffSamplesOverflow - overflow) * BUFF_LEN;
					break;
				}
			}
			tempECGPoints[3] = endPos + (buffSamplesOverflow - overflow) * BUFF_LEN;
		}

	}
#endif

#ifdef P_PEAK
	/*
	 * Delineate the P wave
	 */
	uint16_t negativePWave = FALSE;
	uint16_t auxPosPeak    = (overflow == 0) ? tempECGPoints[4] & MASK_BUFF_LEN : (tempECGPoints[4] & MASK_BUFF_LEN) + BUFF_LEN;
	uint16_t auxEnd        = auxPosPeak - WINDOW_P_OFF;
	uint16_t auxStart      = (startPos > (auxPosPeak - WINDOW_P_ON)) ? startPos : (auxPosPeak - WINDOW_P_ON);
	uint16_t auxMinPos     = auxEnd;
	uint16_t auxMaxPos     = auxEnd;
	int16_t  auxMinVal     = 0;
	int16_t  auxMaxVal     = 0;

	uint16_t i;
	for (i = auxEnd-1; i > auxStart; i--) {
		int16_t tempSampleSD = buffSamples14[(i-1) & MASK_BUFF_LEN]
				- 2 * buffSamples14[i & MASK_BUFF_LEN] + buffSamples14[(i+1) & MASK_BUFF_LEN];
		if (tempSampleSD < auxMinVal) {
			auxMinVal = tempSampleSD;
			auxMinPos = i;
		}
		if (tempSampleSD > auxMaxVal) {
			auxMaxVal = tempSampleSD;
			auxMaxPos = i;
		}
	}

	if (abs(auxMinVal) >= abs(auxMaxVal)) {
		if (abs(auxMinVal) > abs((5*meanThresholdR) >> 9)) {  // Equivalent to 1%
			tempECGPoints[1] = auxMinPos + (buffSamplesOverflow - overflow) * BUFF_LEN;
			auxPosPeak = auxMinPos;
		}
	} else {
		if (abs(auxMaxVal) > abs((5*meanThresholdR) >> 9)) {  // Equivalent to 1%
			tempECGPoints[1] = auxMaxPos + (buffSamplesOverflow - overflow) * BUFF_LEN;
			negativePWave = TRUE;
			auxPosPeak = auxMaxPos;
		}
	}
#endif

#ifdef OP_P
	// P on and end
	if (tempECGPoints[1] > 0) {

		// Tolerance: 2.5% of the P peak amplitude
		int16_t tolerance = (13 * abs(buffSamples40_O_P[auxPosPeak & MASK_BUFF_LEN])) >> 9;  // Equivalent to 5%

		// Buscar P on
		auxStart = auxPosPeak;

		// Find the P on
		if (!negativePWave) {
			int16_t tempThreshold = 20*tolerance;
			uint16_t infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxStart & MASK_BUFF_LEN] > tempThreshold) && infintyLoop) {  // limit: P baseline + 50% of the P amplitude
				auxStart--;
				infintyLoop--;
			}
			tempThreshold = tolerance;
			infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxStart & MASK_BUFF_LEN] > buffSamples40_O_P[(auxStart-1) & MASK_BUFF_LEN]) &&
					(buffSamples40_O_P[auxStart & MASK_BUFF_LEN] > tempThreshold) &&
					infintyLoop) {  // limit: P baseline + 2.5% of the P amplitude
				auxStart--;
				infintyLoop--;
			}
		} else {
			int16_t tempThreshold = -20*tolerance;
			uint16_t infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxStart & MASK_BUFF_LEN] < tempThreshold) && infintyLoop) {  // limit: P baseline - 50% of the P amplitude
				auxStart--;
				infintyLoop--;
			}
			tempThreshold = -tolerance;
			infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxStart & MASK_BUFF_LEN] < buffSamples40_O_P[(auxStart-1) & MASK_BUFF_LEN]) &&
					(buffSamples40_O_P[auxStart & MASK_BUFF_LEN] < tempThreshold) &&
					infintyLoop) {  // limit: P baseline - 2.5% of the P amplitude
				auxStart--;
				infintyLoop--;
			}
		}

		uint32_t auxPon = auxStart + (buffSamplesOverflow - overflow) * BUFF_LEN;
		if (auxPon > ECGPoints[4]) {
			tempECGPoints[0] = auxPon;
		}

		// Find the P end
		auxEnd = auxPosPeak;

		if (!negativePWave) {
			int16_t tempThreshold = 20*tolerance;
			uint16_t infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxEnd & MASK_BUFF_LEN] > tempThreshold) && infintyLoop) {  // limit: P baseline + 50% of the P amplitude
				auxEnd++;
				infintyLoop--;
			}
			tempThreshold = tolerance;
			infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxEnd & MASK_BUFF_LEN] > buffSamples40_O_P[(auxEnd+1) & MASK_BUFF_LEN]) &&
					(buffSamples40_O_P[auxEnd & MASK_BUFF_LEN] > tempThreshold) &&
					infintyLoop) {  // limit: P baseline + 2.5% of the P amplitude
				auxEnd++;
				infintyLoop--;
			}
		} else {
			int16_t tempThreshold = -20*tolerance;
			uint16_t infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxEnd & MASK_BUFF_LEN] < tempThreshold) && infintyLoop) {  // limit: P baseline - 50% of the P amplitude
				auxEnd++;
				infintyLoop--;
			}
			tempThreshold = -tolerance;
			infintyLoop = BUFF_LEN;
			while ((buffSamples40_O_P[auxEnd & MASK_BUFF_LEN] < buffSamples40_O_P[(auxEnd+1) & MASK_BUFF_LEN]) &&
					(buffSamples40_O_P[auxEnd & MASK_BUFF_LEN] < tempThreshold) &&
					infintyLoop) {  // limit: P baseline - 2.5% of the P amplitude
				auxEnd++;
				infintyLoop--;
			}
		}

		uint32_t auxPoff = auxEnd + (buffSamplesOverflow - overflow) * BUFF_LEN;
		if (auxPoff < tempECGPoints[4]) {
			tempECGPoints[2] = auxPoff;
		}

	}
#endif

}
#endif
