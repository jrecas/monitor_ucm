/////////////////////////////////////////////////////////////////////////////////////////
/// \file main.c
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.0
/// \date November 2011
/////////////////////////////////////////////////////////////////////////////////////////

/// NOTE COPIA DE LO QUE HAY EN EL MAKEFILE PARA FACILITAR LA NAVEGACIÓN DE CÓDIGO
//#define GCC
//#define MSP430F1611
//#define SHIMMER2

#include "platform.h"
//#include "BlueTooth.h"
//#include "adcShimmer.h"

//Debug
//#include "leds.h"
#include "serialPrint.h"
//#include "nopDelay.h"

// Delineacion
#include "FIRFilter.h"
#include "BaselineFilter.h"
#include "DelineatorECG.h"
#include <stdio.h>
#include <stdlib.h>

void initTimerA(void);



void delineate(int16_t raw_sample){
	
	// Filtrados FIR
	int16_t FIR_filt_samples[2]={0,0};	// Sera un array con 2 variables (de 14 Hz en [0] y de 40 Hz en [1])
	int16_t BL_filt_sample = 0;
	int16_t openingPSample = 0;
	int16_t openingTSample = 0;
	
	filterSampleFIR(raw_sample, FIR_filt_samples);
	//serialTX(1, (FIR_filt_samples[0] & 0xFF00) >> 8);
	//serialTX(1, (FIR_filt_samples[0] & 0xFF));
	
	if (mode == FULL_MODE) {
		#ifdef BASELINE
		BL_filt_sample = filterSampleBL(FIR_filt_samples[1]);	// Filtrado baseline
		#endif
		#ifdef OP_P
		openingPSample = openingP(FIR_filt_samples[1]);		// Opening para P
		#endif
		#ifdef OP_T
		openingTSample = openingT(FIR_filt_samples[1]);		// Opening para T
		#endif
	}
	
	// Delinear y guardar la delineacion, TRUE si se detecta un nuevo latido
	if (storeSamples(FIR_filt_samples, BL_filt_sample, openingPSample, openingTSample) == TRUE) {
		uint8_t *p = (uint8_t *) ECGPoints;
		uint8_t i;
		for (i = 0; i < sizeof(ECGPoints); i++) {
			serialTX(1, *(p + i));    // Para simulador
			//serialTX(0, *(p + i));  // Para hardware Shimmer
		}
	}
	
}


/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Main function
/////////////////////////////////////////////////////////////////////////////////////////
int main(void) {
	
	if(initPlatform(MCU_FREQ)==FAIL) {
		while(1);
	}
	
	//initLEDs();
	//disableBT();
	//initAdcSh();
	
	if(serialPortInit(0, MCU_FREQ, BAUDRATE_9600, NULL) == FAIL) {
		//allOnLEDs();
		while(1);
	}
	//serialPutStr(0, "Serial Port 0 Initialized\n");
	
	//Consola
	if(serialPortInit(1, MCU_FREQ, BAUDRATE_9600, NULL) == FAIL) {
		//allOnLEDs();
		while(1);
	}
	//serialPutStr(1, "Serial Port 1 Initialized\n");
	
	//initTimerA();
	//_EINT();
	
	for (;;) {
		
		uint8_t data0;
		uint8_t data1;
		
		if (mode == SAVE_MODE) {
			int16_t numSample = 4;
			int i;
			for (i = 0; i < numSample; i++) {
				serialTX(0,0);
				data1 = serialRX(0);
				serialTX(0,0);
				data0 = serialRX(0);
			}
		}
		
		serialTX(0,0);
		data1 = serialRX(0);
		serialTX(0,0);
		data0 = serialRX(0);
		
		//Delineacion
		int16_t raw_sample = (int16_t) ((data1 << 8) + data0);
		delineate(raw_sample);
		
		//toggleYellowLED();
		//__delay_cycle(100);
		//msWaitNOP(500);
		//serialPutStr("Ok!");
		//_no_operation();
	}
	
}

/*
 * /////////////////////////////////////////////////////////////////////////////////////////
 * /// \brief Setup TimerA
 * /////////////////////////////////////////////////////////////////////////////////////////
 * void initTimerA() {
 * //Timer A control
 * //Ensure the timer is stopped.
 * TACTL = 0;
 * _BIC_SR(OSCOFF);
 * 
 * //Run the timer of the ACLK.
 * TACTL = TASSEL_1;
 * //Clear everything to start with.
 * TACTL |= TACLR;
 * //Set the compare match value according to the tick rate we want.
 * TACCR0 = 131; //32768/250
 * //Period= TACCR0/ACLK -> 131/32768 = 3'997803ms
 * //TACCR0 = 16384; test a 0.5Hz
 * //Enable the interrupts.
 * TACCTL0 = CCIE;
 * //Start up clean.
 * TACTL |= TACLR;
 * //Up mode.
 * TACTL |= MC_1;
 * }
 * 
 * // TimerA interrupt
 * __attribute__((__interrupt__(TIMERA0_VECTOR)))
 * void Timer_A (void){
 * unsigned int data = ADC12MEM0;//multiplier;
 * 
 * ADC12CTL0 |= ADC12SC; //Start conversion
 * 
 * toggleRedLED();
 * 
 * }
 */
