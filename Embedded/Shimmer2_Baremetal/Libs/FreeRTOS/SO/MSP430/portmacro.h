/*
    FreeRTOS V6.0.0 - Copyright (C) 2009 Real Time Engineers Ltd.

    ***************************************************************************
    *                                                                         *
    * If you are:                                                             *
    *                                                                         *
    *    + New to FreeRTOS,                                                   *
    *    + Wanting to learn FreeRTOS or multitasking in general quickly       *
    *    + Looking for basic training,                                        *
    *    + Wanting to improve your FreeRTOS skills and productivity           *
    *                                                                         *
    * then take a look at the FreeRTOS eBook                                  *
    *                                                                         *
    *        "Using the FreeRTOS Real Time Kernel - a Practical Guide"        *
    *                  http://www.FreeRTOS.org/Documentation                  *
    *                                                                         *
    * A pdf reference manual is also available.  Both are usually delivered   *
    * to your inbox within 20 minutes to two hours when purchased between 8am *
    * and 8pm GMT (although please allow up to 24 hours in case of            *
    * exceptional circumstances).  Thank you for your support!                *
    *                                                                         *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public 
    License and the FreeRTOS license exception along with FreeRTOS; if not it 
    can be viewed here: http://www.freertos.org/a00114.html and also obtained 
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

#ifndef PORTMACRO_H
#define PORTMACRO_H

//#include "platform.h"

#ifdef __cplusplus
extern "C" {
#endif

/////////////////////////////////////////////////////////////////////////////////////////
/// \file portmacro.h
/// \brief Port specific definitions
///
/// The settings in this file configure FreeRTOS correctly for the given hardware and
/// compiler.
/// \attention These settings should not be altered
/////////////////////////////////////////////////////////////////////////////////////////
 

/* Type definitions. */
#define portCHAR		    char                 ///< FreeRTOS 8b variable
#define portFLOAT		    float                ///< FreeRTOS 2b variable
#define portDOUBLE		  double               ///< FreeRTOS 32b variable
#define portLONG		    long                 ///< FreeRTOS 32b variable
#define portSHORT		    int                  ///< FreeRTOS 16b variable

#ifdef MSP430F1611
  #define portSTACK_TYPE	unsigned portSHORT   ///< FreeRTOS stack type data (16b) 
#endif
#ifdef MSP430F5438A
  #define portSTACK_TYPE  unsigned portLONG    ///< FreeRTOS stack type data (32b) 
#endif
#ifdef MSP430F6638
  #define portSTACK_TYPE  unsigned portLONG    ///< FreeRTOS stack type data (32b) 
#endif

#ifndef portSTACK_TYPE
	#error "Microcontroller not defined (MSP430F1611 or MSP430F5438A or MSP430F6638 Expected)"
#endif

#define portBASE_TYPE	  portSHORT            ///< FreeRTOS base data type (16b)

#if( configUSE_16_BIT_TICKS == 1 )
	typedef unsigned portSHORT portTickType;                ///< The tick variable will be 16b
	#define portMAX_DELAY ( portTickType ) 0xffff           ///< Maximum delay
#else
	typedef unsigned portLONG portTickType;                 ///< The tick variable will be 16b
	#define portMAX_DELAY ( portTickType ) 0xffffffff       ///< Maximum delay
#endif

/*-----------------------------------------------------------*/	
askldjfkjasflljafs
/* Interrupt control macros. */
#if defined(CCS)
  #define portDISABLE_INTERRUPTS()   __disable_interrupt()   ///< Disable Interrupts control macro (Intrinsic IAR function -> 'in430.h')
  #define portENABLE_INTERRUPTS()    __enable_interrupt()    ///< Enable  Interrupts control macro (Intrinsic IAR function -> 'in430.h')
#endif
#if defined(GCC_TI)
  #define portDISABLE_INTERRUPTS()   _disable_interrupts()
  #define portENABLE_INTERRUPTS()    _enable_interrupts()
#endif
#ifdef GCC
  #define portDISABLE_INTERRUPTS()    asm volatile ( "DINT" )
  #define portENABLE_INTERRUPTS()     asm volatile ( "EINT" )
#endif
#ifndef portDISABLE_INTERRUPTS
  #error "Compiler not defined (CCS or GCC)"
#endif
	
/*-----------------------------------------------------------*/

/* Critical section control macros. */
#define portNO_CRITICAL_SECTION_NESTING		( ( unsigned portSHORT ) 0 )   ///< Critical section control for no nesting


/////////////////////////////////////////////////////////////////////////////////////////
///  \def portENTER_CRITICAL()
///  \brief Enter critical section
/////////////////////////////////////////////////////////////////////////////////////////

#define portENTER_CRITICAL()		                                        \
{                                                                           \
extern volatile unsigned portSHORT usCriticalNesting;                       \
                                                                            \
  portDISABLE_INTERRUPTS();                                                 \
                                                                            \
  /* Now interrupts are disabled usCriticalNesting can be accessed */       \
  /* directly.  Increment ulCriticalNesting to keep a count of how many */  \
  /* times portENTER_CRITICAL() has been called. */                         \
  usCriticalNesting++;                                                      \
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \def portEXIT_CRITICAL()
///  \brief Exit critical section
/////////////////////////////////////////////////////////////////////////////////////////
#define portEXIT_CRITICAL()                                                 \
{                                                                           \
extern volatile unsigned portSHORT usCriticalNesting;                       \
                                                                            \
  if( usCriticalNesting > portNO_CRITICAL_SECTION_NESTING )                 \
  {                                                                         \
    /* Decrement the nesting count as we are leaving a critical section. */ \
    usCriticalNesting--;                                                    \
                                                                            \
    /* If the nesting level has reached zero then interrupts should be */   \
    /* re-enabled. */                                                       \
    if( usCriticalNesting == portNO_CRITICAL_SECTION_NESTING )              \
    {                                                                       \
      portENABLE_INTERRUPTS();                                              \
    }                                                                       \
  }                                                                         \
}
/*-----------------------------------------------------------*/

/* Task utilities. */

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Manual context switch called by portYIELD or taskYIELD.
/////////////////////////////////////////////////////////////////////////////////////////
extern void vPortYield( void );
#define portYIELD() vPortYield()                 ///< Yield CPU
/*-----------------------------------------------------------*/

/* Hardware specifics. */
#define portBYTE_ALIGNMENT      2                                                 ///< Byte alignment in RAM
#define portSTACK_GROWTH      ( -1 )                                              ///< Stack grow in RAM
#define portTICK_RATE_MS      ( ( portTickType ) 1000 / configTICK_RATE_HZ )      ///< Tick rate in ms
#define portTICK_RATE_US      ( ( portTickType ) 1000000 / configTICK_RATE_HZ )   ///< Tick rate in ms
//#define portNOP()  asm volatile ( "NOP" )
#ifdef CCS
  #define portNOP()      _nop()                                                   ///< NOP instruction defintion
#endif
#ifdef GCC
  #define portNOP()              asm volatile ( "NOP" )
#endif
/*-----------------------------------------------------------*/

/* Task function macros as described on the FreeRTOS.org WEB site. */
/////////////////////////////////////////////////////////////////////////////////////////
///  \def portTASK_FUNCTION_PROTO()
///  \brief Task function prototype
///  \param vFunction function name
///  \param pvParameters pointer to function parameters
/////////////////////////////////////////////////////////////////////////////////////////
#define portTASK_FUNCTION_PROTO( vFunction, pvParameters ) void vFunction( void *pvParameters )
/////////////////////////////////////////////////////////////////////////////////////////
///  \def portTASK_FUNCTION()
///  \brief Task function prototype
///  \param vFunction function name
///  \param pvParameters pointer to function parameters
/////////////////////////////////////////////////////////////////////////////////////////
#define portTASK_FUNCTION( vFunction, pvParameters )       void vFunction( void *pvParameters ) 
 

#endif /* PORTMACRO_H */

