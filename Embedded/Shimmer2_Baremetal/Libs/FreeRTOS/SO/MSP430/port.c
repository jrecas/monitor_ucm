/*
    FreeRTOS V6.0.5 - Copyright (C) 2009 Real Time Engineers Ltd.

    ***************************************************************************
    *                                                                         *
    * If you are:                                                             *
    *                                                                         *
    *    + New to FreeRTOS,                                                   *
    *    + Wanting to learn FreeRTOS or multitasking in general quickly       *
    *    + Looking for basic training,                                        *
    *    + Wanting to improve your FreeRTOS skills and productivity           *
    *                                                                         *
    * then take a look at the FreeRTOS eBook                                  *
    *                                                                         *
    *        "Using the FreeRTOS Real Time Kernel - a Practical Guide"        *
    *                  http://www.FreeRTOS.org/Documentation                  *
    *                                                                         *
    * A pdf reference manual is also available.  Both are usually delivered   *
    * to your inbox within 20 minutes to two hours when purchased between 8am *
    * and 8pm GMT (although please allow up to 24 hours in case of            *
    * exceptional circumstances).  Thank you for your support!                *
    *                                                                         *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public 
    License and the FreeRTOS license exception along with FreeRTOS; if not it 
    can be viewed here: http://www.freertos.org/a00114.html and also obtained 
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

/////////////////////////////////////////////////////////////////////////////////////////
/// \file port.c
/// \brief Implementation of functions defined in \a portable.h for the MSP430 port.
///
/// Ported by Joaquin Recas (jrecas@fis.ucm.es)
///
/////////////////////////////////////////////////////////////////////////////////////////

//Standard includes.
#include <stdlib.h>
#include <signal.h>

#include "platform.h"

//Scheduler includes.
#include "FreeRTOS.h"
#include "task.h"

#ifdef MSP430F1611
  //Constants required for hardware setup.  The tick ISR runs off the MCLK, not the ACLK.
  //#define portACLK_FREQUENCY_HZ			    ( ( portTickType )       32768 )       ///< Frequency of Auxiliar CLK
  #define portSMCLK_FREQUENCY_HZ			    ( ( portTickType ) configCPU_CLOCK_HZ )       ///< Frequency of Auxiliar CLK
#endif	

#ifdef EXP430
//#ifdef EXP430F5438
  //Constants required for hardware setup.  The tick ISR runs off the MCLK, not the ACLK.
  //#define portACLK_FREQUENCY_HZ			    ( ( portTickType )       32768 )               ///< Frequency of Auxiliar CLK
  #define portSMCLK_FREQUENCY_HZ			    ( ( portTickType ) configCPU_CLOCK_HZ )      ///< Frequency of Auxiliar CLK
#endif	

#ifndef portSMCLK_FREQUENCY_HZ
	#error "Platform not defined (SHIMMER or EXP430 Expected)"
#endif

#define portINITIAL_CRITICAL_NESTING      ( ( unsigned portSHORT )      10 )     ///< Initial value of Critical Nesting
#define portFLAGS_INT_ENABLED			        ( ( portSTACK_TYPE )        0x08 )     ///< Initial value for enabled interrupts

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief We require the address of the pxCurrentTCB variable, but don't want to know any 
/// details of its type.
/////////////////////////////////////////////////////////////////////////////////////////
typedef void tskTCB;
extern volatile tskTCB * volatile pxCurrentTCB;

/////////////////////////////////////////////////////////////////////////////////////////
/// \var usCriticalNesting
/// \brief Critical section nesting counter
///
/// Most ports implement critical sections by placing the interrupt flags on
/// the stack before disabling interrupts.  Exiting the critical section is then
/// simply a case of popping the flags from the stack.  As mspgcc does not use
/// a frame pointer this cannot be done as modifying the stack will clobber all
/// the stack variables.  Instead each task maintains a count of the critical
/// section nesting depth.  Each time a critical section is entered the count is
/// incremented.  Each time a critical section is left the count is decremented -
/// with interrupts only being re-enabled if the count is zero.
/// 
/// usCriticalNesting will get set to zero when the scheduler starts, but must
/// not be initialised to zero as this will cause problems during the startup
/// sequence.
/////////////////////////////////////////////////////////////////////////////////////////
volatile unsigned portSHORT usCriticalNesting = portINITIAL_CRITICAL_NESTING;

#ifdef GCC 
  #ifdef MSP430F1611
  	/////////////////////////////////////////////////////////////////////////////////////////
  	///
  	///  \brief Macro to save a task context to the task stack.
  	///
  	///  This simply pushes all the  general purpose msp430 registers onto the stack, followed
  	///  by the usCriticalNesting value used by the task.  Finally the resultant stack pointer
  	///  value is saved into the task control block so it can be retrieved the next time the
  	///  task executes.
  	///
  	/////////////////////////////////////////////////////////////////////////////////////////
  	#define portSAVE_CONTEXT()                                      \
  	  asm volatile (    "push    r4                        \n\t"    \
  	                    "push    r5                        \n\t"    \
  	                    "push    r6                        \n\t"    \
  	                    "push    r7                        \n\t"    \
  	                    "push    r8                        \n\t"    \
  	                    "push    r9                        \n\t"    \
  	                    "push    r10                       \n\t"    \
  	                    "push    r11                       \n\t"    \
  	                    "push    r12                       \n\t"    \
  	                    "push    r13                       \n\t"    \
  	                    "push    r14                       \n\t"    \
  	                    "push    r15                       \n\t"    \
  	                    "mov.w   usCriticalNesting, r14    \n\t"    \
  	                    "push    r14                       \n\t"    \
  	                    "mov.w   pxCurrentTCB, r12         \n\t"    \
  	                    "mov.w   r1, @r12                  \n\t"    \
  	               );
  	
  	/////////////////////////////////////////////////////////////////////////////////////////
  	///
  	///  \brief Macro to restore a task context from the task stack.
  	///  This is effectively the reverse of portSAVE_CONTEXT().  First the stack pointer
  	///  value is loaded from the task control block.  Next the value for usCriticalNesting
  	///  used by the task is retrieved from the stack - followed by the value of all the
  	///  general purpose msp430 registers.
  	///
  	///  The bic instruction ensures there are no low power bits set in the status
  	///  register that is about to be popped from the stack.
  	///
  	/////////////////////////////////////////////////////////////////////////////////////////
  	#define portRESTORE_CONTEXT()                                   \
  	  asm volatile (    "mov.w   pxCurrentTCB, r12         \n\t"    \
  	                    "mov.w   @r12, r1                  \n\t"    \
  	                    "pop     r15                       \n\t"    \
  	                    "mov.w   r15, usCriticalNesting    \n\t"    \
  	                    "pop     r15                       \n\t"    \
  	                    "pop     r14                       \n\t"    \
  	                    "pop     r13                       \n\t"    \
  	                    "pop     r12                       \n\t"    \
  	                    "pop     r11                       \n\t"    \
  	                    "pop     r10                       \n\t"    \
  	                    "pop     r9                        \n\t"    \
  	                    "pop     r8                        \n\t"    \
  	                    "pop     r7                        \n\t"    \
  	                    "pop     r6                        \n\t"    \
  	                    "pop     r5                        \n\t"    \
  	                    "pop     r4                        \n\t"    \
  	                    "bic     #(0xf0),0(r1)             \n\t"    \
  	                    "reti                              \n\t"    \
  	               );
  #endif
#endif

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Sets up the periodic ISR used for the RTOS tick.
///
///  This uses timer 0, but could have alternatively used the watchdog timer or timer 1.
/////////////////////////////////////////////////////////////////////////////////////////
void prvSetupTimerInterrupt( void );

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Initialise the stack of a task to look exactly as if a call to 
/// \a portSAVE_CONTEXT had been called.
/// 
/// See the header file \a portable.h.
/////////////////////////////////////////////////////////////////////////////////////////
portSTACK_TYPE *pxPortInitialiseStack( portSTACK_TYPE *pxTopOfStack, pdTASK_CODE pxCode, void *pvParameters ){

	//Place a few bytes of known values on the bottom of the stack. 
	//This is just useful for debugging and can be included if required.

	//*pxTopOfStack = ( portSTACK_TYPE ) 0x1111;
	//pxTopOfStack--;
	//*pxTopOfStack = ( portSTACK_TYPE ) 0x2222;
	//pxTopOfStack--;
	//*pxTopOfStack = ( portSTACK_TYPE ) 0x3333;
	//pxTopOfStack--; 

	//The msp430 automatically pushes the PC then SR onto the stack before 
	//executing an ISR.  We want the stack to look just as if this has happened
	//so place a pointer to the start of the task on the stack first - followed
	//by the flags we want the task to use when it starts up.
  #ifdef EXP430
	  *pxTopOfStack = ((((portSTACK_TYPE)pxCode)&0xFFFF) << 16) | ( (portFLAGS_INT_ENABLED&0xFFF) | ( (((portSTACK_TYPE)pxCode)>>4) & 0xF000) );
	  pxTopOfStack--;
  #else
    *pxTopOfStack = ( portSTACK_TYPE ) pxCode;
    pxTopOfStack--;
    *pxTopOfStack = portFLAGS_INT_ENABLED;
    pxTopOfStack--;
  #endif

	//Next the general purpose registers.
	*pxTopOfStack = ( portSTACK_TYPE ) 0x44444;               //R4
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x55555;               //R5
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x66666;               //R6
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x77777;               //R7
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x88888;               //R8
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0x99999;               //R9
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0xaaaaa;               //R10
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0xbbbbb;               //R11
	pxTopOfStack--;

  #ifdef GCC
	  *pxTopOfStack = ( portSTACK_TYPE ) 0xccccc;             //R12
	  pxTopOfStack--;
  #endif
  #ifdef CCS
	  //When the task starts is will expect to find the function parameter in R12.
	  *pxTopOfStack = ( portSTACK_TYPE ) pvParameters;
	  pxTopOfStack--;
  #endif
	
	*pxTopOfStack = ( portSTACK_TYPE ) 0xddddd;               //R13
	pxTopOfStack--;
	*pxTopOfStack = ( portSTACK_TYPE ) 0xeeeee;               //R14
	pxTopOfStack--;

  #ifdef GCC
    //When the task starts is will expect to find the function parameter in R15.
    *pxTopOfStack = ( portSTACK_TYPE ) pvParameters;
    pxTopOfStack--;
  #endif
  #ifdef CCS
	  *pxTopOfStack = ( portSTACK_TYPE ) 0xfffff;             //R15
	  pxTopOfStack--;
  #endif

	//The code generated by the mspgcc compiler does not maintain separate
	//stack and frame pointers. The portENTER_CRITICAL macro cannot therefore
	//use the stack as per other ports.  Instead a variable is used to keep
	//track of the critical section nesting.  This variable has to be stored
	//as part of the task context and is initially set to zero. */
	*pxTopOfStack = ( portSTACK_TYPE ) portNO_CRITICAL_SECTION_NESTING;	

	//Return a pointer to the top of the stack we have generated so this can
	//be stored in the task control block for the task. */
	return pxTopOfStack;
}

#ifdef GCC
  /////////////////////////////////////////////////////////////////////////////////////////
  ///  \brief Init Scheduler.
  ///
  ///  @return pdTRUE
  /////////////////////////////////////////////////////////////////////////////////////////
  portBASE_TYPE xPortStartScheduler( void ){
    //Setup the hardware to generate the tick. 
    //Interrupts are disabled when this function is called.
    prvSetupTimerInterrupt();
  
    //Restore the context of the first task that is going to run.
    portRESTORE_CONTEXT();
  
    //Should not get here as the tasks are now running!
    return pdTRUE;
  }
  #ifdef MSP430F1611
    /////////////////////////////////////////////////////////////////////////////////////////
    /// \brief Manual context switch called by portYIELD or taskYIELD.
    ///
    ///  The first thing we do is save the registers so we can use a naked attribute.
    /////////////////////////////////////////////////////////////////////////////////////////
    void vPortYield( void ) __attribute__ ( ( naked ) );
    void vPortYield( void ){
      //We want the stack of the task being saved to look exactly as if the task
      //was saved during a pre-emptive RTOS tick ISR.  Before calling an ISR the
      //msp430 places the status register onto the stack.  As this is a function
      //call and not an ISR we have to do this manually.
      asm volatile ( "push    r2" );
      _DINT();
    
      //Save the context of the current task.
      portSAVE_CONTEXT();
    
      //Switch to the highest priority task that is ready to run.
      vTaskSwitchContext();
    
      //Restore the context of the new task.
      portRESTORE_CONTEXT();
    }
  #endif
  
  
  /////////////////////////////////////////////////////////////////////////////////////////
  ///  The interrupt service routine used depends on whether the pre-emptive
  ///  scheduler is being used or not.
  /////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////
  ///  \brief Tick ISR for preemptive scheduler.  We can use a naked attribute as
  ///  the context is saved at the start of vPortYieldFromTick().  The tick
  ///  count is incremented after the context is saved.
  /////////////////////////////////////////////////////////////////////////////////////////

  /////////////////////////////////////////////////////////////////////////////////////////
  ///  \brief Tick ISR for the cooperative scheduler.  All this does is increment the
  ///  tick count.  We don't need to switch context, this can only be done by
  ///  manual calls to taskYIELD();
  /////////////////////////////////////////////////////////////////////////////////////////

  //interrupt (TIMERA0_VECTOR) wakeup prvTickISR( void ) __attribute__ ( ( naked ) );  //naked -> Do not generate a prologue or epilogue for the function.
  //interrupt (TIMERA0_VECTOR) wakeup prvTickISR( void ){
  interrupt (TIMERA0_VECTOR) prvTickISR( void ) __attribute__ ( ( naked ) );  //naked -> MANDATORY (NOT CCS SUPPORT) Do not generate a prologue or epilogue for the function.
  interrupt (TIMERA0_VECTOR) prvTickISR( void ){
    #if configUSE_PREEMPTION == 1
      //Save the context of the interrupted task.
      portSAVE_CONTEXT();
    #endif

    //Increment the tick count then switch to the highest priority task that is ready to run.
    vTaskIncrementTick();
    
    #if configUSE_PREEMPTION == 1
      vTaskSwitchContext();
      //Restore the context of the new task.
      portRESTORE_CONTEXT();
    #endif
  }
#endif

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief It is unlikely that the MSP430 port will get stopped.  If required simply
/// disable the tick interrupt here.
/////////////////////////////////////////////////////////////////////////////////////////
void vPortEndScheduler( void ){}

/////////////////////////////////////////////////////////////////////////////////////////
/// Hardware initialisation to generate the RTOS tick.  This uses timer 0
/// but could alternatively use the watchdog timer or timer 1. 
/////////////////////////////////////////////////////////////////////////////////////////
void prvSetupTimerInterrupt( void ){
  #ifdef MSP430F1611
		//Set the compare match value according to the tick rate we want
		//TACCR0 = portACLK_FREQUENCY_HZ / configTICK_RATE_HZ;
		//TACCR0 = (portACLK_FREQUENCY_HZ / configTICK_RATE_HZ) -1;
		TACCR0 = (portSMCLK_FREQUENCY_HZ / configTICK_RATE_HZ) -1;
		
		//TACCR0 = 64-1;
		//Enable the interrupts
		TACCTL0 = CCIE;
	
	  // TACTL    -> TimerA Control Register
	  // .TASSEL  = 2 source SMCLK = XT2CLK/4 (1MHz)
	  // .ID      = 0 input divisor of 1
	  // .MC      = 1 TimerA Up mode
	  // .TACLR   = 0 reset timer A
	  // .TAIE    = 0 disable timer A interrupts
	  // .TAIF    = 0 clear interrupt pending
		//TACTL = TASSEL_1 | MC_1 | TACLR;
		TACTL = TASSEL_2 |  MC_1 | TACLR;
  //MSP430F5438A or MSP430F6638
  #else 
    //Set the compare match value according to the tick rate we want
    TB0CCR0 = (portSMCLK_FREQUENCY_HZ / configTICK_RATE_HZ) -1;

    //Enable the interrupts
    TB0CCTL0 = CCIE;                           // TBCCR0 interrupt enabled
    
    // TBCTL      -> TimerB Control Register
		// .TBCLGRP  =  00 Each TBCLn latch loads independently.
		// .CNTL     =  00 Counter length 16b
		// .TBSSEL   =  10 clock source select SMCLK 
		// .ID       =  00 Input divider /1
		// .MC       =  01 Up mode: Timer counts up to TBCL0
		// .TBCLR    =   1 Timer_B clear
		// .TBIE     =   0 Timer_B interrupt disabled
		// .TBIFG    =   Timer_B interrupt flag
    TB0CTL = TBSSEL_2 + MC_1 + TBCLR;          // SMCLK, upmode, clear TBR
  #endif
}
