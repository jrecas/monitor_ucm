/////////////////////////////////////////////////////////////////////////////////////////
/// \file serialPrint.h
/// \brief Serial COMM utils header file for Shimmer
///
/// Function \a serialPortInit has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date October 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SERIAL_COMMS_SHIMMER_H
#define SERIAL_COMMS_SHIMMER_H

#define  serialGetChar()               (U0RXBUF)

#define  serialPutChar(data){          \
   while ( ! ( IFG1 & UTXIFG0 ) );     \
  U0TXBUF=data;                        \
}
#endif

