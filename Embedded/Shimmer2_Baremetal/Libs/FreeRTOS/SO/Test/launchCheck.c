#include "launchCheck.h"

// Used to detect the idle hook function stalling.
static volatile unsigned long ulIdleLoops = 0UL;

///
//  The function that implements the Check task.  See the comments at the head
//  of the page for implementation details.
///
static void vErrorChecks(void *pvParameters);
///
//  Called by the Check task.  Returns pdPASS if all the other tasks are found
//  to be operating without error - otherwise returns pdFAIL.
///
static short prvCheckOtherTasksAreStillRunning(void);

void launchCheck(void) {
  //Start the standard demo application tasks.
  vStartLEDFlashTasks(mainLED_TASK_PRIORITY);
  vStartIntegerMathTasks(tskIDLE_PRIORITY);
  vStartPolledQueueTasks(mainQUEUE_POLL_PRIORITY);
  //Start the 'Check' task which is defined in this file.
  xTaskCreate(vErrorChecks, (signed char*)"Check", configMINIMAL_STACK_SIZE, NULL, mainCHECK_TASK_PRIORITY, NULL);
}

void testIdleLoop(void){
  ulIdleLoops++;
}

/*-----------------------------------------------------------*/
static void vErrorChecks(void *pvParameters) {
  static volatile unsigned long ulDummyVariable = 3UL;
  portTickType xDelayPeriod = mainNO_ERROR_CHECK_DELAY;

  //serialPutStr("vErrorChecks\n");

  /* Cycle for ever, delaying then checking all the other tasks are still
  operating without error. */
  for (;;) {
    /* Wait until it is time to check again.  The time we wait here depends
    on whether an error has been detected or not.  When an error is
    detected the time is shortened resulting in a faster LED flash rate. */
    vTaskDelay(xDelayPeriod);

    /* Perform a bit of 32bit maths to ensure the registers used by the
    integer tasks get some exercise outside of the integer tasks
    themselves. The result here is not important we are just deliberately
    changing registers used by other tasks to ensure that their context
    switch is operating as required. - see the demo application
      documentation for more info. */
    ulDummyVariable *= 3UL;

    /* See if the other tasks are all ok. */
    if (prvCheckOtherTasksAreStillRunning() != pdPASS) {
      /* An error occurred in one of the tasks so shorten the delay
      period - which has the effect of increasing the frequency of the
      LED toggle. */
      xDelayPeriod = mainERROR_CHECK_DELAY;
    }

    /* Flash! */
    toggleRedLED();
  }
}
/*-----------------------------------------------------------*/

static short prvCheckOtherTasksAreStillRunning(void) {
  static short sNoErrorFound = pdTRUE;
  static unsigned long ulLastIdleLoops = 0UL;

  /* The demo tasks maintain a count that increments every cycle of the task
  provided that the task has never encountered an error.  This function
  checks the counts maintained by the tasks to ensure they are still being
  incremented.  A count remaining at the same value between calls therefore
  indicates that an error has been detected.  Only tasks that do not flash
  an LED are checked. */

  if (xAreIntegerMathsTaskStillRunning() != pdTRUE)
    sNoErrorFound = pdFALSE;

  ///if( xAreComTestTasksStillRunning() != pdTRUE )
  ///  sNoErrorFound = pdFALSE;

  if (xArePollingQueuesStillRunning() != pdTRUE)
    sNoErrorFound = pdFALSE;

  if (ulLastIdleLoops == ulIdleLoops)
    sNoErrorFound = pdFALSE;

  ulLastIdleLoops = ulIdleLoops;

  return sNoErrorFound;
}
/*-----------------------------------------------------------*/
