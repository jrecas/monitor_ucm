#ifndef LAUNCHCKECK_H_
#define LAUNCHCKECK_H_

//#include "platform.h"

//#include "SimpleMAC_Config.h"

#include "flash.h"
#include "integer.h"
#include "partest.h"
#include "PollQ.h"

#define LED_PERIOD   (1000)                      ///< Blink LED test period in ms

/// The frequency at which the 'Check' tasks executes.  See the comments at the
//top of the page.  When the system is operating error free the 'Check' task
//toggles an LED every mainNO_ERROR_CHECK_DELAY us.  If an error is discovered
//in any task the rate is increased to mainERROR_CHECK_DELAY ms.
#define mainNO_ERROR_CHECK_DELAY    ( ( portTickType ) 1000000 / portTICK_RATE_US  )
#define mainERROR_CHECK_DELAY       ( ( portTickType ) 100000  / portTICK_RATE_US  )

// Demo task priorities.
#define mainCHECK_TASK_PRIORITY     ( tskIDLE_PRIORITY + 3 )
#define mainQUEUE_POLL_PRIORITY     ( tskIDLE_PRIORITY + 2 )
#define mainLED_TASK_PRIORITY       ( tskIDLE_PRIORITY + 1 )

void launchCheck(void);
void testIdleLoop(void);


#endif
