#ifndef FREERTOSINC_H_
#define FREERTOSINC_H_

//FreeRTOS Port define
#define FREERTOS_MSP430_PORT

//Configuración del FreeRTOS particular
#include "FreeRTOSConfig.h"

//Por del mimcrocontrolador
#include "portmacro.h"

//Includes del FreeRTOS
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

#endif
