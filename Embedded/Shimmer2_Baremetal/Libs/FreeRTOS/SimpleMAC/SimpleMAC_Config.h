/////////////////////////////////////////////////////////////////////////////////////////
///  \file CC2420.h
///  \brief User Simple 802.15.4 MAC Layer interface for Shimmer
///
///  Function \a simpleMAC_init has to be called before using other functions.
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 1.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SIMPLEMAC_CONFIG_H_
#define SIMPLEMAC_CONFIG_H_

#include <stdint.h>
//FreeRTOS includes
#include "FreeRTOS.h"
#include "queue.h"
#include "task.h"
#include "semphr.h"

#include "platform.h"
#include "CC2420.h"
#include "CC2420Const.h"

//Debugb
#include "serialPrint.h"
#include "leds.h"
#include "nopDelay.h"


#define PHY_PRIORITY        (configMAX_PRIORITIES - 1)  ///< Highest priority
#define TIMER_ASYN_PRIORITY (configMAX_PRIORITIES - 2)  ///< Highest priority -1

//Simple-MAC
#include "Simple_CC2420.h"
#include "MAC.h"
#include "Phy.h"
#include "TimerAsync.h"



//#define LIGHT_VERSION                 1          ///< Reduce the RAM usage?

#define LOGICAL_CHANNEL              20          ///< 802.15.4 logical channel
#define SHORT_LOCAL_ADDRESS           2          ///< TOS shot address

//#define COORDINATOR                   1          ///< Am I a corrdinator?? better in the makefile/project
#define BEACON_ORDER                  6          ///< Beacon Interval     -> BI=960(aBaseSlotDuration*aNumSuperframeSlots)*2^BEACON_ORDER symbols
#define SUPERFRAME_ORDER              4          ///< Superframe Duration -> SD=960(aBaseSlotDuration*aNumSuperframeSlots)*2^SUPERFRAME_ORDER symbols

#define GTS_INIT_TIMESLOT             1          ///< Init of the GTS Time Slot statically assigned to me (minimum 1, maximum 15)
#define GTS_TIMESLOT_DURATION         2          ///< GTS duration in Time Slots statically assigned to me (minimum 1, maximum 15)

#define HEADER                       11          ///< Fram header (without the frame length!!): Frame Control (2B) + seqNum (1B) + Src&Dest PAN&Addr (8B)
#define OVERHEAD              (HEADER+2)         ///< Overhead due to: Fram header(11B) + FCS (2B)
#define MAX_DATA_LENGTH   (127-OVERHEAD)         ///< Masimum data field lencht (Pay-Load)

#if LIGHT_VERSION
  #define MAX_FRAME_QUEUE              1          ///< Maximum number of frames queued to be sent
#else
  #define MAX_FRAME_QUEUE             5           ///< Maximum number of frames queued to be sent
#endif

//Functions comments in Simple_MAC.c
uint8_t simpleMAC_init ( unsigned long smClkHz );
uint8_t simpleMAC_queueFrame ( uint8_t plSize, uint8_t *packet );

#endif /*SIMPLEMAC_CONFIG_H_*/
