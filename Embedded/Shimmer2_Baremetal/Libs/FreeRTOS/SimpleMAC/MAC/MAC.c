/////////////////////////////////////////////////////////////////////////////////////////
/// \file Simple_MAC.c
/// \brief Simple 802.15.4 MAC Layer for Shimmer
///
/// Function \a simpleMAC_init has to be called before using simpleMAC_send.
/// \a simpleMAC_setPSize has also to be called before using simpleMAC_bufferSend.
/// \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
/// \version 1.0
/// \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "MAC.h"

//Standard Beacon
//uint8_t beacon[]={0x0F, 0x40, 0x88, 0xAB, 0x34, 0x12, 0xFF, 0xFF, 0xFF, 0xFF, 0x46, 0xCF, 0x80, 0x00, 0x00, 0x00};
//Final CAP Slot=1 Beacon
#if COORDINATOR
  uint8_t beacon[]={0x0F, 0x40, 0x88, 0xAB, 0x34, 0x12, 0xFF, 0xFF, 0xFF, 0xFF, 0x46, 0xC1, 0x80, 0x00, 0x00, 0x00};
#endif

uint8_t  pSize;                                  ///< Pay-Load size for \a simpleMAC_bufferSend function
uint8_t  tmpPSize=0;                             ///< Buffer index for \a simpleMAC_bufferSend function

/// Simple 802.15.4 Packet structure
typedef struct {
  uint8_t  length;                               ///< MAC Packet length = OVERHEAD + payload
  uint8_t  frCntrl1;                             ///< Frame control1
  uint8_t  frCntrl2;                             ///< Frame control2
  uint8_t  seqNum;                               ///< Sequence number
  uint16_t destPAN;                              ///< Destination PAN
  uint16_t destAddr;                             ///< Destination Address
  uint16_t srcPAN;                               ///< Source PAN
  uint16_t srcAddr;                              ///< Source Address
  uint8_t  data[MAX_DATA_LENGTH];                ///< Payload
  uint8_t  sendTime;                             ///< Estimated time in backoffs neccesary to send the packet
}strDataPacket;

#ifndef LIGHT_VERSION
  strDataPacket dataPacket;
#endif

/// Frame Queue to be send in GTS
struct {
  strDataPacket queue[MAX_FRAME_QUEUE];          ///< Frame array waiting to be sent
  uint8_t nFrames;                               ///< Number of frames in que queue
  uint8_t iInic;                                 ///< Index of the firts Frame in que queue
  uint8_t iEnd;                                  ///< Index of the firts free position in que queue
  uint8_t seqNum;                                ///< New Frame sequence number
  xSemaphoreHandle semaph;                       ///< Binary semaphore for mutual exclusion
} frameQueue;


/// Information about the super frame timing synchronization in symbols
struct {
  uint32_t bi;                      ///< Length of the Superframe (Inter Beacon Period) -> 960symbols(15.36ms)*2^BEACON_ORDER
  uint32_t sd;                      ///< Length of the active part of the Superframe (CAP&CFP) -> 960symbols(15.36ms)*2^SUPERFRAME_ORDER
  uint32_t ts;                      ///< Length of the Superframe Time Slot -> 60(0.96ms)*2^SUPERFRAME_ORDER
} tSF;


/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Set Pay-Load size for \a simpleMAC_bufferSend function
///
/// \param payloadSize Pay-Load size in bytes, with a maximum of \a MAX_DATA_LENGTH.
/////////////////////////////////////////////////////////////////////////////////////////
void simpleMAC_setPSize ( uint8_t payloadSize ) {
  //For constant packet size
  pSize=payloadSize;
  tmpPSize=0;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Init 802.15.4 Simple Mac
///
/// The conexion will be initialized using:
///   - Frame Control: 0x21, 0x88
///   - Init sequence number: 0
///   - Destination PAN: 0x1234
///   - Destination Adsress: 0x0001
///   - Source PAN: 0x1234
///   - Source Address: 0x0002
///
///  It will send a packet with the text "CC2420 Initialized!!" and configure an
///  interrupt for Transmit Done event.
///
/// \return \a TRUE/\a FALSE
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t simpleMAC_init ( unsigned long smClkHz ) {

  uint8_t i;

  #ifndef LIGHT_VERSION
    //Static initialization
    //dataPacket.length=0x11;
    dataPacket.frCntrl1=0x21;   dataPacket.frCntrl2=0x88;
    dataPacket.seqNum=0x00;
    dataPacket.destPAN=0x1234;  dataPacket.destAddr=0x0001;
    dataPacket.srcPAN=0x1234;   dataPacket.srcAddr=0x0002;
  #endif

  //Init Phy layer
  serialPutStr ( "Initializing Simple MAC...\n" );
  if ( ( PhyStdControl_init(smClkHz) !=SUCCESS ) || ( PhyStdControl_start() !=SUCCESS ) )
    return ( FALSE );
  serialPutStr ( "Physical layer initialized!\n" );

  //aBaseSlotDuration=960 symbols (15.36ms for 2.4GHz radio)
  tSF.bi=aBaseSuperframeDuration * ( ( uint32_t ) 1<< BEACON_ORDER );          //Beacon Interval     BI=960(aBaseSlotDuration*aNumSuperframeSlots)*2^BO symbols
  tSF.sd=aBaseSuperframeDuration * ( ( uint32_t ) 1<< SUPERFRAME_ORDER );      //Superframe Duration SD=960(aBaseSlotDuration*aNumSuperframeSlots)*2^SO symbols
  tSF.ts=aBaseSlotDuration       * ( ( uint32_t ) 1<< SUPERFRAME_ORDER );      //Time Slot Duration  TS=60(aBaseSlotDuration)*2^SO symbols


  sprintf ( printStr, "BI (Symbols) %lu, Beacon Order %d\n", tSF.bi, BEACON_ORDER );serialPutStr ( printStr );
  sprintf ( printStr, "SD (Symbols) %lu, SuperFrame Order %d\n", tSF.sd, SUPERFRAME_ORDER );serialPutStr ( printStr );

  TimerAsync_setTiming ( tSF.bi,tSF.sd,tSF.ts );

  //Inic TimerAsync State Machine, but disabled until find a beacon
  if ( TimerAsync_start() != SUCCESS ) {
    serialPutStr ( "Error TimerAsync_start\n" );
    allOnLEDs();
    return ( FAIL );
  }
  vSemaphoreCreateBinary ( frameQueue.semaph );
  if ( frameQueue.semaph == NULL ) {
    serialPutStr ( "Error Queue Semaphore\n" );
    allOnLEDs();
    return ( FAIL );
  }
  frameQueue.nFrames=0;
  frameQueue.iEnd=0;
  frameQueue.iInic=0;
  frameQueue.seqNum=0;
  for ( i=0;i<MAX_FRAME_QUEUE;i++ ) {
    frameQueue.queue[i].frCntrl1=0x21;   frameQueue.queue[i].frCntrl2=0x88;
    frameQueue.queue[i].destPAN=0x1234;  frameQueue.queue[i].destAddr=0x0001;
    frameQueue.queue[i].srcPAN=0x1234;   frameQueue.queue[i].srcAddr=0x0002;
    frameQueue.queue[i].sendTime=MAX_FRAME_TIME;
  }

  #if COORDINATOR
    serialPutStr ( "I am a coordinator\n" );
    //Allways on
    PLME_SET_TRX_STATE_request ( PHY_RX_ON );
  #else
    serialPutStr ( "I am a RFD\n" );
  #endif

  FIFOPInterrupt_disable();
  CCAInterrupt_disable();
  FIFOInterrupt_disable();
  SFDInterrupt_disable();
  //FreeRTOS also enables the interrupts
  //_EINT();
  return ( TRUE );
}

#ifndef LIGHT_VERSION
  /////////////////////////////////////////////////////////////////////////////////////////
  /// \brief Simple Mac send packet
  ///
  ///  It will send a packet of with \a plSize Pay-Load contained in \a packet.
  ///
  /// \param plSize: Pay-Load for the packet
  /// \param packet: Pay-Load data
  ///
  /// \return \a TRUE/\a FALE
  /////////////////////////////////////////////////////////////////////////////////////////
  uint8_t simpleMAC_send ( uint8_t plSize, uint8_t *packet ) {
    uint8_t i;

    if ( plSize>MAX_DATA_LENGTH ) {
      allOnLEDs();
      return ( FALSE );
    }

    //setGreenLED();

    //Size-> Payload+Overhead
    dataPacket.length=plSize+OVERHEAD;
    dataPacket.seqNum++;

    //Copy data into the packet
    for ( i=0;i<plSize;i++ ) {
      dataPacket.data[i]=packet[i];
    }
    //Send packet: copy data in the CC2420 buffer and turn TX-On (12 symbols -> 16us*12=192us)
    if ( PD_DATA_request ( dataPacket.length-1, ( uint8_t* ) &dataPacket ) != SUCCESS ) {
      serialPutStr ( "PD_DATA_request Fail!!!\n" );
      allOnLEDs();
      return ( FALSE );
    }
    return ( TRUE );
  }

  /////////////////////////////////////////////////////////////////////////////////////////
  /// \brief Simple Mac put data in CC2420 TX-Buffer. Turn on TX when packet complete.
  ///
  ///  Send data to the CC2420 TX-Buffer. When a complete packet is stored in TX buffer,
  //   specified by \a pSize, the radio will be turn on and it will send the data.
  ///
  /// \param size: number of bytes to be palec in the CC2420 TX buffer
  /// \param data: data array
  ///
  /// \return \a TRUE/\a FALE
  /////////////////////////////////////////////////////////////////////////////////////////
  uint8_t simpleMAC_bufferSend ( uint8_t size, uint8_t *data ) {
    //uint8_t i;

    if ( ( tmpPSize+size ) >pSize ) {
      allOnLEDs();
      return ( FALSE );
    }

    //sprintf(printStr, "\nBuffSend pSize: 0x%02X, size 0x%02X, tmpPSize 0x%02X\n",pSize, size, tmpPSize);serialPutStr(printStr);

    //Send Header?
    if ( tmpPSize==0 ) {
      //serialPutStr("Header...");
      //Send header plus Frame Length to the CC2420 TX buffer
      dataPacket.length=pSize+OVERHEAD;
      dataPacket.seqNum++;
      if ( HPLCC2420FIFO_writeTXFIFO ( HEADER+1, ( uint8_t* ) &dataPacket ) != SUCCESS ) {
        allOnLEDs();
        return ( FAIL );
      }
    }
    //Send the new data to the radio
    tmpPSize+=size;
    if ( tmpPSize<pSize ) {
      //The packet is not complete, send data to the CC2420 TX buffer
      if ( HPLCC2420FIFO_writeTXFIFO ( size,data ) != SUCCESS ) {
        allOnLEDs();
        return ( FAIL );
      }
    } else {
      //setGreenLED();
      //Send packet: copy data in the CC2420 buffer and turn TX-On (12 symbols -> 16us*12=192us)
      if ( PD_DATA_request ( size,data ) != SUCCESS ) {
        allOnLEDs();
        return ( FALSE );
      }
      tmpPSize=0;
      //serialPutStr("Done!! ");
      //TACCTL0 &= ~CCIE;
    }
    return ( TRUE );
  }

  /////////////////////////////////////////////////////////////////////////////////////////
  /// \brief Simple Mac packet sent event. Puts CC2420 in \a IDLE mode (TX&RX off)
  /////////////////////////////////////////////////////////////////////////////////////////
  void SFDInterrupt_fired ( void ) {
    //uint8_t ret;

    //clearGreenLED();
    serialPutStr ( "SFD interrupt fired!!!\n" );
    SFDInterrupt_clear();
    PLME_SET_TRX_STATE_request ( PHY_TRX_OFF );

    //serialPutStr("Tx Off\n");
    //ret=PLME_SET_TRX_STATE_request(PHY_TRX_OFF);
    //ret=HPLCC2420_cmd(0x06);
    //ret=HPLCC2420_cmd(CC2420_SRFOFF);
    //sprintf(printStr, "TXR_Off: 0x%02X\n",ret);serialPutStr(printStr);
  }
#endif


/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Simple Mac new packet has arrived.
///
///  This function process a new packet arrived and is signaled by the Phy layer
/////////////////////////////////////////////////////////////////////////////////////////
portBASE_TYPE PD_DATA_indication ( uint8_t psduLenght,uint8_t* psdu, portTickType arrTime ) {
  //uint8_t i;
  portTickType time=arrTime;
  strDataPacket *tmpPacket= ( strDataPacket* ) psdu;

  #if COORDINATOR
    serialPutStr ( "PD data intication fired!!!\n" );
  #else
    //Packet nor for us!!
    //if(tmpPacket->destPAN!=0x1234 || tmpPacket->destAddr!=0xFFFF ||  tmpPacket->srcPAN=0x1234;   tmpPacket->srcAddr=0x0002){
    if ( tmpPacket->destPAN!=0x1234 ) {
      serialPutStr ( "\nPacket from outside the PAN!!!" );
    } else {
      //Beacon
      if ( ( tmpPacket->frCntrl1&0x7 ) == CC2420_DEF_FCF_TYPE_BEACON ) {
        if ( xQueueSendToBack ( queueBeacon,&time,portMAX_DELAY ) != pdTRUE ) {
          serialPutStr ( "Error queueing Beacon\n" );
          //allOnLEDs();
        }
      }else{
          serialPutStr ( "\nBNC expected, unidentified packet rcv!!" );
      }
    }
  #endif
  //serialPutStr("\nPacket received: ");
  //for(i=0;i<=psduLenght;i++){
  //  sprintf(printStr, "0x%02X ",psdu[i]);serialPutStr(printStr);
  //}
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Before Beacon Interval event
///
/// Timer event used to signal a time defined constant (\a BEFORE_BI_INTERVAL) before the
/// beacon interval fired. This is used to turn the transceiver on before the beacon
/// interval fires so that if the device is a pan coordinator the transceiver will be
/// ready to transmit the beacon and if the device is a normal node the transceiver will
/// be ready to receive the beacon.
/// \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t TimerAsync_before_bi_fired() {
  //Before receiving the beacon, turn off the radio
  //setOrangeLED();
  PLME_SET_TRX_STATE_request ( PHY_RX_ON );
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Beacon Interval event
///
/// Timer event used to signal the beginning of the beacon interval
/// \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t TimerAsync_bi_fired() {
  #if COORDINATOR
    //Increment seq number
    beacon[3]++;
    //Send the beacon
    if ( PD_DATA_request ( beacon[0],beacon ) != SUCCESS ) {
      serialPutStr ( "PD_DATA_request Fail!!!\n" );
      allOnLEDs();
      return ( FALSE );
    }
  #else
    //clearOrangeLED();
    //After receiving the beacon, turn off the radio
    PLME_SET_TRX_STATE_request ( PHY_TRX_OFF );
  #endif
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Superframe Duration Interval event
///
/// Timer even tused to signal the end of the superframe duration. This is used for the
/// device to enter the sleep period (if applied) It is also used to disable the time slot
///  and backoffs events because during the sleep period there is no activity (if applied)
/// \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t TimerAsync_gts_fired ( portTickType endGTS ) {
  portTickType cTime,sTime;
  strDataPacket *pFrame;

  //sprintf(printStr, "endGTS=%li(%li)\n",endGTS,xTaskGetTickCount());serialPutStr(printStr);
  //sprintf(printStr, "Sent(%li)\n",xTaskGetTickCount());serialPutStr(printStr);

  do {
    cTime=xTaskGetTickCount();
    //sprintf(printStr, "Do(%d)\n",cTime);serialPutStr(printStr);
    //Anything to be send? and CC2420 idle?
    if ( frameQueue.nFrames ) {
      //sprintf(printStr, "Frame(%d)!\n",cTime);serialPutStr(printStr);
      pFrame=frameQueue.queue+frameQueue.iInic;
      sTime=pFrame->sendTime*TICKS_PER_BACKOFF;
      //The packet will be sent before the end of the GTS?
      if ( ( cTime+sTime ) >= endGTS ) {
        //sprintf(printStr, "NoSentOT(%d)\n",cTime+sTime);serialPutStr(printStr);
        cTime=endGTS;
      }
      //Send the Frame
      else if ( xSemaphoreTake ( frameQueue.semaph,endGTS- ( cTime+sTime ) ) == pdTRUE ) {
        //sprintf(printStr, "Send(%d)\n",xTaskGetTickCount());serialPutStr(printStr);
        //sprintf(printStr, "Send#%d(%d-%d)\n",frameQueue.nFrames,pFrame->length,pFrame->seqNum);serialPutStr(printStr);

        //setRedLED();
        //Send packet: copy data in the CC2420 buffer and turn TX-On (12 symbols -> 16us*12=192us)
        if ( PD_DATA_request ( pFrame->length-1, ( uint8_t* ) pFrame ) != SUCCESS ) {
          xSemaphoreGive ( frameQueue.queue );
          serialPutStr ( "PD_DATA_request Fail!!!\n" );
          allOnLEDs();
          return FALSE;
        }
        SFD_startWait ( FALSE );
        frameQueue.nFrames--;
        frameQueue.iInic= ( frameQueue.iInic+1 ) %MAX_FRAME_QUEUE;
        xSemaphoreGive ( frameQueue.semaph );
        //sprintf(printStr, "SD!(%d)\n",xTaskGetTickCount());serialPutStr(printStr);
        //vTaskDelay(sTime);

        //Update time after a time consuming task (send)
        if ( xQueueReceive ( queueSFD,&cTime,endGTS-cTime ) ==pdFALSE ) {
          serialPutStr ( "Error receiving SFD\n" );
          allOnLEDs();
        }
        PLME_SET_TRX_STATE_request ( PHY_TRX_OFF );
        //clearRedLED();

        SFD_disable();
        //sprintf(printStr, "Recv(%d)\n",xTaskGetTickCount());serialPutStr(printStr);
        cTime=xTaskGetTickCount();
      } else {
        //sprintf(printStr, "Blocked!!(%d)\n",xTaskGetTickCount());serialPutStr(printStr);
        //The queue is blocked until the end of the GTS!!
        cTime=endGTS;
      }
    } else {
      //Wait a little bit for a packet to be queued
      //sprintf(printStr, "Wait...(%d)\n",xTaskGetTickCount());serialPutStr(printStr);
      PLME_SET_TRX_STATE_request ( PHY_TRX_OFF );
      //vTaskDelay ( 2 );
      vTaskDelay ( ( portTickType ) 10000 / portTICK_RATE_US );

      //Update time after a time consuming task (wait)
      cTime=xTaskGetTickCount();
    }
  } while ( cTime<endGTS );
  //sprintf(printStr, "Done(%d)\n",xTaskGetTickCount());serialPutStr(printStr);

  return SUCCESS;
}


/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Queue a MAC Frame to be sent when possible
///
/// \return \a SUCCESS/\a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t simpleMAC_queueFrame ( uint8_t plSize, uint8_t *packet ) {

  strDataPacket *pFrame;
  uint8_t i;

  //Queue full?
  if ( frameQueue.nFrames == MAX_FRAME_QUEUE || plSize>MAX_DATA_LENGTH ) {
    allOnLEDs();
    return FAIL;
  }
  //simpleMAC_send ( plSize, packet );

  //Take the queue empty slot
  xSemaphoreTake ( frameQueue.semaph,portMAX_DELAY );{
    //sprintf(printStr, "T#%d",frameQueue.nFrames);serialPutStr(printStr);
    frameQueue.nFrames++;
    pFrame=frameQueue.queue+frameQueue.iEnd;
    pFrame->seqNum=frameQueue.seqNum++;
    frameQueue.iEnd= ( frameQueue.iEnd+1 ) %MAX_FRAME_QUEUE;
    pFrame->length=plSize+OVERHEAD;
    //Copy data into the packet
    for ( i=0;i<plSize;i++ ) {
      pFrame->data[i]=packet[i];
    }
  }
  xSemaphoreGive ( frameQueue.semaph );
  //serialPutStr("G\n");


  return SUCCESS;
}
