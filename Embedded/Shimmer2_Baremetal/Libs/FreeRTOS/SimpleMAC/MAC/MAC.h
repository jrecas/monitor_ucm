/////////////////////////////////////////////////////////////////////////////////////////
///  \file Simple_MAC.h
///  \brief Simple 802.15.4 MAC Layer for Shimmer
///
///  Function \a simpleMAC_init has to be called before using simpleMAC_send.
///  \a simpleMAC_setPSize has also to be called before using simpleMAC_bufferSend.
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 1.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SIMPLE_MAC_H_
#define SIMPLE_MAC_H_

#include "SimpleMAC_Config.h"


#define MAX_FRAME_TIME               16          ///< Maximum necessary time to send a full MAC packet in backoffs (16*0.32=5.12ms)

#define aBaseSlotDuration            60          ///< Symbols per Superframe Time Slot = aBaseSlotDuration*2^SUPERFRAME_ORDER (16 per Superframe duration)
#define aBaseSuperframeDuration     960          ///< aBaseSlotDuration*aNumSuperframeSlots=60*16=960 symbols or 15.36ms for 2.4GHz radio

#define aMaxBE                        5          ///< CSMA-CA
#define aMaxBeaconOverhead           75
#define aMaxBeaconPayloadLength       aMaxPHYPacketSize-aMaxBeaconOverhead
#define aGTSDescPersistenceTime       4
#define aMaxFrameOverhead            25
#define aMaxFrameResponseTime      1220
#define aMaxFrameRetries              1

//(SYNC)number of beacons lost before sending a Beacon-Lost indication
#define aMaxLostBeacons               4
#define aMaxMACFrameSize              aMaxPHYPacketSize-aMaxFrameOverhead
#define aMaxSIFSFrameSize            18
#define aMinCAPLength               440
#define aMinLIFSPeriod               40
#define aMinSIFSPeriod               12
#define aNumSuperframeSlots          16          ///< The number of slots contained in any superframe
#define aResponseWaitTime            32*aBaseSuperframeDuration
#define aUnitBackoffPeriod           20          ///< 20 symbols or 320us for 2.4GHz radio

#define PLME_SET_TRX_STATE_confirm(status)                       SUCCESS       ///< Phy event generated after Set/Clear RX & TX
#define PLME_SET_confirm(status, PIBAttribute)                   SUCCESS       ///< Phy event generated after a modification of Phy configuration
#define PD_DATA_confirm(status)                                  SUCCESS       ///< Phy event generated after sending a command to the CC2420 Radio


//Functions comments in Simple_MAC.c
//uint8_t simpleMAC_queueFrame ( uint8_t plSize, uint8_t *packet );


void    simpleMAC_setPSize ( uint8_t payloadSize );
//uint8_t simpleMAC_init ( unsigned long cpuHz );
uint8_t simpleMAC_send ( uint8_t plSize, uint8_t *packet );
uint8_t simpleMAC_bufferSend ( uint8_t size, uint8_t *data );
void SFDInterrupt_fired ( void );

portBASE_TYPE PD_DATA_indication ( uint8_t psduLenght,uint8_t* psdu, portTickType arrTime );

result_t TimerAsync_backoff_fired ( void );
result_t TimerAsync_before_bi_fired ( void );
result_t TimerAsync_bi_fired ( void );
result_t TimerAsync_sd_fired ( void );
result_t TimerAsync_gts_fired ( portTickType endGTS );


#endif /*SIMPLE_MAC_H_*/
