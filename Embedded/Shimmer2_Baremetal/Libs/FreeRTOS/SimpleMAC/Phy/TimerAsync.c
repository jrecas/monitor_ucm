/////////////////////////////////////////////////////////////////////////////////////////
/// \file TimerAsync.c
/// \brief Code file for the timing of the Superframe Duration utilities
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "TimerAsync.h"

xQueueHandle queueBeacon;                      ///< Queue that indicates the reception of a beacon in system ticks

/// Inter events delay in ticks
struct tickDelayFiniteStateMachine {
    portTickType BI;
    portTickType GTS;
    portTickType BBI_BI;
    portTickType BI_GTS;
    portTickType GTS_BBI;
}delayFSM;

void vTimerAysncTask(void * pvParameters);

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Start TimerAsync
///
/// Create a task that fires the events related to a Inter-Beacon Period bases on a
/// state machine
/// \return \a xTaskCreate status
/////////////////////////////////////////////////////////////////////////////////////////
result_t TimerAsync_start() {
    serialPutStr("Launching vTimerAysncTask....");

    if ( xTaskCreate( vTimerAysncTask, ( signed portCHAR * ) "TMRASYNC", configMINIMAL_STACK_SIZE*2, NULL, TIMER_ASYN_PRIORITY, ( xTaskHandle * ) NULL ) != pdPASS) {
        serialPutStr("Error creating vTaskPostTask\n");
        allOnLEDs();
        return(FAIL);
    }

#ifndef COORDINATOR
    ///< Queue that indicates the reception of a beacon in system ticks
    if ( (queueBeacon=xQueueCreate(1, sizeof(portTickType))) == 0) {
        serialPutStr("Error creating queueBeacon\n");
        allOnLEDs();
        return(FAIL);
    }
#endif

    sprintf(printStr, " Launched. Stack: %d\n", configMINIMAL_STACK_SIZE);
    serialPutStr((char*)printStr);
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Timer Asyc task
///
/// \param pvParameters not used
/////////////////////////////////////////////////////////////////////////////////////////
void vTimerAysncTask(void * pvParameters) {
    portTickType xLastWakeTime;
    uint8_t samples[10]={0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9};
#if INCLUDE_uxTaskGetStackHighWaterMark
    unsigned portBASE_TYPE uxHighWaterMark=configMINIMAL_STACK_SIZE;
#endif

    sprintf(printStr, "TICKS_PER_BACKOFF %d\n",TICKS_PER_BACKOFF);
    serialPutStr(printStr);
    sprintf(printStr, "BI %d\n",delayFSM.BI);
    serialPutStr(printStr);
    sprintf(printStr, "GTS %d\n",delayFSM.GTS);
    serialPutStr(printStr);
    sprintf(printStr, "BI_GTS %d\n",delayFSM.BI_GTS);
    serialPutStr(printStr);
    sprintf(printStr, "GTS_BBI %d\n",delayFSM.GTS_BBI);
    serialPutStr(printStr);
    sprintf(printStr, "BBI_BI %d\n",delayFSM.BBI_BI);
    serialPutStr(printStr);

    //Initialise the xLastWakeTime variable with the current time.

#if COORDINATOR
    xLastWakeTime=xTaskGetTickCount();
    serialPutStr("Sending Beacons...\n");
#else
    //Find a beacon and synchronize
    PLME_SET_TRX_STATE_request(PHY_RX_ON);
    //FIFOP is active low (not default)
    //When a packet is waiting in the RxBuff or the buffer has more than FIFOP_THR bytes
    FIFOP_startWait(FALSE);
    serialPutStr("Looking for a Beacon...\n");
#endif

    char tChar='t';
    vTaskSetApplicationTaskTag( NULL, ( void * ) &tChar );

    for (;;) {
#if COORDINATOR
        //Send the beacon
        //setGreenLED();
        toggleGreenLED();
        TimerAsync_bi_fired();
        serialPutStr(".");
        //clearGreenLED();
        vTaskDelayUntil(&xLastWakeTime,delayFSM.BI);
#else
        //Wait for a Beacon
        //serialPutStr("WB-");
        if (xQueueReceive(queueBeacon,&xLastWakeTime,portMAX_DELAY)==pdFALSE) {
            serialPutStr("Error receiving beacon\n");
            allOnLEDs();
        }
        //Compensate (empirically) for the delay between the Base Station send the beacon and the receiver triggers FIFOP
        xLastWakeTime-=DELAY_BICOOR_RCVRFD*TICKS_PER_BACKOFF;
        TimerAsync_bi_fired();

        //Wait unit GTS Time Slot
        vTaskDelayUntil(&xLastWakeTime,delayFSM.BI_GTS);

        //Call GTS event function
        TimerAsync_gts_fired(xLastWakeTime+delayFSM.GTS);

        //Wait unit Before Beacon Interval
        vTaskDelayUntil(&xLastWakeTime,delayFSM.GTS_BBI);
        TimerAsync_before_bi_fired();
#endif
#if INCLUDE_uxTaskGetStackHighWaterMark
        uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
        if (uxHighWaterMark<=WARN_STACK_LEFT){
            sprintf(printStr, "WARNING!! vTimerAysncTask StackWM 0x%X\n", uxHighWaterMark); serialPutStr(printStr);
        }
#endif
    }
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Set BeaconInterval and SuperframeDuration (Symbols)
///
/// \param bi_symbols Beacon Interval duration in symbols
/// \param sd_symbols Superframe Duration in symbols
/////////////////////////////////////////////////////////////////////////////////////////
void TimerAsync_setTiming(uint32_t bi_symbols,uint32_t sd_symbols,uint32_t ts_symbols) {
    //sprintf(printStr, "BI %d\n",bi_symbols);serialPutStr(printStr);
    //sprintf(printStr, "SD %d\n",sd_symbols);serialPutStr(printStr);
    //sprintf(printStr, "TS %d\n",ts_symbols);serialPutStr(printStr);
    vTaskSuspendAll(); {
        //Beacon interval period in ticks
        delayFSM.BI=bi_symbols/SYMBOLS_PER_BACKOFF*TICKS_PER_BACKOFF;
        //GTS interval in ticks
        delayFSM.GTS=GTS_TIMESLOT_DURATION*ts_symbols/SYMBOLS_PER_BACKOFF*TICKS_PER_BACKOFF;

        //Delay between Before Beacon Interval and Beacon Interval in ticks
        delayFSM.BBI_BI=BEFORE_BI_INTERVAL*TICKS_PER_BACKOFF;

        //Delay between Beacon Interval and GTS time slot in ticks
        delayFSM.BI_GTS=GTS_INIT_TIMESLOT*ts_symbols/SYMBOLS_PER_BACKOFF*TICKS_PER_BACKOFF;
        //Delay between init GTS and Before Beacon Interval in ticks
        //delayFSM.GTS_BBI=delayFSM.BI-delayFSM.BI_GTS-delayFSM.BBI_BI-DELAY_BICOOR_RCVRFD;
        delayFSM.GTS_BBI=delayFSM.BI-delayFSM.BI_GTS-delayFSM.BBI_BI;

    }xTaskResumeAll();
}
