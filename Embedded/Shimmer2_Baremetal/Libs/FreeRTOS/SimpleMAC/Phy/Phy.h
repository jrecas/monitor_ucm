/////////////////////////////////////////////////////////////////////////////////////////
///  \file Phy.h
///  \brief CC2420 Physical Layer
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef PHY_H_
#define PHY_H_

#include "SimpleMAC_Config.h"
#include "frame_format.h"

#define INIT_CURRENTCHANNEL    0x15
#define INIT_CHANNELSSUPPORTED 0x0
#define INIT_TRANSMITPOWER     15
#define INIT_CCA_MODE          0

#define CCA_IDLE               0
#define CCA_BUSY               0x80    ///< P2.7 jRecas

enum{
  PHY_BUSY                  = 0x00,
  PHY_BUSY_RX               = 0x01,
  PHY_BUSY_TX               = 0x02,
  PHY_FORCE_TRX_OFF         = 0x03,
  PHY_IDLE                  = 0x04,
  PHY_INVALID_PARAMETER     = 0x05,
  PHY_RX_ON                 = 0x06,
  PHY_SUCCESS               = 0x07,
  PHY_TRX_OFF               = 0x08,
  PHY_TX_ON                 = 0x09,
  PHY_UNSUPPORTED_ATTRIBUTE = 0x0a
};


//phy PIB attributes enumerations
enum{
  PHYCURRENTCHANNEL         = 0x00,
  PHYCHANNELSSUPPORTED      = 0X01,
  PHYTRANSMITPOWER          = 0X02,
  PHYCCAMODE                = 0X03
};

// PHY PIB attribute and psdu
typedef struct{
  uint8_t phyCurrentChannel;
  uint8_t phyChannelsSupported;
  uint8_t phyTransmitPower;
  uint8_t phyCcaMode;
} phyPIB;


result_t PhyStdControl_init(unsigned long smClkHz);
result_t PhyStdControl_start(void);
result_t PhyStdControl_stop(void);

void vPhyTask(void * pvParameters);

result_t PD_DATA_request(uint8_t psduLength, uint8_t* psdu);
result_t PLME_SET_TRX_STATE_request(uint8_t state);

bool SetRegs(void);
uint8_t GetRFPower(void);
result_t SetRFPower(uint8_t power);
//result_t VREFOn(void);
//result_t VREFOff(void);

result_t TunePreset(uint8_t chnl);
result_t setShortAddress(uint16_t addr);
#endif /*PHY_H_*/
