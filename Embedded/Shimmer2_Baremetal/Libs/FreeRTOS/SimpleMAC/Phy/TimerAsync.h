/////////////////////////////////////////////////////////////////////////////////////////
/// \file TimerAsync.h
/// \brief Header file for the timing of the Superframe Duration utilities
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef TIMERASYNC_H_
#define TIMERASYNC_H_

#include "SimpleMAC_Config.h"

#define SYMBOLS_PER_BACKOFF   20               ///< Number of symbols per backoff (default = 20 or 320us for 2.4GHz radio)
#define TICKS_PER_BACKOFF      1               ///< Number of clock ticks per backoff, tick rate = 20 symbols, 1BackOff = 20 symbols
#define BEFORE_BI_INTERVAL     3               ///< Backoffs before BI to wake up (320us*3=0.96ms)
#define DELAY_BICOOR_RCVRFD    6               ///< Period, in Backoffs, between the coordinator triggers BI_fired and Reduced Function Device receives a packet (Empiric, 2ms)

extern xQueueHandle queueBeacon;

result_t TimerAsync_start ( void );
void TimerAsync_reset ( void );
void TimerAsync_setTiming(uint32_t bi_symbols,uint32_t sd_symbols,uint32_t ts_symbols);

#endif /*TIMERASYNC_H_*/
