/////////////////////////////////////////////////////////////////////////////////////////
///  \file Phy.c
///  \brief CC2420 Physical Layer
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "Phy.h"

xQueueHandle queueFIFOP=NULL;                         ///< Queue for FIFOP events
xQueueHandle queueSFD=NULL;                           ///< Queue for SFD events
uint16_t gCurrentParameters[14];                      ///< CC2420 current internal register values
phyPIB phy_PIB;                                       ///< Physical PAN Information Base information
volatile uint8_t currentRxTxState = PHY_TRX_OFF;      ///< CC2420 current status (PHY_TRX_OFF, PHY_RX_ON or PHY_TX_ON)

//message received
//------------------------------ BEACON MANAGEMENT ------------------------------------
#ifdef GCC
  MPDU rxmpdu __attribute__ ((aligned (2)));          ///< Rx MAC Protocol Data Unit
#else
  #pragma DATA_ALIGN ( rxmpdu , 2 );
  MPDU rxmpdu;                                        ///< Rx MAC Protocol Data Unit
#endif

MPDU *rxmpdu_ptr;                                     ///< pointer to \a rxmpdu

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Init Phy Layer
///
///  Call \a HPLCC2420_init and initialice Phy parameters
///  \return \a SUCESS/\a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
result_t PhyStdControl_init(unsigned long smClkHz) {

    //Set PIN directions and init values, USART -> SPI mode,
    //Tx&Rx interrupts disabled, create bus arbitration semaphore
    if ( HPLCC2420_init(smClkHz)!=SUCCESS  )
        return(FALSE);

    // Set default parameters
    //MAIN CONTROL REGISTER
    gCurrentParameters[CP_MAIN] = 0xf800;
    //MODEM CONTROL REGISTER 0
    gCurrentParameters[CP_MDMCTRL0] = ((0 << CC2420_MDMCTRL0_ADRDECODE) |
                                       (2 << CC2420_MDMCTRL0_CCAHIST)   | (3 << CC2420_MDMCTRL0_CCAMODE)  |
                                       (1 << CC2420_MDMCTRL0_AUTOCRC)   | (2 << CC2420_MDMCTRL0_PREAMBL));

    gCurrentParameters[CP_MDMCTRL1] = 20 << CC2420_MDMCTRL1_CORRTHRESH;

    gCurrentParameters[CP_RSSI] =     0xE080;
    gCurrentParameters[CP_SYNCWORD] = 0xA70F;
    gCurrentParameters[CP_TXCTRL]   = ((1 << CC2420_TXCTRL_BUFCUR)     |
                                       (1 << CC2420_TXCTRL_TURNARND)   | (3 << CC2420_TXCTRL_PACUR) |
                                       (1 << CC2420_TXCTRL_PADIFF)     | (0x1f << CC2420_TXCTRL_PAPWR));

    gCurrentParameters[CP_RXCTRL0] =  ((1 << CC2420_RXCTRL0_BUFCUR)    |
                                       (2 << CC2420_RXCTRL0_MLNAG)     | (3 << CC2420_RXCTRL0_LOLNAG) |
                                       (2 << CC2420_RXCTRL0_HICUR)     | (1 << CC2420_RXCTRL0_MCUR) |
                                       (1 << CC2420_RXCTRL0_LOCUR));

    gCurrentParameters[CP_RXCTRL1]  = ((1 << CC2420_RXCTRL1_LOLOGAIN) |
                                       (1 << CC2420_RXCTRL1_HIHGM)    |  (1 << CC2420_RXCTRL1_LNACAP) |
                                       (1 << CC2420_RXCTRL1_RMIXT)    |  (1 << CC2420_RXCTRL1_RMIXV)  |
                                       (2 << CC2420_RXCTRL1_RMIXCUR));

    gCurrentParameters[CP_FSCTRL]   = ((1 << CC2420_FSCTRL_LOCK)      |
                                       ((357+5*(CC2420_DEF_CHANNEL-11)) << CC2420_FSCTRL_FREQ));

    gCurrentParameters[CP_SECCTRL0] = ((1 << CC2420_SECCTRL0_CBCHEAD) |
                                       (1 << CC2420_SECCTRL0_SAKEYSEL)  | (1 << CC2420_SECCTRL0_TXKEYSEL) |
                                       (1 << CC2420_SECCTRL0_SECM));

    gCurrentParameters[CP_SECCTRL1] = 0;
    gCurrentParameters[CP_BATTMON]  = 0;

    // Set fifop threshold (FIFOP_THR) to greater than size of tos msg (the maximum, 64B (default)),
    // Fifop goes active (low) at end of msg (or FIFOP_THR bytes in the RxBuff)
    gCurrentParameters[CP_IOCFG0]   = (((127) << CC2420_IOCFG0_FIFOTHR) |
                                       (1 <<CC2420_IOCFG0_FIFOPPOL)) ;

    gCurrentParameters[CP_IOCFG1]   =  0;

    //PHY PIB initialization
    //phy_PIB.phyCurrentChannel=INIT_CURRENTCHANNEL;
    phy_PIB.phyCurrentChannel=LOGICAL_CHANNEL;
    phy_PIB.phyChannelsSupported=INIT_CHANNELSSUPPORTED;
    phy_PIB.phyTransmitPower=INIT_TRANSMITPOWER;
    phy_PIB.phyCcaMode=INIT_CCA_MODE;

    ///< Queue that indicates the reception of a a new packet with time stamp in system ticks
    if ( (queueFIFOP=xQueueCreate(1, sizeof(portTickType))) == 0) {
        serialPutStr("Error creating queueBeacon\n");
        allOnLEDs();
        return(FAIL);
    }

    ///< Queue that indicates the reception of a a new packet or end transmission
    if ( (queueSFD=xQueueCreate(1, sizeof(portTickType))) == 0) {
        serialPutStr("Error creating queueSFD\n");
        allOnLEDs();
        return(FAIL);
    }

    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Start Phy Layer
///
///  Call \a HPLCC2420_start and enable CC2420 Radio
///  \return \a SUCESS/\a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
result_t PhyStdControl_start() {
    result_t status=0;

    //vTaskSuspendAll();{
    rxmpdu_ptr = &rxmpdu;
    //}xTaskResumeAll();

    //Init internal variables
    if ( HPLCC2420_start()!=SUCCESS  ) {
        serialPutStr("Error HPLCC2420_start\n");
        return(FALSE);
    }
    //turn on power
    VREFOn();
    ResetOff();
    // toggle reset
    ResetOn();       //CLR_CC_RSTN_PIN();
    msWaitNOP(10);   //Hold
    ResetOff();      //SET_CC_RSTN_PIN();
    msWaitNOP(10);   //Wait
    
    // turn on crystal, takes about 860 usec,
    // chk CC2420 status reg for stablize
    status = HPLCC2420_cmd(CC2420_SXOSCON);
    sprintf(printStr, "CC2420_SXOSCON Status 0x%X\n",status);
    serialPutStr(printStr);
    msWaitNOP(100);
    status = HPLCC2420_cmd(CC2420_SNOP);
    sprintf(printStr, "CC2420_SNOP Status 0x%X\n",status);
    serialPutStr(printStr);
    if ( !(status & (1<<CC2420_XOSC16M_STABLE)) ) {
        serialPutStr("Error CC2420_SXOSCON\n");
        return(FALSE);
    }
    msWaitNOP(100);
    //set freq, load regs
    if (SetRegs() != SUCCESS) {
        serialPutStr("Error SetRegs\n");
        return(FALSE);
    }
    msWaitNOP(100);
    //status = default_CC2420_registers() && status;
    status = setShortAddress(SHORT_LOCAL_ADDRESS);
    //sprintf(printStr, "TOS_LOCAL_ADDRESS Status 0x%X\n",status);serialPutStr(printStr);

    msWaitNOP(100);
    // status = status && TunePreset(CC2420_DEF_CHANNEL);
    //SET RF power
    SetRFPower(phy_PIB.phyTransmitPower);
    msWaitNOP(100);
    //SET CHANNEL
    TunePreset(phy_PIB.phyCurrentChannel);

    msWaitNOP(100);
    HPLCC2420_cmd(CC2420_SFLUSHRX);
    msWaitNOP(100);
    //FIFOP_startWait(FALSE); //->enableFIFOP();

    msWaitNOP(100);
    status = HPLCC2420_cmd(CC2420_SNOP);
    if (!(status & (1<<CC2420_LOCK))) {
        serialPutStr("Error CC2420_LOCK\n");
        return(FALSE);
    }

    if ( xTaskCreate( vPhyTask, ( signed portCHAR * ) "PHY", configMINIMAL_STACK_SIZE, NULL, PHY_PRIORITY, ( xTaskHandle * ) NULL ) != pdPASS) {
        serialPutStr("Error creating vTaskPostTask\n");
        allOnLEDs();
        return(FAIL);
    }

    //sprintf(printStr, "CC2420_SNOP Status 0x%X\n",status);serialPutStr(printStr);
    return(SUCCESS);
}


/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Stop Phy Layer
///
///  Power off CC2420 Radio
///  \return \a SUCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t PhyStdControl_stop() {
    //disable interrupts
    FIFOP_disable();

    HPLCC2420_cmd(CC2420_SXOSCOFF);
    HPLCC2420_stop();

    ResetOn(); //CLR_CC_RSTN_PIN();
    VREFOff();
    ResetOff(); //SET_CC_RSTN_PIN();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Phy task for receiving CC2420 packets
///
///  \param pvParameters not used
/////////////////////////////////////////////////////////////////////////////////////////
void vPhyTask(void * pvParameters) {
    portTickType arrTime;
#if INCLUDE_uxTaskGetStackHighWaterMark
    unsigned portBASE_TYPE uxHighWaterMark=configMINIMAL_STACK_SIZE;
#endif

    char tChar='p';
    vTaskSetApplicationTaskTag( NULL, ( void * ) &tChar );

    for (;;) {
        //vTaskDelay(1500);
        //Wait for a new packet from HPLCC2420Interrupt
        if (xQueueReceive(queueFIFOP,&arrTime,portMAX_DELAY)==pdFALSE) {
            serialPutStr("Error receiving FIFOP\n");
            allOnLEDs();
        }
        setYellowLED();
        //Read the packet, why 100?? should be 127
        HPLCC2420FIFO_readRXFIFO(MAX_DATA_LENGTH+OVERHEAD,(uint8_t*)&rxmpdu);
        //rssi =  (uint8_t) HPLCC2420_read(CC2420_RSSI);
        PD_DATA_indication(rxmpdu_ptr->length,(uint8_t*)rxmpdu_ptr, arrTime);
        //Flush Rx Buffer
        HPLCC2420_cmd(CC2420_SFLUSHRX);
        //FIFOPInterrupt_enable();
        clearYellowLED();
#if INCLUDE_uxTaskGetStackHighWaterMark
        uxHighWaterMark = uxTaskGetStackHighWaterMark( NULL );
        if (uxHighWaterMark<=WARN_STACK_LEFT){
            sprintf(printStr, "WARNING!! vPhyTask StackWM 0x%X\n", uxHighWaterMark);serialPutStr(printStr);
        }
#endif

    }
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Pysical Data Request, send data to the Tx Buffer of the CC2420 Radio
///
///  After sending the command, \a PD_DATA_confirm(=SUCCESS) will be called
///
///  \param psduLength length of the data buffer
///  \param psdu data to be send to the CC2420
///  \return \a SUCESS/\a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
result_t PD_DATA_request(uint8_t psduLength, uint8_t* psdu) {
    if ( HPLCC2420FIFO_writeTXFIFO(psduLength,(uint8_t *)psdu) != SUCCESS )
        return(FAIL);
    HPLCC2420_cmd(CC2420_STXON);
    currentRxTxState = PHY_TX_ON;                  //jRecas no estaba especificado!!
    return(PD_DATA_confirm(PHY_SUCCESS));
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Pysical Layer Management Entity Enable/Disable RX & TX on CC2420
///
///  Modify the actual state of the CC2420
///  \param state new RT/TX state
///  \return \a PLME_SET_TRX_STATE_confirm function
/////////////////////////////////////////////////////////////////////////////////////////
result_t PLME_SET_TRX_STATE_request(uint8_t state) {
    result_t ret=SUCCESS;
    //printfUART("set %i %i\n",currentRxTxState,state);
    //MAC is demanding for PHY to change to the requested transceiver radio state
    //sprintf(printStr, "CState from 0x%02X to 0x%02X\n",currentRxTxState, state);serialPutStr(printStr);
    //vTaskSuspendAll();{
    switch (state) {
    case PHY_RX_ON:
        switch (currentRxTxState) {
            //transceiver state already on
        case PHY_RX_ON:
            //serialPutStr("RX already ON\n");
            ret=PLME_SET_TRX_STATE_confirm(PHY_RX_ON);
            break;
        case PHY_TRX_OFF:
            //serialPutStr("TRX_OFF -> Enabling RX_ON...\n");
            HPLCC2420_cmd(CC2420_SRXON);
            currentRxTxState = PHY_RX_ON;
            ret=PLME_SET_TRX_STATE_confirm(SUCCESS);
            break;
        case PHY_TX_ON:
            //serialPutStr("TX_ON -> Enabling RX_ON...\n");
            ret=PLME_SET_TRX_STATE_confirm(PHY_BUSY_TX);
            break;
        }
        break;

    case PHY_TRX_OFF:
        switch (currentRxTxState) {
            //transceiver state already on
        case PHY_RX_ON:
            ret=HPLCC2420_cmd(CC2420_SRFOFF);
            currentRxTxState = PHY_TRX_OFF;
            //ret=PLME_SET_TRX_STATE_confirm(PHY_BUSY_RX);    jrecas
            break;
        case PHY_TRX_OFF:
            ret=PLME_SET_TRX_STATE_confirm(PHY_TRX_OFF);
            break;
        case PHY_TX_ON:
            ret=HPLCC2420_cmd(CC2420_SRFOFF);
            currentRxTxState = PHY_TRX_OFF;
            //ret=PLME_SET_TRX_STATE_confirm(PHY_BUSY_TX);   jrecas
            break;
        }
        break;

    case PHY_FORCE_TRX_OFF:
        HPLCC2420_cmd(CC2420_SRFOFF);
        currentRxTxState = PHY_TRX_OFF;
        break;
    case PHY_TX_ON:
        switch (currentRxTxState) {
            //transceiver state already on
        case PHY_RX_ON:
            ret=PLME_SET_TRX_STATE_confirm(PHY_BUSY_RX);
            break;
        case PHY_TRX_OFF:
            //call HPLCC2420.cmd(CC2420_SXOSCON);
            HPLCC2420_cmd(CC2420_STXON);
            currentRxTxState = PHY_TX_ON;
            ret=PLME_SET_TRX_STATE_confirm(SUCCESS);
            break;
        case PHY_TX_ON:
            ret=PLME_SET_TRX_STATE_confirm(PHY_TX_ON);
            break;
        }
        break;
    }
    //}xTaskResumeAll();

//printfUART("set tx%i\n",currentRxTxState);
//sprintf(printStr, "CState: 0x%02X\n",currentRxTxState);serialPutStr(printStr);

    return(ret);
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Modify CC2420 internal registers
///
///  Configure CC2420 registers with current values
///  <br>Readback 1st register written to make sure electrical connection OK
///  \return \a SUCESS/\a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
bool SetRegs() {
    uint16_t data;

    HPLCC2420_write(CC2420_MAIN,     gCurrentParameters[CP_MAIN]);     //Write Main Control Register
    HPLCC2420_write(CC2420_MDMCTRL0, gCurrentParameters[CP_MDMCTRL0]);
    data = HPLCC2420_read(CC2420_MDMCTRL0);
    if (data != gCurrentParameters[CP_MDMCTRL0]) return FAIL;

    HPLCC2420_write(CC2420_MDMCTRL1, gCurrentParameters[CP_MDMCTRL1]);
    HPLCC2420_write(CC2420_RSSI,     gCurrentParameters[CP_RSSI]);
    HPLCC2420_write(CC2420_SYNCWORD, gCurrentParameters[CP_SYNCWORD]);
    HPLCC2420_write(CC2420_TXCTRL,   gCurrentParameters[CP_TXCTRL]);
    HPLCC2420_write(CC2420_RXCTRL0,  gCurrentParameters[CP_RXCTRL0]);
    HPLCC2420_write(CC2420_RXCTRL1,  gCurrentParameters[CP_RXCTRL1]);
    HPLCC2420_write(CC2420_FSCTRL,   gCurrentParameters[CP_FSCTRL]);

    HPLCC2420_write(CC2420_SECCTRL0, gCurrentParameters[CP_SECCTRL0]);
    HPLCC2420_write(CC2420_SECCTRL1, gCurrentParameters[CP_SECCTRL1]);
    HPLCC2420_write(CC2420_IOCFG0,   gCurrentParameters[CP_IOCFG0]);
    HPLCC2420_write(CC2420_IOCFG1,   gCurrentParameters[CP_IOCFG1]);

    HPLCC2420_cmd(CC2420_SFLUSHTX);    //flush Tx fifo
    HPLCC2420_cmd(CC2420_SFLUSHRX);

    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Return power seeting
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t GetRFPower() {
    return (gCurrentParameters[CP_TXCTRL] & 0x000f); //rfpower;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Set CC2420 power seeting
///
///  Power:
///  - 31: full power    (0dbm)
///  -  3: lowest power  (-25dbm)
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t SetRFPower(uint8_t power) {
    gCurrentParameters[CP_TXCTRL] = (gCurrentParameters[CP_FSCTRL] & 0xfff0) | (power << CC2420_TXCTRL_PAPWR);
    HPLCC2420_write(CC2420_TXCTRL,gCurrentParameters[CP_TXCTRL]);
    return SUCCESS;
}
/*
/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Turns on the 1.8V references on the CC2420.
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t VREFOn(){
  CLR_CC_VREN_PIN();
  //CC2420 spec: 600us max turn on time
  msWait(1);
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Turns off the 1.8V references on the CC2420.
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t VREFOff(){
    SET_CC_VREN_PIN();
    return SUCCESS;
}
*/
/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Set CC2420 channel
///
///  Valid channel values are 11 through 26.
///  The channels are calculated by:
///  Freq = 2405 + 5(k-11) MHz for k = 11,12,...,26
///  \param chnl requested 802.15.4 channel
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t TunePreset(uint8_t chnl) {
    int fsctrl;
    uint8_t status2;

    fsctrl = 357 + 5*(chnl-11);
    gCurrentParameters[CP_FSCTRL] = (gCurrentParameters[CP_FSCTRL] & 0xfc00) | (fsctrl << CC2420_FSCTRL_FREQ);
    status2 = HPLCC2420_write(CC2420_FSCTRL, gCurrentParameters[CP_FSCTRL]);
    // if the oscillator is started, recalibrate for the new frequency
    // if the oscillator is NOT on, we should not transition to RX mode
    if (status2 & (1 << CC2420_XOSC16M_STABLE))
        HPLCC2420_cmd(CC2420_SRXON);
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Set short address
///
///  \param addr 16b short address
///  \return result of \a HPLCC2420RAM_write
/////////////////////////////////////////////////////////////////////////////////////////
result_t setShortAddress(uint16_t addr) {
    //sprintf(printStr, "Original: 0x%04X\n",addr);serialPutStr(printStr);
    //addr = toLSB16(addr);
    //sprintf(printStr, "toLSB16:  0x%04X\n",addr);serialPutStr(printStr);
    return HPLCC2420RAM_write(CC2420_RAM_SHORTADR, 2, (uint8_t*)&addr);
}
