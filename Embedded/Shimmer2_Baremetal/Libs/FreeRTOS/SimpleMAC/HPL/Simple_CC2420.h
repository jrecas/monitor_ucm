/////////////////////////////////////////////////////////////////////////////////////////
///  \file HPLCC2420.h
///  \brief Definitions for msp430-CC2420 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SIMPLE_CC2420_H_
#define SIMPLE_CC2420_H_

#include "SimpleMAC_Config.h"

extern xQueueHandle queueFIFOP;
extern xQueueHandle queueSFD;

/////////////////////////////////////////////////////////////////////////////////////////
// Zero out the reserved bits since they can be either 0 or 1.
// This allows the use of "if !cmd(x)" in the radio stack
/////////////////////////////////////////////////////////////////////////////////////////
#define adjustStatusByte(status) (status&0x7E)

uint8_t HPLCC2420_cmd ( uint8_t addr );
uint8_t HPLCC2420_write ( uint8_t addr, uint16_t data );
uint16_t HPLCC2420_read ( uint8_t addr );
result_t HPLCC2420RAM_read ( uint16_t addr, uint8_t _length, uint8_t* buffer );
result_t HPLCC2420RAM_write ( uint16_t addr, uint8_t _length, uint8_t* buffer );
result_t HPLCC2420FIFO_readRXFIFO ( uint8_t length, uint8_t *data );
result_t HPLCC2420FIFO_writeTXFIFO ( uint8_t length, uint8_t *data );

result_t FIFOP_startWait ( bool low_to_high );
result_t FIFOP_disable ( void );
result_t FIFOP_disable ( void );
result_t FIFO_disable ( void );
result_t CCA_disable ( void );
result_t SFD_disable ( void );
result_t SFD_startWait ( bool low_to_high );


#define                                                                                        \
FIFOP_interrupt() {                                                                            \
    portTickType time=xTaskGetTickCountFromISR();                                              \
    portBASE_TYPE xHigherPriorityTaskWoken=pdFALSE;                                            \
    setGreenLED();                                                                             \
    FIFOPInterrupt_clear();                                                                    \
    if ( xQueueSendToBackFromISR ( queueFIFOP,&time,&xHigherPriorityTaskWoken ) != pdTRUE ) {  \
    serialPutStr ( "Error queueing New Packet!!\n" );                                          \
    /*allOnLEDs();*/                                                                           \
    }                                                                                          \
    if ( xHigherPriorityTaskWoken==pdTRUE ){                                                   \
      taskYIELD();                                                                             \
      /*serialPutStr ( "_YFP_" );*/                                                                \
    }                                                                                          \
    clearGreenLED();                                                                           \
}

#define                                                                                        \
SFD_interrupt() {                                                                              \
    portTickType time=xTaskGetTickCountFromISR();                                              \
    portBASE_TYPE xHigherPriorityTaskWoken=pdFALSE;                                            \
    /*SFDInterrupt_fired();*/                                                                  \
    /*SFD_disable();*/                                                                         \
    SFDInterrupt_clear();                                                                      \
    /*sprintf(printStr, "SFD(%d)\n",time);serialPutStr(printStr);*/                           \
    if ( xQueueSendToBackFromISR ( queueSFD,&time,&xHigherPriorityTaskWoken ) != pdTRUE ) {    \
        serialPutStr ( "Error queueing New Packet!!\n" );                                      \
        /*allOnLEDs();*/                                                                       \
    }                                                                                          \
    if ( xHigherPriorityTaskWoken==pdTRUE ){                                                   \
      taskYIELD();                                                                             \
      /*serialPutStr ( "_YS_" );*/                                                                 \
    }                                                                                          \
}

#define                                                                                        \
CCA_interrupt() {                                                                              \
    CCAInterrupt_clear();                                                                      \
    CCAInterrupt_disable();                                                                    \
}

#define                                                                                        \
FIFO_interrupt() {                                                                             \
    FIFOInterrupt_clear();                                                                     \
    FIFOInterrupt_disable();                                                                   \
}


#endif /*SIMPLE_CC2420_H_*/
