/////////////////////////////////////////////////////////////////////////////////////////
///  \file HPLCC2420.c
///  \brief Definitions for msp430-CC2420 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "Simple_CC2420.h"
uint8_t HPLCC2420_send(uint8_t data);
uint8_t HPLCC2420_send(uint8_t data) {
  CC2420USART_tx(data);
  while(!SPI_isTxDone());
  while(!SPI_isRxDone());
  return(CC2420USART_rx());
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Send a command strobe
///
///  \param addr: register address
///  \return status byte from the chipcon.  0xff is return of command failed.
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t HPLCC2420_cmd ( uint8_t addr ) {
  uint8_t status;

  CLR_RADIO_CSN_PIN();
  //while (RF_PORT_IN & RF_SPI_SOMI); //WTF!!
  
  status = adjustStatusByte ( HPLCC2420_send(addr) );
    
  //while(UCB0STAT & UCBUSY);  //Not neccesary??
  SET_RADIO_CSN_PIN();
  return status;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Write 16-bit data to CC2420 RAM.
///
///  \param addr: register address
///  \return status byte from the chipcon.  0xff is return of command failed.
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t HPLCC2420_write ( uint8_t addr, uint16_t data ) {
  uint8_t status;

  CLR_RADIO_CSN_PIN();

  status = adjustStatusByte ( HPLCC2420_send(addr) );

  HPLCC2420_send( (data >>8 ) & 0x0FF );
  HPLCC2420_send( data        & 0x0FF );

  SET_RADIO_CSN_PIN();
  return status;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Read 16-bit data from CC2420 RAM.
///
///  \param addr: register address
///  \return 16-bit register value
/////////////////////////////////////////////////////////////////////////////////////////
uint16_t HPLCC2420_read ( uint8_t addr ) {
  uint16_t data;

  CLR_RADIO_CSN_PIN();

  HPLCC2420_send( addr | 0x40 );

  data =(HPLCC2420_send(0)<<8) & 0xFF00;
  data = data | (HPLCC2420_send(0) & 0x0FF);

  SET_RADIO_CSN_PIN();
  return data;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Read a series of bytes from CC2420 RAM.
///
///  \param addr: RAM address
///  \param length: bytes to be writen
///  \param data: buffer
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420RAM_read ( uint16_t addr, uint8_t length, uint8_t* buffer ) {
  uint8_t i;

  CLR_RADIO_CSN_PIN();

  HPLCC2420_send( (addr&0x7F) | 0x80 );
  HPLCC2420_send( ((addr>>1) & 0xC0 ) | 0x20 );

  //if ( length > 0 )
    for ( i = 0; i < length; i++ )
      buffer[i] = HPLCC2420_send(0);

  SET_RADIO_CSN_PIN();
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Writes a series of bytes to the CC2420 RAM.
///
///  \param addr: RAM address
///  \param length: bytes to be writen
///  \param data: buffer
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420RAM_write ( uint16_t addr, uint8_t _length, uint8_t* buffer ) {
  uint8_t i;

  CLR_RADIO_CSN_PIN();

  HPLCC2420_send( (addr & 0x7F) | 0x80 );
  HPLCC2420_send( ((addr >> 1) & 0xC0 ) );

  for ( i = 0; i < _length; i++ )
    HPLCC2420_send( buffer[i] );

  SET_RADIO_CSN_PIN();
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Read a series of bytes from the Rx FIFO.
///
///  \param length: readed bytes
///  \param data: buffer
///  \return \a SUCCESS, \a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420FIFO_readRXFIFO ( uint8_t length, uint8_t *data ) {
  uint8_t i, rxlen;

  CLR_RADIO_CSN_PIN();

  rxlen = HPLCC2420_send( CC2420_RXFIFO | 0x40 );
  rxlen = HPLCC2420_send( 0 );

  if ( rxlen > 0 ) {
    data[0] = rxlen;
    // total length including the length byte
    rxlen++;
    // protect against writing more bytes to the buffer than we have.
    // If this happens something is wrong with the packet (or the caller)
    if ( rxlen > length ) {
      rxlen = length;
      return FAIL;
    }
    for ( i=1; i<rxlen; i++ )
      data[i] = HPLCC2420_send( 0 );
  }
  SET_RADIO_CSN_PIN();

  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Writes a series of bytes to the transmit FIFO.
///
///  \param length: bytes to be writen
///  \param data: buffer
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420FIFO_writeTXFIFO ( uint8_t length, uint8_t *data ) {
  uint8_t i = 0;

  CLR_RADIO_CSN_PIN();
  HPLCC2420_send( CC2420_TXFIFO );
  
  for ( i = 0; i < length; i++ )
    HPLCC2420_send( data[i] );

  SET_RADIO_CSN_PIN();
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Enable an edge interrupt on the FIFOP pin
///
///  This pin signals the arrival of a complete packet or Rx Buffer exceeds FIFOP_THR
///
///  \param low_to_high: Capture Low to High edge or not -> High to Low
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t FIFOP_startWait ( bool low_to_high ) {
    FIFOP_disable();
    FIFOPInterrupt_edge ( low_to_high );
    FIFOPInterrupt_enable();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable an edge interrupt on the FIFOP pin
///
///  This pin signals the arrival of a complete packet or Rx Buffer exceeds FIFOP_THR
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t FIFOP_disable() {
    vTaskSuspendAll();{
        FIFOPInterrupt_disable();
        FIFOPInterrupt_clear();
    }
    xTaskResumeAll();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable an edge interrupt on the FIFO pin
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t FIFO_disable() {
    vTaskSuspendAll();{
        FIFOInterrupt_disable();
        FIFOInterrupt_clear();
    }
    xTaskResumeAll();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable an edge interrupt on the CCA pin
///
///  \warning vTaskSuspendAll/xTaskResumeAll removed to be able to be called form ISR!!
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t CCA_disable() {
    vTaskSuspendAll();{
        CCAInterrupt_disable();
        CCAInterrupt_clear();
    }
    xTaskResumeAll();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable an edge interrupt on the SFD pin
///
///  \warning vTaskSuspendAll/xTaskResumeAll removed to be able to be called form ISR!!
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t SFD_disable() {
    vTaskSuspendAll();{
        SFDInterrupt_disable();
        SFDInterrupt_clear();
    }
    xTaskResumeAll();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Enable an edge interrupt on the SFD pin
///
///  This pin signals the arrival of a packet (recognised or not) or las byte transmited
///
///  \warning vTaskSuspendAll/xTaskResumeAll removed to be able to be called form ISR!!
///
///  \param low_to_high: Capture Low to High edge or not -> High to Low
///
///  \return \a SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t SFD_startWait ( bool low_to_high ) {
    SFD_disable();
    SFDInterrupt_edge ( low_to_high );
    SFDInterrupt_enable();
    return SUCCESS;
}
