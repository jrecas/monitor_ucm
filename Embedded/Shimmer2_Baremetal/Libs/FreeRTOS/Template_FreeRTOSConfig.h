/*
    FreeRTOS V6.0.5 - Copyright (C) 2010 Real Time Engineers Ltd.

    ***************************************************************************
    *                                                                         *
    * If you are:                                                             *
    *                                                                         *
    *    + New to FreeRTOS,                                                   *
    *    + Wanting to learn FreeRTOS or multitasking in general quickly       *
    *    + Looking for basic training,                                        *
    *    + Wanting to improve your FreeRTOS skills and productivity           *
    *                                                                         *
    * then take a look at the FreeRTOS eBook                                  *
    *                                                                         *
    *        "Using the FreeRTOS Real Time Kernel - a Practical Guide"        *
    *                  http://www.FreeRTOS.org/Documentation                  *
    *                                                                         *
    * A pdf reference manual is also available.  Both are usually delivered   *
    * to your inbox within 20 minutes to two hours when purchased between 8am *
    * and 8pm GMT (although please allow up to 24 hours in case of            *
    * exceptional circumstances).  Thank you for your support!                *
    *                                                                         *
    ***************************************************************************

    This file is part of the FreeRTOS distribution.

    FreeRTOS is free software; you can redistribute it and/or modify it under
    the terms of the GNU General Public License (version 2) as published by the
    Free Software Foundation AND MODIFIED BY the FreeRTOS exception.
    ***NOTE*** The exception to the GPL is included to allow you to distribute
    a combined work that includes FreeRTOS without being obliged to provide the
    source code for proprietary components outside of the FreeRTOS kernel.
    FreeRTOS is distributed in the hope that it will be useful, but WITHOUT
    ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
    FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
    more details. You should have received a copy of the GNU General Public
    License and the FreeRTOS license exception along with FreeRTOS; if not it
    can be viewed here: http://www.freertos.org/a00114.html and also obtained
    by writing to Richard Barry, contact details for whom are available on the
    FreeRTOS WEB site.

    1 tab == 4 spaces!

    http://www.FreeRTOS.org - Documentation, latest information, license and
    contact details.

    http://www.SafeRTOS.com - A version that is certified for use in safety
    critical systems.

    http://www.OpenRTOS.com - Commercial support, development, porting,
    licensing and training services.
*/

#ifndef FREERTOS_CONFIG_H
#define FREERTOS_CONFIG_H

/*-----------------------------------------------------------
 * Application specific definitions.
 *
 * These definitions should be adjusted for your particular hardware and
 * application requirements.
 *
 * THESE PARAMETERS ARE DESCRIBED WITHIN THE 'CONFIGURATION' SECTION OF THE
 * FreeRTOS API DOCUMENTATION AVAILABLE ON THE FreeRTOS.org WEB SITE.
 *
 * See http://www.freertos.org/a00110.html.
 *----------------------------------------------------------*/


#define configUSE_PREEMPTION                                   1         ///< Use preemtion
#define configUSE_IDLE_HOOK                                    1         ///< The idle task can optionally call an application defined hook "void vApplicationIdleHook( void );"
#define configUSE_TICK_HOOK                                    0         ///< Use tick hook, if true, the tick interrupt will call "void vApplicationTickHook( void );"

#ifdef MSP430F1611
  #define configCPU_CLOCK_HZ      ( ( unsigned long )  8000000 )         ///< Clock setup
  #define configTICK_RATE_HZ      ( ( portTickType )      3125 )         ///< Port tick rate = 20 Symbols (1symbol=16us) = 1 BackOff
#endif

#ifdef EXP430
  #define configCPU_CLOCK_HZ      ( ( unsigned long )  8000000 )         ///< Clock setup
  #define configTICK_RATE_HZ      ( ( portTickType )      3125 )         ///< Port tick rate = 20 Symbols (1symbol=16us) = 1 BackOff
//  #define configCPU_CLOCK_HZ      ( ( unsigned long ) 16000000 )         ///< Clock setup
//  #define configTICK_RATE_HZ      ( ( portTickType  )    15625 )         ///< Port tick rate = 4 Symbols (1symbol=16us)
#endif

#ifndef configCPU_CLOCK_HZ
	#error "Plattfor not defined (SHIMMER or EXP430 Expected)"
#endif

#define configMAX_PRIORITIES        ( ( unsigned portBASE_TYPE )   5 )   ///< Max priorities

//#define configMINIMAL_STACK_SIZE  ( ( unsigned short ) 50 )
//#define configMINIMAL_STACK_SIZE  ( ( unsigned portSHORT )    64 )       ///< Minimal stack size
#define configMINIMAL_STACK_SIZE  ( ( unsigned portSHORT )   128 )       ///< Minimal stack size
//#define configMINIMAL_STACK_SIZE  ( ( unsigned portSHORT )   256 )       ///< Minimal stack size

//#define configTOTAL_HEAP_SIZE   ( ( size_t ) ( 1800 ) )
//#define configTOTAL_HEAP_SIZE    ( ( size_t )        ( 2500 ) )          ///< Total heap size
//#define configTOTAL_HEAP_SIZE    ( ( size_t )        ( 1 * 1024 ) )      ///< Total heap size
#define configTOTAL_HEAP_SIZE    ( ( size_t )        ( 5 * 1024 ) )      ///< Total heap size
//#define configTOTAL_HEAP_SIZE    ( ( size_t )        ( 7 * 1024 ) )      ///< Total heap size
//#define configTOTAL_HEAP_SIZE    ( ( size_t )        ( 9 * 1024 ) )      ///< Total heap size

#define configMAX_TASK_NAME_LEN                              ( 8 )       ///< Maximum task lengt name
#define configUSE_TRACE_FACILITY                               0         ///<
#define configUSE_16_BIT_TICKS                                 0         ///< Use a 16b variable for the tick counter (if not, 32b)
#define configIDLE_SHOULD_YIELD                                0         ///< Idle task should yield

/* Co-routine definitions. */
#define configUSE_CO_ROUTINES                                  0         ///< Use co routines
#define configMAX_CO_ROUTINE_PRIORITIES                      ( 1 )

#define configCHECK_FOR_STACK_OVERFLOW                         2         ///< Check for stack overflow during switch context?
#define configUSE_APPLICATION_TASK_TAG                         1         ///> Aplication task tag (can be *f())
#define INCLUDE_uxTaskGetStackHighWaterMark                    1         ///< uxTaskGetStackHighWaterMark() utility
#define WARN_STACK_LEFT                                       32         ///< Minimum value for free stack until warning

/* Set the following definitions to 1 to include the API function, or zero
to exclude the API function. */

#define INCLUDE_vTaskPrioritySet                               0        ///<
#define INCLUDE_uxTaskPriorityGet                              0        ///<
#define INCLUDE_vTaskDelete                                    0        ///<
#define INCLUDE_vTaskCleanUpResources                          0        ///<
#define INCLUDE_vTaskSuspend                                   0        ///< Task suspend enabled
#define INCLUDE_vTaskDelayUntil                                1        ///< Task delay until
#define INCLUDE_vTaskDelay                                     1        ///<


#endif /* FREERTOS_CONFIG_H */
