/////////////////////////////////////////////////////////////////////////////////////////
/// \file leds.h
/// \brief LEDs utils header file for Exp430
///
/// Function \a initLEDs has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date November 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef LEDS_H
#define LEDS_H

//#include "platform.h"

#ifdef SHIMMER1
  #include "ledsShimmer.h"
#endif	

#ifdef SHIMMER2
  #include "ledsShimmer.h"
#endif	

#ifdef TS430PM64
  #include "ledsTS430PM64.h"
#endif	

#ifdef EXP430
  #include "ledsExp430.h"
#endif	

#ifndef allOnLEDs
  #error "Platform not defined (SHIMMER or EXP430)"
#endif

#endif

