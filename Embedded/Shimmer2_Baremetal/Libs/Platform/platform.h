/////////////////////////////////////////////////////////////////////////////////////////
/// \file platform.h
/// \brief Definition of the current platform used in the project and useful definitions
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 3.0
/// \date Novermber 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef PLATFORM_H_
#define PLATFORM_H_

#include "stdint.h"

//Plattform
#if !defined(EXP430) && !defined(SHIMMER1) && !defined(SHIMMER2) && !defined(TS430PM64)
	#error "Platform not defined (EXP430 or TS430PM64 or SHIMMER1 or SHIMMER2 Expected)"
#endif

//Compiler
#if !defined(GCC) && !defined(CCS) && !defined(GCC_TI)
	#error "Compiler not defined (GCC or CSS Expected)"
#endif

//Microprocessor
#if !defined(MSP430F1611) && !defined(MSP430F5438A) && !defined(MSP430F6638)
	#error "MCU not defined (MSP430F1611 or MSP430F5438A or MSP430F6638 Expected)"
#endif


//FreeRTOS Port
//#define FREERTOS_MSP430_PORT
//FreeRTOS
//#include "portmacro.h"
//#include "FreeRTOS.h"
//#include "FreeRTOSConfig.h"
//#include "task.h"
//#include "queue.h"
//#include "semphr.h"

#ifdef MSP430F6638
  #include <msp430f6638.h>
#endif

#ifdef MSP430F5438A
  #include <msp430x54xA.h>
#endif

#ifdef MSP430F1611
#ifdef GCC_TI
#include <msp430f1611.h>
#else
  #include <msp430x16x.h>
#endif
#endif

#if defined(SHIMMER1) || defined (SHIMMER2)
#include "Shimmer.h"
#define MCU_FREQ      ( ( unsigned long )  8000000 )
#endif


#ifdef EXP430
  #include "exp430_hal.h"
#endif

int initPlatform(unsigned long cpuHz);

typedef uint8_t result_t;   ///< Typical data type to be returned by functions
typedef uint8_t bool;       ///< Bool data type

/// Valid values for \a bool variables
enum {
  FALSE = 0,
  TRUE = 1
};

/// Standard codes for result_t
enum {
  FAIL = 0,
  SUCCESS = 1
};

#endif //PLATFORM_H_
