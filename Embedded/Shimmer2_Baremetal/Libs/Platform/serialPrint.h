/////////////////////////////////////////////////////////////////////////////////////////
/// \file serialPrint.h
/// \brief Serial COMM utils header file for Shimmer
///
/// Function \a serialPortInit has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date October 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SERIAL_COMMS_H
#define SERIAL_COMMS_H

//sprintf support
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <signal.h>
#include "platform.h"

#if defined(SHIMMER1) || defined(SHIMMER2)
  #include "serialPort1611.h"
#endif

#ifdef EXP430
  #include "sPrExp430.h"
#endif

#define SIZE_PRINTSTR   50                       ///< Size of the global string for printing data
extern char printStr[SIZE_PRINTSTR];             ///< Global String for printing data

/// Valid values for BaudRate
enum {
  BAUDRATE_9600,
  BAUDRATE_57600,
  BAUDRATE_115200
};

//Functions commens in serial.c
uint8_t serialPortInit ( uint8_t channel, unsigned long fCLK, uint8_t baudRate, void ( *newDataFunc ) ( uint8_t ) );
uint8_t serialRX( uint8_t channel );
void serialTX( uint8_t channel, uint8_t data );
void serialPutStr ( uint8_t portNumber, char * string );

#endif

