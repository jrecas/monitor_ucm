/////////////////////////////////////////////////////////////////////////////////////////
/// \file serialPrint.c
/// \brief Serial COMM utils code file for EXP430
///
/// Function \a serialPortInit has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date October 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "serialPrint.h"

char printStr[SIZE_PRINTSTR];          ///< Global String for printing data

/// Global function pointer for the function to be called when new data arrives
void ( *fpNewData ) ( uint8_t ) =NULL;

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Init Serial Port 0
///
/// The serial port will be initialized:
///   - For msp430f5438a: USART 1 Clock:     SMCLK
///   - Data bits: 8
///   - Stop bit:  1
///   - No parity
///
/// \param fCLK CLK frequency. Valid frequencies definition in \a serialPrint.h
/// \param baudRate serial link BaudRate. Valid BaudRates definition in \a serialPrint.h
/// \param newData function to be called when a byte arrives. Null if discard.
///
/// \return \a SUCCESS/\a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t serialPortInit ( unsigned long fCLK, uint8_t baudRate, void ( *newDataFunc ) ( uint8_t ) ) {

  USB_PORT_SEL |= USB_PIN_RXD + USB_PIN_TXD;
  USB_PORT_DIR |= USB_PIN_TXD;
  USB_PORT_DIR &= ~USB_PIN_RXD;
  
  // UCA1CTL1    ->  USCI_A1 Control Register 1
  //  .UCSSEL1   =   USCI clock source select (2 SMCLK)
  //  .UCRXEIE   =   Receive erroneous-character interrupt enable
  //  .UCBRKIE   =   Receive break character interrupt enable
  //  .UCDORM    =   Dormant. Puts USCI into sleep mode.
  //  .UCTXADDR  =   Transmit address
  //  .UCTXBRK   =   Transmit break
  //  .UCSWRST   =   Software reset enable
  UCA1CTL1 = UCSSEL_2 | UCSWRST;                          //Reset State                      
  
  // UCA1CTL0    ->  USCI_A1 Control Register 0
  //  .UCPEN     =   Parity enable
  //  .UCPAR     =   Parity select.
  //  .UCMSB     =   MSB first select.
  //  .UC7BIT    =   Character length (0 8b data).
  //  .UCSPB     =   Stop bit select.
  //  .UCMODEx   =   USCI mode (00 UART mode).
  //  .UCSYNC    =   Synchronous mode enable.
  UCA1CTL0 = UCMODE_0;
  UCA1CTL0 &= ~UC7BIT;  
    
  //f=25000000;b=57600;N=f/b;
  //ucbr=floor(N)=0x1B2
  //ucbrs=round( (N-ucbr)*8)=0
  switch ( baudRate ) {
    case BAUDRATE_57600:
      switch ( fCLK ) {
      	//PARECE QUE NO FUNCIONA BIEN, NO SE POR QU�
        case 8000000:   
          //UCBR1
          UCA1BR0 = 16;    //USCI_A1 Baud Rate Control Register 0 (UCA1BR0)                            
          UCA1BR1 = 1;    //USCI_A1 Baud Rate Control Register 1 (UCA1BR1)                             
          // USCI_A1   -> Modulation Control Register (UCAxMCTL)
          //  .UCBRF1  =  First modulation stage select.
          //  .UCBRS1  =  Second modulation stage select.
          //  .UCOS16  =  Oversampling mode enabled (Disabled)
          UCA1MCTL = 0xE;
          break;
        case 16000000:
          //UCBR1
          UCA1BR0 = 0x15;    //USCI_A1 Baud Rate Control Register 0 (UCA1BR0)                            
          UCA1BR1 = 0x01;    //USCI_A1 Baud Rate Control Register 1 (UCA1BR1)                             
          // USCI_A1   -> Modulation Control Register (UCAxMCTL)
          //  .UCBRF1  =  First modulation stage select.
          //  .UCBRS1  =  Second modulation stage select.
          //  .UCOS16  =  Oversampling mode enabled (Disabled)
          UCA1MCTL = UCBRF_0 | UCBRS_7;
          break;
        case 25000000:     //57600Baud @ 25MHz
          //UCBR1
          UCA1BR0 = 0xB2;    //USCI_A1 Baud Rate Control Register 0 (UCA1BR0)                            
          UCA1BR1 = 0x01;    //USCI_A1 Baud Rate Control Register 1 (UCA1BR1)                             
          // USCI_A1   -> Modulation Control Register (UCAxMCTL)
          //  .UCBRF1  =  First modulation stage select.
          //  .UCBRS1  =  Second modulation stage select.
          //  .UCOS16  =  Oversampling mode enabled (Disabled)
          UCA1MCTL = UCBRF_0 | UCBRS_0;
          break;
        default:
          return ( FAIL );
      }
      break;
        
    case BAUDRATE_115200:
      switch ( fCLK ) {
        case 8000000:
          //UCBR1
          UCA1BR0 = 0x45;    //USCI_A1 Baud Rate Control Register 0 (UCA1BR0)                            
          UCA1BR1 = 0x00;    //USCI_A1 Baud Rate Control Register 1 (UCA1BR1)                             
          // USCI_A1   -> Modulation Control Register (UCAxMCTL)
          //  .UCBRF1  =  First modulation stage select.
          //  .UCBRS1  =  Second modulation stage select.
          //  .UCOS16  =  Oversampling mode enabled (Disabled)
          UCA1MCTL = UCBRF_0 | UCBRS_4;
          break;
        case 16000000:
          //UCBR1
          UCA1BR0 = 0x8A;    //USCI_A1 Baud Rate Control Register 0 (UCA1BR0)                            
          UCA1BR1 = 0x00;    //USCI_A1 Baud Rate Control Register 1 (UCA1BR1)                             
          // USCI_A1   -> Modulation Control Register (UCAxMCTL)
          //  .UCBRF1  =  First modulation stage select.
          //  .UCBRS1  =  Second modulation stage select.
          //  .UCOS16  =  Oversampling mode enabled (Disabled)
          UCA1MCTL = UCBRF_0 | UCBRS_7;
          break;
        case 25000000:
          //UCBR1
          UCA1BR0 = 0xD9;    //USCI_A1 Baud Rate Control Register 0 (UCA1BR0)                            
          UCA1BR1 = 0x00;    //USCI_A1 Baud Rate Control Register 1 (UCA1BR1)                             
          // USCI_A1   -> Modulation Control Register (UCAxMCTL)
          //  .UCBRF1  =  First modulation stage select.
          //  .UCBRS1  =  Second modulation stage select.
          //  .UCOS16  =  Oversampling mode enabled (Disabled)
          UCA1MCTL = UCBRF_0 | UCBRS_0;
          break;
        default:
          return ( FAIL );
      }
      break;
    default:
      return ( FAIL );
  }
  
  //Enable UART
  UCA1CTL1 &= ~UCSWRST;
    
  //A function has to be called when new data arrives?
  if ( newDataFunc ) {
    fpNewData=newDataFunc;
    //Enable interrupt for reception
    UCA1IE |= UCRXIE;
    //Enable Global Interrupts
    __bis_SR_register(GIE);     
  }
  return ( SUCCESS );
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Put a string into Serial Port 0
///
/// Put a null ended string in the serial port 0 output buffer and wait to be sended
/// \param string Char string to be placed on the output buffer ('\\0' ended)
/////////////////////////////////////////////////////////////////////////////////////////
void serialPutStr ( char * string ) {
  unsigned char i=0;
  while ( string[i] ) {
    serialPutChar(string[i++]);
  }
}
