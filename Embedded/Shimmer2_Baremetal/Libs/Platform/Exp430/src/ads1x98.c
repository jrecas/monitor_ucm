/////////////////////////////////////////////////////////////////////////////////////////
///  \file ads1x98.c
///  \brief Definitions for msp430-ads1x98 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date October 2013
/////////////////////////////////////////////////////////////////////////////////////////

#include "ads1x98.h"

void initADS_USART(unsigned long smClkHz);


/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Initialice CC2420 Hardware Presentation Layer
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t ADS1X98_init(unsigned long smClkHz) {

	//SPI
    SEL_ADS_SIMO_MODFUNC();
    SEL_ADS_SOMI_MODFUNC();
    SEL_ADS_CLK_MODFUNC();

    //Start Select
    MAKE_ADS_START_OUTPUT();
    SEL_ADS_START_IOFUNC();
    CLR_ADS_START_PIN();
    
	//Reset
    MAKE_ADS_RST_OUTPUT();
    SEL_ADS_RST_IOFUNC();
    CLR_ADS_RST_PIN();

    //Chip Select
    MAKE_ADS_CS_OUTPUT();
    SEL_ADS_CS_IOFUNC();
    SET_ADS_CS_PIN();

	//Power Down
    MAKE_ADS_PDOWN_OUTPUT();
    SEL_ADS_PDOWN_IOFUNC();
    CLR_ADS_PDOWN_PIN();
    
    //Clock Select
    MAKE_ADS_CLKSEL_OUTPUT();
    SEL_ADS_CLKSEL_IOFUNC();
    CLR_ADS_CLKSEL_PIN();

	//Data ready
    MAKE_ADS_DRDY_INPUT();
    SEL_ADS_DRDY_IOFUNC();
    
    ADS_disableRxIntr();
    ADS_disableTxIntr();

    initADS_USART(smClkHz);

	//Power-Up sequencing
    CLR_ADS_CLKSEL_PIN();		//Internal Clock
    msWaitNOP(1000);
    SET_ADS_PDOWN_PIN();		//Power up
    SET_ADS_RST_PIN();			//Reset cycle
    msWaitNOP(2000);
	CLR_ADS_CS_PIN();
	
	
    //send the stop command	(SDATAC)
	ADS_send_data(0x11);
    msWaitNOP(1000);

    //read the reg from the 1th reg and the number is c+1;
	ADS_send_data(0x21);
    msWaitNOP(1000);
  	ADS_send_data(0x0c);
    msWaitNOP(1000);
      	
    return ( SUCCESS );
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable CC2420 USART comunication
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t ADS_stop() {
    UCB0CTL1 |= UCSWRST;                    // USART1 SPI module disable
    SEL_ADS_SIMO_IOFUNC();
    SEL_ADS_SOMI_IOFUNC();
    SEL_ADS_CLK_IOFUNC();
    CLR_ADS_CS_PIN();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Config serial link between MSP430 and CC2420 for EXP430
/////////////////////////////////////////////////////////////////////////////////////////
void initADS_USART(unsigned long smClkHz) {

  UCB0CTL1 = UCSWRST;
   
  // 8-bit SPI Master 3-pin mode, with SMCLK as clock source
  UCB0CTL0 |= UCCKPH + UCMSB + UCMST ;
  UCB0CTL1 |= UCSSEL_2;

  if(smClkHz>ADS1X98_MAX_FREQ){
    UCB0BR0 = smClkHz/ADS1X98_MAX_FREQ;     // Clk div Low Byte
  }   
  UCB0BR1 = 0;                              // Clk div High Byte
  UCB0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  UCB0IFG  &= ~UCRXIFG;
}


/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Tx pending bit is set (and clear it).
///
///  USART1 transmit interrupt flag UTXIFG1 is set when U1TXBUF is empty.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t ADS_isTxIntrPending(void) {
    if (UCB0IFG & UCTXIFG) {
        UCB0IFG &= ~UCTXIFG;
        return SUCCESS;
    }
    return FAIL;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Rx pending bit is set (and clear it).
///
///  USART1 receive interrupt flag URXIFG1 is set when U1RXBUF has received a complete
///  character.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t ADS_isRxIntrPending(void) {
    if (UCB0IFG & UCRXIFG) {
        UCB0IFG &= ~UCRXIFG;
        return SUCCESS;
    }
    return FAIL;
}


/*
/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Transmitter empty flag bit is set
///
///   TXEPT is set if U1TXBUF and TX shift register are empty.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t CC2420USART_isTxEmpty(void) {
    if (U1TCTL & TXEPT) {
        return SUCCESS;
    }
    return FAIL;
}
*/

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief ISR for Port1
///
///  ***NOTE*** Do not call FreeRTOS functions unless they have 'FromISR' tail
///
/////////////////////////////////////////////////////////////////////////////////////////

//wakeup -> Wake the processor from any low power state as the routine exits
#ifdef CCS
  #pragma vector=PORT1_VECTOR
  __interrupt void Port1_isr ( void ) {
#endif

    volatile int n = P1IFG & P1IE;
/*
    //FIFOP interrupt
    if ( n & ( BIT6 ) ) {
        FIFOP_interrupt();
        return;
    }
    //CCA interrupt
    if ( n & ( BIT7 ) ) {
        CCA_interrupt();
        return;
    }
    //FIFO interrupt
    if ( n & ( BIT5 ) ) {
        FIFO_interrupt();
        return;
    }
    //SFD interrupt
    if ( n & ( BIT3 ) ) {
        SFD_interrupt();
        return;
    }
    */
    while ( 1 );
}

