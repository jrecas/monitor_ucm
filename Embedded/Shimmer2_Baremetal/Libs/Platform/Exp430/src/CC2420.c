/////////////////////////////////////////////////////////////////////////////////////////
///  \file HPLCC2420.c
///  \brief Definitions for msp430-CC2420 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "CC2420.h"

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Initialice CC2420 Hardware Presentation Layer
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420_init(unsigned long smClkHz) {

    //RADIO PINS
    //CC2420 pins
    MAKE_RADIO_CSN_OUTPUT();
    SEL_RADIO_CSN_IOFUNC();
    SET_RADIO_CSN_PIN();

    MAKE_CC_RSTN_OUTPUT();
    SEL_CC_RSTN_IOFUNC();
    CLR_CC_RSTN_PIN();

    MAKE_CC_VREN_OUTPUT();
    SEL_CC_VREN_IOFUNC();
    CLR_CC_VREN_PIN();

    MAKE_CC_CCA_INPUT();
    SEL_CC_CCA_IOFUNC();

    MAKE_CC_FIFO_INPUT();
    SEL_CC_FIFO_IOFUNC();

    MAKE_CC_FIFOP_INPUT();
    SEL_CC_FIFOP_IOFUNC();

    MAKE_CC_SFD_INPUT();
    SEL_CC_SFD_IOFUNC();

    SEL_SIMO_MODFUNC();
    SEL_SOMI_MODFUNC();
    SEL_UCLK_MODFUNC();

  // Set up pins used by peripheral unit
  //RF_PORT_SEL |= RF_SPI_SIMO + RF_SPI_SOMI + RF_SPI_CLK;
  //RF_PORT_DIR |= RF_SPI_CS;

    initCC2420USART(smClkHz);
    
    CC2420USART_disableRxIntr();
    CC2420USART_disableTxIntr();

    return ( SUCCESS );
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Start CC2420 Hardware Presentation Layer
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420_start() {
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable CC2420 USART comunication
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420_stop() {
    CC2420USART_disableSPI();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Config serial link between MSP430 and CC2420 for EXP430
/////////////////////////////////////////////////////////////////////////////////////////
void initCC2420USART(unsigned long smClkHz) {

  UCB0CTL1 = UCSWRST;
   
  // 8-bit SPI Master 3-pin mode, with SMCLK as clock source
  UCB0CTL0 |= UCCKPH + UCMSB + UCMST ;
  UCB0CTL1 |= UCSSEL_2;

  if(smClkHz>CC2420_MAX_FREQ){
    UCB0BR0 = smClkHz/CC2420_MAX_FREQ;      // Clk div Low Byte
  }   
  UCB0BR1 = 0;                              // Clk div High Byte
  UCB0CTL1 &= ~UCSWRST;                     // **Initialize USCI state machine**
  UCB0IFG &= ~UCRXIFG;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable USART1 SPI module
/////////////////////////////////////////////////////////////////////////////////////////
void CC2420USART_disableSPI(void) {
    UCB0CTL1 |= UCSWRST;                    // USART1 SPI module disable
    SEL_SIMO_IOFUNC();
    SEL_SOMI_IOFUNC();
    SEL_UCLK_IOFUNC();
    CLR_RADIO_CSN_PIN();
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Tx pending bit is set (and clear it).
///
///  USART1 transmit interrupt flag UTXIFG1 is set when U1TXBUF is empty.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t CC2420USART_isTxIntrPending(void) {
    if (UCB0IFG & UCTXIFG) {
        UCB0IFG &= ~UCTXIFG;
        return SUCCESS;
    }
    return FAIL;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Rx pending bit is set (and clear it).
///
///  USART1 receive interrupt flag URXIFG1 is set when U1RXBUF has received a complete
///  character.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t CC2420USART_isRxIntrPending(void) {
    if (UCB0IFG & UCRXIFG) {
        UCB0IFG &= ~UCRXIFG;
        return SUCCESS;
    }
    return FAIL;
}


/*
/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Transmitter empty flag bit is set
///
///   TXEPT is set if U1TXBUF and TX shift register are empty.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t CC2420USART_isTxEmpty(void) {
    if (U1TCTL & TXEPT) {
        return SUCCESS;
    }
    return FAIL;
}
*/

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief ISR for Port1
///
///  ***NOTE*** Do not call FreeRTOS functions unless they have 'FromISR' tail
///
/////////////////////////////////////////////////////////////////////////////////////////

//wakeup -> Wake the processor from any low power state as the routine exits
#ifdef CCS
  #pragma vector=PORT1_VECTOR
  __interrupt void Port1_isr ( void ) {
#endif

    volatile int n = P1IFG & P1IE;

    //FIFOP interrupt
    if ( n & ( BIT6 ) ) {
        FIFOP_interrupt();
        return;
    }
    //CCA interrupt
    if ( n & ( BIT7 ) ) {
        CCA_interrupt();
        return;
    }
    //FIFO interrupt
    if ( n & ( BIT5 ) ) {
        FIFO_interrupt();
        return;
    }
    //SFD interrupt
    if ( n & ( BIT3 ) ) {
        SFD_interrupt();
        return;
    }
    
    while ( 1 );
}

