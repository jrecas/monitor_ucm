/////////////////////////////////////////////////////////////////////////////////////////
///  \file CC2420.h
///  \brief Definitions for msp430-CC2420 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef ADS1X98_H_
#define ADS1X98_H_

#include "platform.h"

//ISR definitions
#include <signal.h>

#define ADS1X98_MAX_FREQ           9000000

#ifdef MSP430F5438A

	#define  SEL_ADS_SIMO_MODFUNC()       (P3SEL |=  BIT1)    ///< P3.1/SIMO
	#define  SEL_ADS_SOMI_MODFUNC()       (P3SEL |=  BIT2)    ///< P3.2/SOMI
	#define  SEL_ADS_CLK_MODFUNC()        (P3SEL |=  BIT3)    ///< P3.3/UCLK

	#define  SEL_ADS_SIMO_IOFUNC()        (P3SEL &= ~BIT1)    ///< P3.1/SIMO
	#define  SEL_ADS_SOMI_IOFUNC()        (P3SEL &= ~BIT2)    ///< P3.2/SOMI
	#define  SEL_ADS_CLK_IOFUNC()         (P3SEL &= ~BIT3)    ///< P3.3/UCLK

	#define  MAKE_ADS_CS_OUTPUT()         (P3DIR |=  BIT0)    //P3.0 -> SPI1_CS
	#define  SEL_ADS_CS_IOFUNC()          (P3SEL &= ~BIT0)
	#define  SET_ADS_CS_PIN()             (P3OUT |=  BIT0)
	#define  CLR_ADS_CS_PIN()             (P3OUT &= ~BIT0)

	#define  MAKE_ADS_RST_OUTPUT()        (P1DIR |=  BIT2)    //P1.2 -> RESET
	#define  SEL_ADS_RST_IOFUNC()         (P1SEL &= ~BIT2)
	#define  CLR_ADS_RST_PIN()            (P1OUT &= ~BIT2)
	#define  SET_ADS_RST_PIN()            (P1OUT |=  BIT2)

	#define  MAKE_ADS_PDOWN_OUTPUT()      (P1DIR |=  BIT4)    //P1.4 -> POWER DOWN
	#define  SEL_ADS_PDOWN_IOFUNC()       (P1SEL &= ~BIT4)
	#define  CLR_ADS_PDOWN_PIN()          (P1OUT &= ~BIT4)
	#define  SET_ADS_PDOWN_PIN()          (P1OUT |=  BIT4)

	#define  MAKE_ADS_CLKSEL_OUTPUT()     (P1DIR |=  BIT7)    //P1.7 -> CLK SEL PIN
	#define  SEL_ADS_CLKSEL_IOFUNC()      (P1SEL &= ~BIT7)
	#define  CLR_ADS_CLKSEL_PIN()         (P1OUT &= ~BIT7)
	#define  SET_ADS_CLKSEL_PIN()         (P1OUT |=  BIT7)

	#define  MAKE_ADS_START_OUTPUT()      (P1DIR |=  BIT6)    //P1.6 -> START
	#define  SEL_ADS_START_IOFUNC()       (P1SEL &= ~BIT6)
	#define  CLR_ADS_START_PIN()          (P1OUT &= ~BIT6)
	#define  SET_ADS_START_PIN()          (P1OUT |=  BIT6)

	#define  MAKE_ADS_DRDY_INPUT()        (P1DIR &= ~BIT5)    //P1.5 -> RADIO_FIFO  OK
	#define  SEL_ADS_DRDY_IOFUNC()        (P1SEL &= ~BIT5)
	
	#define  ADS_disableRxIntr()          (UCB0IE &= ~UCRXIE) //
	#define  ADS_disableTxIntr()          (UCB0IE &= ~UCTXIE) //

	#define SPI_isTxDone()                (UCB0IFG & UCTXIFG)
	#define SPI_isRxDone()                (UCB0IFG & UCRXIFG)

	#define  ADS_rx()                     (UCB0RXBUF)         //
	#define  ADS_tx(data)                 (UCB0TXBUF = (data))//

	#define ADS_send_data(data)                                       \
	                                      while(!SPI_isTxDone());     \
	                                      ADS_tx(data);




	#define  MAKE_CC_FIFOP_INPUT()      (P1DIR &= ~BIT6)    //P1.6 -> RADIO_FIFOP OK
	#define  SEL_CC_FIFOP_IOFUNC()      (P1SEL &= ~BIT6)

	#define  MAKE_CC_SFD_INPUT()        (P1DIR &= ~BIT3)    //P1.3 -> RADIO_SFD  OK
	#define  SEL_CC_SFD_IOFUNC()        (P1SEL &= ~BIT3)
	#define  READ_CC_SFD_PIN()          (P1IN  &   BIT3)


	//#define  SEL_UTXD1_IOFUNC()            (P3SEL &= ~0x40)    ///< P3.6/UTXD1
	//#define  SEL_URXD1_IOFUNC()            (P3SEL &= ~0x80)    ///< P3.7/URXD1



	#define FIFOPInterrupt_enable()   P1IE  |=  (BIT6)  //Ok
	#define FIFOPInterrupt_disable()  P1IE  &= ~(BIT6)
	#define FIFOPInterrupt_clear()    P1IFG &= ~(BIT6)

	#define CCAInterrupt_enable()     P1IE  |=  (BIT7)  //Ok
	#define CCAInterrupt_disable()    P1IE  &= ~(BIT7)
	#define CCAInterrupt_clear()      P1IFG &= ~(BIT7)

	#define FIFOInterrupt_enable()    P1IE  |=  (BIT5)  //Ok
	#define FIFOInterrupt_disable()   P1IE  &= ~(BIT5)
	#define FIFOInterrupt_clear()     P1IFG &= ~(BIT5)

	#define SFDInterrupt_enable()     P1IE  |=  (BIT3)  //Ok
	#define SFDInterrupt_disable()    P1IE  &= ~(BIT3)
	#define SFDInterrupt_clear()      P1IFG &= ~(BIT3)

	#define FIFOPInterrupt_edge(low_to_high)         \
	          if(low_to_high)  P1IES &= ~(BIT6);     \
	          else             P1IES |=  (BIT6);

	#define SFDInterrupt_edge(low_to_high)           \
	          if(low_to_high)  P1IES &= ~(BIT3);     \
	          else             P1IES |=  (BIT3);

	#define CCAInterrupt_edge(low_to_high)           \
	          if(low_to_high)  P1IES &= ~(BIT7);     \
	          else             P1IES |=  (BIT7);

	#define CC2420USART_isTxEmpty()  (!(UCB0STAT&UCBUSY))


#endif

#ifndef SEL_ADS_CLK_MODFUNC
	#error "Microcontroller not defined (MSP430F5438A Expected)"
#endif


#define VREFOn()                 SET_CC_VREN_PIN()
#define VREFOff()                CLR_CC_VREN_PIN()

#define ResetOn()                CLR_CC_RSTN_PIN()
#define ResetOff()               SET_CC_RSTN_PIN()

result_t ADS1X98_init(unsigned long smClkHz);
result_t ADS_stop(void);


//result_t CC2420USART_isTxIntrPending(void);
result_t CC2420USART_isRxIntrPending(void);
//result_t CC2420USART_isTxEmpty(void);


#endif /*ADS1X98_H_*/
