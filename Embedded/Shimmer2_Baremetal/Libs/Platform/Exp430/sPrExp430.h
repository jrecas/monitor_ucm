/////////////////////////////////////////////////////////////////////////////////////////
/// \file serialPrint.h
/// \brief Serial COMM utils header file for Shimmer
///
/// Function \a serialPortInit has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date October 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SERIAL_COMMS_EXP430_H
#define SERIAL_COMMS_EXP430_H

//#ifdef MSP430F5438A
  #define USB_PORT_OUT      P5OUT
  #define USB_PORT_SEL      P5SEL
  #define USB_PORT_DIR      P5DIR
  #define USB_PORT_REN      P5REN
  #define USB_PIN_TXD       BIT6
  #define USB_PIN_RXD       BIT7

#define  serialGetChar()               (U1RXBUF)
#define  serialPutChar(data){          \
  while (!(UCA1IFG & UCTXIFG));        \
  UCA1TXBUF = data;                    \
}
//#endif


#endif

