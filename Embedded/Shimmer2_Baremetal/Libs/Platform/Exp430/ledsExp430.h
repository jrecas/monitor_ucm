/////////////////////////////////////////////////////////////////////////////////////////
/// \file leds.h
/// \brief LEDs utils header file for Exp430
///
/// Function \a initLEDs has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date November 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef LEDS_EXP430_H
#define LEDS_EXP430_H

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Initialize LED's HW and set them off
/////////////////////////////////////////////////////////////////////////////////////////
#ifdef MSP430F5438A
  #define initLEDs() { \
    P1SEL &= ~0x03;    \
    P1DIR |=  0x03;    \
    P1OUT &= ~0x03;    \
  }
#endif	
#ifdef MSP430F6638
  #define initLEDs() { \
    P2SEL &= ~0x03;    \
    P2DIR |=  0x03;    \
    P2OUT &= ~0x03;    \
  }
#endif  

#ifdef MSP430F5438A
  #define  setRedLED()       P1OUT |=  0x01;      ///< Set Red LED
  #define  setYellowLED()    P1OUT |=  0x02;      ///< Set Orange LED
  #define  clearRedLED()     P1OUT &= ~0x01;      ///< Clear Red LED
  #define  clearYellowLED()  P1OUT &= ~0x02;      ///< Clear Orange LED
  #define  toggleRedLED()    P1OUT ^=  0x01;      ///< Toggle Red LED
  #define  toggleYellowLED() P1OUT ^=  0x02;      ///< Toggle Orange LED
  #define  toggleGreen()
#endif  
#ifdef MSP430F6638
  #define  setRedLED()       P2OUT |=  0x01;      ///< Set Red LED
  #define  setYellowLED()    P2OUT |=  0x02;      ///< Set Orange LED
  #define  clearRedLED()     P2OUT &= ~0x01;      ///< Clear Red LED
  #define  clearYellowLED()  P2OUT &= ~0x02;      ///< Clear Orange LED
  #define  toggleRedLED()    P2OUT ^=  0x01;      ///< Toggle Red LED
  #define  toggleYellowLED() P2OUT ^=  0x02;      ///< Toggle Orange LED
#endif  

#ifndef setRedLED
	#error "Microcontroller not defined (MSP430F5438A or MSP430F6638 Expected)"
#endif

#define  setGreenLED()
#define  clearGreenLED()
#define  toggleGreenLED()

#ifdef MSP430F5438A
  #define allOnLEDs()        P1OUT |=  0x03;      ///< Set all LEDs
#endif  
#ifdef MSP430F6638
  #define allOnLEDs()        P2OUT |=  0x03;      ///< Set all LEDs
#endif  

#endif

