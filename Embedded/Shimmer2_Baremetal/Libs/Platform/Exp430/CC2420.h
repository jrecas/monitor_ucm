/////////////////////////////////////////////////////////////////////////////////////////
///  \file CC2420.h
///  \brief Definitions for msp430-CC2420 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef CC2420_H_
#define CC2420_H_

//#include "platform.h"

//ISR definitions
#include <signal.h>

#include "Simple_CC2420.h"
#include "CC2420Const.h"
/*
#define RF_PORT_OUT     P3OUT
#define RF_PORT_DIR     P3DIR
#define RF_PORT_SEL     P3SEL
#define RF_PORT_IN      P3IN
#define RF_SPI_SIMO     BIT1
#define RF_SPI_SOMI     BIT2
#define RF_SPI_CLK      BIT3
#define RF_SPI_CS       BIT0
*/

#ifdef MSP430F5438A
	#define  MAKE_RADIO_CSN_OUTPUT()    (P3DIR |=  BIT0)    //P3.0 -> SPI1_CS_RADIO_N OK
	#define  SEL_RADIO_CSN_IOFUNC()     (P3SEL &= ~BIT0)
	#define  SET_RADIO_CSN_PIN()        (P3OUT |=  BIT0)
	#define  CLR_RADIO_CSN_PIN()        (P3OUT &= ~BIT0)

	#define  MAKE_CC_RSTN_OUTPUT()      (P1DIR |=  BIT2)    //P1.2 -> RESET_RADIO_N  OK
	#define  SEL_CC_RSTN_IOFUNC()       (P1SEL &= ~BIT2)
	#define  CLR_CC_RSTN_PIN()          (P1OUT &= ~BIT2)
	#define  SET_CC_RSTN_PIN()          (P1OUT |=  BIT2)

	#define  MAKE_CC_VREN_OUTPUT()      (P1DIR |=  BIT4)    //P1.4 -> REG_1V8_EN_N  OK
	#define  SEL_CC_VREN_IOFUNC()       (P1SEL &= ~BIT4)
	#define  CLR_CC_VREN_PIN()          (P1OUT &= ~BIT4)
	#define  SET_CC_VREN_PIN()          (P1OUT |=  BIT4)

	#define  MAKE_CC_CCA_INPUT()        (P1DIR &= ~BIT7)    //P1.7 -> RADIO_CCA  OK
	#define  SEL_CC_CCA_IOFUNC()        (P1SEL &= ~BIT7)
	#define  READ_CC_CCA_PIN()          (P1IN  &   BIT7)

	#define  MAKE_CC_FIFO_INPUT()       (P1DIR &= ~BIT5)    //P1.5 -> RADIO_FIFO  OK
	#define  SEL_CC_FIFO_IOFUNC()       (P1SEL &= ~BIT5)

	#define  MAKE_CC_FIFOP_INPUT()      (P1DIR &= ~BIT6)    //P1.6 -> RADIO_FIFOP OK
	#define  SEL_CC_FIFOP_IOFUNC()      (P1SEL &= ~BIT6)

	#define  MAKE_CC_SFD_INPUT()        (P1DIR &= ~BIT3)    //P1.3 -> RADIO_SFD  OK
	#define  SEL_CC_SFD_IOFUNC()        (P1SEL &= ~BIT3)
	#define  READ_CC_SFD_PIN()          (P1IN  &   BIT3)

	#define  CC2420USART_disableRxIntr()   (UCB0IE &= ~UCRXIE)    //Ok
	#define  CC2420USART_disableTxIntr()   (UCB0IE &= ~UCTXIE)    //Ok

	#define  CC2420USART_rx()              (UCB0RXBUF)            //Ok
	#define  CC2420USART_tx(data)          (UCB0TXBUF = data)     //Ok

	//#define  SEL_UTXD1_IOFUNC()            (P3SEL &= ~0x40)    ///< P3.6/UTXD1
	//#define  SEL_URXD1_IOFUNC()            (P3SEL &= ~0x80)    ///< P3.7/URXD1

	#define  SEL_SIMO_MODFUNC()           (P3SEL |=  BIT1)    ///< P3.1/SIMO  OK
	#define  SEL_SOMI_MODFUNC()           (P3SEL |=  BIT2)    ///< P3.2/SOMI  OK
	#define  SEL_UCLK_MODFUNC()           (P3SEL |=  BIT3)    ///< P3.3/UCLK  OK

	#define  SEL_SIMO_IOFUNC()            (P3SEL &= ~BIT1)    ///< P3.1/SIMO  OK
	#define  SEL_SOMI_IOFUNC()            (P3SEL &= ~BIT2)    ///< P3.2/SOMI  OK
	#define  SEL_UCLK_IOFUNC()            (P3SEL &= ~BIT3)    ///< P3.3/UCLK  OK

	#define FIFOPInterrupt_enable()   P1IE  |=  (BIT6)  //Ok
	#define FIFOPInterrupt_disable()  P1IE  &= ~(BIT6)
	#define FIFOPInterrupt_clear()    P1IFG &= ~(BIT6)

	#define CCAInterrupt_enable()     P1IE  |=  (BIT7)  //Ok
	#define CCAInterrupt_disable()    P1IE  &= ~(BIT7)
	#define CCAInterrupt_clear()      P1IFG &= ~(BIT7)

	#define FIFOInterrupt_enable()    P1IE  |=  (BIT5)  //Ok
	#define FIFOInterrupt_disable()   P1IE  &= ~(BIT5)
	#define FIFOInterrupt_clear()     P1IFG &= ~(BIT5)

	#define SFDInterrupt_enable()     P1IE  |=  (BIT3)  //Ok
	#define SFDInterrupt_disable()    P1IE  &= ~(BIT3)
	#define SFDInterrupt_clear()      P1IFG &= ~(BIT3)

	#define FIFOPInterrupt_edge(low_to_high)         \
	          if(low_to_high)  P1IES &= ~(BIT6);     \
	          else             P1IES |=  (BIT6);

	#define SFDInterrupt_edge(low_to_high)           \
	          if(low_to_high)  P1IES &= ~(BIT3);     \
	          else             P1IES |=  (BIT3);

	#define CCAInterrupt_edge(low_to_high)           \
	          if(low_to_high)  P1IES &= ~(BIT7);     \
	          else             P1IES |=  (BIT7);

	#define CC2420USART_isTxEmpty()  (!(UCB0STAT&UCBUSY))

	#define SPI_isTxDone()  (UCB0IFG & UCTXIFG)
	#define SPI_isRxDone()  (UCB0IFG & UCRXIFG)

#endif
#ifdef MSP430F6638
	#define  MAKE_RADIO_CSN_OUTPUT()    (P5DIR |=  BIT5)    //P5.5 -> SPI1_CS_RADIO_N
	#define  SEL_RADIO_CSN_IOFUNC()     (P5SEL &= ~BIT5)
	#define  SET_RADIO_CSN_PIN()        (P5OUT |=  BIT5)
	#define  CLR_RADIO_CSN_PIN()        (P5OUT &= ~BIT5)

	#define  MAKE_CC_RSTN_OUTPUT()      (P2DIR |=  BIT2)    //P2.2 -> RESET_RADIO_N
	#define  SEL_CC_RSTN_IOFUNC()       (P2SEL &= ~BIT2)
	#define  CLR_CC_RSTN_PIN()          (P2OUT &= ~BIT2)
	#define  SET_CC_RSTN_PIN()          (P2OUT |=  BIT2)

	#define  MAKE_CC_VREN_OUTPUT()      (P2DIR |=  BIT4)    //P2.4 -> REG_1V8_EN_N
	#define  SEL_CC_VREN_IOFUNC()       (P2SEL &= ~BIT4)
	#define  CLR_CC_VREN_PIN()          (P2OUT &= ~BIT4)
	#define  SET_CC_VREN_PIN()          (P2OUT |=  BIT4)

	#define  MAKE_CC_CCA_INPUT()        (P2DIR &= ~BIT7)    //P2.7 -> RADIO_CCA
	#define  SEL_CC_CCA_IOFUNC()        (P2SEL &= ~BIT7)
	#define  READ_CC_CCA_PIN()          (P2IN  &   BIT7)

	#define  MAKE_CC_FIFO_INPUT()       (P2DIR &= ~BIT5)    //P2.5 -> RADIO_FIFO
	#define  SEL_CC_FIFO_IOFUNC()       (P2SEL &= ~BIT5)

	#define  MAKE_CC_FIFOP_INPUT()      (P2DIR &= ~BIT6)    //P2.6 -> RADIO_FIFOP OK
	#define  SEL_CC_FIFOP_IOFUNC()      (P2SEL &= ~BIT6)

	#define  MAKE_CC_SFD_INPUT()        (P2DIR &= ~BIT3)    //P2.3 -> RADIO_SFD  OK
	#define  SEL_CC_SFD_IOFUNC()        (P2SEL &= ~BIT3)
	#define  READ_CC_SFD_PIN()          (P2IN  &   BIT3)
#endif

#ifndef MAKE_RADIO_CSN_OUTPUT
	#error "Microcontroller not defined (MSP430F5438A or MSP430F6638 Expected)"
#endif

#define VREFOn()                 SET_CC_VREN_PIN()
#define VREFOff()                CLR_CC_VREN_PIN()

#define ResetOn()                CLR_CC_RSTN_PIN()
#define ResetOff()               SET_CC_RSTN_PIN()

result_t HPLCC2420_init ( unsigned long smClkHz );
result_t HPLCC2420_start ( void );
result_t HPLCC2420_stop ( void );

void initCC2420USART(unsigned long smClkHz);
void CC2420USART_disableSPI(void);
//result_t CC2420USART_isTxIntrPending(void);
result_t CC2420USART_isRxIntrPending(void);
//result_t CC2420USART_isTxEmpty(void);


#endif /*CC2420_H_*/
