/////////////////////////////////////////////////////////////////////////////////////////
/// \file nopDelay.h
/// \brief Usefull delay functions based on NOP intruction for MSP430
///
/// This module assumes that the CPU is running at 8MHz
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.3
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef NOPDELAY_H_
#define NOPDELAY_H_

//Functions commens in leds.h
#define msWaitNOP(ms)           __delay_cycles(configCPU_CLOCK_HZ/1000*ms);
#define usWaitNOP(us)           __delay_cycles(configCPU_CLOCK_HZ/1000000*us);

#endif /*NOPDELAY_H_*/

