/////////////////////////////////////////////////////////////////////////////////////////
/// \file serialPort.c
/// \brief Serial COMM utils code file for Shimmer
///
/// Function \a serialPortInit has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date October 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "serialPrint.h"
#include "serialPort1611.h"

#include "leds.h"

char printStr[SIZE_PRINTSTR];          ///< Global String for printing data

/// Global function pointer for the function to be called when new data arrives
void (*fpNewData0)(uint8_t) = NULL;
void (*fpNewData1)(uint8_t) = NULL;

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Init Serial Port 0
///
/// The serial port will be initialized:
///   - For msp430f1611:  USART 0 Clock:     SMCLK if fCLK > 32,768KHz, else ACLK
///   - Data bits: 8
///   - Stop bit:  1
///   - No parity
///
/// \param fCLK CLK frequency. Valid frequencies definition in \a serialPrint.h
/// \param baudRate serial link BaudRate. Valid BaudRates definition in \a serialPrint.h
/// \param newData function to be called when a byte arrives. Null if discard.
///
/// \return \a SUCCESS/\a FAIL
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t serialPortInit( uint8_t channel, unsigned long fCLK, uint8_t baudRate, void (*newDataFunc)(uint8_t) ) {

  uint8_t UxCTL=0, UxTCTL=0, UxBR0=0, UxBR1=0, UxMCTL;
  
  // U0CTL     ->   Usart Control Register
  //  .PENA    =  0 Parity disabled
  //  .PEV     =  0 Odd parity
  //  .SPB     =  0 One stop bit
  //  .CHAR    =  1 8b data
  //  .LISTEN  =  0 Listen disabled (Loopback)
  //  .SYNC    =  0 Transmitter emty flag
  //  .MM      =  0 Iddle line multiprocessor protocol
  //  .SWRST   =  1 Usart logic held in reset state
  UxCTL   = SWRST | CHAR;
  
  //Setup baud rate (http://mspgcc.sourceforge.net/baudrate.html)
  switch(baudRate) {
    case BAUDRATE_9600:

      if(ACLK != 32768)
        return (FAIL);
      
      // U0TCTL   ->   Usart Transmit Control Register
      //  .UCLKI  =  0 Normal polarity, UCLK
      //  .SSEL0  =  1 ACLK -> 32768
      //  .URXSE  =  0 Receive start-edge disabled
      //  .TXWAKE =  0 Transmitter wake data
      //  .TXEPT  =  0 Transmitter emty flag
      UxTCTL = SSEL0;

      UxBR1   = 0x00;
      UxBR0   = 0x03;
      UxMCTL  = 0x4A;
      break;

    case BAUDRATE_115200:
      // U0TCTL   ->   Usart Transmit Control Register
      //  .UCLKI  =  0 Normal polarity, UCLK
      //  .SSEL0  =  2 SMCLK
      //  .URXSE  =  0 Receive start-edge disabled
      //  .TXWAKE =  0 Transmitter wake data
      //  .TXEPT  =  0 Transmitter emty flag
      UxTCTL = SSEL1;

      switch(fCLK) {
        case 1000000:     //115200Baud @ 1MHz     (maxErr 7.36%)
          UxBR0   = 0x08;
          UxBR1   = 0x00;
          UxMCTL  = 0x5B;
          break;
        case 8000000:     //115200Baud @ 8MHz     (maxErr 0.08%)
          UxBR0   = 0x45;
          UxBR1   = 0x00;
          UxMCTL  = 0xAA;
          break;
        default:
          return (FAIL);
      }
      break;

    default:
      return (FAIL);
  }

  if(channel==0){
    //Set pin function
    P3SEL  |= (BIT4|BIT5); // P3.4 and P3.5

    U0CTL  = UxCTL;
    U0TCTL = UxTCTL;
    U0BR1  = UxBR1;
    U0BR0  = UxBR0;
    U0MCTL = UxMCTL;
    
    // ME1      ->   Module Enable Register 1
    //  .UTXE0  =  1 USART0 trasmitter enabled
    //  .URXE0  =  1 USART0 receiver enabled
    ME1     = UTXE0 + URXE0;

    //Leave reset state
    U0CTL  &= ~SWRST;

    //A function has to be called when new data arrives?
    if(newDataFunc) {
      fpNewData0=newDataFunc;
      //USART0 Rx Interrupt Enabled
      IE1    |= URXIE0;
      //Enable global interrupts
      _BIS_SR(GIE);
    }

  }else if(channel==1){
    //Set pin function
    P3SEL  |= (BIT6|BIT7); // P3.6 and P3.7

    U1CTL  = UxCTL;
    U1TCTL = UxTCTL;
    U1BR1  = UxBR1;
    U1BR0  = UxBR0;
    U1MCTL = UxMCTL;

    // ME2      ->   Module Enable Register 2
    //  .UTXE1  =  1 USART1 trasmitter enabled
    //  .URXE1  =  1 USART1 receiver enabled
    ME2     = UTXE1 + URXE1;

    //Leave reset state
    U1CTL  &= ~SWRST;

    //A function has to be called when new data arrives?
    if(newDataFunc) {
      fpNewData1=newDataFunc;
      //USART1 Rx Interrupt Enabled
      IE2    |= URXIE1;
      //Enable global interrupts
      _BIS_SR(GIE);
    }
  }
  else
    return FAIL;

  return (SUCCESS);
}

/**
 * @brief Get a byte from the serial channel
 * 
 * @param channel 
 * @return uint8_t
 */
uint8_t serialRX( uint8_t channel ){
  if(channel==0){
    while(!RX_DONE_0());
    return(RX_0());
  }else if(channel==1){
    while(!RX_DONE_1());
    return(RX_1());
  }
  return(0);
}

/**
 * @brief Put a byte in the serial channel
 * 
 * @param channel 
 * @param data 
 * @return void
 */
void serialTX( uint8_t channel, uint8_t data ){
  if(channel==0){
    while(!TX_DONE_0());
    TX_0(data);
  }else if(channel==1){
    while(!TX_DONE_1());
    TX_1(data);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Put a string into Serial Port 0
///
/// Put a null ended string in the serial port 0 output buffer and wait to be sended
/// \param string Char string to be placed on the output buffer ('\\0' ended)
/////////////////////////////////////////////////////////////////////////////////////////
void serialPutStr( uint8_t channel, char *string ) {
  unsigned char i=0;
  while(string[i]) {
    serialTX(channel, string[i++]);
  }
}


/////////////////////////////////////////////////////////////////////////////////////////
/// \brief UART RX 0 Interrupt Service Routine.
/////////////////////////////////////////////////////////////////////////////////////////
__attribute__((__interrupt__(USART0RX_VECTOR)))
void USART0RX_isr (void){
//interrupt ( USART0RX_VECTOR ) USART0RX_isr ( void );
//interrupt ( USART0RX_VECTOR ) USART0RX_isr ( void ) {
  if ( fpNewData0 ) {
    ( *fpNewData0 ) ( ( uint8_t ) RX_0() );
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief UART RX 1 Interrupt Service Routine.
/////////////////////////////////////////////////////////////////////////////////////////
__attribute__((__interrupt__(USART1RX_VECTOR)))
void USART1RX_isr (void){
//interrupt ( USART0RX_VECTOR ) USART0RX_isr ( void );
//interrupt ( USART0RX_VECTOR ) USART0RX_isr ( void ) {
  if ( fpNewData1 ) {
    ( *fpNewData1 ) ( ( uint8_t ) RX_1() );
  }
}

