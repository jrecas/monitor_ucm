/////////////////////////////////////////////////////////////////////////////////////////
/// \file nopDelay.c
/// \brief Usefull delay functions based on NOP intruction for MSP430
///
/// This module assumes that the CPU is running at 8MHz
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.3
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#include "nopDelay.h"

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Inline mSec busy wait
///
/// This function assumes that the CPU is running at 8MHz
/// \param ms milli seconds to wait
/////////////////////////////////////////////////////////////////////////////////////////
void msWaitNOP ( uint16_t ms ) {
  uint16_t i,j;
  for ( i=0;i<ms;i++ ) {
    for ( j=0;j<100;j++ ) {
      nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
      nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
      nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
      nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
      nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
      nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
      nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop(); nop();
      nop(); nop(); nop(); nop();
      //__delay_cycles(74);
    }
  }
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Inline uSec busy wait
///
/// This function assumes that the CPU is running at 8MHz
/// \param us micro seconds to wait
/////////////////////////////////////////////////////////////////////////////////////////
void usWaitNOP ( uint16_t us ) {
  uint16_t i;
  for ( i=0;i<us;i++ ) {
    nop(); nop(); nop(); nop(); nop();
    //__delay_cycles(5);
  }
}

