/////////////////////////////////////////////////////////////////////////////////////////
/// \file internalFlash.c
/// \brief Internal Flash write code file for MSP430F1611
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.0
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#include "internalFlash.h"

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Write data in Flash memory
///
/// \param ptrFlash pointer to flash segment (FLASH_SEG_A/B)
/// \param data array of \a uint8_t to be writed
/// \param length length of the \a data array
/// \return FAIL/SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t writeFlash ( uint8_t * ptrFlash, uint8_t *data, uint8_t length ) {
  unsigned int i;

  if ( length>SEGMENT_SIZE )
    return ( FAIL );

  //Flash_ptr = (char *) 0x1080;              // Initialize Flash pointer
  FCTL1 = FWKEY + ERASE;                    // Set Erase bit
  FCTL3 = FWKEY;                            // Clear Lock bit
  *ptrFlash = 0;                            // Dummy write to erase Flash segment

  FCTL1 = FWKEY + WRT;                      // Set WRT bit for write operation

  for ( i=0; i<length; i++ ) {
    *ptrFlash++ = *data++;                  // Write value to flash
  }

  for ( i=length; i<SEGMENT_SIZE; i++ ) {
    *ptrFlash++ = 0x00;                     // Erase the rest
  }

  FCTL1 = FWKEY;                            // Clear WRT bit
  FCTL3 = FWKEY + LOCK;                     // Set LOCK bit

  return ( SUCCESS );
}
