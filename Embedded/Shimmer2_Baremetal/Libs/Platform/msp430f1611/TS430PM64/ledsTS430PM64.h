/////////////////////////////////////////////////////////////////////////////////////////
/// \file leds.h
/// \brief LEDs utils header file for Shimmer
///
/// Function \a initLEDs has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date November 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef LEDS_TS430PM64_H
#define LEDS_TS430PM64_H

#include "platform.h"

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Initialize LED's HW and set them off
/////////////////////////////////////////////////////////////////////////////////////////
#define initLEDs() { \
  P1SEL &= ~0x01;    \
  P1DIR |=  0x01;    \
  P1OUT |=  0x01;    \
}

#define  setRedLED()                            ///< Set Red LED
#define  setOrangeLED()                         ///< Set Orange LED
#define  setYellowLED()    P1OUT |=  0x01;      ///< Set Yellow LED
#define  setGreenLED()                          ///< Set Green LED
#define  clearRedLED()                          ///< Clear Red LED
#define  clearOrangeLED()                       ///< Clear Orange LED
#define  clearYellowLED()  P1OUT &= ~0x01;      ///< Clear Yellow LED
#define  clearGreenLED()                        ///< Clear Green LED
#define  toggleRedLED()                         ///< Toggle Red LED
#define  toggleOrangeLED()                      ///< Toggle Orange LED
#define  toggleYellowLED() P1OUT ^=  0x01;      ///< Toggle Yellow LED
#define  toggleGreenLED()                       ///< Toggle Green LED

#define allOnLEDs()        P1OUT &= ~0x01;      ///< Set all LEDs

#endif

