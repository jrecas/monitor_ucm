/////////////////////////////////////////////////////////////////////////////////////////
/// \file nopDelay.h
/// \brief Usefull delay functions based on NOP intruction for MSP430
///
/// This module assumes that the CPU is running at 8MHz
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.3
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef NOPDELAY_H_
#define NOPDELAY_H_
#include "platform.h"

#ifdef GCC_TI
#define nop()  __nop()
#endif

//Functions commens in leds.h
void msWaitNOP ( uint16_t ms );
void usWaitNOP ( uint16_t us );

//#define msWaitNOP(ms)           delay_cycles(configCPU_CLOCK_HZ/2000*ms);
//#define usWaitNOP(us)           delay_cycles(configCPU_CLOCK_HZ/2000000*us);

/*
// Delay Routine from mspgcc help file
static void __inline__ delay_cycles(register unsigned int n){
  __asm__ __volatile__ (
  "1: \n"
  " dec %[n] \n"
  " jne 1b \n"
  : [n] "+r"(n));
}
*/
#endif /*NOPDELAY_H_*/

