/////////////////////////////////////////////////////////////////////////////////////////
/// \file serialPrint.h
/// \brief Serial COMM utils header file for Shimmer
///
/// Function \a serialPortInit has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date October 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SERIAL_COMMS_SHIMMER_H
#define SERIAL_COMMS_SHIMMER_H

#define  TX_DONE_0()    (IFG1 & UTXIFG0)
#define  TX_DONE_1()    (IFG2 & UTXIFG1)

#define  RX_DONE_0()    (IFG1 & URXIFG0)
#define  RX_DONE_1()    (IFG2 & URXIFG1)

#define  RX_0()         (U0RXBUF)
#define  RX_1()         (U1RXBUF)
 
#define  TX_0(data)     (U0TXBUF = (data))
#define  TX_1(data)     (U1TXBUF = (data))
          
#define  PUT_CHAR_0(data){    \
  while(!TX_DONE_0());        \
  TX_0(data);                 \
}

#define  PUT_CHAR_1(data){    \
  while(!TX_DONE_1());        \
  TX_1(data);                 \
}

#endif

