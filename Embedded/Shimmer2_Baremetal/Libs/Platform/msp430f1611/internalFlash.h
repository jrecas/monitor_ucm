/////////////////////////////////////////////////////////////////////////////////////////
/// \file internalFlash.h
/// \brief Internal Flash write header file for MSP430F1611
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.0
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef INTERNALFLASH_H_
#define INTERNALFLASH_H_

#include "platform.h"

#define FLASH_SEG_A 0x1080                       ///< Flash segment A memory location
#define FLASH_SEG_B 0x1000                       ///< Flash segment B memory location

#define SEGMENT_SIZE 128                         ///< Segment size

uint8_t writeFlash ( uint8_t * ptrFlash, uint8_t *data, uint8_t length );

#endif /*INTERNALFLASH_H_*/
