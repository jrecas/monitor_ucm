/////////////////////////////////////////////////////////////////////////////////////////
/// \file leds.h
/// \brief LEDs utils header file for Shimmer
///
/// Function \a initLEDs has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 2.0
/// \date November 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef LEDS_SHIMMER_H
#define LEDS_SHIMMER_H

#include "platform.h"

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Initialize LED's HW and set them off
/////////////////////////////////////////////////////////////////////////////////////////


#ifdef SHIMMER1
  #define initLEDs() { \
    P4SEL &= ~0x0F;    \
    P4DIR |=  0x0F;    \
    P4OUT |=  0x0F;    \
  }

  #define  setRedLED()       P4OUT &= ~0x01;      ///< Set Red LED
  #define  setOrangeLED()    P4OUT &= ~0x02;      ///< Set Orange LED
  #define  setYellowLED()    P4OUT &= ~0x04;      ///< Set Yellow LED
  #define  setGreenLED()     P4OUT &= ~0x08;      ///< Set Green LED
  #define  clearRedLED()     P4OUT |=  0x01;      ///< Clear Red LED
  #define  clearOrangeLED()  P4OUT |=  0x02;      ///< Clear Orange LED
  #define  clearYellowLED()  P4OUT |=  0x04;      ///< Clear Yellow LED
  #define  clearGreenLED()   P4OUT |=  0x08;      ///< Clear Green LED
  #define  toggleRedLED()    P4OUT ^=  0x01;      ///< Toggle Red LED
  #define  toggleOrangeLED() P4OUT ^=  0x02;      ///< Toggle Orange LED
  #define  toggleYellowLED() P4OUT ^=  0x04;      ///< Toggle Yellow LED
  #define  toggleGreenLED()  P4OUT ^=  0x08;      ///< Toggle Green LED

  #define allOnLEDs()        P4OUT &= ~0x0F;      ///< Set all LEDs
#endif


#ifdef SHIMMER2
#define LEDSBITS   (BIT0|BIT2|BIT3)

  #define initLEDs() {     \
    P4SEL &= ~LEDSBITS;    \
    P4DIR |=  LEDSBITS;    \
    P4OUT |=  LEDSBITS;    \
  }

  #define  setRedLED()       P4OUT &= ~BIT0;      ///< Set Red LED
  #define  setOrangeLED()    
  #define  setYellowLED()    P4OUT &= ~BIT2;      ///< Set Yellow LED
  #define  setGreenLED()     P4OUT &= ~BIT3;      ///< Set Green LED

  #define  clearRedLED()     P4OUT |=  BIT0;      ///< Clear Red LED
  #define  clearOrangeLED()  
  #define  clearYellowLED()  P4OUT |=  BIT2;      ///< Clear Yellow LED
  #define  clearGreenLED()   P4OUT |=  BIT3;      ///< Clear Green LED

  #define  toggleRedLED()    P4OUT ^=  BIT0;      ///< Toggle Red LED
  #define  toggleOrangeLED() 
  #define  toggleYellowLED() P4OUT ^=  BIT2;      ///< Toggle Yellow LED
  #define  toggleGreenLED()  P4OUT ^=  BIT3;      ///< Toggle Green LED

  #define allOnLEDs()        P4OUT &= ~LEDSBITS;  ///< Set all LEDs

  #endif

#endif

