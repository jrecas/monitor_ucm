/////////////////////////////////////////////////////////////////////////////////////////
/// \file BlueTooth.h
/// \brief BlueTooth utils header file for Shimmer
///
/// Function \a BT_init has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.2
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include "platform.h"
//#include "FreeRTOSInc.h"
#include "nopDelay.h"

/// \brief Disable BlueTooth Module
#define disableBT(){                                                     \
  P5DIR |=  0x20;                      /* Output (0x01 << 5)  */         \
  P5SEL &= ~0x20;                      /* IO Func.            */         \
  P5OUT &= ~0x20;                      /* Switch off the radio*/         \
}

#define  dataAvialableBT()      (IFG2 & URXIFG1)

#define  getCharBT()            (U1RXBUF)

#define  sendCharBT(data){       \
  while(!(IFG2 & UTXIFG1));      \
  TXBUF1 = data;                 \
}

uint8_t initBT(uint8_t rcvInterrupt);
//uint8_t mutexSendCharBT(uint8_t data, portTickType wait);
//uint8_t mutexSendBuffBT(uint8_t *data, uint8_t len, portTickType wait);
uint8_t receiveBT(uint8_t *data);


/*uint8_t BT_sendCommand(uint8_t *cmd, uint8_t *resp);

uint8_t BT_enterCMD(void);
uint8_t BT_leaveCMD(void);

uint8_t BT_sniffMode(uint16_t usec);

*/
#endif /*BLUETOOTH_H_*/
