/////////////////////////////////////////////////////////////////////////////////////////
/// \file Shimmer.h
/// \brief Common include files for Shimmer
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.0
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef SHIMMER_H
#define SHIMMER_H

//Encendemos el regulador +-5 del AN0
#define enableAnExVreg() {     \
  P1SEL &= ~BIT3;              \
  P1DIR |=  BIT3;              \
  P1OUT |=  BIT3;              \
}


#define MCLK        8000000
#define SMCLK          MCLK
#define ACLK          32768

#define CLK_32KHZ            0
#define CLK_1MHZ             1
#define CLK_4MHZ             2
#define CLK_8MHZ             3

#endif
