/////////////////////////////////////////////////////////////////////////////////////////
/// \file BlueTooth.c
/// \brief BlueTooth utils source file for Shimmer
///
/// Function \a BT_init has to be called before using these utilities
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.2
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#include "BlueTooth.h"
//#include "semphr.h"

#define BT_RESET (0x01 << 5)   ///< BlueTooth Reset (P5.5)
#define BT_RTS   (0x01 << 6)   ///< BlueTooth Ready To Send (P1.6)
#define BT_CTS   (0x01 << 7)   ///< BlueTooth Clear To Send (P1.7)
#define BT_PIO   (0x01 << 5)   ///< BlueTooth PIO Pin (P1.5)
#define BT_TXD   (0x01 << 6)   ///< BlueTooth TxD Pin (P3.6)
#define BT_RXD   (0x01 << 7)   ///< BlueTooth RxD Pin (P3.7)


#define BT_ENTER_CMD         (uint8_t *)"$$$"     ///< Command to enter in command mode
#define BT_ENTER_CMD_RESP    (uint8_t *)"CMD"     ///< Enter command mode expected response
#define BT_LEAVE_CMD         (uint8_t *)"---\n"   ///< Command to leave command mode
#define BT_LEAVE_CMD_RESP    (uint8_t *)"END"     ///< Leave command mode expected response

#define BT_Off()             P5OUT &= ~BT_RESET   ///< Switch off the radio
#define BT_On()              P5OUT |=  BT_RESET   ///< Switch off the radio

#define BT_link()            P1IN & BT_PIO        ///< BlueTooth link established

//xSemaphoreHandle xLock;

//uint8_t sniffMode=FALSE;               ///< Sniff mode enabled

//uint8_t commFwd=TRUE;                  ///< Forward data received form BT to serial Comm
//uint8_t buffStore=FALSE;               ///< Store data received form BT?

//#define BT_BUFF_SIZE   10              ///< BT buffer size
//uint8_t BT_buffer[BT_BUFF_SIZE], bInd; ///< BT buffer in case if not forwarding

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Init BlueTooth (Serial Port 1)
///
/// Configure BlueTooth Shimmer pins and perform a reset sequence
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t initBT(uint8_t rcvInterrupt) {

  //if((xLock=xSemaphoreCreateMutex()) == NULL)
  //  return FAIL;

  // Initialize the serial port anyway
  UCTL1   = CHAR  | SWRST;             // 8 data bits, 1 stop bit, no parity
  ME2    |= UTXE1 | URXE1;             // Enable TX and RX
  UTCTL1 |= SSEL1;                     // Use SMLCK as input (~1 MHz)
  // Generator: http://mspgcc.sourceforge.net/baudrate.html
  UBR01 =0x45;
  UBR11 =0x00;
  UMCTL1=0xAA;                          // USART1 8000000Hz 115107bps
  UCTL1 &= ~SWRST;                     // start USART1

  if(rcvInterrupt==TRUE) {
    IE2    |= URXIE1;                    // USART1 Rx Interrupt Enabled
    _BIS_SR(GIE);                        //Enable global interrupts
  }

  // BT PINS
  // Reset: 5.5, GPIO
  P5DIR |=  BT_RESET;                  // Output
  P5SEL &= ~BT_RESET;                  // IO Func.
  BT_Off();                            // This switches off the radio (reset active-low)

  // RTS/CTS = 1.6/7, GPIO
  P1DIR &= ~BT_RTS;                    // Input
  P1DIR |=  BT_CTS;                    // Output
  P1SEL &= ~(BT_RTS | BT_CTS);         // IO Func.

  // Conexion PIO = 1.5, GPIO
  P1DIR &= ~BT_PIO;                    // Input
  P1SEL &= ~BT_PIO;                    // IO Func.

  // TXD/RXD = 3.6/7, UART1
  P3DIR |=  BT_TXD;                    // Output
  P3DIR &= ~BT_RXD;                    // Input
  P3SEL |= (BT_TXD | BT_RXD);          // Function

  msWaitNOP(100);
  BT_On();                             // This activates the radio
  //serialPutStr("BTReset\n");
  msWaitNOP(100);

  //Make sure CTS=0, and after that, WakeUp
  P1OUT &= ~BT_CTS;
  msWaitNOP(10);
  //void wakeUpBT(void) {
  P1OUT |=  BT_CTS;
  msWaitNOP(7);
  P1OUT &= ~BT_CTS;
  //}
  //serialPutStr("BTToggle done\n");

  msWaitNOP(100);

  return SUCCESS;
}
/*
/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Send a byte to the Bluetooth module as a monitor
///
/// \param data byte to be sent through BlueTooth
/// \param wait maximium ticks blocked in the mutex
///
/// \return \a SUCCESS if correctly sent \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t mutexSendCharBT(uint8_t data, portTickType wait) {
  if(xSemaphoreTake(xLock, wait) != pdTRUE)
    return FAIL;

  sendCharBT(data);

  xSemaphoreGive(xLock);
  return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Send a byte array to the Bluetooth module
///
/// \param '\\0' data bytes array to be sent through BlueTooth
/// \param wait maximium ticks blocked in the mutex
///
/// \return \a SUCCESS if correctly sent \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t mutexSendBuffBT(uint8_t *data, uint8_t len, portTickType wait) {
  uint8_t i=0;

  if(xSemaphoreTake(xLock, wait) != pdTRUE)
    return FAIL;

  for(i=0; i<len; i++)
    sendCharBT(data[i]);

  xSemaphoreGive(xLock);
  return SUCCESS;

}
*/
/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Receive a byte from the Bluetooth module
///
/// \param data byte received from the BlueTooth
/// \return \a SUCCESS data contain a valid BT data \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t receiveBT(uint8_t *data) {

  if(dataAvialableBT()) {
    *data=getCharBT();
    return (SUCCESS);
  }
  return (FAIL);
}

/*
/////////////////////////////////////////////////////////////////////////////////////////
/// \brief send a command to the BlueTooth, and check the answer
///
/// \return \a TRUE on success, \a FALSE otherwise
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t BT_sendCommand(uint8_t *cmd, uint8_t *resp) {

  //uint8_t i=0;

  //Clear BT buffer
  bInd=0;
  //Store BT response
  buffStore=TRUE;
  //Send command
  BT_sendBuff(cmd);
  msWaitNOP(500);
  //Stop storing
  buffStore=FALSE;
//  serialPutStr("\nReceive: -");
//  for(i=0;i<bInd;i++)
//    serialPutChar(BT_buffer[i]);
//  serialPutStr("-\nExpected: -");
//  serialPutStr(resp);
//  serialPutStr("-\n");
  //Check answer
  if(!strncmp((const char *) BT_buffer, (const char *) resp, strlen((const char *) resp)))
    return (TRUE);

  return (FALSE);
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief blueTooth enter in command mode
///
/// \return \a TRUE on success, \a FALSE otherwise
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t BT_enterCMD(void) {
  return (BT_sendCommand(BT_ENTER_CMD, BT_ENTER_CMD_RESP));
}

////////////////////////////////////////////////////////////////////////////////////////
/// \brief blueTooth leave command mode
///
/// \return \a TRUE on success, \a FALSE otherwise
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t BT_leaveCMD(void) {
  return (BT_sendCommand(BT_LEAVE_CMD, BT_LEAVE_CMD_RESP));
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief send a command to the BlueTooth, and check the answer
///
/// Enables sleep periods of \a usec. If \a usec is set to 0, sniff mode is disabled.
/// When sniff mode is enabled, the radio has to be wake-up before send data
///
/// \param usec time spent in sleep mode
/// \return \a TRUE on success, \a FALSE otherwise
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t BT_sniffMode(uint16_t usec) {
  char str[10];
  uint16_t hexTime= (uint16_t)((float) usec/0.625);

  //Disable sniff mode?
  sniffMode= (hexTime) ?TRUE:FALSE;

  //Create the command
  sprintf(str, "SW,%04X\n",hexTime);

  //Send the command
  return (BT_sendCommand((uint8_t *) str, (uint8_t *) "AOK"));
}



/////////////////////////////////////////////////////////////////////////////////////////
/// \brief USART1 RX ISR
/////////////////////////////////////////////////////////////////////////////////////////
#ifdef CCS
  #pragma vector=USART1RX_VECTOR
  __interrupt void USART1RX_isr(void) {
#endif

#ifdef GCC
  interrupt ( USART1RX_VECTOR ) USART1RX_isr ( void );
  interrupt ( USART1RX_VECTOR ) USART1RX_isr ( void ) {
#endif
  uint8_t data=RXBUF1;                 //Clear interrupt flag & read data

  //Forward data?
  if(commFwd)
    serialPutChar(data);

  //Store data?
  if(buffStore) {
    if(bInd<BT_BUFF_SIZE)
      BT_buffer[bInd++]=data;
  }
}
*/
