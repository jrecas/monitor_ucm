/////////////////////////////////////////////////////////////////////////////////////////
/// \file saveSeed.c
/// \brief Generate and save in Flash a new seed based on Internal Chip for Shimmer
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.0
/// \date June 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "saveSeed.h"

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Save random seed in Flash Memory
///
/// \return Result of the \a writeFlash function (\a FAIL/\a SUCCESS)
/////////////////////////////////////////////////////////////////////////////////////////
uint8_t saveSeed ( void ) {
  uint8_t* ptrFlash= ( uint8_t * ) FLASH_SEG_A;  //SegmentA non volatile data pointer
  strFData flashData;                            //Structured non volatile data in RAM
  uint16_t chipID[3], i;

  for ( i=0; i<3; i++ )
    chipID[i]=0;

  //Get non volatile information to RAM
  memcpy ( &flashData,ptrFlash,sizeof ( flashData ) );
  //If there is no seed, create one based on chipID
  //sprintf ( printStr, "Stored Seed 0x%X\n",flashData.randSeed );serialPutStr ( printStr );
  if ( flashData.randSeed==0x0000 || flashData.randSeed==0xFFFF ) {
    if ( readIDChip ( ( uint8_t* ) chipID ) ==FAIL ) {
      //serialPutStr ( "Error reading ChipID\n" );
      allOnLEDs();
      return ( FAIL );
    }
    flashData.randSeed=chipID[0]^chipID[1]^chipID[2];
    //sprintf ( printStr, "ChipID: 0x%04X%04X%04X, Xor: %04X\n", chipID[0], chipID[1], chipID[2], flashData.randSeed );serialPutStr ( printStr );
  }
  srand ( flashData.randSeed );
  flashData.randSeed=rand();
  //Write new non volatile data
  if ( writeFlash ( ptrFlash, ( uint8_t* ) &flashData, sizeof ( strFData ) ) ==FAIL ) {
    //serialPutStr ( "Error writing Flash\n" );
    allOnLEDs();
    return ( FAIL );
  }

  //Write new non volatile data
  return ( writeFlash ( ptrFlash, ( uint8_t* ) &flashData, sizeof ( strFData ) ) );
}
