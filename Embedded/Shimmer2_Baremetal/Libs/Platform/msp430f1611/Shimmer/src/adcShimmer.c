#include"adcShimmer.h"

void initAdcSh() {
  P6SEL = 0xFF;          //ADCs modfunc(1)
  
  #ifdef SHIMMER1
  P4DIR |= 0x70;
  P4OUT = (P4OUT & 0x0F) | 0xE0;
  #endif
  
  #ifdef SHIMMER2
  P5DIR |= 0x01;         //TOSH_ASSIGN_PIN(ACCEL_SLEEP_N, 5, 0); -> output(1), set(1)
  P5OUT |= 0x01;
  
  P4DIR |= 0x12;         //TOSH_ASSIGN_PIN(ACCEL_SEL0,  4, 1);   -> output(1)
  //TOSH_ASSIGN_PIN(ACCEL_SEL1,  4, 4);
  
  P4OUT &= ~(1<<1);         //ACCEL_SEL0 -> 0
  P4OUT &= ~(1<<4);         //ACCEL_SEL1 -> 0
  
  //P4OUT |= 1<<1;         //ACCEL_SEL0 -> 1
  //P4OUT |= 1<<4;         //ACCEL_SEL1 -> 1
  #endif
  
  // ADC12CTL0
  //  -SHT1x:      Sample-and-hold time. These bits define the number of ADC12CLK cycles in the sampling period for registers ADC12MEM8 to ADC12MEM15. SHT0_0 -> 4 Cycles
  //  -SHT0x:      Sample-and-hold time. These bits define the number of ADC12CLK cycles in the sampling period for registers ADC12MEM0 to ADC12MEM7.
  //  -MSC:        Multiple sample and conversion. 1: The first rising edge of the SHI signal triggers the sampling timer, but
  //               further sample-and-conversions are performed automatically as soon as the prior conversion is completed.
  //  -REF2_5V:    Reference generator voltage. REFON must also be set. 1: 2.5 V
  //  -REFON:      Reference generator on
  //  -ADC12ON:    ADC12 on
  //  -ADC12OVIE:  ADC12MEMx overflow-interrupt enable. The GIE bit must also be set to enable the interrupt.
  //  -ADC12TOVIE: ADC12 conversion-time-overflow interrupt enable. The GIE bit must also be  set to enable the interrupt.
  //  -ENC:        Enable conversion
  //  -ADC12SC:    Start conversion. Software-controlled sample-and-conversion start. ADC12SC and ENC may be set together with one instruction.
  //
  //Tiempo de conversión con SHP=1 -> 1(synh)+4(sample)+13(convert)=18ciclos de ADC12CLK si ADC12CLK=8MHz -> 2.25us por canal
  ADC12CTL0 =  SHT0_0 | SHT1_0 | MSC | REF2_5V | REFON | ADC12ON;
  
  // ADC12CTL1
  //  -CSTARTADDx: Conversion start address. These bits select which ADC12 conversion-memory register is used for a single conversion or for the first conversion in a sequence
  //  -SHSx:       Sample-and-hold source select. SHS_0 -> ADC12SC
  //  -SHP:        Sample-and-hold pulse-mode select. This bit selects the source of the sampling signal (SAMPCON) to be either the output of the sampling timer or the sample-input signal directly.
  //  -ISSH:       Invert signal sample-and-hold
  //  -ADC12DIVx:  ADC12 clock divider. ADC12DIV_0 -> 0
  //  -ADC12SSELx: ADC12 clock source select. ADC12SSEL_2 -> MCLK
  //  -CONSEQx:    Conversion sequence mode select. CONSEQ_1 -> Sequence-of-channels
  //  -ADC12BUSY:  ADC12 busy. This bit indicates an active sample or conversion operation.
  ADC12CTL1 = CSTARTADD_0 | SHS_0 | SHP | ADC12DIV_0 | ADC12SSEL_2 | CONSEQ_1  ;
  
  ADC12MCTL0 = INCH_1 | SREF_1;         //Channel 1 ECG
  ADC12MCTL1 = INCH_2 | SREF_1;         //Channel 2 ECG
  //ADC12MCTL1 = INCH_2 | SREF_1 | EOS ;  //Channel 2 ECG AND STOP!!!
  
  ADC12MCTL2 = INCH_5;                  //Accel X
  ADC12MCTL3 = INCH_4;                  //Accel Y
  //ADC12MCTL4 = INCH_3;                  //Accel Z
  ADC12MCTL4 = INCH_3 | EOS;            //Accel Z AND STOP!!!
  
  //ADC12MCTL5 = INCH_0;                  //Analog board A0
  //ADC12MCTL6 = INCH_7 | EOS;            //Analog board A7 AND STOP!!!
  
  
  ADC12CTL0 |= ENC | ADC12SC; /* Start conversion */
}
