/////////////////////////////////////////////////////////////////////////////////////////
/// \file chipID.c
/// \brief Read unique Chip ID number for the Shimmer board
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.0
/// \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "chipID.h"

enum {
  STD_A = 6,
  STD_B = 64,
  STD_C = 60,
  STD_D = 10,
  STD_E = 9,
  STD_F = 55,
  STD_G = 0,
  STD_H = 480,
  STD_I = 90,
  STD_J = 220
};


void init_pins ( void );
void clear_pins ( void );
uint8_t reset ( void );
void write_bit_one ( void );
void write_bit_zero ( void );
void write_bit ( int is_one );
uint8_t read_bit ( void );
void write_byte ( uint8_t byte );
uint8_t read_byte ( void );
uint8_t crc8_byte ( uint8_t crc, uint8_t byte );

void init_pins() {
//#ifdef ID_CHIP_POWER
  SET_ONEWIRE_PWR_PIN();
  MAKE_ONEWIRE_PWR_OUTPUT();
  SEL_ONEWIRE_PWR_IOFUNC();
//#endif
  SEL_ONEWIRE_IOFUNC();
  MAKE_ONEWIRE_INPUT();
  CLR_ONEWIRE_PIN();
}

void clear_pins() {
//#ifdef TOSH_SET_ONEWIRE_POWER_PIN
  CLR_ONEWIRE_PWR_PIN();
//#endif
  // Don't need to fix ONEWIRE...it finishes as an INPUT
}

uint8_t reset ( void ) { // >= 960us
  int present;
  MAKE_ONEWIRE_OUTPUT();
  usWaitNOP ( STD_H ); //t_RSTL
  MAKE_ONEWIRE_INPUT();
  usWaitNOP ( STD_I );  //t_MSP
  present = READ_ONEWIRE_PIN();
  usWaitNOP ( STD_J );  //t_REC
  return ( present == 0 );
}

void write_bit_one ( void ) { // >= 70us
  MAKE_ONEWIRE_OUTPUT();
  usWaitNOP ( STD_A );  //t_W1L
  MAKE_ONEWIRE_INPUT();
  usWaitNOP ( STD_B );  //t_SLOT - t_W1L
}

void write_bit_zero ( void ) { // >= 70us
  MAKE_ONEWIRE_OUTPUT();
  usWaitNOP ( STD_C );  //t_W0L
  MAKE_ONEWIRE_INPUT();
  usWaitNOP ( STD_D );  //t_SLOT - t_W0L
}

void write_bit ( int is_one ) { // >= 70us
  if ( is_one )
    write_bit_one();
  else
    write_bit_zero();
}

uint8_t read_bit ( void ) { // >= 70us
  int bit;
  MAKE_ONEWIRE_OUTPUT();
  usWaitNOP ( STD_A );  //t_RL
  MAKE_ONEWIRE_INPUT();
  usWaitNOP ( STD_E ); //near-max t_MSR
  bit = READ_ONEWIRE_PIN();
  usWaitNOP ( STD_F );  //t_REC
  return bit;
}

void write_byte ( uint8_t byte ) { // >= 560us
  uint8_t bit;
  for ( bit=0x01; bit!=0; bit<<=1 )
    write_bit ( byte & bit );
}

uint8_t read_byte ( void ) { // >= 560us
  uint8_t byte = 0;
  uint8_t bit;
  for ( bit=0x01; bit!=0; bit<<=1 ) {
    if ( read_bit() )
      byte |= bit;
  }
  return byte;
}

uint8_t crc8_byte ( uint8_t crc, uint8_t byte ) {
  int i;
  crc ^= byte;
  for ( i=0; i<8; i++ ) {
    if ( crc & 1 )
      crc = ( crc >> 1 ) ^ 0x8c;
    else
      crc >>= 1;
  }
  return crc;
}

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Reset the DS2411 chip and read the 8 bytes of data out.
///
/// We verify the CRC to ensure good data, dump the family byte (it should be '1')
/// and fill a buffer with the 6 good uniqut address bytes.
///
/// It is possible for the initialization to fail.
/////////////////////////////////////////////////////////////////////////////////////////

result_t readIDChip ( uint8_t *id_buf ) { // >= 6000us
  int retry = 5;
  uint8_t id[8];

  init_pins();
  usWaitNOP ( 1200 ); // Delay a bit at start up (as per DS2411 data sheet)

  while ( retry-- >0 ) {
    int crc = 0;
    if ( reset() ) {
      uint8_t* byte;

      write_byte ( 0x33 ); //read rom
      for ( byte=id+7; byte!=id-1; byte-- )
        crc = crc8_byte ( crc, *byte=read_byte() );

      if ( crc == 0 ) {
        memcpy ( id_buf, id + 1, 6 );
        clear_pins();
        return SUCCESS;
      }
    }
  }
  clear_pins();
  return FAIL;
}
