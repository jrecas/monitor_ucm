/////////////////////////////////////////////////////////////////////////////////////////
///  \file HPLCC2420.c
///  \brief Definitions for msp430-CC2420 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#include "CC2420.h"

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Initialice CC2420 Hardware Presentation Layer
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420_init(unsigned long smClkHz) {

    //RADIO PINS
    //CC2420 pins
    MAKE_RADIO_CSN_OUTPUT();
    SEL_RADIO_CSN_IOFUNC();
    SET_RADIO_CSN_PIN();

    MAKE_CC_RSTN_OUTPUT();
    SEL_CC_RSTN_IOFUNC();
    CLR_CC_RSTN_PIN();

    MAKE_CC_VREN_OUTPUT();
    SEL_CC_VREN_IOFUNC();
    CLR_CC_VREN_PIN();

    MAKE_CC_CCA_INPUT();
    MAKE_CC_FIFO_INPUT();
    MAKE_CC_FIFOP_INPUT();
    MAKE_CC_SFD_INPUT();

    SEL_CC_CCA_IOFUNC();
    SEL_CC_FIFO_IOFUNC();
    SEL_CC_FIFOP_IOFUNC();
    SEL_CC_SFD_IOFUNC();

    SET_RADIO_CSN_PIN();
    MAKE_RADIO_CSN_OUTPUT();

    initCC2420USART(smClkHz);
    CC2420USART_disableRxIntr();
    CC2420USART_disableTxIntr();

    return ( SUCCESS );
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Start CC2420 Hardware Presentation Layer
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420_start() {
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable CC2420 USART comunication
///
///  \return SUCCESS
/////////////////////////////////////////////////////////////////////////////////////////
result_t HPLCC2420_stop() {
    CC2420USART_disableSPI();
    return SUCCESS;
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Config serial link between MSP430 and CC2420 for Shimmer
/////////////////////////////////////////////////////////////////////////////////////////
void initCC2420USART(unsigned long smClkHz) {

    //USARTControl.disableUART
    ME2 &= ~(UTXE1 | URXE1);                       // USART1 UART module disabled
    SEL_UTXD1_IOFUNC();
    SEL_URXD1_IOFUNC();
    //call USARTControl.disableI2C();

    SEL_SIMO1_MODFUNC();
    SEL_SOMI1_MODFUNC();
    SEL_UCLK1_MODFUNC();

    IE2 &= ~(UTXIE1 | URXIE1);                     // interrupt disable

    U1CTL |= SWRST;
    U1CTL |= CHAR | SYNC | MM;                     // 8-bit char, spi-mode, USART as master
    U1CTL &= ~(0x20);

    U1TCTL = STC ;                                 // 3-pin
    U1TCTL |= CKPH;                                // half-cycle delayed UCLK

    U1TCTL &= ~(SSEL0 | SSEL1);
    U1TCTL |= SSEL1;                               // use SMCLK, assuming 1MHz

    U1BR0 = 0x02;                                  // as fast as possible
    U1BR1 = 0x00;

    U1MCTL = 0;

    ME2 &= ~(UTXE1 | URXE1);                      //USART UART module disable
    ME2 |= USPIE1;                                // USART SPI module enable
    U1CTL &= ~SWRST;

    IFG2 &= ~(UTXIFG1 | URXIFG1);
    IE2 &= ~(UTXIE1 | URXIE1);                    // interrupt disabled
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Disable USART1 SPI module
/////////////////////////////////////////////////////////////////////////////////////////
void CC2420USART_disableSPI(void) {
    ME2 &= ~USPIE1;                               // USART1 SPI module disable
    SEL_SIMO1_IOFUNC();
    SEL_SOMI1_IOFUNC();
    SEL_UCLK1_IOFUNC();
}
/*
/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Tx pending bit is set (and clear it).
///
///  USART1 transmit interrupt flag UTXIFG1 is set when U1TXBUF is empty.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t CC2420USART_isTxIntrPending(void) {
    if (IFG2 & UTXIFG1) {
        IFG2 &= ~UTXIFG1;
        return SUCCESS;
    }
    return FAIL;
}
*/
/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Rx pending bit is set (and clear it).
///
///  USART1 receive interrupt flag URXIFG1 is set when U1RXBUF has received a complete
///  character.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t CC2420USART_isRxIntrPending(void) {
    if (IFG2 & URXIFG1) {
        IFG2 &= ~URXIFG1;
        return SUCCESS;
    }
    return FAIL;
}


/*
/////////////////////////////////////////////////////////////////////////////////////////
///  \brief Check if the Transmitter empty flag bit is set
///
///   TXEPT is set if U1TXBUF and TX shift register are empty.
///
///  \return \a SUCCESS if set, \a FAIL otherwise
/////////////////////////////////////////////////////////////////////////////////////////
result_t CC2420USART_isTxEmpty(void) {
    if (U1TCTL & TXEPT) {
        return SUCCESS;
    }
    return FAIL;
}
*/

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief ISR for Port2 -> CC2420 FIFOP (2.6) & CCA (2.7)
///
///  \todo Implement CCA procesing
///
///  ***NOTE*** Do not call FreeRTOS functions unless they have 'FromISR' tail
///
///  \param payloadSize Pay-Load size in bytes, with a maximum of \a MAX_DATA_LENGTH.
/////////////////////////////////////////////////////////////////////////////////////////

//wakeup -> Wake the processor from any low power state as the routine exits
interrupt (PORT2_VECTOR) wakeup Port2_isr( void );
interrupt (PORT2_VECTOR) wakeup Port2_isr( void ) {
//interrupt ( PORT2_VECTOR ) Port2_isr ( void );
//interrupt ( PORT2_VECTOR ) Port2_isr ( void ) {

    volatile int n = P2IFG & P2IE;

    //FIFOP interrupt
    if ( n & ( 1 << 6 ) ) {
        FIFOP_interrupt();
        return;
    }
    //FIFOP interrupt
    if ( n & ( 1 << 7 ) ) {
        CCA_interrupt();
        return;
    }
    while ( 1 );
}

/////////////////////////////////////////////////////////////////////////////////////////
///  \brief ISR for Port1 -> CC2420 FIFO (1.0) & SFD (1.2)
///
///  \todo Implement FIFO procesing
///  \todo Modify SFD procesing
///
///  ***NOTE*** Do not call FreeRTOS functions unless they have 'FromISR' tail
///
///  \param payloadSize Pay-Load size in bytes, with a maximum of \a MAX_DATA_LENGTH.
/////////////////////////////////////////////////////////////////////////////////////////
interrupt ( PORT1_VECTOR ) wakeup Port1_isr ( void );
interrupt ( PORT1_VECTOR ) wakeup Port1_isr ( void ) {
//interrupt ( PORT1_VECTOR ) Port1_isr ( void );
//interrupt ( PORT1_VECTOR ) Port1_isr ( void ) {

    volatile int n = P1IFG & P1IE;
    if ( n & ( 1 << 0 ) ) {
        FIFO_interrupt();
        return;
    }
    if ( n & ( 1 << 2 ) ) {
        SFD_interrupt();
        return;
    }

    while ( 1 );
}
