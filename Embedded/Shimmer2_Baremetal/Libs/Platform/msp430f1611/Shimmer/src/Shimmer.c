
#include "platform.h"
#include "BlueTooth.h"

/////////////////////////////////////////////////////////////////////////////////////////
/// \brief Perform Clocks setup
///
///  Shimmer configuration:
///    - WatchDog off
///    - MCLK: Main System Clock, external 8MHz
///    - ACLK: Auxillary Clock, external 32.768KHz
///    - SMCLK: Sub System Clock, 8MHZ (based on XT2CLK 8MHz)
///    - All I/O Ports as output low
/////////////////////////////////////////////////////////////////////////////////////////
int setupCLKs ( unsigned long cpuHz );
int setupCLKs ( unsigned long cpuHz ) {

  //Stop the watchdog
  WDTCTL = WDTPW + WDTHOLD;
  if(cpuHz!=MCLK)
    return(FAIL);
  
  //Clk setup

  // BCSCTL1  ->  Basic Clock System Control Register 1
  // .XT2OFF = 0, Enable the external oscillator XT2
  // .XTS    = 0, Set low frequency mode for LXFT1 (XIN=32.768KHz)
  // .DIVA   = 0, Set the divisor on ACLK to 1
  // .RSEL   = 0, Resistor Select
  BCSCTL1 = DIVA_0;
  do {
    unsigned int i;
    IFG1 &= ~OFIFG;                 // Clear OSCFault flag
    for ( i = 0; i < 0xff; i++ );   // Time for flag to set
  } while ( ( IFG1 & OFIFG ) != 0 );     // OSCFault flag still set?

  // BCSCTL2 -> Basic Clock System Control Register 2
  // .SELM = 2; MCLK conected to XT2CLK (8.0MHz)
  // .DIVM = 0; set the divisor of MCLK to 1
  // .SELS = 1; select XT2 as source for SMCLK
  // .DIVS = 0; set the divisor of SMCLK to 1 (8MHZ)
  // .DCOR = 0; select internal resistor for DCO
  //BCSCTL2 = SELM_2 | DIVM_0 | SELS | DIVS_0;
  BCSCTL2 = SELM_2 | DIVM_0 | SELS;// | ( DIVS0 | DIVS1 );

  //DCOCTL ->   DCO Control Register
  //DCOx   = 0;
  //MODx   = 0; Modular selection off
  DCOCTL = 0x00;

  //IO_init
  //PxSEL = 0 -> Digital I/O pin
  //PxDIR = 0 -> Input
  //PxOUT = 0 -> Low level

  // P1:
  P1SEL = 0x00; // DI/O
  P1DIR = 0x00; // Input
  P1OUT = 0x00; // Low level

  // P2:
  P2SEL = 0x00; // DI/O
  P2DIR = 0x00; // Input
  P2OUT = 0x00; // Low level

  // P3:
  P3SEL = 0x00; // DI/O
  P3DIR = 0x00; // Input
  P3OUT = 0x00; // Low level

  // P4:
  P4SEL = 0x00; // DI/O
  P4DIR = 0x00; // Input
  P4OUT = 0x00; // Low level

  // P5:
  P5SEL = 0x00; // DI/O
  P5DIR = 0x00; // Input
  P5OUT = 0x00; // Low level

  // P6:
  P6SEL = 0x00; // DI/O
  P6DIR = 0x00; // Input
  P6OUT = 0x00; // Low level

  return(SUCCESS);
}

int initPlatform(unsigned long cpuHz){
  //uint32_t paramRate=LED_TASK_PERIOD;

  //Setup the hardware ready for the demo
  if(setupCLKs(cpuHz)!=SUCCESS){
    return(FAIL);
  }
  disableBT();

  return SUCCESS;
}
