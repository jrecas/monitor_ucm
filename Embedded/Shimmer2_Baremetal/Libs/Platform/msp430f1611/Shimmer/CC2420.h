/////////////////////////////////////////////////////////////////////////////////////////
///  \file CC2420.h
///  \brief Definitions for msp430-CC2420 Hardware Presentation Layer interface
///
///  \author Joaquin Recas Piorno (jrecas@gmail.com) (Based on Shimmer&Hurray)
///  \version 2.0
///  \date February 2010
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef CC2420_H_
#define CC2420_H_

//TinyOS definitions
#include "Shimmer.h"

//ISR definitions
#include <signal.h>

#include "Simple_CC2420.h"
#include "CC2420Const.h"

#define  MAKE_RADIO_CSN_OUTPUT()    (P5DIR |=  0x10)    //P5.4 -> SPI1_CS_RADIO_N
#define  SEL_RADIO_CSN_IOFUNC()     (P5SEL &= ~0x10)
#define  SET_RADIO_CSN_PIN()        (P5OUT |=  0x10)
#define  CLR_RADIO_CSN_PIN()        (P5OUT &= ~0x10)

#define  MAKE_CC_RSTN_OUTPUT()      (P5DIR |=  0x80)    //P5.7 -> RESET_RADIO_N
#define  SEL_CC_RSTN_IOFUNC()       (P5SEL &= ~0x80)
#define  CLR_CC_RSTN_PIN()          (P5OUT &= ~0x80)
#define  SET_CC_RSTN_PIN()          (P5OUT |=  0x80)

#define  MAKE_CC_VREN_OUTPUT()      (P4DIR |=  0x80)    //P4.7 -> REG_1V8_EN_N
#define  SEL_CC_VREN_IOFUNC()       (P4SEL &= ~0x80)
#define  CLR_CC_VREN_PIN()          (P4OUT &= ~0x80)
#define  SET_CC_VREN_PIN()          (P4OUT |=  0x80)

#define  MAKE_CC_CCA_INPUT()        (P2DIR &= ~0x80)    //P2.7 -> RADIO_CCA
#define  SEL_CC_CCA_IOFUNC()        (P2SEL &= ~0x80)
#define  READ_CC_CCA_PIN()          (P2IN  &   0x80)

#define  MAKE_CC_FIFO_INPUT()       (P1DIR &= ~0x01)    //P1.0 -> RADIO_FIFO
#define  SEL_CC_FIFO_IOFUNC()       (P1SEL &= ~0x01)

#define  MAKE_CC_FIFOP_INPUT()      (P2DIR &= ~0x40)    //P2.6 -> RADIO_FIFOP
#define  SEL_CC_FIFOP_IOFUNC()      (P2SEL &= ~0x40)

#define  MAKE_CC_SFD_INPUT()        (P1DIR &= ~0x04)    //P1.2 -> RADIO_SFD
#define  SEL_CC_SFD_IOFUNC()        (P1SEL &= ~0x04)
#define  READ_CC_SFD_PIN()          (P1IN  &   0x04)

#define  CC2420USART_disableRxIntr()   (IE2   &= ~URXIE1)
#define  CC2420USART_disableTxIntr()   (IE2   &= ~UTXIE1)

#define  CC2420USART_rx()              (U1RXBUF)

#define  CC2420USART_tx(data)          (U1TXBUF = data)

#define  SEL_UTXD1_IOFUNC()            (P3SEL &= ~0x40)    ///< P3.6/UTXD1
#define  SEL_URXD1_IOFUNC()            (P3SEL &= ~0x80)    ///< P3.7/URXD1

#define  SEL_SIMO1_MODFUNC()           (P5SEL |=  0x02)    ///< P5.1/SIMO1
#define  SEL_SOMI1_MODFUNC()           (P5SEL |=  0x04)    ///< P5.2/SOMI1
#define  SEL_UCLK1_MODFUNC()           (P5SEL |=  0x08)    ///< P5.3/UCLK1

#define  SEL_SIMO1_IOFUNC()            (P5SEL &= ~0x02)    ///< P5.1/SIMO1
#define  SEL_SOMI1_IOFUNC()            (P5SEL &= ~0x04)    ///< P5.2/SOMI1
#define  SEL_UCLK1_IOFUNC()            (P5SEL &= ~0x08)    ///< P5.3/UCLK1

#define FIFOPInterrupt_enable()   P2IE  |=  (1 << 6)
#define FIFOPInterrupt_disable()  P2IE  &= ~(1 << 6)
#define FIFOPInterrupt_clear()    P2IFG &= ~(1 << 6)

#define CCAInterrupt_enable()     P2IE  |=  (1 << 7)
#define CCAInterrupt_disable()    P2IE  &= ~(1 << 7)
#define CCAInterrupt_clear()      P2IFG &= ~(1 << 7)

#define FIFOInterrupt_enable()    P1IE  |=  (1 << 0)
#define FIFOInterrupt_disable()   P1IE  &= ~(1 << 0)
#define FIFOInterrupt_clear()     P1IFG &= ~(1 << 0)

#define SFDInterrupt_enable()     P1IE  |=  (1 << 2)
#define SFDInterrupt_disable()    P1IE  &= ~(1 << 2)
#define SFDInterrupt_clear()      P1IFG &= ~(1 << 2)

#define FIFOPInterrupt_edge(low_to_high)         \
          if(low_to_high)  P2IES &= ~(1 << 6);   \
          else             P2IES |=  (1 << 6);

#define SFDInterrupt_edge(low_to_high)           \
          if(low_to_high)  P1IES &= ~(1 << 2);   \
          else             P1IES |=  (1 << 2);

#define CCAInterrupt_edge(low_to_high)           \
          if(low_to_high)  P2IES &= ~(1 << 7);   \
          else             P2IES |=  (1 << 7);

#define VREFOn()                 CLR_CC_VREN_PIN()
#define VREFOff()                SET_CC_VREN_PIN()

#define ResetOn()                CLR_CC_RSTN_PIN()
#define ResetOff()               SET_CC_RSTN_PIN()

#define CC2420USART_isTxEmpty()  (U1TCTL & TXEPT)

#define SPI_isTxDone()  (IFG2 & UTXIFG1)
#define SPI_isRxDone()  (IFG2 & URXIFG1)

                         
result_t HPLCC2420_init(unsigned long smClkHz);
result_t HPLCC2420_start ( void );
result_t HPLCC2420_stop ( void );

void initCC2420USART(unsigned long smClkHz);
void CC2420USART_disableSPI(void);
//result_t CC2420USART_isTxIntrPending(void);
result_t CC2420USART_isRxIntrPending(void);
//result_t CC2420USART_isTxEmpty(void);


#endif /*CC2420_H_*/
