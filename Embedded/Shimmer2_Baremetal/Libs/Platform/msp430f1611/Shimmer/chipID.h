/////////////////////////////////////////////////////////////////////////////////////////
/// \file chipID.h
/// \brief Read unique Chip ID number for the Shimmer board
///
/// \author Joaquin Recas Piorno (jrecas@gmail.com)
/// \version 1.1
/// \date January 2011
/////////////////////////////////////////////////////////////////////////////////////////

#ifndef CHIPID_H_
#define CHIPID_H_

#include "Shimmer.h"
#include <string.h>

#define  MAKE_ONEWIRE_PWR_OUTPUT()  (P2DIR |=  0x08)
#define  SEL_ONEWIRE_PWR_IOFUNC()   (P2SEL &= ~0x08)
#define  SET_ONEWIRE_PWR_PIN()      (P2OUT |=  0x08)
#define  CLR_ONEWIRE_PWR_PIN()      (P2OUT &= ~0x08)


#define  MAKE_ONEWIRE_INPUT()       (P2DIR &= ~0x10)
#define  MAKE_ONEWIRE_OUTPUT()      (P2DIR |=  0x10)
#define  SEL_ONEWIRE_IOFUNC()       (P2SEL &= ~0x10)
#define  SET_ONEWIRE_PIN()          (P2OUT |=  0x10)
#define  CLR_ONEWIRE_PIN()          (P2OUT &= ~0x10)
#define  READ_ONEWIRE_PIN()         (P2IN  &   0x10)

result_t readIDChip ( uint8_t *id_buf );

#endif /*CHIPID_H_*/
