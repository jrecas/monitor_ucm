#ifndef ECG_SHIMMER_H
#define ECG_SHIMMER_H

#include "platform.h"

void initAdcSh(void);


#define  getEcgCh1()     (ADC12MEM0)
#define  getEcgCh2()     (ADC12MEM1)

#define  getAccX()       (ADC12MEM2)
#define  getAccY()       (ADC12MEM3)
#define  getAccZ()       (ADC12MEM4)

//#define  getA0()         (ADC12MEM5)                  //Analog expanion board for Shimmer
//#define  getA7()         (ADC12MEM6)                  //Analog expanion board for Shimmer

//#define  getSpO2Bit()    getA0()                      //Bitmos interface
//#define  getPlethBit()   getA7()                      //Bitmos interface


#define  triggerADC()    (ADC12CTL0 |= ADC12SC)

#endif
